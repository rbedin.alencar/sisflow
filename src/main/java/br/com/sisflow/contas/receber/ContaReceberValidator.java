package br.com.sisflow.contas.receber;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class ContaReceberValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return ContaReceber.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "descricao", "field.required.contas.receber.descricao");	
	}
}
