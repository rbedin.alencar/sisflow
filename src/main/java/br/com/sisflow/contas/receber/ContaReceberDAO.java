package br.com.sisflow.contas.receber;

import java.util.List;

import javax.transaction.Transactional;

@Transactional
public interface ContaReceberDAO{
	public void Salvar(ContaReceber contaReceber);
	public void Atualizar(ContaReceber contaReceber);
	public void Excluir(ContaReceber contaReceber);
	public ContaReceber Carregar(Long id);
	public List<ContaReceber> Listar();
	
	public List<ContaReceber> ListarPorAnoMes(Integer ano, Integer mes);
}
