package br.com.sisflow.contas.receber.tipo;

public enum ContaReceberTipo{
	
	RECEITA("Receitas em geral"),
	VENDA("Vendas"), 
	INVESTIMENTO("Investimentos"),
	PREMIO("Premios");
	
	private String descricao;
	
	ContaReceberTipo(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
}
