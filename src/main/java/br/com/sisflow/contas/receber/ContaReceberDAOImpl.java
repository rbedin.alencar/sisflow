package br.com.sisflow.contas.receber;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

@Repository
public class ContaReceberDAOImpl implements ContaReceberDAO {

	@PersistenceContext
	private EntityManager manager;

	public void Salvar(ContaReceber contaReceber) {
		manager.persist(contaReceber);
	}

	public void Atualizar(ContaReceber contaReceber) {
		this.manager.merge(contaReceber);
	}

	public void Excluir(ContaReceber contaReceber) {
		this.manager.remove(contaReceber);
	}

	public ContaReceber Carregar(Long id) {
		return this.manager.find(ContaReceber.class, id);
	}

	public List<ContaReceber> Listar() {		
		return this.manager.createQuery("select cr from ContaReceber cr order by cr.data_recebimento asc", ContaReceber.class).getResultList();		
	}

	@Override
	public List<ContaReceber> ListarPorAnoMes(Integer ano, Integer mes) {
		return this.manager.createQuery("select cr from ContaReceber cr where month(cr.data_recebimento) = :mes and year(cr.data_recebimento) = :ano", ContaReceber.class)
				.setParameter("mes", mes)
				.setParameter("ano", ano)
				.getResultList();		
	}
}
