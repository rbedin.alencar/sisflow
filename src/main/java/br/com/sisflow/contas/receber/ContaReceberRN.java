package br.com.sisflow.contas.receber;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class ContaReceberRN{
	
	@Autowired
	private ContaReceberDAO dao;
	
	public void Salvar(ContaReceber contaReceber) {
		
		Long id = contaReceber.getId();
		if(id == null) {
			contaReceber.setCriado_em(new Date(System.currentTimeMillis()));
			this.dao.Salvar(contaReceber);
		}
		else {
			contaReceber.setAtualizado_em(new Date(System.currentTimeMillis()));
			this.dao.Atualizar(contaReceber);
		}
	}
	
	public void Excluir(ContaReceber contaReceber) {
		this.dao.Excluir(contaReceber);
	}
	
	public ContaReceber Carregar(Long id) {
		return this.dao.Carregar(id);
	}
	
	public List<ContaReceber> Listar(){
		return this.dao.Listar();
	}
	
	public List<ContaReceber> ListarPorAnoMes(Integer ano, Integer mes){
		return this.dao.ListarPorAnoMes(ano, mes);
	}
}
