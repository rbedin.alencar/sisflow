package br.com.sisflow.contas.receber;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.sisflow.contas.receber.tipo.ContaReceberTipo;
import br.com.sisflow.core.negocio.NegocioRN;

@Controller
@Transactional
@RequestMapping("/contas/receber")
public class ContaReceberController{
	
	/* LOGGER */
	
	protected static Logger logger = Logger.getLogger(ContaReceberController.class);
	
	/* VALIDATORS */
	
	@Autowired
	private ContaReceberValidator contaReceberValidator;
	
	/* SERVICES */
	
	@Autowired
	private ContaReceberRN contaReceberRN;
	
	@Autowired
	private NegocioRN negocioRN;
	
	/* BINDERS */
	
	@InitBinder
    protected void initBinder(WebDataBinder binder) {
		binder.setValidator(this.contaReceberValidator);
    }
	
	@InitBinder
	public void initBigDecimalBinder(WebDataBinder binder) throws Exception{
		DecimalFormat df = new DecimalFormat();
		DecimalFormatSymbols dfs = new DecimalFormatSymbols();
		dfs.setGroupingSeparator('.');
		dfs.setDecimalSeparator(',');
		df.setDecimalFormatSymbols(dfs);
		binder.registerCustomEditor(BigDecimal.class, new CustomNumberEditor(BigDecimal.class, df, true));
	}
	
	/* ROUTES */
	
	@GetMapping
	@Cacheable(value="contas.receber")
	public ModelAndView index() {
		ModelAndView mv = new ModelAndView("contas/receber/index");
		mv.addObject("contaReceberDS", this.contaReceberRN.Listar());
		return mv;
	}
	
	@GetMapping("/cadastro")
	public ModelAndView cadastro(ContaReceber contaReceber) {
		
		ModelAndView mv = new ModelAndView("contas/receber/cadastro");
		
		/* THIS */
		mv.addObject("contaReceber", contaReceber);
		
		/* DS */
		mv.addObject("negocioDS", this.negocioRN.Listar());
		
		mv.addObject("contaReceberTipoDS", ContaReceberTipo.values());
		
		/* VIEW */
		return mv;
	}
	
	@GetMapping("/altera/{id}")
	public ModelAndView altera(@PathVariable("id") Long id) {
		ContaReceber contaReceber = this.contaReceberRN.Carregar(id);
		if(contaReceber != null)
			return cadastro(contaReceber);
		else
			return new ModelAndView("redirect:/contas/receber");
	}
	
	@GetMapping("/exclui/{id}")
	@CacheEvict(value="contas.receber", allEntries=true)
	public ModelAndView exclui(@PathVariable("id") Long id, RedirectAttributes redirectAttributes) {

		ContaReceber contaReceber = this.contaReceberRN.Carregar(id);
		if(contaReceber != null)
			this.contaReceberRN.Excluir(contaReceber);	

		redirectAttributes.addFlashAttribute("msg", "Registro excluido com sucesso!");
		
		return new ModelAndView("redirect:/contas/receber");
	}
	
	@PostMapping("/salvar")
	@CacheEvict(value="contas.receber", allEntries=true)
	public ModelAndView salvar(@ModelAttribute @Valid ContaReceber contaReceber, BindingResult result, RedirectAttributes redirectAttributes, HttpServletRequest request){
		
		if(result.hasErrors())
			return cadastro(contaReceber);
	
		/* SAVE */
		this.contaReceberRN.Salvar(contaReceber);

		redirectAttributes.addFlashAttribute("msg", "Registro salvo com sucesso!");
		
		return new ModelAndView("redirect:/contas/receber");
	}
}
