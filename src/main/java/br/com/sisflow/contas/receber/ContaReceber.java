package br.com.sisflow.contas.receber;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.sisflow.contas.Conta;
import br.com.sisflow.contas.receber.tipo.ContaReceberTipo;

@Entity
@Table(name="conta_receber")
@PrimaryKeyJoinColumn(name="conta_id")
public class ContaReceber extends Conta implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3071672909125005860L;

	@Enumerated(EnumType.STRING)
	@Column(length=16, nullable=false)
	private ContaReceberTipo tipo;
	
	@Column(nullable=true)
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date data_recebimento;
	
	@Column(nullable=false, columnDefinition="bit default 1")
	private boolean recebido = true;

	public ContaReceberTipo getTipo() {
		return tipo;
	}

	public void setTipo(ContaReceberTipo tipo) {
		this.tipo = tipo;
	}

	public Date getData_recebimento() {
		return data_recebimento;
	}

	public void setData_recebimento(Date data_recebimento) {
		this.data_recebimento = data_recebimento;
	}

	public boolean isRecebido() {
		return recebido;
	}

	public void setRecebido(boolean recebido) {
		this.recebido = recebido;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((data_recebimento == null) ? 0 : data_recebimento.hashCode());
		result = prime * result + (recebido ? 1231 : 1237);
		result = prime * result + ((tipo == null) ? 0 : tipo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContaReceber other = (ContaReceber) obj;
		if (data_recebimento == null) {
			if (other.data_recebimento != null)
				return false;
		} else if (!data_recebimento.equals(other.data_recebimento))
			return false;
		if (recebido != other.recebido)
			return false;
		if (tipo != other.tipo)
			return false;
		return true;
	}
}
