package br.com.sisflow.contas;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ibm.icu.math.BigDecimal;

import br.com.sisflow.contas.pagar.ContaPagar;
import br.com.sisflow.contas.pagar.ContaPagarRN;
import br.com.sisflow.contas.receber.ContaReceber;
import br.com.sisflow.contas.receber.ContaReceberRN;
import br.com.sisflow.core.negocio.NegocioRN;

@Controller
@Transactional
@RequestMapping("/contas")
public class ContasController{
	
	/* LOGGER */
	
	protected static Logger logger = Logger.getLogger(ContasController.class);
	
	/* SERVICES */
	
	@Autowired
	private NegocioRN negocioRN;
	
	@Autowired
	private ContaReceberRN contaReceberRN;
	
	@Autowired
	private ContaPagarRN contaPagarRN;
	
	
	/* CLASS */
	
	public class Filtro{
		
		private Integer mes = null;
		private Integer ano = null;
		
		public Integer getMes() {
			return mes;
		}

		public void setMes(Integer mes) {
			this.mes = mes;
		}

		public Integer getAno() {
			return ano;
		}

		public void setAno(Integer ano) {
			this.ano = ano;
		}
	};
	
	/* ACTIONS */
	
	@GetMapping
	@Cacheable(value="contas")
	public ModelAndView index(Filtro filtro) {
		
		ModelAndView mv = new ModelAndView("contas/index");

		/* DS */
		
		mv.addObject("filtro", filtro);
		
		mv.addObject("negociODS", this.negocioRN.Listar());
		
		Map<Integer, String> meses = new HashMap<Integer, String>(); 
		meses.put(1, "Janeiro");
		meses.put(2, "Fevereiro");
		meses.put(3, "Março");
		meses.put(4, "Abril");
		meses.put(5, "Maio");
		meses.put(6, "Junho");
		meses.put(7, "Julho");
		meses.put(8, "Agosto");
		meses.put(9, "Setembro");
		meses.put(10, "Outubro");
		meses.put(11, "Novembro");
		meses.put(12, "Dezembro");
		mv.addObject("mesDS", meses);
		
		GregorianCalendar calendar = new GregorianCalendar();
		
		Integer ano_de 	= (calendar.get(Calendar.YEAR) - 10);
		Integer ano_ate = (calendar.get(Calendar.YEAR) + 10);
		
		Map<Integer, String> anos = new LinkedHashMap<Integer, String>();
		for(int i=ano_de; i<=ano_ate; i++)
			anos.put(i, String.valueOf(i));
		mv.addObject("anoDS", anos);
		
		/* DEFAULT */
		
		if(filtro.getMes() == null)
			filtro.setMes(calendar.get(Calendar.MONTH) + 1);
		
		if(filtro.getAno() == null)
			filtro.setAno(calendar.get(Calendar.YEAR));	
		
		/* CONTAS.RECEBER */
		BigDecimal totalContaReceber = new BigDecimal(0);

		List<ContaReceber> contaReceberDS = this.contaReceberRN.ListarPorAnoMes(filtro.ano, filtro.mes);
		if(contaReceberDS != null)
			for(ContaReceber contaReceber : contaReceberDS)
				totalContaReceber = totalContaReceber.add(new BigDecimal(contaReceber.getValor()));

		mv.addObject("totalContaReceber", totalContaReceber);
		mv.addObject("contaReceberDS", contaReceberDS);
		
		/* CONTAS.PAGAR */
		BigDecimal totalContaPagar = new BigDecimal(0);

		List<ContaPagar> contaPagarDS = this.contaPagarRN.ListarPorAnoMes(filtro.ano, filtro.mes);
		if(contaPagarDS != null) {
			for(ContaPagar contaPagar : contaPagarDS) {
				if(contaPagar.isCobranca_paga() == false)
					totalContaPagar = totalContaPagar.add(new BigDecimal(contaPagar.getValor()));
			}
		}
		mv.addObject("totalContaPagar", totalContaPagar);
		mv.addObject("contaPagarDS", contaPagarDS);
		
		/* TOTAL */
		BigDecimal totalConta = new BigDecimal(0);
		totalConta = totalContaReceber.subtract(totalContaPagar);
		mv.addObject("totalConta", totalConta);
		
		/* VIEW */
		return mv;
	}
}
