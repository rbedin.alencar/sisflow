package br.com.sisflow.contas;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;

import br.com.sisflow.core.negocio.Negocio;
import br.com.sisflow.core.usuario.Usuario;

@Entity
@Table(name="conta")
@Inheritance(strategy=InheritanceType.JOINED)
public class Conta implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2730604506448953320L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="conta_id")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="negocio_id")
	@NotNull(message="Selecione a empresa.")
	private Negocio negocio;
	
	@Column(nullable=true, columnDefinition="TEXT")
	private String documento;
	
	@Column(nullable=true, columnDefinition="TEXT")
	private String descricao;	
	
	@Column(precision=10, scale=2)
	private BigDecimal valor = new BigDecimal(0);
	
	@ManyToOne
	@JoinColumn(name="usuario_cadastro_id")
	private Usuario usuario_cadastro;
	
	@ManyToOne
	@JoinColumn(name="usuario_atualiza_id")
	private Usuario usuario_atualiza;
	
	@Column(columnDefinition="DATETIME", updatable=false)
	@Temporal(TemporalType.TIMESTAMP)
	@CreationTimestamp
	private Date criado_em;
	
	@Column(columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date atualizado_em;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Negocio getNegocio() {
		return negocio;
	}

	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public Usuario getUsuario_cadastro() {
		return usuario_cadastro;
	}

	public void setUsuario_cadastro(Usuario usuario_cadastro) {
		this.usuario_cadastro = usuario_cadastro;
	}

	public Usuario getUsuario_atualiza() {
		return usuario_atualiza;
	}

	public void setUsuario_atualiza(Usuario usuario_atualiza) {
		this.usuario_atualiza = usuario_atualiza;
	}

	public Date getCriado_em() {
		return criado_em;
	}

	public void setCriado_em(Date criado_em) {
		this.criado_em = criado_em;
	}

	public Date getAtualizado_em() {
		return atualizado_em;
	}

	public void setAtualizado_em(Date atualizado_em) {
		this.atualizado_em = atualizado_em;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((atualizado_em == null) ? 0 : atualizado_em.hashCode());
		result = prime * result + ((criado_em == null) ? 0 : criado_em.hashCode());
		result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + ((documento == null) ? 0 : documento.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((negocio == null) ? 0 : negocio.hashCode());
		result = prime * result + ((usuario_atualiza == null) ? 0 : usuario_atualiza.hashCode());
		result = prime * result + ((usuario_cadastro == null) ? 0 : usuario_cadastro.hashCode());
		result = prime * result + ((valor == null) ? 0 : valor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Conta other = (Conta) obj;
		if (atualizado_em == null) {
			if (other.atualizado_em != null)
				return false;
		} else if (!atualizado_em.equals(other.atualizado_em))
			return false;
		if (criado_em == null) {
			if (other.criado_em != null)
				return false;
		} else if (!criado_em.equals(other.criado_em))
			return false;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (documento == null) {
			if (other.documento != null)
				return false;
		} else if (!documento.equals(other.documento))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (negocio == null) {
			if (other.negocio != null)
				return false;
		} else if (!negocio.equals(other.negocio))
			return false;
		if (usuario_atualiza == null) {
			if (other.usuario_atualiza != null)
				return false;
		} else if (!usuario_atualiza.equals(other.usuario_atualiza))
			return false;
		if (usuario_cadastro == null) {
			if (other.usuario_cadastro != null)
				return false;
		} else if (!usuario_cadastro.equals(other.usuario_cadastro))
			return false;
		if (valor == null) {
			if (other.valor != null)
				return false;
		} else if (!valor.equals(other.valor))
			return false;
		return true;
	}
}
