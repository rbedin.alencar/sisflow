package br.com.sisflow.contas.pagar.tipo;

public enum ContaPagarTipo{
	
	DESPESA("Despesa em geral"),
	INVESTIMENTO("Investimento"),
	ALUGUEL("Aluguel"),
	COMBUSTIVEL("Combustível"),
	TRANSPORTADORA("Transportadora"), 
	FRETE("Frete"), 
	EMPRESTIMO("Empréstimo"),
	ENCARGO("Encargo Trabalhista"),
	IMPOSTO("Imposto"),
	LIMPEZA("Limpeza"),
	MANUTENCAO("Manutenção"),
	MATERIAL("Material em geral"),
	SEGURANCA("Segurança"),
	TELEFONIA("Telefonia"),
	INTERNET("Internet"), 
	VIAGEM("Viagem"),
	COMISSAO("Comissão");
	
	private String descricao;
	
	ContaPagarTipo(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
}
