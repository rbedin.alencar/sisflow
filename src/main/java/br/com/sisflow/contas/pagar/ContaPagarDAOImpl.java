package br.com.sisflow.contas.pagar;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

@Repository
public class ContaPagarDAOImpl implements ContaPagarDAO {

	@PersistenceContext
	private EntityManager manager;

	public void Salvar(ContaPagar contaPagar) {
		manager.persist(contaPagar);
	}

	public void Atualizar(ContaPagar contaPagar) {
		this.manager.merge(contaPagar);
	}

	public void Excluir(ContaPagar contaPagar) {
		this.manager.remove(contaPagar);
	}

	public ContaPagar Carregar(Long id) {
		return this.manager.find(ContaPagar.class, id);
	}

	public List<ContaPagar> Listar() {		
		return this.manager.createQuery("select cp from ContaPagar cp order by cp.data_vencimento asc", ContaPagar.class).getResultList();		
	}

	@Override
	public List<ContaPagar> ListarPorAnoMes(Integer ano, Integer mes) {
		return this.manager.createQuery("select cp from ContaPagar cp where month(cp.data_vencimento) = :mes and year(cp.data_vencimento) = :ano", ContaPagar.class)
				.setParameter("mes", mes)
				.setParameter("ano", ano)
				.getResultList();		
	}
}