package br.com.sisflow.contas.pagar;

import java.util.List;

import javax.transaction.Transactional;

@Transactional
public interface ContaPagarDAO{
	public void Salvar(ContaPagar contaPagar);
	public void Atualizar(ContaPagar contaPagar);
	public void Excluir(ContaPagar contaPagar);
	public ContaPagar Carregar(Long id);
	public List<ContaPagar> Listar();
	
	public List<ContaPagar> ListarPorAnoMes(Integer ano, Integer mes);
}
