package br.com.sisflow.contas.pagar;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class ContaPagarValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return ContaPagar.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "descricao", "field.required.contas.pagar.descricao");	
	}
}
