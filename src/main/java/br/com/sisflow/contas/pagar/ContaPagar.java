package br.com.sisflow.contas.pagar;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.sisflow.contas.Conta;
import br.com.sisflow.contas.pagar.tipo.ContaPagarTipo;

@Entity
@Table(name="conta_pagar")
@PrimaryKeyJoinColumn(name="conta_id")
public class ContaPagar extends Conta implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7580182282733468217L;

	@Enumerated(EnumType.STRING)
	@Column(length=16, nullable=false)
	private ContaPagarTipo tipo;
	
	@Column(nullable=true)
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date data_vencimento;
	
	@Column(nullable=false, columnDefinition="bit default 0")
	private boolean cobranca_paga = false;

	public ContaPagarTipo getTipo() {
		return tipo;
	}

	public void setTipo(ContaPagarTipo tipo) {
		this.tipo = tipo;
	}

	public Date getData_vencimento() {
		return data_vencimento;
	}

	public void setData_vencimento(Date data_vencimento) {
		this.data_vencimento = data_vencimento;
	}

	public boolean isCobranca_paga() {
		return cobranca_paga;
	}

	public void setCobranca_paga(boolean cobranca_paga) {
		this.cobranca_paga = cobranca_paga;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (cobranca_paga ? 1231 : 1237);
		result = prime * result + ((data_vencimento == null) ? 0 : data_vencimento.hashCode());
		result = prime * result + ((tipo == null) ? 0 : tipo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContaPagar other = (ContaPagar) obj;
		if (cobranca_paga != other.cobranca_paga)
			return false;
		if (data_vencimento == null) {
			if (other.data_vencimento != null)
				return false;
		} else if (!data_vencimento.equals(other.data_vencimento))
			return false;
		if (tipo != other.tipo)
			return false;
		return true;
	}
}