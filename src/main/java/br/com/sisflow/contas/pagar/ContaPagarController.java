package br.com.sisflow.contas.pagar;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.sisflow.contas.pagar.tipo.ContaPagarTipo;
import br.com.sisflow.core.negocio.NegocioRN;

@Controller
@Transactional
@RequestMapping("/contas/pagar")
public class ContaPagarController{
	
	/* LOGGER */
	
	protected static Logger logger = Logger.getLogger(ContaPagarController.class);
	
	/* VALIDATORS */
	
	@Autowired
	private ContaPagarValidator contaPagarValidator;
	
	/* SERVICES */
	
	@Autowired
	private ContaPagarRN contaPagarRN;
	
	@Autowired
	private NegocioRN negocioRN;
	
	/* BINDERS */
	
	@InitBinder
    protected void initBinder(WebDataBinder binder) {
		binder.setValidator(this.contaPagarValidator);
    }
	
	@InitBinder
	public void initBigDecimalBinder(WebDataBinder binder) throws Exception{
		DecimalFormat df = new DecimalFormat();
		DecimalFormatSymbols dfs = new DecimalFormatSymbols();
		dfs.setGroupingSeparator('.');
		dfs.setDecimalSeparator(',');
		df.setDecimalFormatSymbols(dfs);
		binder.registerCustomEditor(BigDecimal.class, new CustomNumberEditor(BigDecimal.class, df, true));
	}
	
	/* ROUTES */
	
	@GetMapping
	@Cacheable(value="contas.pagar")
	public ModelAndView index() {
		ModelAndView mv = new ModelAndView("contas/pagar/index");
		mv.addObject("contaPagarDS", this.contaPagarRN.Listar());
		return mv;
	}
	
	@GetMapping("/cadastro")
	public ModelAndView cadastro(ContaPagar contaPagarRN) {
		
		ModelAndView mv = new ModelAndView("contas/pagar/cadastro");
		
		/* THIS */
		mv.addObject("contaPagar", contaPagarRN);
		
		/* DS */
		mv.addObject("negocioDS", this.negocioRN.Listar());
		
		mv.addObject("contaPagarTipoDS", ContaPagarTipo.values());
		
		/* VIEW */
		return mv;
	}
	
	@GetMapping("/altera/{id}")
	public ModelAndView altera(@PathVariable("id") Long id) {
		ContaPagar contaPagar = this.contaPagarRN.Carregar(id);
		if(contaPagar != null)
			return cadastro(contaPagar);
		else
			return new ModelAndView("redirect:/contas/pagar");
	}
	
	@GetMapping("/exclui/{id}")
	@CacheEvict(value="contas.pagar", allEntries=true)
	public ModelAndView exclui(@PathVariable("id") Long id, RedirectAttributes redirectAttributes) {

		ContaPagar contaPagar = this.contaPagarRN.Carregar(id);
		if(contaPagar != null)
			this.contaPagarRN.Excluir(contaPagar);	

		redirectAttributes.addFlashAttribute("msg", "Registro excluido com sucesso!");
		
		return new ModelAndView("redirect:/contas/pagar");
	}
	
	@PostMapping("/salvar")
	@CacheEvict(value="contas.pagar", allEntries=true)
	public ModelAndView salvar(@ModelAttribute @Valid ContaPagar contaPagar, BindingResult result, RedirectAttributes redirectAttributes, HttpServletRequest request){
		
		if(result.hasErrors())
			return cadastro(contaPagar);
	
		/* SAVE */
		
		String repetir_str = request.getParameter("repetir");
		Integer repetir = Integer.parseInt(repetir_str);
		
		ContaPagar cp = null;
		Date data_vencimento = contaPagar.getData_vencimento();
		for(int i=0; i<repetir; i++) {
			
			cp = new ContaPagar();
			cp.setId(contaPagar.getId());

			if(i>0) {
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(data_vencimento);
				calendar.add(Calendar.MONTH, 1);
				data_vencimento = calendar.getTime();
				contaPagar.setData_vencimento(data_vencimento);
			}
			cp.setData_vencimento(contaPagar.getData_vencimento());

			cp.setDescricao(contaPagar.getDescricao());
			cp.setDocumento(contaPagar.getDocumento());
			cp.setNegocio(contaPagar.getNegocio());
			cp.setTipo(contaPagar.getTipo());
			cp.setValor(contaPagar.getValor());
			cp.setCobranca_paga(contaPagar.isCobranca_paga());
			
			cp.setUsuario_atualiza(contaPagar.getUsuario_atualiza());
			cp.setUsuario_cadastro(contaPagar.getUsuario_cadastro());
			cp.setCriado_em(contaPagar.getCriado_em());
			cp.setAtualizado_em(contaPagar.getAtualizado_em());
			
			this.contaPagarRN.Salvar(cp);
		}

		redirectAttributes.addFlashAttribute("msg", "Registro salvo com sucesso!");
		
		return new ModelAndView("redirect:/contas/pagar");
	}
}