package br.com.sisflow.contas.pagar;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class ContaPagarRN{
	
	@Autowired
	private ContaPagarDAO dao;
	
	public void Salvar(ContaPagar contaPagar) {
		
		Long id = contaPagar.getId();
		if(id == null) {
			contaPagar.setCriado_em(new Date(System.currentTimeMillis()));
			this.dao.Salvar(contaPagar);
		}
		else {
			contaPagar.setAtualizado_em(new Date(System.currentTimeMillis()));
			this.dao.Atualizar(contaPagar);
		}
	}
	
	public void Excluir(ContaPagar contaPagar) {
		this.dao.Excluir(contaPagar);
	}
	
	public ContaPagar Carregar(Long id) {
		return this.dao.Carregar(id);
	}
	
	public List<ContaPagar> Listar(){
		return this.dao.Listar();
	}
	
	public List<ContaPagar> ListarPorAnoMes(Integer ano, Integer mes){
		return this.dao.ListarPorAnoMes(ano, mes);
	}
}
