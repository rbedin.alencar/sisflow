package br.com.sisflow.pet.hospedagem;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import br.com.sisflow.cliente.Cliente;

@Repository
public class HospedagemImpl implements HospedagemDAO {

	@PersistenceContext
	private EntityManager manager;

	public void Salvar(Hospedagem hospedagem) {
		manager.persist(hospedagem);
	}

	public void Atualizar(Hospedagem hospedagem) {
		this.manager.merge(hospedagem);
	}

	public void Excluir(Hospedagem hospedagem) {
		this.manager.remove(hospedagem);
	}

	public Hospedagem Carregar(Long id) {
		return this.manager.find(Hospedagem.class, id);
	}

	@Override
	public List<Hospedagem> Listar() {
		return this.manager.createQuery("select h from Hospedagem h", Hospedagem.class).getResultList();
	}
	
	public List<Hospedagem> Listar(Cliente cliente) {		
		return this.manager.createQuery("select h from Hospedagem h where h.cliente.id = :cliente_id", Hospedagem.class)
				.setParameter("cliente_id", cliente.getId())
				.getResultList();		
	}

	@Override
	public Long Total() {
		return (Long) this.manager.createQuery("select count(h) from Hospedagem h").getSingleResult();
	}
}
