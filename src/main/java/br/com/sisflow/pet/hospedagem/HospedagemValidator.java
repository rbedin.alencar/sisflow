package br.com.sisflow.pet.hospedagem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class HospedagemValidator implements Validator {

	@Autowired
	private HospedagemRN hospedagemRN;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return Hospedagem.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {	
	}
}
