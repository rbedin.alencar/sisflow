package br.com.sisflow.pet.hospedagem;

import java.util.List;

import javax.transaction.Transactional;

import br.com.sisflow.cliente.Cliente;

@Transactional
public interface HospedagemDAO{
	public void Salvar(Hospedagem hospedagem);
	public void Atualizar(Hospedagem hospedagem);
	public void Excluir(Hospedagem hospedagem);
	public Hospedagem Carregar(Long id);
	
	public List<Hospedagem> Listar();
	public List<Hospedagem> Listar(Cliente cliente);
	
	public Long Total();
}
