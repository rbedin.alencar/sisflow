package br.com.sisflow.pet.hospedagem;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.CreationTimestamp;
import org.springframework.format.annotation.DateTimeFormat;

import br.com.sisflow.cliente.Cliente;
import br.com.sisflow.core.negocio.Negocio;
import br.com.sisflow.core.usuario.Usuario;
import br.com.sisflow.oferta.hospedagem.OfertaHospedagem;
import br.com.sisflow.pet.animal.Animal;

@Entity
@Table(name="pet_hospedagem")
public class Hospedagem implements Serializable{

	private static final long serialVersionUID = -4452455198948792183L;
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="hospedagem_id")
	private Long id;

	@ManyToOne
	@JoinColumn(name="negocio_id", nullable=false)
	private Negocio negocio;
	
	@ManyToOne
	@JoinColumn(name="cliente_id")
	@NotNull(message="Selecione o cliente.")
	private Cliente cliente;
	
	@ManyToOne
	@JoinColumn(name="oferta_id")
	@NotNull(message="Selecione a hospedagem.")
	private OfertaHospedagem ofertaHospedagem;
	
	@OneToMany(cascade=CascadeType.MERGE, fetch=FetchType.LAZY, targetEntity=Animal.class)
	@JoinTable(name="pet_hospedagem_animal", joinColumns=@JoinColumn(name="hospedagem_id"), inverseJoinColumns=@JoinColumn(name="animal_id"))
	private Set<Animal> animais = new HashSet<>();
	
	@Column(columnDefinition="DATE", nullable=false)
	@DateTimeFormat(pattern="dd/MM/yyyy")
	@Temporal(TemporalType.DATE)
	@NotNull(message="Informe a data válida do Check-In.")
	private Date data_checkin;
	
	@Column(nullable = true)
	@Temporal(TemporalType.TIME)
	@DateTimeFormat(pattern="HH:mm:ss")
	@NotNull(message="Informe a hora do Check-In.")
	private Date hora_checkin;
	
	@Column(nullable=true)
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date data_checkout;
	
	@Column(nullable = true)
	@Temporal(TemporalType.TIME)
	@DateTimeFormat(pattern="HH:mm:ss")
	@NotNull(message="Informe a hora do Check-Out.")
	private Date hora_checkout;
	
	@Column(nullable=true, columnDefinition="TEXT")
	private String observacao;
	
	@ManyToOne
	@JoinColumn(name="usuario_cadastro_id")
	private Usuario usuario_cadastro;
	
	@ManyToOne
	@JoinColumn(name="usuario_atualizacao_id")
	private Usuario usuario_atualiza;
	
	@Column(columnDefinition="DATETIME", updatable=false)
	@Temporal(TemporalType.TIMESTAMP)
	@CreationTimestamp
	private Date criado_em;
	
	@Column(columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date atualizado_em;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Negocio getNegocio() {
		return negocio;
	}

	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public OfertaHospedagem getOfertaHospedagem() {
		return ofertaHospedagem;
	}

	public void setOfertaHospedagem(OfertaHospedagem ofertaHospedagem) {
		this.ofertaHospedagem = ofertaHospedagem;
	}

	public Set<Animal> getAnimais() {
		return animais;
	}

	public void setAnimais(Set<Animal> animais) {
		this.animais = animais;
	}

	public Date getData_checkin() {
		return data_checkin;
	}

	public void setData_checkin(Date data_checkin) {
		this.data_checkin = data_checkin;
	}

	public Date getHora_checkin() {
		return hora_checkin;
	}

	public void setHora_checkin(Date hora_checkin) {
		this.hora_checkin = hora_checkin;
	}

	public Date getData_checkout() {
		return data_checkout;
	}

	public void setData_checkout(Date data_checkout) {
		this.data_checkout = data_checkout;
	}

	public Date getHora_checkout() {
		return hora_checkout;
	}

	public void setHora_checkout(Date hora_checkout) {
		this.hora_checkout = hora_checkout;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Usuario getUsuario_cadastro() {
		return usuario_cadastro;
	}

	public void setUsuario_cadastro(Usuario usuario_cadastro) {
		this.usuario_cadastro = usuario_cadastro;
	}

	public Usuario getUsuario_atualiza() {
		return usuario_atualiza;
	}

	public void setUsuario_atualiza(Usuario usuario_atualiza) {
		this.usuario_atualiza = usuario_atualiza;
	}

	public Date getCriado_em() {
		return criado_em;
	}

	public void setCriado_em(Date criado_em) {
		this.criado_em = criado_em;
	}

	public Date getAtualizado_em() {
		return atualizado_em;
	}

	public void setAtualizado_em(Date atualizado_em) {
		this.atualizado_em = atualizado_em;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((animais == null) ? 0 : animais.hashCode());
		result = prime * result + ((atualizado_em == null) ? 0 : atualizado_em.hashCode());
		result = prime * result + ((cliente == null) ? 0 : cliente.hashCode());
		result = prime * result + ((criado_em == null) ? 0 : criado_em.hashCode());
		result = prime * result + ((data_checkin == null) ? 0 : data_checkin.hashCode());
		result = prime * result + ((data_checkout == null) ? 0 : data_checkout.hashCode());
		result = prime * result + ((hora_checkin == null) ? 0 : hora_checkin.hashCode());
		result = prime * result + ((hora_checkout == null) ? 0 : hora_checkout.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((negocio == null) ? 0 : negocio.hashCode());
		result = prime * result + ((observacao == null) ? 0 : observacao.hashCode());
		result = prime * result + ((ofertaHospedagem == null) ? 0 : ofertaHospedagem.hashCode());
		result = prime * result + ((usuario_atualiza == null) ? 0 : usuario_atualiza.hashCode());
		result = prime * result + ((usuario_cadastro == null) ? 0 : usuario_cadastro.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Hospedagem other = (Hospedagem) obj;
		if (animais == null) {
			if (other.animais != null)
				return false;
		} else if (!animais.equals(other.animais))
			return false;
		if (atualizado_em == null) {
			if (other.atualizado_em != null)
				return false;
		} else if (!atualizado_em.equals(other.atualizado_em))
			return false;
		if (cliente == null) {
			if (other.cliente != null)
				return false;
		} else if (!cliente.equals(other.cliente))
			return false;
		if (criado_em == null) {
			if (other.criado_em != null)
				return false;
		} else if (!criado_em.equals(other.criado_em))
			return false;
		if (data_checkin == null) {
			if (other.data_checkin != null)
				return false;
		} else if (!data_checkin.equals(other.data_checkin))
			return false;
		if (data_checkout == null) {
			if (other.data_checkout != null)
				return false;
		} else if (!data_checkout.equals(other.data_checkout))
			return false;
		if (hora_checkin == null) {
			if (other.hora_checkin != null)
				return false;
		} else if (!hora_checkin.equals(other.hora_checkin))
			return false;
		if (hora_checkout == null) {
			if (other.hora_checkout != null)
				return false;
		} else if (!hora_checkout.equals(other.hora_checkout))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (negocio == null) {
			if (other.negocio != null)
				return false;
		} else if (!negocio.equals(other.negocio))
			return false;
		if (observacao == null) {
			if (other.observacao != null)
				return false;
		} else if (!observacao.equals(other.observacao))
			return false;
		if (ofertaHospedagem == null) {
			if (other.ofertaHospedagem != null)
				return false;
		} else if (!ofertaHospedagem.equals(other.ofertaHospedagem))
			return false;
		if (usuario_atualiza == null) {
			if (other.usuario_atualiza != null)
				return false;
		} else if (!usuario_atualiza.equals(other.usuario_atualiza))
			return false;
		if (usuario_cadastro == null) {
			if (other.usuario_cadastro != null)
				return false;
		} else if (!usuario_cadastro.equals(other.usuario_cadastro))
			return false;
		return true;
	}
}