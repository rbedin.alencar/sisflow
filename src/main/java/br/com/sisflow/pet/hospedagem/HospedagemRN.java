package br.com.sisflow.pet.hospedagem;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.sisflow.cliente.Cliente;

@Service
@Transactional
public class HospedagemRN{
	
	@Autowired
	private HospedagemDAO dao;
	
	public void Salvar(Hospedagem hospedagem) {
		
		Long id = hospedagem.getId();
		if(id == null)
			this.dao.Salvar(hospedagem);
		else
			this.dao.Atualizar(hospedagem);
	}
	
	public void Excluir(Hospedagem hospedagem) {
		this.dao.Excluir(hospedagem);
	}
	
	public Hospedagem Carregar(Long id) {
		return this.dao.Carregar(id);
	}

	public List<Hospedagem> Listar(){
		return this.dao.Listar();
	}
	
	public List<Hospedagem> Listar(Cliente cliente){
		return this.dao.Listar(cliente);
	}
	
	public Long Total(){
		return this.dao.Total();
	}
}