package br.com.sisflow.pet.hospedagem;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.sisflow.cliente.ClienteRN;
import br.com.sisflow.core.negocio.NegocioRN;
import br.com.sisflow.oferta.hospedagem.OfertaHospedagemRN;
import br.com.sisflow.pet.animal.AnimalRN;
import br.com.sisflow.venda.VendaRN;

@Controller
@Transactional
@RequestMapping("/pet/hospedagem")
public class HospedagemController{
	
	/* LOGGER */
	
	protected static Logger logger = Logger.getLogger(HospedagemController.class);
	
	/* VALIDATOR */
	
	@Autowired
	private HospedagemValidator hospedagemValidator;
	
	/* SERVICES */
	
	@Autowired
	private HospedagemRN hospedagemRN;
	
	@Autowired
	private ClienteRN clienteRN;
	
	@Autowired
	private AnimalRN animalRN;
	
	@Autowired
	private VendaRN vendaRN;
	
	@Autowired
	private NegocioRN negocioRN;
	
	@Autowired
	private OfertaHospedagemRN ofertaHospedagemRN;
	
	
	/* BINDER */
	
	@InitBinder
    protected void initBinder(WebDataBinder binder) {
		binder.setValidator(this.hospedagemValidator);
    }
	
	@InitBinder
	public void initBigDecimalBinder(WebDataBinder binder) throws Exception{
		DecimalFormat df = new DecimalFormat();
		DecimalFormatSymbols dfs = new DecimalFormatSymbols();
		dfs.setGroupingSeparator('.');
		dfs.setDecimalSeparator(',');
		df.setDecimalFormatSymbols(dfs);
		binder.registerCustomEditor(BigDecimal.class, new CustomNumberEditor(BigDecimal.class, df, true));
	}
	
	/* ROUTES */
	
	@GetMapping
	@Cacheable(value="hospedagem")
	public ModelAndView index() {
		ModelAndView mv = new ModelAndView("pet/hospedagem/index");
		mv.addObject("hospedagemDS", this.hospedagemRN.Listar());
		return mv;
	}
	
	@GetMapping({"/cadastro", "/cadastro/{cliente_id}"}) 
	public ModelAndView cadastro(Hospedagem hospedagem, @PathVariable("cliente_id") Optional<Long> cliente_id) {
		
		ModelAndView mv = new ModelAndView("pet/hospedagem/cadastro");
		
		/* THIS */
		mv.addObject("hospedagem", hospedagem);
		
		/* DS */		
		mv.addObject("clienteDS", this.clienteRN.Listar());
		if(cliente_id != null && cliente_id.isPresent())
			hospedagem.setCliente(clienteRN.Carregar(cliente_id.get()));

		mv.addObject("negocioDS", this.negocioRN.Listar());
		mv.addObject("clienteDS", this.clienteRN.Listar());
		mv.addObject("animalDS", this.animalRN.Listar());
		
		mv.addObject("ofertaHospedagemDS", this.ofertaHospedagemRN.Listar());
		
		/* DEFAULT */
		if(hospedagem.getId() == null) {
			hospedagem.setData_checkin(new Date(System.currentTimeMillis()));
			hospedagem.setHora_checkin(new Date(System.currentTimeMillis()));
		}
		
		/* VIEW */
		return mv;
	}
	
	@GetMapping("/altera/{id}")
	public ModelAndView altera(@PathVariable("id") Long id) {
		Hospedagem hospedagem = this.hospedagemRN.Carregar(id);
		if(hospedagem != null)
			return cadastro(hospedagem, null);
		else
			return new ModelAndView("redirect:/pet/hospedagem");
	}
	
	@GetMapping("/exclui/{id}")
	@CacheEvict(value="hospedagem", allEntries=true)
	public ModelAndView exclui(@PathVariable("id") Long id, RedirectAttributes redirectAttributes) {

		Hospedagem hospedagem = this.hospedagemRN.Carregar(id);
		if(hospedagem != null)
			this.hospedagemRN.Excluir(hospedagem);	

		redirectAttributes.addFlashAttribute("msg", "Registro excluido com sucesso!");
		
		return new ModelAndView("redirect:/pet/hospedagem");
	}
	
	@PostMapping("/salvar")
	@CacheEvict(value="hospedagem", allEntries=true)
	public ModelAndView salvar(@ModelAttribute @Valid Hospedagem hospedagem, BindingResult result, RedirectAttributes redirectAttributes, HttpServletRequest request){
		
		System.out.println("################### HOSPEDAGEM");
		
		if(result.hasErrors())
			return cadastro(hospedagem, null);
		
		System.out.println("################### HOSPEDAGEM");
		
		//checkin
		if(hospedagem.getData_checkin() != null) {
			Date fmtDate = null;
			DateFormat df = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy", Locale.US);
			try {
				fmtDate = (Date) df.parse(hospedagem.getData_checkin().toString());
			} catch (ParseException e) {
				logger.info("Não foi possivel formatar a data: " + e.getMessage());
			}
			finally {
				hospedagem.setData_checkin(fmtDate);
			}
		}
		hospedagem.setHora_checkin(new Date());
		
		//checkout
		if(hospedagem.getData_checkout() != null) {
			Date fmtDate = null;
			DateFormat df = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy", Locale.US);
			try {
				fmtDate = (Date) df.parse(hospedagem.getData_checkout().toString());
			} catch (ParseException e) {
				logger.info("Não foi possivel formatar a data: " + e.getMessage());
			}
			finally {
				hospedagem.setData_checkout(fmtDate);
			}
		}		
		hospedagem.setHora_checkout(new Date());
		
		System.out.println("###### HORA: " + hospedagem.getHora_checkin());
		
		/*
		long diffInMilles = Math.abs(hospedagem.getData_checkout().getTime() - hospedagem.getData_checkin().getTime());
		long diff = TimeUnit.DAYS.convert(diffInMilles, TimeUnit.MILLISECONDS);
		*/
		
			
		/* SAVE */
		this.hospedagemRN.Salvar(hospedagem);
		
		redirectAttributes.addFlashAttribute("msg", "Registro salvo com sucesso!");
		
		return new ModelAndView("redirect:/oferta");
	}
}