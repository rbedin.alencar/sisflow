package br.com.sisflow.pet.animal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import net.sf.jasperreports.engine.JRException;

@Service("animalReport")
public class AnimalReport{
	
	
	public List<Map<String, Object>> Ficha(Animal animal) throws JRException{
		
		List<Map<String, Object>> ds = new ArrayList<Map<String, Object>>();
		
		Map<String, Object> m = new HashMap<String, Object>();
		
		m.put("animal_id", animal.getId());
		m.put("nome", animal.getNome());
		m.put("especie", animal.getEspecie().getNome());
		m.put("raca", animal.getRaca().getNome());
		m.put("pelagem", animal.getPelagem().getNome());
		m.put("peso", animal.getPeso());
		m.put("pedigree", animal.getPedigree());
		m.put("chip", animal.getChip());
		m.put("castrado", animal.isCastrado());
		m.put("observacao", animal.getObservacao());
		m.put("data_nascimento", animal.getData_nascimento());
		m.put("sexo", animal.getSexo());
		m.put("porte", animal.getPorte());
		
		return ds;
	}	
}
