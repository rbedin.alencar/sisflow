package br.com.sisflow.pet.animal.sexo;

public enum AnimalSexo {
	
	M("Macho"), 
	F("Fêmea");
	
	private String descricao;
	
	AnimalSexo(String descricao){
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return this.descricao;
	}
}
