package br.com.sisflow.pet.animal;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

@Component
public class AnimalFormatter implements Formatter<Animal> {
	
	@Autowired
	private AnimalRN animalRN;
	
	@Override
	public String print(Animal object, Locale locale) {
		return object.getId().toString();
	}

	@Override
	public Animal parse(String text, Locale locale) throws ParseException {
		return this.animalRN.Carregar(Long.parseLong(text));
	}
}
