package br.com.sisflow.pet.animal.especie;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

@Repository
public class AnimalEspecieImpl implements AnimalEspecieDAO {
	
	@PersistenceContext
	private EntityManager manager;

	@Override
	public void Salvar(AnimalEspecie animalEspecie) {
		manager.persist(animalEspecie);
	}

	@Override
	public void Atualizar(AnimalEspecie animalEspecie) {
		this.manager.merge(animalEspecie);
	}

	@Override
	public void Excluir(AnimalEspecie animalEspecie) {
		this.manager.remove(animalEspecie);
	}

	@Override
	public AnimalEspecie Carregar(Long id) {
		return this.manager.find(AnimalEspecie.class, id);
	}
	
	@Override
	public List<AnimalEspecie> Listar() {		
		return this.manager.createQuery("select ae from AnimalEspecie ae", AnimalEspecie.class).getResultList();		
	}
}
