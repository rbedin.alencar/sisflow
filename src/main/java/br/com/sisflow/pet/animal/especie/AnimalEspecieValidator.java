package br.com.sisflow.pet.animal.especie;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class AnimalEspecieValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return AnimalEspecie.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nome", "field.required");		
	}

}
