package br.com.sisflow.pet.animal.raca;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.sisflow.pet.animal.especie.AnimalEspecie;
import br.com.sisflow.pet.animal.especie.AnimalEspecieRN;

@RestController
@RequestMapping("/api/pet/animal/raca")
public class AnimalRacaControllerAPI{
	
	protected static Logger logger = Logger.getLogger(AnimalRacaControllerAPI.class);

	@Autowired
	private AnimalEspecieRN animalEspecieRN;
	
	@Autowired
	private AnimalRacaRN animalRacaRN;
	
	@GetMapping("/listar/{animal_especie_id}")
	public List<AnimalRaca> Listar(@PathVariable Long animal_especie_id){
		AnimalEspecie especie = this.animalEspecieRN.Carregar(animal_especie_id);
		if(especie != null)
			return this.animalRacaRN.Listar(especie);
		return new ArrayList<AnimalRaca>();
	}
}