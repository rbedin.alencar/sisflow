package br.com.sisflow.pet.animal;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import br.com.sisflow.cliente.Cliente;
import br.com.sisflow.pet.animal.especie.AnimalEspecie;
import br.com.sisflow.pet.animal.pelagem.AnimalPelagem;
import br.com.sisflow.pet.animal.raca.AnimalRaca;

@Repository
public class AnimalImpl implements AnimalDAO {

	@PersistenceContext
	private EntityManager manager;

	public void Salvar(Animal animal) {
		manager.persist(animal);
	}

	public void Atualizar(Animal animal) {
		this.manager.merge(animal);
	}

	public void Excluir(Animal animal) {
		this.manager.remove(animal);
	}

	public Animal Carregar(Long id) {
		return this.manager.find(Animal.class, id);
	}

	@Override
	public List<Animal> Listar() {
		return this.manager.createQuery("select a from Animal a", Animal.class).getResultList();
	}
	
	@Override
	public List<Animal> Listar(AnimalEspecie animalEspecie) {
		return this.manager.createQuery("select a from Animal a where a.especie.id =:animal_especie_id", Animal.class)
				.setParameter("animal_especie_id", animalEspecie.getId())
				.getResultList();
	}
	
	public List<Animal> Listar(Cliente cliente) {		
		return this.manager.createQuery("select a from Animal a where a.cliente.id = :cliente_id", Animal.class)
				.setParameter("cliente_id", cliente.getId())
				.getResultList();		
	}

	@Override
	public List<Animal> Listar(AnimalRaca animalRaca) {
		return this.manager.createQuery("select a from Animal a where a.raca.id =:animal_raca_id", Animal.class)
				.setParameter("animal_raca_id", animalRaca.getId())
				.getResultList();
	}

	@Override
	public List<Animal> Listar(AnimalPelagem animalPelagem) {
		return this.manager.createQuery("select a from Animal a where a.pelagem.id =:animal_pelagem_id", Animal.class)
				.setParameter("animal_pelagem_id", animalPelagem.getId())
				.getResultList();
	}

	@Override
	public Long Total() {
		return (Long) this.manager.createQuery("select count(a) from Animal a").getSingleResult();
	}
}