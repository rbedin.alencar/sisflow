package br.com.sisflow.pet.animal.raca;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.sisflow.pet.animal.Animal;
import br.com.sisflow.pet.animal.AnimalRN;
import br.com.sisflow.pet.animal.especie.AnimalEspecie;

@Service
@Transactional
public class AnimalRacaRN{
	
	@Autowired
	private AnimalRacaDAO dao = null;	
	
	@Autowired
	private AnimalRN animalRN;
	
	public void Salvar(AnimalRaca animalRaca) {
		Long id = animalRaca.getId();
		if(id == null)
			this.dao.Salvar(animalRaca);
		else
			this.dao.Atualizar(animalRaca);
	}
	
	public void Excluir(AnimalRaca animalRaca) throws Exception {
		Long id = animalRaca.getId();
		if(id == 1)
			throw new Exception("Não é possivel excluir o registro padrão do sistema.");
		else {						
			List<Animal> animais = this.animalRN.Listar(animalRaca);
			if(animais.size() > 0) {
				AnimalRaca racaDefault = this.Carregar(Long.parseLong("1"));
				for(Animal a : animais) {
					a.setRaca(racaDefault);
					this.animalRN.Salvar(a);
				}
			}
			this.dao.Excluir(animalRaca);
		}
	}
	
	public AnimalRaca Carregar(Long animal_raca_id) {
		return this.dao.Carregar(animal_raca_id);
	}
	
	public List<AnimalRaca> Listar(){
		return this.dao.Listar();
	}
	
	public List<AnimalRaca> Listar(AnimalEspecie animalEspecie){
		return this.dao.Listar(animalEspecie);
	}
}