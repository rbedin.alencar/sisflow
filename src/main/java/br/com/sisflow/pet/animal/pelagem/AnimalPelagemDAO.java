package br.com.sisflow.pet.animal.pelagem;

import java.util.List;

import javax.transaction.Transactional;

@Transactional
public interface AnimalPelagemDAO{
	public void Salvar(AnimalPelagem animalPelagem);
	public void Atualizar(AnimalPelagem animalPelagem);
	public void Excluir(AnimalPelagem animalPelagem);
	public AnimalPelagem Carregar(Long animal_pelagem_id);
	public List<AnimalPelagem> Listar();
}
