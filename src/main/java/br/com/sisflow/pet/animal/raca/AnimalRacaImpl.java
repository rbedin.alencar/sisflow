package br.com.sisflow.pet.animal.raca;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import br.com.sisflow.pet.animal.especie.AnimalEspecie;

@Repository
public class AnimalRacaImpl implements AnimalRacaDAO {
	
	@PersistenceContext
	private EntityManager manager;

	@Override
	public void Salvar(AnimalRaca animalRaca) {
		manager.persist(animalRaca);
	}

	@Override
	public void Atualizar(AnimalRaca animalRaca) {
		this.manager.merge(animalRaca);
	}

	@Override
	public void Excluir(AnimalRaca animalRaca) {
		this.manager.remove(animalRaca);
	}

	@Override
	public AnimalRaca Carregar(Long id) {
		return this.manager.find(AnimalRaca.class, id);
	}
	
	@Override
	public List<AnimalRaca> Listar() {		
		return this.manager.createQuery("SELECT ar FROM AnimalRaca ar", AnimalRaca.class).getResultList();		
	}

	@Override
	public List<AnimalRaca> Listar(AnimalEspecie animalEspecie) {
		return this.manager.createQuery("select ar from AnimalRaca ar where ar.especie.id = :animal_especie_id", AnimalRaca.class)
				.setParameter("animal_especie_id", animalEspecie.getId())
				.getResultList();
	}
}
