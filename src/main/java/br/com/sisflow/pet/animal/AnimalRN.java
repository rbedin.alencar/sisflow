package br.com.sisflow.pet.animal;

import java.io.IOException;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.sisflow.cliente.Cliente;
import br.com.sisflow.conf.SisflowConfiguration;
import br.com.sisflow.core.upload.FileUploadFTP;
import br.com.sisflow.pet.animal.especie.AnimalEspecie;
import br.com.sisflow.pet.animal.pelagem.AnimalPelagem;
import br.com.sisflow.pet.animal.raca.AnimalRaca;

@Service
@Transactional
public class AnimalRN{
	
	@Autowired
	private AnimalDAO dao;
	
	public void Salvar(Animal animal) {
		
		Long id = animal.getId();
		if(id == null)
			this.dao.Salvar(animal);
		else
			this.dao.Atualizar(animal);
	}
	
	public void Excluir(Animal animal) {
		
		/*
		 * Conecta ao FTP e exclui as imagens 
		 * ligadas ao registro.
		 */
		String[] files_to_delete = {
				animal.getId() + ".jpg", 
				"thumb_" + animal.getId() + ".jpg"
				};
		
		FileUploadFTP fileUploadFTP = new FileUploadFTP();
		for(String f : files_to_delete) {
			try {
				fileUploadFTP.Delete(SisflowConfiguration.getValue("client.storage.directory") + "/animal/" + f);			
			} catch (IOException e) {
				System.out.println("Não foi possivel excluir o arquivo ["+f+"] do servidor: " + e.getMessage());
			}	
		}
		
		/*
		 * Exclui o registro.
		 */
		this.dao.Excluir(animal);
	}
	
	public Animal Carregar(Long id) {
		return this.dao.Carregar(id);
	}
	
	public List<Animal> Listar(){
		return this.dao.Listar();
	}
	
	public List<Animal> Listar(AnimalEspecie animalEspecie){
		return this.dao.Listar(animalEspecie);
	}
	
	public List<Animal> Listar(AnimalRaca animalRaca){
		return this.dao.Listar(animalRaca);
	}
	
	public List<Animal> Listar(AnimalPelagem animalPelagem){
		return this.dao.Listar(animalPelagem);
	}
	
	public List<Animal> Listar(Cliente cliente){
		return this.dao.Listar(cliente);
	}
	
	public Long Total(){
		return this.dao.Total();
	}
}