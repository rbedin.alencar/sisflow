package br.com.sisflow.pet.animal.porte;

public enum AnimalPorte {
	
	MINI("Mini"),
	PEQUENO("Pequeno"), 
	MEDIO("Médio"), 
	GRANDE("Grande"), 
	GIGANTE("Gigante");
	
	private String descricao;
	
	AnimalPorte(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
}
