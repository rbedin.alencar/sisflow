package br.com.sisflow.pet.animal.especie;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

@Component
public class AnimalEspecieFormatter implements Formatter<AnimalEspecie> {
	
	@Autowired
	private AnimalEspecieRN animalEspecieRN;
	
	@Override
	public String print(AnimalEspecie object, Locale locale) {
		return object.getId().toString();
	}

	@Override
	public AnimalEspecie parse(String text, Locale locale) throws ParseException {
		return this.animalEspecieRN.Carregar(Long.parseLong(text));
	}
}
