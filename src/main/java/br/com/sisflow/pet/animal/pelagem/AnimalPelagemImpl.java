package br.com.sisflow.pet.animal.pelagem;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

@Repository
public class AnimalPelagemImpl implements AnimalPelagemDAO {
	
	@PersistenceContext
	private EntityManager manager;

	@Override
	public void Salvar(AnimalPelagem animalPelagem) {
		manager.persist(animalPelagem);
	}

	@Override
	public void Atualizar(AnimalPelagem animalPelagem) {
		this.manager.merge(animalPelagem);
	}

	@Override
	public void Excluir(AnimalPelagem animalPelagem) {
		this.manager.remove(animalPelagem);
	}

	@Override
	public AnimalPelagem Carregar(Long id) {
		return this.manager.find(AnimalPelagem.class, id);
	}
	
	@Override
	public List<AnimalPelagem> Listar() {		
		return this.manager.createQuery("select ap from AnimalPelagem ap", AnimalPelagem.class).getResultList();		
	}
}
