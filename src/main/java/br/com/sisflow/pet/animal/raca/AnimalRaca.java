package br.com.sisflow.pet.animal.raca;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.sisflow.pet.animal.especie.AnimalEspecie;

@Entity
@Table(name="pet_animal_raca")
public class AnimalRaca implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3461571926974462177L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="animal_raca_id")
	private Long id;
	
	@Column(length=32, nullable=false)
	private String nome;
	
	@Column(columnDefinition="bit(1) default 1")
	private boolean ativo = true;
	
	@ManyToOne
	@JoinColumn(name="animal_especie_id")
	@JsonIgnoreProperties("racas") // necessário para ignorar o relacionamento e não cair em loop quando listar
	private AnimalEspecie especie;
	
	@Column(nullable=true, columnDefinition="TEXT")
	private String url;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public AnimalEspecie getEspecie() {
		return especie;
	}

	public void setEspecie(AnimalEspecie especie) {
		this.especie = especie;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (ativo ? 1231 : 1237);
		result = prime * result + ((especie == null) ? 0 : especie.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AnimalRaca other = (AnimalRaca) obj;
		if (ativo != other.ativo)
			return false;
		if (especie == null) {
			if (other.especie != null)
				return false;
		} else if (!especie.equals(other.especie))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		return true;
	}
}