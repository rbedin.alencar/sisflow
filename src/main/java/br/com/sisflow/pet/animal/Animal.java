package br.com.sisflow.pet.animal;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.sisflow.cliente.Cliente;
import br.com.sisflow.pet.animal.especie.AnimalEspecie;
import br.com.sisflow.pet.animal.pelagem.AnimalPelagem;
import br.com.sisflow.pet.animal.porte.AnimalPorte;
import br.com.sisflow.pet.animal.raca.AnimalRaca;
import br.com.sisflow.pet.animal.sexo.AnimalSexo;

@Entity
@Table(name="pet_animal")
public class Animal implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8335994967997828998L;

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="animal_id")
	private Long id;
	
	@Column(length=45)
	private String nome;
	
	@ManyToOne
	@JoinColumn(name="cliente_id")
	@JsonIgnore
	private Cliente cliente;
	
	@ManyToOne
	@JoinColumn(name="animal_especie_id")
	private AnimalEspecie especie;
	
	@ManyToOne
	@JoinColumn(name="animal_raca_id")
	private AnimalRaca raca;

	@ManyToOne
	@JoinColumn(name="animal_pelagem_id")
	private AnimalPelagem pelagem;
	
	@Enumerated(EnumType.STRING)
	@Column(length=1)
	private AnimalSexo sexo;
	
	@Enumerated(EnumType.STRING)
	@Column(length=10)
	private AnimalPorte porte;
	
	@Column(nullable=true)
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date data_nascimento;
	
	@Column(nullable=true)
	private Float peso;
	
	@Column(length=32, nullable=true)
	private String pedigree;

	@Column(length=32, nullable=true)
	private String chip;
	
	@Column(columnDefinition="bit(1) default 0")
	private boolean castrado = false;
	
	@Column(columnDefinition="bit(1) default 0")
	private boolean falecido = false;
	
	@Column(nullable=true, columnDefinition="TEXT")
	private String vacinas;
	
	@Column(nullable=true, columnDefinition="TEXT")
	private String observacao;
	
	@Column(columnDefinition="DATETIME", updatable=false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date criado_em;
	
	@Column(columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date atualizado_em;

	/* GETTERS & SETTERS */
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public AnimalEspecie getEspecie() {
		return especie;
	}

	public void setEspecie(AnimalEspecie especie) {
		this.especie = especie;
	}

	public AnimalRaca getRaca() {
		return raca;
	}

	public void setRaca(AnimalRaca raca) {
		this.raca = raca;
	}

	public AnimalPelagem getPelagem() {
		return pelagem;
	}

	public void setPelagem(AnimalPelagem pelagem) {
		this.pelagem = pelagem;
	}

	public AnimalSexo getSexo() {
		return sexo;
	}

	public void setSexo(AnimalSexo sexo) {
		this.sexo = sexo;
	}

	public AnimalPorte getPorte() {
		return porte;
	}

	public void setPorte(AnimalPorte porte) {
		this.porte = porte;
	}

	public Date getData_nascimento() {
		return data_nascimento;
	}

	public void setData_nascimento(Date data_nascimento) {
		this.data_nascimento = data_nascimento;
	}

	public Float getPeso() {
		return peso;
	}

	public void setPeso(Float peso) {
		this.peso = peso;
	}

	public String getPedigree() {
		return pedigree;
	}

	public void setPedigree(String pedigree) {
		this.pedigree = pedigree;
	}

	public String getChip() {
		return chip;
	}

	public void setChip(String chip) {
		this.chip = chip;
	}

	public boolean isCastrado() {
		return castrado;
	}

	public void setCastrado(boolean castrado) {
		this.castrado = castrado;
	}

	public boolean isFalecido() {
		return falecido;
	}

	public void setFalecido(boolean falecido) {
		this.falecido = falecido;
	}

	public String getVacinas() {
		return vacinas;
	}

	public void setVacinas(String vacinas) {
		this.vacinas = vacinas;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Date getCriado_em() {
		return criado_em;
	}

	public void setCriado_em(Date criado_em) {
		this.criado_em = criado_em;
	}

	public Date getAtualizado_em() {
		return atualizado_em;
	}

	public void setAtualizado_em(Date atualizado_em) {
		this.atualizado_em = atualizado_em;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((atualizado_em == null) ? 0 : atualizado_em.hashCode());
		result = prime * result + (castrado ? 1231 : 1237);
		result = prime * result + ((chip == null) ? 0 : chip.hashCode());
		result = prime * result + ((cliente == null) ? 0 : cliente.hashCode());
		result = prime * result + ((criado_em == null) ? 0 : criado_em.hashCode());
		result = prime * result + ((data_nascimento == null) ? 0 : data_nascimento.hashCode());
		result = prime * result + ((especie == null) ? 0 : especie.hashCode());
		result = prime * result + (falecido ? 1231 : 1237);
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((observacao == null) ? 0 : observacao.hashCode());
		result = prime * result + ((pedigree == null) ? 0 : pedigree.hashCode());
		result = prime * result + ((pelagem == null) ? 0 : pelagem.hashCode());
		result = prime * result + ((peso == null) ? 0 : peso.hashCode());
		result = prime * result + ((porte == null) ? 0 : porte.hashCode());
		result = prime * result + ((raca == null) ? 0 : raca.hashCode());
		result = prime * result + ((sexo == null) ? 0 : sexo.hashCode());
		result = prime * result + ((vacinas == null) ? 0 : vacinas.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Animal other = (Animal) obj;
		if (atualizado_em == null) {
			if (other.atualizado_em != null)
				return false;
		} else if (!atualizado_em.equals(other.atualizado_em))
			return false;
		if (castrado != other.castrado)
			return false;
		if (chip == null) {
			if (other.chip != null)
				return false;
		} else if (!chip.equals(other.chip))
			return false;
		if (cliente == null) {
			if (other.cliente != null)
				return false;
		} else if (!cliente.equals(other.cliente))
			return false;
		if (criado_em == null) {
			if (other.criado_em != null)
				return false;
		} else if (!criado_em.equals(other.criado_em))
			return false;
		if (data_nascimento == null) {
			if (other.data_nascimento != null)
				return false;
		} else if (!data_nascimento.equals(other.data_nascimento))
			return false;
		if (especie == null) {
			if (other.especie != null)
				return false;
		} else if (!especie.equals(other.especie))
			return false;
		if (falecido != other.falecido)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (observacao == null) {
			if (other.observacao != null)
				return false;
		} else if (!observacao.equals(other.observacao))
			return false;
		if (pedigree == null) {
			if (other.pedigree != null)
				return false;
		} else if (!pedigree.equals(other.pedigree))
			return false;
		if (pelagem == null) {
			if (other.pelagem != null)
				return false;
		} else if (!pelagem.equals(other.pelagem))
			return false;
		if (peso == null) {
			if (other.peso != null)
				return false;
		} else if (!peso.equals(other.peso))
			return false;
		if (porte != other.porte)
			return false;
		if (raca == null) {
			if (other.raca != null)
				return false;
		} else if (!raca.equals(other.raca))
			return false;
		if (sexo != other.sexo)
			return false;
		if (vacinas == null) {
			if (other.vacinas != null)
				return false;
		} else if (!vacinas.equals(other.vacinas))
			return false;
		return true;
	}
}