package br.com.sisflow.pet.animal;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class AnimalValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return Animal.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {		
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nome", "field.required.pet.animal.nome");	
		
	}
}
