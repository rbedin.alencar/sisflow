package br.com.sisflow.pet.animal.especie;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@Transactional
@RequestMapping("/pet/animal/especie")
public class AnimalEspecieController{
	
	/* LOGGER */
	
	protected static Logger logger = Logger.getLogger(AnimalEspecieController.class);
	
	/* VALIDATOR */
	
	@Autowired
	private AnimalEspecieValidator animalEspecieValidator;
	
	/* SERVICES */
	
	@Autowired
	private AnimalEspecieRN animalEspecieRN;

	/* BINDER */
	
	@InitBinder
    protected void initBinder(WebDataBinder binder) {
		binder.setValidator(animalEspecieValidator);
    }
	
	/* ROUTES */
	
	@GetMapping
	@Cacheable(value="animal.especie")
	public ModelAndView index() {
		ModelAndView mv = new ModelAndView("pet/animal/especie/index");
		mv.addObject("animalEspecieDS", this.animalEspecieRN.Listar());
		return mv;
	}
	
	@GetMapping("/cadastro")
	public ModelAndView cadastro(AnimalEspecie animalEspecie) {
		ModelAndView mv = new ModelAndView("pet/animal/especie/cadastro");
		
		/* THIS */
		mv.addObject("animalEspecie", animalEspecie);
		
		return mv;
	}
	
	@GetMapping("/altera/{id}")
	public ModelAndView altera(@PathVariable("id") Long id) {

		AnimalEspecie animalEspecie = this.animalEspecieRN.Carregar(id);
		if(animalEspecie != null)
			return cadastro(animalEspecie);
		else
			return new ModelAndView("redirect:/pet/animal/especie");
	}
	
	@GetMapping("/exclui/{id}")
	@CacheEvict(value= {"animal.especie", "animal"}, allEntries=true)
	public ModelAndView exclui(@PathVariable("id") Long id, RedirectAttributes redirectAttributes){

		AnimalEspecie animalEspecie = this.animalEspecieRN.Carregar(id);
		if(animalEspecie != null) {
			
			try {
				this.animalEspecieRN.Excluir(animalEspecie);	
				redirectAttributes.addFlashAttribute("msg", "Espécie excluída com sucesso!");
			}
			catch (Exception e) {
				redirectAttributes.addFlashAttribute("msg", e.getMessage());
			}
		}
		
		return new ModelAndView("redirect:/pet/animal/especie");
	}
	
	@PostMapping("/salvar")
	@CacheEvict(value="animal.especie", allEntries=true)
	public ModelAndView salvar(@ModelAttribute @Valid AnimalEspecie animalEspecie, BindingResult result, RedirectAttributes redirectAttributes, HttpServletRequest request){
		
		if(result.hasErrors())
			return cadastro(animalEspecie);
		
		/* SAVE */
		this.animalEspecieRN.Salvar(animalEspecie);
		
		redirectAttributes.addFlashAttribute("msg", "Espécie salva com sucesso!");
		
		return new ModelAndView("redirect:/pet/animal/especie");
	}
}