package br.com.sisflow.pet.animal.especie;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.sisflow.pet.animal.Animal;
import br.com.sisflow.pet.animal.AnimalRN;
import br.com.sisflow.pet.animal.raca.AnimalRaca;
import br.com.sisflow.pet.animal.raca.AnimalRacaRN;

@Service
@Transactional
public class AnimalEspecieRN{
	
	@Autowired
	private AnimalEspecieDAO dao = null;	
	
	@Autowired
	private AnimalRN animalRN;
	
	@Autowired
	private AnimalRacaRN animalRacaRN;
	
	public void Salvar(AnimalEspecie animalEspecie) {
		Long id = animalEspecie.getId();
		if(id == null)
			this.dao.Salvar(animalEspecie);
		else
			this.dao.Atualizar(animalEspecie);
	}
	
	public void Excluir(AnimalEspecie animalEspecie) throws Exception {
		Long id = animalEspecie.getId();
		if(id == 1)
			throw new Exception("Não é possivel excluir o registro padrão do sistema.");
		else {
			
			/*
			 * AnimalEspecie
			 * 
			 * Verifica os registro que tem ligação e 
			 * altera para o padrão do sistema.
			 */
			List<Animal> animais = this.animalRN.Listar(animalEspecie);
			if(animais.size() > 0) {
				
				AnimalEspecie especieDefault = this.Carregar(Long.parseLong("1")); //default especie
				AnimalRaca racaDefault = this.animalRacaRN.Carregar(Long.parseLong("1")); //default raca
				
				for(Animal a : animais) {
					
					a.setEspecie(especieDefault);
					a.setRaca(racaDefault);
					
					this.animalRN.Salvar(a);
				}
			}
			
			/*
			 * Exclui o registro.
			 */
			this.dao.Excluir(animalEspecie);
		}
	}
	
	public AnimalEspecie Carregar(Long animal_especie_id) {
		return this.dao.Carregar(animal_especie_id);
	}
	
	public List<AnimalEspecie> Listar(){
		return this.dao.Listar();
	}
}