package br.com.sisflow.pet.animal;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.sisflow.cliente.Cliente;
import br.com.sisflow.cliente.ClienteRN;

@RestController
@RequestMapping("/api/pet/animal")
public class AnimalControllerAPI{
	
	protected static Logger logger = Logger.getLogger(AnimalControllerAPI.class);
	
	@Autowired
	private AnimalRN animalRN;
	
	@Autowired
	private ClienteRN clienteRN;
	
	@GetMapping("/listar/{cliente_id}")
	public List<Animal> Listar(@PathVariable Long cliente_id){
		Cliente cliente = this.clienteRN.Carregar(cliente_id);
		if(cliente != null)
			return this.animalRN.Listar(cliente);
		return new ArrayList<Animal>();
	}
}