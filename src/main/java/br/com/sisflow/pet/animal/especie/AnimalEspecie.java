package br.com.sisflow.pet.animal.especie;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import br.com.sisflow.pet.animal.raca.AnimalRaca;

@Entity
@Table(name="pet_animal_especie")
public class AnimalEspecie implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3461571926974462177L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="animal_especie_id")
	private Long id;
	
	@Column(length=32, nullable=false)
	private String nome;
	
	@Column(columnDefinition="bit(1) default 1")
	private boolean ativo = true;

	@OneToMany(mappedBy="especie", orphanRemoval=true, cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	private List<AnimalRaca> racas;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public List<AnimalRaca> getRacas() {
		return racas;
	}

	public void setRacas(List<AnimalRaca> racas) {
		this.racas = racas;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (ativo ? 1231 : 1237);
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((racas == null) ? 0 : racas.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AnimalEspecie other = (AnimalEspecie) obj;
		if (ativo != other.ativo)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (racas == null) {
			if (other.racas != null)
				return false;
		} else if (!racas.equals(other.racas))
			return false;
		return true;
	}
}