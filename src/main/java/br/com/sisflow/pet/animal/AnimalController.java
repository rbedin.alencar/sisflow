package br.com.sisflow.pet.animal;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.sisflow.cliente.ClienteRN;
import br.com.sisflow.cliente.endereco.ClienteEndereco;
import br.com.sisflow.cliente.tipo.ClienteTipo;
import br.com.sisflow.conf.SisflowConfiguration;
import br.com.sisflow.core.negocio.tipo.NegocioTipo;
import br.com.sisflow.core.pessoa.fisica.PessoaFisica;
import br.com.sisflow.core.pessoa.juridica.PessoaJuridica;
import br.com.sisflow.core.upload.FileUploadImg;
import br.com.sisflow.pet.animal.especie.AnimalEspecieRN;
import br.com.sisflow.pet.animal.pelagem.AnimalPelagemRN;
import br.com.sisflow.pet.animal.porte.AnimalPorte;
import br.com.sisflow.pet.animal.raca.AnimalRacaRN;
import br.com.sisflow.pet.animal.sexo.AnimalSexo;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;

@Controller
@Transactional
@RequestMapping("/pet/animal")
public class AnimalController{
	
	/* LOGGER */
	
	protected static Logger logger = Logger.getLogger(AnimalController.class);
	
	/* CONTEXT */
	
	@Autowired
	private ServletContext context;
	
	/* VALIDATOR */
	
	@Autowired
	private AnimalValidator animalValidator;
	
	/* SERVICES */
	
	@Autowired
	private AnimalRN animalRN;
	
	@Autowired
	private ClienteRN clienteRN;

	@Autowired
	private AnimalEspecieRN animalEspecieRN;
	
	@Autowired
	private AnimalRacaRN animalRacaRN;
	
	@Autowired
	private AnimalPelagemRN animalPelagemRN;

	@Autowired
	private AnimalReport animalReport;
	
	/* BINDER */
	
	@InitBinder
    protected void initBinder(WebDataBinder binder) {
		binder.setValidator(animalValidator);
    }
	
	/* ROUTES */
	
	@GetMapping
	@Cacheable(value="animal")
	public ModelAndView index() {
		ModelAndView mv = new ModelAndView("pet/animal/index");
		mv.addObject("animalDS", this.animalRN.Listar());
		return mv;
	}
	
	@GetMapping(value= {"/cadastro", "/cadastro/{cliente_id}"})
	public ModelAndView cadastro(Animal animal, @PathVariable("cliente_id") Optional<Long> cliente_id) {
		
		ModelAndView mv = new ModelAndView("pet/animal/cadastro");
		
		/* THIS */
		mv.addObject("animal", animal);
		
		/* DS */
		mv.addObject("clienteDS", this.clienteRN.Listar());
		if(cliente_id != null && cliente_id.isPresent())
			animal.setCliente(clienteRN.Carregar(cliente_id.get()));
		
		mv.addObject("animalSexoDS", AnimalSexo.values());
		
		mv.addObject("animalEspecieDS", this.animalEspecieRN.Listar());
		
		mv.addObject("animalRacaDS", this.animalRacaRN.Listar());
		
		mv.addObject("animalPorteDS", AnimalPorte.values());
		
		mv.addObject("animalPelagemDS", this.animalPelagemRN.Listar());
		
		/* PHOTO */
		
		/* VIEW */
		return mv;
	}
	
	@GetMapping("/altera/{id}")
	public ModelAndView altera(@PathVariable("id") Long id) {

		Animal animal = this.animalRN.Carregar(id);
		if(animal != null)
			return cadastro(animal, null);
		else
			return new ModelAndView("redirect:/pet/animal");
	}
	
	@GetMapping("/exclui/{id}")
	@CacheEvict(value="animal", allEntries=true)
	public ModelAndView exclui(@PathVariable("id") Long id, RedirectAttributes redirectAttributes) {

		Animal animal = this.animalRN.Carregar(id);
		if(animal != null)
			this.animalRN.Excluir(animal);	

		redirectAttributes.addFlashAttribute("success", "Animal excluído com sucesso!");
		
		return new ModelAndView("redirect:/pet/animal");
	}
	
	@GetMapping("/visualiza/{id}")
	public ModelAndView visualiza(@PathVariable("id") Long id, RedirectAttributes redirectAttributes) {
		
		/* LOAD */
		Animal animal = this.animalRN.Carregar(id);
		if(animal == null)
			return new ModelAndView("redirect:/pet/animal");
		
		/* VIEW */
		ModelAndView mv = new ModelAndView("pet/animal/visualiza");
		
		/* DS */
		mv.addObject("animal", animal);
		
		/* PET */


		/* RETURN */
		return mv;
	}
	
	@PostMapping("/salvar")
	@CacheEvict(value="animal", allEntries=true)
	public ModelAndView salvar(@ModelAttribute @Valid Animal animal, BindingResult result, @RequestParam("file") MultipartFile[] files, RedirectAttributes redirectAttributes, HttpServletRequest request){
		
		if(result.hasErrors())
			return cadastro(animal, null);
		
		//data_nascimento
		if(animal.getData_nascimento() != null) {
			Date fmtDate = null;
			DateFormat df = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy", Locale.US);
			try {
				fmtDate = (Date) df.parse(animal.getData_nascimento().toString());
			} catch (ParseException e) {
				logger.info("Não foi possivel formatar a data: " + e.getMessage());
			}
			finally {
				animal.setData_nascimento(fmtDate);
			}
		}
		
		/* SAVE */
		this.animalRN.Salvar(animal);
		
		/* UPLOAD */
		for(MultipartFile file : files) {
			if(file.isEmpty()) continue;
			try {
				
				FileUploadImg upload = new FileUploadImg(file);
				upload.setFolder(this.context.getRealPath("/uploads/animal"));
				upload.setFilename(animal.getId().toString());
				
				upload.Resize(640, 480);
				upload.setPreserveAspectRatio(true);
				upload.setFormat("jpg");
				
				upload.Thumb(54, 54);
				
				upload.setFtpSync(true);
				upload.setFtpDir(SisflowConfiguration.getValue("client.storage.directory") + "/animal");
				
				upload.Start();
								
			} catch (IOException e) {
				System.out.println("Não foi possivel realizar o upload do arquivo: " + e.getMessage());
			}
		}
		
		redirectAttributes.addFlashAttribute("success", "Registro salvo com sucesso!");
		
		return new ModelAndView("redirect:/pet/animal");
	}
	
	@GetMapping("/ficha/{id}")
	public void ficha(@PathVariable("id") Long id, HttpServletResponse response) throws Exception {
		
		Animal animal = this.animalRN.Carregar(id);
		if(animal != null) {
			
			/*
			 * Params
			 */
			Map<String, Object> params = new HashMap<String, Object>();
			
			/*
			 * Negocio
			 */
			String negocio = SisflowConfiguration.getValue("client.label");
			String negocio_documento = "";
			if(animal.getCliente().getNegocio().getTipo() == NegocioTipo.PF) {
				PessoaFisica pf = (PessoaFisica)animal.getCliente().getNegocio().getPessoa();
				negocio = pf.getNome() + " " + pf.getSobrenome();
				negocio_documento = "CPF: " + pf.getCpf();
			}
			else if(animal.getCliente().getNegocio().getTipo() == NegocioTipo.PJ) {
				PessoaJuridica pj = (PessoaJuridica)animal.getCliente().getNegocio().getPessoa();
				negocio = pj.getNome_fantasia();
				negocio_documento = "CNPJ: " + pj.getCnpj();
			}
			params.put("negocio", negocio);
			params.put("negocio_documento", negocio_documento);

			/*
			 * Cliente
			 */
			String cliente = SisflowConfiguration.getValue("client.label");
			if(animal.getCliente().getTipo() == ClienteTipo.PF) {
				PessoaFisica pf = (PessoaFisica)animal.getCliente().getPessoa();
				cliente = pf.getNome() + " " + pf.getSobrenome();
			}
			else if(animal.getCliente().getTipo() == ClienteTipo.PJ) {
				PessoaJuridica pj = (PessoaJuridica)animal.getCliente().getPessoa();
				cliente = pj.getNome_fantasia();
			}
			params.put("cliente", cliente);
			
			/*
			 * Cliente Endereço
			 */
			String cliente_endereco = "";
			for(ClienteEndereco clienteEndereco : animal.getCliente().getEnderecos()) {
				cliente_endereco = clienteEndereco.getLogradouro() + ", " + clienteEndereco.getNumero() + " - " + clienteEndereco.getComplemento() + " - " + clienteEndereco.getBairro() + ", " + clienteEndereco.getCidade() + " - " + clienteEndereco.getUf() + "\n";
			}
			params.put("cliente_endereco", cliente_endereco);
			
			/*
			 * Jasper
			 */
			JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(this.animalReport.Ficha(animal));

			String jrxmlFile = this.context.getRealPath("/resources/report/animal/ficha.jasper");
			InputStream is = new FileInputStream(new File(jrxmlFile));

			//JasperReport jasperReport = JasperCompileManager.compileReport(is);
			JasperReport jasperReport = (JasperReport) JRLoader.loadObject(is);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, ds);

			response.setContentType("application/pdf");
			response.setHeader("Content-disposition", "inline; filename=animal_ficha_" + animal.getId().toString() + ".pdf");
			
			final OutputStream outputStream = response.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);
			
			response.flushBuffer();
		}
	}
}