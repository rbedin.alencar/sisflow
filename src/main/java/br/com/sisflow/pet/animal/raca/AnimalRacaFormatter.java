package br.com.sisflow.pet.animal.raca;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

@Component
public class AnimalRacaFormatter implements Formatter<AnimalRaca> {
	
	@Autowired
	private AnimalRacaRN animalRacaRN;
	
	@Override
	public String print(AnimalRaca object, Locale locale) {
		return object.getId().toString();
	}

	@Override
	public AnimalRaca parse(String text, Locale locale) throws ParseException {
		return this.animalRacaRN.Carregar(Long.parseLong(text));
	}
}
