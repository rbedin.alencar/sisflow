package br.com.sisflow.pet.animal.pelagem;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.sisflow.pet.animal.Animal;
import br.com.sisflow.pet.animal.AnimalRN;

@Service
@Transactional
public class AnimalPelagemRN{
	
	@Autowired
	private AnimalPelagemDAO dao = null;	
	
	@Autowired
	private AnimalRN animalRN;
	
	public void Salvar(AnimalPelagem animalPelagem) {
		Long id = animalPelagem.getId();
		if(id == null)
			this.dao.Salvar(animalPelagem);
		else
			this.dao.Atualizar(animalPelagem);
	}
	
	public void Excluir(AnimalPelagem animalPelagem) throws Exception {
		Long id = animalPelagem.getId();
		if(id == 1)
			throw new Exception("Não é possivel excluir o registro padrão do sistema.");
		else {						
			List<Animal> animais = this.animalRN.Listar(animalPelagem);
			if(animais.size() > 0) {
				AnimalPelagem pelagemDefault = this.Carregar(Long.parseLong("1"));
				for(Animal a : animais) {
					a.setPelagem(pelagemDefault);
					this.animalRN.Salvar(a);
				}
			}
			this.dao.Excluir(animalPelagem);
		}
	}
	
	public AnimalPelagem Carregar(Long animal_pelagem_id) {
		return this.dao.Carregar(animal_pelagem_id);
	}
	
	public List<AnimalPelagem> Listar(){
		return this.dao.Listar();
	}
}