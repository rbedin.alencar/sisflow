package br.com.sisflow.pet.animal.pelagem;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@Transactional
@RequestMapping("/pet/animal/pelagem")
public class AnimalPelagemController{
	
	/* LOGGER */
	
	protected static Logger logger = Logger.getLogger(AnimalPelagemController.class);
	
	/* VALIDATOR */
	
	@Autowired
	private AnimalPelagemValidator animalPelagemValidator;
	
	/* SERVICES */
	
	@Autowired
	private AnimalPelagemRN animalPelagemRN;

	/* BINDER */
	
	@InitBinder
    protected void initBinder(WebDataBinder binder) {
		binder.setValidator(animalPelagemValidator);
    }
	
	/* ROUTES */
	
	@GetMapping
	@Cacheable(value="animal.pelagem")
	public ModelAndView index() {
		ModelAndView mv = new ModelAndView("pet/animal/pelagem/index");
		mv.addObject("animalPelagemDS", this.animalPelagemRN.Listar());
		return mv;
	}
	
	@GetMapping("/cadastro")
	public ModelAndView cadastro(AnimalPelagem animalPelagem) {
		ModelAndView mv = new ModelAndView("pet/animal/pelagem/cadastro");
		
		/* THIS */
		mv.addObject("animalPelagem", animalPelagem);
		
		return mv;
	}
	
	@GetMapping("/altera/{id}")
	public ModelAndView altera(@PathVariable("id") Long id) {

		AnimalPelagem animalPelagem = this.animalPelagemRN.Carregar(id);
		if(animalPelagem != null)
			return cadastro(animalPelagem);
		else
			return new ModelAndView("redirect:/pet/animal/pelagem");
	}
	
	@GetMapping("/exclui/{id}")
	@CacheEvict(value= {"animal.pelagem", "animal"}, allEntries=true)
	public ModelAndView exclui(@PathVariable("id") Long id, RedirectAttributes redirectAttributes){

		AnimalPelagem animalPelagem = this.animalPelagemRN.Carregar(id);
		if(animalPelagem != null) {
			
			try {
				this.animalPelagemRN.Excluir(animalPelagem);	
				redirectAttributes.addFlashAttribute("msg", "Pelagem excluída com sucesso!");
			}
			catch (Exception e) {
				redirectAttributes.addFlashAttribute("msg", e.getMessage());
			}
		}
		
		return new ModelAndView("redirect:/pet/animal/pelagem");
	}
	
	@PostMapping("/salvar")
	@CacheEvict(value="animal.pelagem", allEntries=true)
	public ModelAndView salvar(@ModelAttribute @Valid AnimalPelagem animalPelagem, BindingResult result, RedirectAttributes redirectAttributes, HttpServletRequest request){
		
		if(result.hasErrors())
			return cadastro(animalPelagem);
		
		/* SAVE */
		this.animalPelagemRN.Salvar(animalPelagem);
		
		redirectAttributes.addFlashAttribute("msg", "Pelagem salva com sucesso!");
		
		return new ModelAndView("redirect:/pet/animal/pelagem");
	}
}