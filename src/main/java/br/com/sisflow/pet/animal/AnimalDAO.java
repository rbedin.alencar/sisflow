package br.com.sisflow.pet.animal;

import java.util.List;

import javax.transaction.Transactional;

import br.com.sisflow.cliente.Cliente;
import br.com.sisflow.pet.animal.especie.AnimalEspecie;
import br.com.sisflow.pet.animal.pelagem.AnimalPelagem;
import br.com.sisflow.pet.animal.raca.AnimalRaca;

@Transactional
public interface AnimalDAO{
	public void Salvar(Animal animal);
	public void Atualizar(Animal animal);
	public void Excluir(Animal animal);
	public Animal Carregar(Long id);
	
	public List<Animal> Listar();
	public List<Animal> Listar(AnimalEspecie animalEspecie);
	public List<Animal> Listar(AnimalRaca animalRaca);
	public List<Animal> Listar(AnimalPelagem animalPelagem);
	
	public List<Animal> Listar(Cliente cliente);
	
	public Long Total();
}
