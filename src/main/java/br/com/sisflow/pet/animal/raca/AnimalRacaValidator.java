package br.com.sisflow.pet.animal.raca;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class AnimalRacaValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return AnimalRaca.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		
		AnimalRaca animalRaca = (AnimalRaca) target;
		
		if(animalRaca.getEspecie() == null)
			errors.rejectValue("especie", "field.required.pet.animal.raca.especie");
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nome", "field.required");		
	}

}
