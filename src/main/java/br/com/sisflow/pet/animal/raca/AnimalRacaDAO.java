package br.com.sisflow.pet.animal.raca;

import java.util.List;

import javax.transaction.Transactional;

import br.com.sisflow.pet.animal.especie.AnimalEspecie;

@Transactional
public interface AnimalRacaDAO{
	public void Salvar(AnimalRaca animalRaca);
	public void Atualizar(AnimalRaca animalRaca);
	public void Excluir(AnimalRaca animalRaca);
	public AnimalRaca Carregar(Long animal_raca_id);
	public List<AnimalRaca> Listar();
	public List<AnimalRaca> Listar(AnimalEspecie animalEspecie);
}
