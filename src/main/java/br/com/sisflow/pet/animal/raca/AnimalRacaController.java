package br.com.sisflow.pet.animal.raca;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.sisflow.pet.animal.especie.AnimalEspecieRN;

@Controller
@Transactional
@RequestMapping("/pet/animal/raca")
public class AnimalRacaController{
	
	/* LOGGER */
	
	protected static Logger logger = Logger.getLogger(AnimalRacaController.class);
	
	/* VALIDATOR */
	
	@Autowired
	private AnimalRacaValidator animalRacaValidator;
	
	/* SERVICES */
	
	@Autowired
	private AnimalRacaRN animalRacaRN;

	@Autowired
	private AnimalEspecieRN animalEspecieRN;
	
	/* BINDER */
	
	@InitBinder
    protected void initBinder(WebDataBinder binder) {
		binder.setValidator(animalRacaValidator);
    }
	
	/* ROUTES */
	
	@GetMapping
	@Cacheable(value="animal.raca")
	public ModelAndView index() {
		ModelAndView mv = new ModelAndView("pet/animal/raca/index");
		mv.addObject("animalRacaDS", this.animalRacaRN.Listar());
		return mv;
	}
	
	@GetMapping("/cadastro")
	public ModelAndView cadastro(AnimalRaca animalRaca) {
		ModelAndView mv = new ModelAndView("pet/animal/raca/cadastro");
		
		/* THIS */
		mv.addObject("animalRaca", animalRaca);
		
		/* DS */
		mv.addObject("animalEspecieDS", animalEspecieRN.Listar());
		
		return mv;
	}
	
	@GetMapping("/altera/{id}")
	public ModelAndView altera(@PathVariable("id") Long id) {

		AnimalRaca animalRaca = this.animalRacaRN.Carregar(id);
		if(animalRaca != null)
			return cadastro(animalRaca);
		else
			return new ModelAndView("redirect:/pet/animal/raca");
	}
	
	@GetMapping("/exclui/{id}")
	@CacheEvict(value= {"animal.raca", "animal"}, allEntries=true)
	public ModelAndView exclui(@PathVariable("id") Long id, RedirectAttributes redirectAttributes){

		AnimalRaca animalRaca = this.animalRacaRN.Carregar(id);
		if(animalRaca != null) {
			
			try {
				this.animalRacaRN.Excluir(animalRaca);	
				redirectAttributes.addFlashAttribute("msg", "Raça excluída com sucesso!");
			}
			catch (Exception e) {
				redirectAttributes.addFlashAttribute("msg", e.getMessage());
			}
		}
		
		return new ModelAndView("redirect:/pet/animal/raca");
	}
	
	@PostMapping("/salvar")
	@CacheEvict(value="animal.raca", allEntries=true)
	public ModelAndView salvar(@ModelAttribute @Valid AnimalRaca animalRaca, BindingResult result, RedirectAttributes redirectAttributes, HttpServletRequest request){
		
		if(result.hasErrors())
			return cadastro(animalRaca);
		
		/* SAVE */
		this.animalRacaRN.Salvar(animalRaca);
		
		redirectAttributes.addFlashAttribute("msg", "Raça salva com sucesso!");
		
		return new ModelAndView("redirect:/pet/animal/raca");
	}
}