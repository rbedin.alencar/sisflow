package br.com.sisflow.pet.animal.especie;

import java.util.List;

import javax.transaction.Transactional;

@Transactional
public interface AnimalEspecieDAO{
	public void Salvar(AnimalEspecie animalEspecie);
	public void Atualizar(AnimalEspecie animalEspecie);
	public void Excluir(AnimalEspecie animalEspecie);
	public AnimalEspecie Carregar(Long animal_especie_id);
	public List<AnimalEspecie> Listar();
}
