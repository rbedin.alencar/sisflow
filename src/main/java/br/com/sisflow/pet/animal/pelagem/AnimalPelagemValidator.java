package br.com.sisflow.pet.animal.pelagem;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class AnimalPelagemValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return AnimalPelagem.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nome", "field.required");		
	}

}
