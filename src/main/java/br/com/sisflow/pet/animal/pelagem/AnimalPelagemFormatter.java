package br.com.sisflow.pet.animal.pelagem;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

@Component
public class AnimalPelagemFormatter implements Formatter<AnimalPelagem> {
	
	@Autowired
	private AnimalPelagemRN animalPelagemRN;
	
	@Override
	public String print(AnimalPelagem object, Locale locale) {
		return object.getId().toString();
	}

	@Override
	public AnimalPelagem parse(String text, Locale locale) throws ParseException {
		return this.animalPelagemRN.Carregar(Long.parseLong(text));
	}
}
