package br.com.sisflow.oferta;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/oferta")
public class OfertaControllerAPI{
	
	protected static Logger logger = Logger.getLogger(OfertaControllerAPI.class);
	
	@Autowired
	private OfertaRN ofertaRN;

	@GetMapping("/listar")
	public List<Oferta> Listar(){
		return this.ofertaRN.Listar();
	}
	
	@GetMapping("/listar/{nome}")
	public List<Oferta> ListarPorNome(@PathVariable String nome){
		return this.ofertaRN.ListarPorNome(nome);
	}
}
