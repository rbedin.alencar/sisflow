package br.com.sisflow.oferta;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import br.com.sisflow.oferta.status.OfertaStatus;

@Repository
public class OfertaDAOImpl implements OfertaDAO {

	@PersistenceContext
	private EntityManager manager;

	public void Salvar(Oferta oferta) {
		manager.persist(oferta);
	}

	public void Atualizar(Oferta oferta) {
		this.manager.merge(oferta);
	}

	public void Excluir(Oferta oferta) {
		this.manager.remove(oferta);
	}

	public Oferta Carregar(Long id) {
		return this.manager.find(Oferta.class, id);
	}

	public List<Oferta> Listar() {		
		return this.manager.createQuery("select o from Oferta o", Oferta.class).getResultList();		
	}

	@Override
	public Oferta CarregarPorReferencia(String referencia) {
		try {
			return this.manager.createQuery("select o from Oferta o where o.referencia=:referencia", Oferta.class)
					.setParameter("referencia", referencia)
					.getSingleResult();
		}
		catch (NoResultException e) {
			return null;
		}
	}
	
	public List<Oferta> ListarPorNome(String nome) {		
		return this.manager.createQuery("select o from Oferta o where o.status=:status and o.nome like :nome", Oferta.class)
				.setParameter("status", OfertaStatus.ATIVO)
				.setParameter("nome", "%" + nome.toLowerCase() + "%")
				.getResultList();		
	}
}