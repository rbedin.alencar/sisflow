package br.com.sisflow.oferta.produto;

import java.util.List;

import javax.transaction.Transactional;

@Transactional
public interface OfertaProdutoDAO{
	public void Salvar(OfertaProduto ofertaProduto);
	public void Atualizar(OfertaProduto ofertaProduto);
	public void Excluir(OfertaProduto ofertaProduto);
	public OfertaProduto Carregar(Long id);
	public List<OfertaProduto> Listar();
	
	public OfertaProduto CarregarPorReferencia(String referencia);
}
