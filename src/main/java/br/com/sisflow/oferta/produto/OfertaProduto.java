package br.com.sisflow.oferta.produto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import br.com.sisflow.oferta.Oferta;

@Entity
@Table(name="oferta_produto")
@PrimaryKeyJoinColumn(name="oferta_id")
public class OfertaProduto extends Oferta implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3989058169210690347L;
	
	@Column(length=45)
	private String codigo_barra;
	
	private Integer estoque_min;
	
	private Integer estoque_max;
	
	private boolean controle_validade = false;

	public String getCodigo_barra() {
		return codigo_barra;
	}

	public void setCodigo_barra(String codigo_barra) {
		this.codigo_barra = codigo_barra;
	}

	public Integer getEstoque_min() {
		return estoque_min;
	}

	public void setEstoque_min(Integer estoque_min) {
		this.estoque_min = estoque_min;
	}

	public Integer getEstoque_max() {
		return estoque_max;
	}

	public void setEstoque_max(Integer estoque_max) {
		this.estoque_max = estoque_max;
	}

	public boolean isControle_validade() {
		return controle_validade;
	}

	public void setControle_validade(boolean controle_validade) {
		this.controle_validade = controle_validade;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((codigo_barra == null) ? 0 : codigo_barra.hashCode());
		result = prime * result + (controle_validade ? 1231 : 1237);
		result = prime * result + ((estoque_max == null) ? 0 : estoque_max.hashCode());
		result = prime * result + ((estoque_min == null) ? 0 : estoque_min.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		OfertaProduto other = (OfertaProduto) obj;
		if (codigo_barra == null) {
			if (other.codigo_barra != null)
				return false;
		} else if (!codigo_barra.equals(other.codigo_barra))
			return false;
		if (controle_validade != other.controle_validade)
			return false;
		if (estoque_max == null) {
			if (other.estoque_max != null)
				return false;
		} else if (!estoque_max.equals(other.estoque_max))
			return false;
		if (estoque_min == null) {
			if (other.estoque_min != null)
				return false;
		} else if (!estoque_min.equals(other.estoque_min))
			return false;
		return true;
	}
}