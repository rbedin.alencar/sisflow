package br.com.sisflow.oferta.produto;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class OfertaProdutoRN{
	
	@Autowired
	private OfertaProdutoDAO dao;
	
	public void Salvar(OfertaProduto ofertaProduto) {
		
		Long id = ofertaProduto.getId();
		if(id == null) {
			ofertaProduto.setCriado_em(new Date(System.currentTimeMillis()));
			this.dao.Salvar(ofertaProduto);
		}
		else {
			ofertaProduto.setAtualizado_em(new Date(System.currentTimeMillis()));
			this.dao.Atualizar(ofertaProduto);
		}
	}
	
	public void Excluir(OfertaProduto ofertaProduto) {
		this.dao.Excluir(ofertaProduto);
	}
	
	public OfertaProduto Carregar(Long id) {
		return this.dao.Carregar(id);
	}
	
	public List<OfertaProduto> Listar(){
		return this.dao.Listar();
	}
	
	public OfertaProduto CarregarPorReferencia(String referencia) {
		return this.dao.CarregarPorReferencia(referencia);
	}
}