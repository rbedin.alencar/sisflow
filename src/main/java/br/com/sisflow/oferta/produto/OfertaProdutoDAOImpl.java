package br.com.sisflow.oferta.produto;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

@Repository
public class OfertaProdutoDAOImpl implements OfertaProdutoDAO {

	@PersistenceContext
	private EntityManager manager;

	public void Salvar(OfertaProduto ofertaProduto) {
		manager.persist(ofertaProduto);
	}

	public void Atualizar(OfertaProduto ofertaProduto) {
		this.manager.merge(ofertaProduto);
	}

	public void Excluir(OfertaProduto ofertaProduto) {
		this.manager.remove(ofertaProduto);
	}

	public OfertaProduto Carregar(Long id) {
		return this.manager.find(OfertaProduto.class, id);
	}

	public List<OfertaProduto> Listar() {		
		return this.manager.createQuery("select op from OfertaProduto op", OfertaProduto.class).getResultList();		
	}

	@Override
	public OfertaProduto CarregarPorReferencia(String referencia) {
		try {
			return this.manager.createQuery("select op from OfertaProduto op where op.referencia=:referencia", OfertaProduto.class)
					.setParameter("referencia", referencia)
					.getSingleResult();
		}
		catch (NoResultException e) {
			return null;
		}
	}
}