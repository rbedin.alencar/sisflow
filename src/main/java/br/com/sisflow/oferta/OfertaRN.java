package br.com.sisflow.oferta;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class OfertaRN{
	
	@Autowired
	private OfertaDAO dao;
	
	public void Salvar(Oferta oferta) {
		
		Long id = oferta.getId();
		if(id == null) {
			oferta.setCriado_em(new Date(System.currentTimeMillis()));
			this.dao.Salvar(oferta);
		}
		else {
			oferta.setAtualizado_em(new Date(System.currentTimeMillis()));
			this.dao.Atualizar(oferta);
		}
	}
	
	public void Excluir(Oferta oferta) {
		this.dao.Excluir(oferta);
	}
	
	public Oferta Carregar(Long id) {
		return this.dao.Carregar(id);
	}
	
	public List<Oferta> Listar(){
		return this.dao.Listar();
	}
	
	public Oferta CarregarPorReferencia(String referencia) {
		return this.dao.CarregarPorReferencia(referencia);
	}
	
	public List<Oferta> ListarPorNome(String nome){
		return this.dao.ListarPorNome(nome);
	}
}