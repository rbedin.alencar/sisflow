package br.com.sisflow.oferta;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.sisflow.core.negocio.NegocioRN;
import br.com.sisflow.oferta.hospedagem.OfertaHospedagem;
import br.com.sisflow.oferta.hospedagem.OfertaHospedagemRN;
import br.com.sisflow.oferta.produto.OfertaProduto;
import br.com.sisflow.oferta.produto.OfertaProdutoRN;
import br.com.sisflow.oferta.servico.OfertaServico;
import br.com.sisflow.oferta.servico.OfertaServicoRN;
import br.com.sisflow.oferta.status.OfertaStatus;
import br.com.sisflow.oferta.tipo.OfertaTipo;
import br.com.sisflow.venda.oferta.VendaOferta;
import br.com.sisflow.venda.oferta.VendaOfertaRN;

@Controller
@Transactional
@RequestMapping("/oferta")
public class OfertaController{
	
	/* LOGGER */
	
	protected static Logger logger = Logger.getLogger(OfertaController.class);
	
	/* VALIDATOR */
	
	@Autowired
	private OfertaValidator ofertaValidator;
	
	/* SERVICES */
	
	@Autowired
	private OfertaRN ofertaRN;
	
	@Autowired
	private OfertaProdutoRN ofertaProdutoRN;
	
	@Autowired
	private OfertaServicoRN ofertaServicoRN;
	
	@Autowired
	private OfertaHospedagemRN ofertaHospedagemRN;
	
	@Autowired
	private NegocioRN negocioRN;
	
	@Autowired
	private VendaOfertaRN vendaOfertaRN;

	/* BINDER */
	
	@InitBinder
    protected void initBinder(WebDataBinder binder) {
		binder.setValidator(this.ofertaValidator);
    }
	
	@InitBinder
	public void initBigDecimalBinder(WebDataBinder binder) throws Exception{
		DecimalFormat df = new DecimalFormat();
		DecimalFormatSymbols dfs = new DecimalFormatSymbols();
		dfs.setGroupingSeparator('.');
		dfs.setDecimalSeparator(',');
		df.setDecimalFormatSymbols(dfs);
		binder.registerCustomEditor(BigDecimal.class, new CustomNumberEditor(BigDecimal.class, df, true));
	}
	
	/* ROUTES */
	
	@GetMapping
	@Cacheable(value="oferta")
	public ModelAndView index() {
		ModelAndView mv = new ModelAndView("oferta/index");
		mv.addObject("ofertaDS", this.ofertaRN.Listar());
		return mv;
	}
	
	@GetMapping("/cadastro")
	public ModelAndView cadastro(Oferta oferta) {
		
		ModelAndView mv = new ModelAndView("oferta/cadastro");
		
		/* THIS */
		mv.addObject("oferta", oferta);
		
		/* DS */		
		mv.addObject("ofertaStatusDS", OfertaStatus.values());
		mv.addObject("ofertaTipoDS", OfertaTipo.values());
		mv.addObject("negocioDS", this.negocioRN.Listar());
		
		/* PHOTO */
		
		/* VIEW */
		return mv;
	}
	
	@GetMapping("/altera/{id}")
	public ModelAndView altera(@PathVariable("id") Long id) {

		Oferta oferta = this.ofertaRN.Carregar(id);
		if(oferta != null) {
			switch (oferta.getTipo()) {
				
			case PRODUTO:
				return cadastro(this.ofertaProdutoRN.Carregar(id));
					
			case SERVICO:
				return cadastro(this.ofertaServicoRN.Carregar(id));
				
			case HOSPEDAGEM:
				return cadastro(this.ofertaHospedagemRN.Carregar(id));
			}
			return null;
		}
		else
			return new ModelAndView("redirect:/oferta");
	}
	
	@GetMapping("/exclui/{id}")
	@CacheEvict(value="oferta", allEntries=true)
	public ModelAndView exclui(@PathVariable("id") Long id, RedirectAttributes redirectAttributes) {

		Oferta oferta = this.ofertaRN.Carregar(id);
		if(oferta != null) {
			List<VendaOferta> vendaOfertas = this.vendaOfertaRN.Listar(oferta);			
			if(vendaOfertas.isEmpty()) {
				this.ofertaRN.Excluir(oferta);
				redirectAttributes.addFlashAttribute("msg", "Registro excluido com sucesso!");
			}
			else {
				redirectAttributes.addFlashAttribute("msg", "Não é possivel remover o registro, ele pode estar atrelado a outro cadastro!");
			}
		}
		return new ModelAndView("redirect:/oferta");
	}
	
	@PostMapping("/salvar")
	@CacheEvict(value="oferta", allEntries=true)
	public ModelAndView salvar(@ModelAttribute @Valid Oferta oferta, BindingResult result, RedirectAttributes redirectAttributes, HttpServletRequest request){
		
		if(result.hasErrors())
			return cadastro(oferta);
		
		/* SAVE */
		switch (oferta.getTipo()) {
		
			case PRODUTO:
				
				OfertaProduto op = new OfertaProduto();
				
				op.setId(oferta.getId());
				op.setAtualizado_em(oferta.getAtualizado_em());
				op.setCriado_em(oferta.getCriado_em());
				op.setNome(oferta.getNome());
				op.setDescricao(oferta.getDescricao());
				op.setValor(oferta.getValor());
				op.setNegocio(oferta.getNegocio());
				op.setTipo(oferta.getTipo());
				op.setReferencia(oferta.getReferencia());
				op.setStatus(oferta.getStatus());
				
				op.setCodigo_barra(request.getParameter("codigo_barra"));
				
				//controle validade
				boolean controle_validade = false;
				try {
					controle_validade = Boolean.parseBoolean(request.getParameter("controle_validade"));
				}
				catch (Exception e) {
					System.out.println("Não foi possivel formatar o valor booleano: " + e.getMessage());
				}
				finally {
					op.setControle_validade(controle_validade);
				}

				//estoque min
				Integer estoque_min = 0;
				try {
					estoque_min = Integer.parseInt(request.getParameter("estoque_min"));
				}
				catch (NumberFormatException e) {
					System.out.println("Não foi possivel formatar o número: " + e.getMessage());
				}
				finally {
					op.setEstoque_min(estoque_min);
				}
				
				//estoque max
				Integer estoque_max = 0;
				try {
					estoque_max = Integer.parseInt(request.getParameter("estoque_max"));
				}
				catch (NumberFormatException e) {
					System.out.println("Não foi possivel formatar o número: " + e.getMessage());
				}
				finally {
					op.setEstoque_max(estoque_max);
				}

				this.ofertaProdutoRN.Salvar(op);
				
				break;
	
			case SERVICO:
				
				OfertaServico os = new OfertaServico();
				
				os.setId(oferta.getId());
				os.setAtualizado_em(oferta.getAtualizado_em());
				os.setCriado_em(oferta.getCriado_em());
				os.setNome(oferta.getNome());
				os.setDescricao(oferta.getDescricao());
				os.setValor(oferta.getValor());
				os.setNegocio(oferta.getNegocio());
				os.setTipo(oferta.getTipo());
				os.setReferencia(oferta.getReferencia());
				os.setStatus(oferta.getStatus());
				
				this.ofertaServicoRN.Salvar(os);
				
				break;
				
			case HOSPEDAGEM:
				
				OfertaHospedagem oh = new OfertaHospedagem();
				
				oh.setId(oferta.getId());
				oh.setAtualizado_em(oferta.getAtualizado_em());
				oh.setCriado_em(oferta.getCriado_em());
				oh.setNome(oferta.getNome());
				oh.setDescricao(oferta.getDescricao());
				oh.setValor(oferta.getValor());
				oh.setNegocio(oferta.getNegocio());
				oh.setTipo(oferta.getTipo());
				oh.setReferencia(oferta.getReferencia());
				oh.setStatus(oferta.getStatus());
				
				this.ofertaHospedagemRN.Salvar(oh);
		}
	
		redirectAttributes.addFlashAttribute("msg", "Registro salvo com sucesso!");
		
		return new ModelAndView("redirect:/oferta");
	}
}