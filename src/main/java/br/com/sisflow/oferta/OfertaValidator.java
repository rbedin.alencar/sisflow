package br.com.sisflow.oferta;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class OfertaValidator implements Validator {

	@Autowired
	private OfertaRN ofertaRN;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return Oferta.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {	
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "referencia", "field.required.oferta.referencia");	
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nome", "field.required.oferta.nome");	
		
		Oferta oferta 			= (Oferta) target;
		Oferta ofertaExistente	= this.ofertaRN.CarregarPorReferencia(oferta.getReferencia());
		
		if(ofertaExistente != null && (oferta.getId() == null || ofertaExistente.getId() != oferta.getId()))
			errors.rejectValue("referencia", "field.exists.oferta.referencia");
	}
}
