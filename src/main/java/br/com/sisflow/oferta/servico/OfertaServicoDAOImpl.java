package br.com.sisflow.oferta.servico;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

@Repository
public class OfertaServicoDAOImpl implements OfertaServicoDAO {

	@PersistenceContext
	private EntityManager manager;

	public void Salvar(OfertaServico ofertaServico) {
		manager.persist(ofertaServico);
	}

	public void Atualizar(OfertaServico ofertaServico) {
		this.manager.merge(ofertaServico);
	}

	public void Excluir(OfertaServico ofertaServico) {
		this.manager.remove(ofertaServico);
	}

	public OfertaServico Carregar(Long id) {
		return this.manager.find(OfertaServico.class, id);
	}

	public List<OfertaServico> Listar() {		
		return this.manager.createQuery("select os from OfertaServico os", OfertaServico.class).getResultList();		
	}

	@Override
	public OfertaServico CarregarPorReferencia(String referencia) {
		try {
			return this.manager.createQuery("select os from OfertaServico os where os.referencia=:referencia", OfertaServico.class)
					.setParameter("referencia", referencia)
					.getSingleResult();
		}
		catch (NoResultException e) {
			return null;
		}
	}
}