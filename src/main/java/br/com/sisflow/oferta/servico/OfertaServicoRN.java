package br.com.sisflow.oferta.servico;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class OfertaServicoRN{
	
	@Autowired
	private OfertaServicoDAO dao;
	
	public void Salvar(OfertaServico ofertaServico) {
		
		Long id = ofertaServico.getId();
		if(id == null) {
			ofertaServico.setCriado_em(new Date(System.currentTimeMillis()));
			this.dao.Salvar(ofertaServico);
		}
		else {
			ofertaServico.setAtualizado_em(new Date(System.currentTimeMillis()));
			this.dao.Atualizar(ofertaServico);
		}
	}
	
	public void Excluir(OfertaServico ofertaServico) {
		this.dao.Excluir(ofertaServico);
	}
	
	public OfertaServico Carregar(Long id) {
		return this.dao.Carregar(id);
	}
	
	public List<OfertaServico> Listar(){
		return this.dao.Listar();
	}
	
	public OfertaServico CarregarPorReferencia(String referencia) {
		return this.dao.CarregarPorReferencia(referencia);
	}
}