package br.com.sisflow.oferta.servico;

import java.util.List;

import javax.transaction.Transactional;

@Transactional
public interface OfertaServicoDAO{
	public void Salvar(OfertaServico ofertaServico);
	public void Atualizar(OfertaServico ofertaServico);
	public void Excluir(OfertaServico ofertaServico);
	public OfertaServico Carregar(Long id);
	public List<OfertaServico> Listar();
	
	public OfertaServico CarregarPorReferencia(String referencia);
}
