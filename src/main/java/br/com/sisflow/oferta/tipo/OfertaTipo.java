package br.com.sisflow.oferta.tipo;

public enum OfertaTipo{
	
	PRODUTO("Produto"), 
	SERVICO("Serviço"), 
	HOSPEDAGEM("Hospedagem");
	
	private String descricao;
	
	OfertaTipo(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
}
