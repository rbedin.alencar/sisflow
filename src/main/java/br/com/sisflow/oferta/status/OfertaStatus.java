package br.com.sisflow.oferta.status;

public enum OfertaStatus{
	
	ATIVO("Ativo"), 
	INATIVO("Inativo");
	
	private String descricao;
	
	OfertaStatus(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
}
