package br.com.sisflow.oferta;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;

import br.com.sisflow.core.negocio.Negocio;
import br.com.sisflow.oferta.produto.OfertaProduto;
import br.com.sisflow.oferta.servico.OfertaServico;
import br.com.sisflow.oferta.status.OfertaStatus;
import br.com.sisflow.oferta.tipo.OfertaTipo;

@Entity
@Table(name="oferta")
@Inheritance(strategy=InheritanceType.JOINED)
public class Oferta implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6427915894641623262L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="oferta_id")
	private Long id;
	
	@Column(length=45)
	private String referencia;
	
	@ManyToOne
	@JoinColumn(name="negocio_id", nullable=false)
	private Negocio negocio;
	
	@Enumerated(EnumType.STRING)
	@Column(length=10, nullable=false)
	private OfertaTipo tipo;
	
	@Transient
	private OfertaProduto produto;
	
	@Transient
	private OfertaServico servico;
	
	@Column(length=128)
	private String nome;
	
	@Column(nullable=true, columnDefinition="TEXT")
	private String descricao;
	
	@Column(precision=10, scale=2)
	private BigDecimal valor = new BigDecimal(0);
	
	@Enumerated(EnumType.STRING)
	@Column(length=10, nullable=false)
	private OfertaStatus status;
	
	@Column(columnDefinition="DATETIME", updatable=false)
	@Temporal(TemporalType.TIMESTAMP)
	@CreationTimestamp
	private Date criado_em;
	
	@Column(columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date atualizado_em;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public Negocio getNegocio() {
		return negocio;
	}

	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}

	public OfertaTipo getTipo() {
		return tipo;
	}

	public void setTipo(OfertaTipo tipo) {
		this.tipo = tipo;
	}

	public OfertaProduto getProduto() {
		return produto;
	}

	public void setProduto(OfertaProduto produto) {
		this.produto = produto;
	}

	public OfertaServico getServico() {
		return servico;
	}

	public void setServico(OfertaServico servico) {
		this.servico = servico;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public OfertaStatus getStatus() {
		return status;
	}

	public void setStatus(OfertaStatus status) {
		this.status = status;
	}

	public Date getCriado_em() {
		return criado_em;
	}

	public void setCriado_em(Date criado_em) {
		this.criado_em = criado_em;
	}

	public Date getAtualizado_em() {
		return atualizado_em;
	}

	public void setAtualizado_em(Date atualizado_em) {
		this.atualizado_em = atualizado_em;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((atualizado_em == null) ? 0 : atualizado_em.hashCode());
		result = prime * result + ((criado_em == null) ? 0 : criado_em.hashCode());
		result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((negocio == null) ? 0 : negocio.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((produto == null) ? 0 : produto.hashCode());
		result = prime * result + ((referencia == null) ? 0 : referencia.hashCode());
		result = prime * result + ((servico == null) ? 0 : servico.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((tipo == null) ? 0 : tipo.hashCode());
		result = prime * result + ((valor == null) ? 0 : valor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Oferta other = (Oferta) obj;
		if (atualizado_em == null) {
			if (other.atualizado_em != null)
				return false;
		} else if (!atualizado_em.equals(other.atualizado_em))
			return false;
		if (criado_em == null) {
			if (other.criado_em != null)
				return false;
		} else if (!criado_em.equals(other.criado_em))
			return false;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (negocio == null) {
			if (other.negocio != null)
				return false;
		} else if (!negocio.equals(other.negocio))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (produto == null) {
			if (other.produto != null)
				return false;
		} else if (!produto.equals(other.produto))
			return false;
		if (referencia == null) {
			if (other.referencia != null)
				return false;
		} else if (!referencia.equals(other.referencia))
			return false;
		if (servico == null) {
			if (other.servico != null)
				return false;
		} else if (!servico.equals(other.servico))
			return false;
		if (status != other.status)
			return false;
		if (tipo != other.tipo)
			return false;
		if (valor == null) {
			if (other.valor != null)
				return false;
		} else if (!valor.equals(other.valor))
			return false;
		return true;
	}
}