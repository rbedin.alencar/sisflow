package br.com.sisflow.oferta.hospedagem;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import br.com.sisflow.oferta.Oferta;

@Entity
@Table(name="oferta_hospedagem")
@PrimaryKeyJoinColumn(name="oferta_id")
public class OfertaHospedagem extends Oferta implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3046798400421999525L;

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}