package br.com.sisflow.oferta.hospedagem;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

@Repository
public class OfertaHospedagemDAOImpl implements OfertaHospedagemDAO {

	@PersistenceContext
	private EntityManager manager;

	public void Salvar(OfertaHospedagem ofertaHospedagem) {
		manager.persist(ofertaHospedagem);
	}

	public void Atualizar(OfertaHospedagem ofertaHospedagem) {
		this.manager.merge(ofertaHospedagem);
	}

	public void Excluir(OfertaHospedagem ofertaHospedagem) {
		this.manager.remove(ofertaHospedagem);
	}

	public OfertaHospedagem Carregar(Long id) {
		return this.manager.find(OfertaHospedagem.class, id);
	}

	public List<OfertaHospedagem> Listar() {		
		return this.manager.createQuery("select oh from OfertaHospedagem oh", OfertaHospedagem.class).getResultList();		
	}

	@Override
	public OfertaHospedagem Carregar(String referencia) {
		try {
			return this.manager.createQuery("select oh from OfertaHospedagem oh where oh.referencia=:referencia", OfertaHospedagem.class)
					.setParameter("referencia", referencia)
					.getSingleResult();
		}
		catch (NoResultException e) {
			return null;
		}
	}
}