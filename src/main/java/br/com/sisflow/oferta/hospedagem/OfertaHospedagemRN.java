package br.com.sisflow.oferta.hospedagem;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class OfertaHospedagemRN{
	
	@Autowired
	private OfertaHospedagemDAO dao;
	
	public void Salvar(OfertaHospedagem ofertaHospedagem) {
		
		Long id = ofertaHospedagem.getId();
		if(id == null) {
			ofertaHospedagem.setCriado_em(new Date(System.currentTimeMillis()));
			this.dao.Salvar(ofertaHospedagem);
		}
		else {
			ofertaHospedagem.setAtualizado_em(new Date(System.currentTimeMillis()));
			this.dao.Atualizar(ofertaHospedagem);
		}
	}
	
	public void Excluir(OfertaHospedagem ofertaHospedagem) {
		this.dao.Excluir(ofertaHospedagem);
	}
	
	public OfertaHospedagem Carregar(Long id) {
		return this.dao.Carregar(id);
	}
	
	public List<OfertaHospedagem> Listar(){
		return this.dao.Listar();
	}
	
	public OfertaHospedagem Carregar(String referencia) {
		return this.dao.Carregar(referencia);
	}
}