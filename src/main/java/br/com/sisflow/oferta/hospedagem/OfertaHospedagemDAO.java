package br.com.sisflow.oferta.hospedagem;

import java.util.List;

import javax.transaction.Transactional;

@Transactional
public interface OfertaHospedagemDAO{
	public void Salvar(OfertaHospedagem ofertaHospedagem);
	public void Atualizar(OfertaHospedagem ofertaHospedagem);
	public void Excluir(OfertaHospedagem ofertaHospedagem);
	
	public OfertaHospedagem Carregar(Long id);
	public OfertaHospedagem Carregar(String referencia);
	
	public List<OfertaHospedagem> Listar();
}
