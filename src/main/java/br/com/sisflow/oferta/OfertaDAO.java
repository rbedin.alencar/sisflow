package br.com.sisflow.oferta;

import java.util.List;

import javax.transaction.Transactional;

@Transactional
public interface OfertaDAO{
	public void Salvar(Oferta oferta);
	public void Atualizar(Oferta oferta);
	public void Excluir(Oferta oferta);
	public Oferta Carregar(Long id);
	public List<Oferta> Listar();
	
	public Oferta CarregarPorReferencia(String referencia);
	public List<Oferta> ListarPorNome(String nome);
}
