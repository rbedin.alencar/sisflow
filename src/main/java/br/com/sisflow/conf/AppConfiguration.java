package br.com.sisflow.conf;

import java.io.IOException;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.format.FormatterRegistry;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver;

import br.com.sisflow.cliente.ClienteFormatter;
import br.com.sisflow.cliente.origem.ClienteOrigemFormatter;
import br.com.sisflow.core.negocio.NegocioFormatter;
import br.com.sisflow.core.regra.RegraFormatter;
import br.com.sisflow.core.usuario.UsuarioFormatter;
import br.com.sisflow.pet.animal.AnimalFormatter;
import br.com.sisflow.pet.animal.especie.AnimalEspecieFormatter;
import br.com.sisflow.pet.animal.pelagem.AnimalPelagemFormatter;
import br.com.sisflow.pet.animal.raca.AnimalRacaFormatter;

@Configuration
@EnableWebMvc
@EnableCaching
@ComponentScan(basePackages="br.com.sisflow")
public class AppConfiguration implements WebMvcConfigurer{
	
	/* FORMATTERS */
	
	@Autowired
	private NegocioFormatter negocioFormatter;
	
	@Autowired
	private UsuarioFormatter usuarioFormatter;
	
	@Autowired
	private RegraFormatter regraFormatter;
	
	@Autowired
	private ClienteFormatter clienteFormatter;
	
	@Autowired
	private ClienteOrigemFormatter clienteOrigemFormatter;
	
	
	/* FORMATTERS : PET */
	
	@Autowired
	private AnimalFormatter animalFormatter;
	
	@Autowired
	private AnimalEspecieFormatter animalEspecieFormatter;
	
	@Autowired
	private AnimalRacaFormatter animalRacaFormatter;
	
	@Autowired
	private AnimalPelagemFormatter animalPelagemFormatter;
	
	/* CONFIG */
	
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		configurer.enable();
	}
	
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new LocaleChangeInterceptor());
	}

	@Override
	public void addFormatters(FormatterRegistry registry) {
		/* CORE */
		registry.addFormatter(this.negocioFormatter);
		registry.addFormatter(this.usuarioFormatter);
		registry.addFormatter(this.regraFormatter);
		registry.addFormatter(this.clienteFormatter);
		registry.addFormatter(this.clienteOrigemFormatter);
		/* PET */
		registry.addFormatter(this.animalFormatter);
		registry.addFormatter(this.animalEspecieFormatter);
		registry.addFormatter(this.animalRacaFormatter);
		registry.addFormatter(this.animalPelagemFormatter);
	}
	
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
	}
		
	@Bean
	public InternalResourceViewResolver internalResourceViewResolver() {
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/views/");
		resolver.setSuffix(".jsp");
		return resolver;
	}
	
	@Bean
	public CacheManager cacheManager() {
		return new ConcurrentMapCacheManager();
	}
	
	@Bean
	public MultipartResolver multipartResolver() {
		return new StandardServletMultipartResolver();
	}
	
	@Bean(name="messageSource")
	public MessageSource loadBundle() {
		ReloadableResourceBundleMessageSource bundle = new ReloadableResourceBundleMessageSource();
		bundle.setBasename("/WEB-INF/messages");
		//bundle.setDefaultEncoding("UTF-8");
		bundle.setCacheSeconds(1);
		return bundle;
	}
	
	/*
	@Bean
	public MailSender mailSender() throws IOException {
	
		JavaMailSenderImpl impl = new JavaMailSenderImpl();
		impl.setHost(SisflowProperties.getValue("mail.host"));
		impl.setPort(Integer.parseInt(SisflowProperties.getValue("mail.port")));
		impl.setUsername(SisflowProperties.getValue("mail.username"));
		impl.setPassword(SisflowProperties.getValue("mail.password"));
		
		Properties mailProperties = new Properties();
		mailProperties.put("mail.smtp.auth", SisflowProperties.getValue("mail.smtp.auth"));
		mailProperties.put("mail.smtp.starttls.enable", SisflowProperties.getValue("mail.smtp.starttls.enable"));
		mailProperties.put("mail.smtp.ssl.enable", SisflowProperties.getValue("mail.smtp.ssl.enable"));
		impl.setJavaMailProperties(mailProperties);
		
		return impl;	
	}
	*/
	
	@Bean
	public JavaMailSender mailSender() {
		
		JavaMailSenderImpl impl = new JavaMailSenderImpl();
		try {
			impl.setHost(SisflowConfiguration.getValue("mail.host"));
			impl.setPort(Integer.parseInt(SisflowConfiguration.getValue("mail.port")));
			impl.setUsername(SisflowConfiguration.getValue("mail.username"));
			impl.setPassword(SisflowConfiguration.getValue("mail.password"));
			impl.setProtocol("smtp");	
		} catch (IOException e) {
			System.out.println("Não foi possivel obter a configuração de email do arquivo de propriedades: ["+e.getMessage()+"]");
		}		
		
		Properties mailProperties = new Properties();
		mailProperties.put("mail.smtp.auth", "true");
		mailProperties.put("mail.smtp.starttls.enable", "true");
		mailProperties.put("mail.smtp.ssl.enable", "true");
		impl.setJavaMailProperties(mailProperties);
		
		return impl;
	}
	
	@Bean
	public SpringTemplateEngine springTemplateEngine() {
		SpringTemplateEngine springTemplateEngine = new SpringTemplateEngine();
		springTemplateEngine.addTemplateResolver(htmlTemplateResolver());
		return springTemplateEngine;
	}
	
	@Bean
	public SpringResourceTemplateResolver htmlTemplateResolver() {
		SpringResourceTemplateResolver emailTemplateResolver = new SpringResourceTemplateResolver();
		emailTemplateResolver.setPrefix("classpath:/templates/");
		emailTemplateResolver.setSuffix(".html");
		//emailTemplateResolver.setTemplateMode(TemplateMode.HTML5);
		return emailTemplateResolver;
	}
	
	
}
