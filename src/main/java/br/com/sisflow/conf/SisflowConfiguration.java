package br.com.sisflow.conf;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class SisflowConfiguration {
	
	private static Properties properties = loadProperties();

	private static Properties loadProperties(){
		
		Properties properties = new Properties();		
		InputStream is = null;

		try {
			String filename = "sisflow.properties";
			
			is = SisflowConfiguration.class.getClassLoader().getResourceAsStream(filename);
			
			if(is == null)
				System.out.println("Não foi possivel encontrar o arquivo: [" + filename + "]");

			properties.load(is);
			
			return properties;
		}
		catch (IOException e) {
			System.out.println("Não foi carregar o arquivo sisflow de propriedades: [" + e.getMessage() + "]");
		}
		finally {
			if(is != null)
				try {
					is.close();
				}
				catch (IOException e2) {
					System.out.println("Não foi fechar o arquivo sisflow de propriedades: [" + e2.getMessage() + "]");
				}
		}
		
		return null;	
	}
	
	public static String getValue(String key) throws IOException{
		return (String) properties.getProperty(key);
	}
}
