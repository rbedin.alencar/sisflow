package br.com.sisflow.conf;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

@Configuration
public class SecurityFilterConfiguration extends AbstractSecurityWebApplicationInitializer {

}
