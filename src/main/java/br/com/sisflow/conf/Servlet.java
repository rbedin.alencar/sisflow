package br.com.sisflow.conf;

import java.io.File;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletRegistration.Dynamic;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class Servlet extends AbstractAnnotationConfigDispatcherServletInitializer{
	
	/* CONFIG */
	private int maxUploadSizeInMb = 5 * 1024 * 1024; // 5 MB
	
	@Override
	protected Class<?>[] getRootConfigClasses() {
		
		return new Class[] {
				AppConfiguration.class, 
				JPAConfiguration.class,
				SecurityConfiguration.class
		};
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class[] {};
	}

	@Override
	protected String[] getServletMappings() {
		return new String[] {
				"/"
		};
	}
	
	@Override
	protected void customizeRegistration(Dynamic registration) {
		
		File uploadDirectory = new File(System.getProperty("java.io.tmpdir"));
		
		MultipartConfigElement configElement = new MultipartConfigElement(
				uploadDirectory.getAbsolutePath(), 
				this.maxUploadSizeInMb, 
				this.maxUploadSizeInMb * 2, 
				this.maxUploadSizeInMb / 2);
		
		registration.setMultipartConfig(configElement);
		
		//registration.setMultipartConfig(new MultipartConfigElement("", 5242880, 20971520, 0));
	}	
}
