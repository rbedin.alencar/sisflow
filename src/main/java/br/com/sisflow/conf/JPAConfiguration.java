package br.com.sisflow.conf;

import java.io.IOException;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
public class JPAConfiguration{
	
	@Bean
	public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(emf);
		return transactionManager;
	}
	
	@Bean
	public PersistenceExceptionTranslationPostProcessor exceptionTranslation(){
		return new PersistenceExceptionTranslationPostProcessor();
	}
	
	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean() {
		
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(dataSource());
		em.setPackagesToScan(new String[] {
				"br.com.sisflow"
		});
		
		JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendorAdapter);
		em.setJpaProperties(additionalProperties());
		
		return em;
	}
	
	@Bean
	public DataSource dataSource() {		
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		try {
			dataSource.setUrl("jdbc:mysql://"+SisflowConfiguration.getValue("database.host")+":"+SisflowConfiguration.getValue("database.port")+"/"+SisflowConfiguration.getValue("database.name")+"?useSSL=false");
			dataSource.setUsername(SisflowConfiguration.getValue("database.username"));
			dataSource.setPassword(SisflowConfiguration.getValue("database.password"));
		} catch (IOException e) {
			System.out.println("Não foi possivel configurar a conexão com a base de dados: ["+e.getMessage()+"]");
		}
		return dataSource;
	}

	public Properties additionalProperties() {
				
		Properties properties = new Properties();
		
		properties.setProperty("hibernate.hbm2ddl.auto", "update");
		properties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
		properties.setProperty("current_session_context_class", "thread");
		properties.setProperty("hibernate.enable_lazy_load_no_trans", "true");
		properties.setProperty("hibernate.event.merge.entity_copy_observer", "allow");
		
		//sql
		properties.setProperty("hibernate.show_sql", "true");
		properties.setProperty("format_sql", "true");		
		
		//c3p0
		properties.setProperty("hibernate.c3p0.acquire_increment", "5");
		properties.setProperty("hibernate.c3p0.idle_test_period", "3000");
		properties.setProperty("hibernate.c3p0.max_size", "20");
		properties.setProperty("hibernate.c3p0.max_statements", "50");
		properties.setProperty("hibernate.c3p0.min_size", "10");
		properties.setProperty("hibernate.c3p0.timeout", "1800");

		return properties;
	}
}
