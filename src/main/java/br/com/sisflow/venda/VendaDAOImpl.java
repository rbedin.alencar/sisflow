package br.com.sisflow.venda;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import br.com.sisflow.cliente.Cliente;
import br.com.sisflow.oferta.Oferta;

@Repository
public class VendaDAOImpl implements VendaDAO {

	@PersistenceContext
	private EntityManager manager;

	public void Salvar(Venda venda) {
		manager.persist(venda);
	}

	public void Atualizar(Venda venda) {
		this.manager.merge(venda);
	}

	public void Excluir(Venda venda) {
		this.manager.remove(venda);
	}
	
	public void Excluir(Cliente cliente) {
		this.manager.createQuery("delete from Venda v where v.cliente.id = :cliente_id")
			.setParameter("cliente_id", cliente.getId())
			.executeUpdate();
	}

	public Venda Carregar(Long id) {
		return this.manager.find(Venda.class, id);
	}

	public List<Venda> Listar() {		
		return this.manager.createQuery("select v from Venda v", Venda.class).getResultList();		
	}

	@Override
	public List<Venda> Listar(Cliente cliente) {
		return this.manager.createQuery("select v from Venda v where v.cliente.id = :cliente_id", Venda.class)
				.setParameter("cliente_id", cliente.getId())
				.getResultList();		
	}
	
	@Override
	public Long Total() {
		return (Long) this.manager.createQuery("select count(v) from Venda v").getSingleResult();
	}
}