package br.com.sisflow.venda.oferta;

import java.util.List;

import javax.transaction.Transactional;

import br.com.sisflow.oferta.Oferta;

@Transactional
public interface VendaOfertaDAO{
	public void Salvar(VendaOferta vendaOferta);
	public void Atualizar(VendaOferta vendaOferta);
	public void Excluir(VendaOferta vendaOferta);
	public VendaOferta Carregar(Long id);
	public List<VendaOferta> Listar();
	public List<VendaOferta> Listar(Oferta oferta);
}