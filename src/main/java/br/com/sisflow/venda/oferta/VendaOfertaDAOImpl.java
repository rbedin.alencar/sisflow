package br.com.sisflow.venda.oferta;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import br.com.sisflow.oferta.Oferta;

@Repository
public class VendaOfertaDAOImpl implements VendaOfertaDAO {

	@PersistenceContext
	private EntityManager manager;

	public void Salvar(VendaOferta vendaOferta) {
		manager.persist(vendaOferta);
	}

	public void Atualizar(VendaOferta vendaOferta) {
		this.manager.merge(vendaOferta);
	}

	public void Excluir(VendaOferta vendaOferta) {
		this.manager.remove(vendaOferta);
	}

	public VendaOferta Carregar(Long id) {
		return this.manager.find(VendaOferta.class, id);
	}

	public List<VendaOferta> Listar() {		
		return this.manager.createQuery("select vo from VendaOferta vo", VendaOferta.class).getResultList();		
	}

	@Override
	public List<VendaOferta> Listar(Oferta oferta) {
		return this.manager.createQuery("select vo from VendaOferta vo where vo.oferta.id = :oferta_id", VendaOferta.class)
				.setParameter("oferta_id", oferta.getId())
				.getResultList();
	}
}