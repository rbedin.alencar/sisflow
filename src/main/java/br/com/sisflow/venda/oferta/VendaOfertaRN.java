package br.com.sisflow.venda.oferta;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.sisflow.oferta.Oferta;

@Service
@Transactional
public class VendaOfertaRN{
	
	@Autowired
	private VendaOfertaDAO dao;
	
	public void Salvar(VendaOferta vendaOferta) {
		
		Long id = vendaOferta.getId();
		if(id == null)
			this.dao.Salvar(vendaOferta);
		else
			this.dao.Atualizar(vendaOferta);
	}
	
	public void Excluir(VendaOferta vendaOferta) {
		this.dao.Excluir(vendaOferta);
	}
	
	public VendaOferta Carregar(Long id) {
		return this.dao.Carregar(id);
	}
	
	public List<VendaOferta> Listar(){
		return this.dao.Listar();
	}

	public List<VendaOferta> Listar(Oferta oferta) {
		return this.dao.Listar(oferta);
	}
}