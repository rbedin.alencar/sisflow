package br.com.sisflow.venda.pgto;

import java.util.List;

import javax.transaction.Transactional;

import br.com.sisflow.venda.Venda;

@Transactional
public interface VendaPgtoDAO{
	public void Salvar(VendaPgto vendaPgto);
	public void Atualizar(VendaPgto vendaPgto);
	
	public void Excluir(VendaPgto venda);
	public void Excluir(Venda venda);
	
	public VendaPgto Carregar(Long id);

	public List<VendaPgto> Listar();
	public List<VendaPgto> Listar(Venda venda);
}
