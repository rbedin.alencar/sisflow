package br.com.sisflow.venda.pgto;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import br.com.sisflow.venda.Venda;

@Repository
public class VendaPgtoDAOImpl implements VendaPgtoDAO {

	@PersistenceContext
	private EntityManager manager;

	public void Salvar(VendaPgto vendaPgto) {
		manager.persist(vendaPgto);
	}

	public void Atualizar(VendaPgto vendaPgto) {
		this.manager.merge(vendaPgto);
	}

	public void Excluir(VendaPgto vendaPgto) {
		this.manager.remove(vendaPgto);
	}
	
	@Override
	public void Excluir(Venda venda) {
		/*
		this.manager.createQuery("delete from VendaPgto vp where vp.venda.id = :venda_id")
			.setParameter("venda_id", venda.getId())
			.executeUpdate();		
			*/
	}

	public VendaPgto Carregar(Long id) {
		return this.manager.find(VendaPgto.class, id);
	}

	public List<VendaPgto> Listar() {		
		return this.manager.createQuery("select vp from VendaPgto vp", VendaPgto.class).getResultList();		
	}

	@Override
	public List<VendaPgto> Listar(Venda venda) {
		return this.manager.createQuery("select vp from VendaPgto vp where vp.venda.id =:venda_id", VendaPgto.class)
				.setParameter("venda_id", venda.getId())
				.getResultList();
	}
}