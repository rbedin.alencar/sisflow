package br.com.sisflow.venda.pgto;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.sisflow.venda.Venda;

@Service
@Transactional
public class VendaPgtoRN{
	
	@Autowired
	private VendaPgtoDAO dao;
	
	public void Salvar(VendaPgto vendaPgto) {
		
		Long id = vendaPgto.getId();
		if(id == null)
			this.dao.Salvar(vendaPgto);
		else
			this.dao.Atualizar(vendaPgto);
	}
	
	public void Excluir(VendaPgto vendaPgto) {
		this.dao.Excluir(vendaPgto);
	}
	
	public void Excluir(Venda venda) {
		this.dao.Excluir(venda);
	}
	
	public VendaPgto Carregar(Long id) {
		return this.dao.Carregar(id);
	}
	
	public List<VendaPgto> Listar(Venda venda){
		return this.dao.Listar(venda);
	}
}