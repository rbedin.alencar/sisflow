package br.com.sisflow.venda.pgto.tipo;

public enum VendaPgtoTipo {
	
	DINHEIRO("Dinheiro"), 
	DEBITO("Cartão de Débito"), 
	CREDITO("Cartão de Crédito"), 
	CHEQUE("Cheque");
	
	private String descricao;
	
	VendaPgtoTipo(String descricao){
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return this.descricao;
	}
}