package br.com.sisflow.venda;

import java.util.List;

import javax.transaction.Transactional;

import br.com.sisflow.cliente.Cliente;

@Transactional
public interface VendaDAO{
	public void Salvar(Venda venda);
	public void Atualizar(Venda venda);
	
	public void Excluir(Venda venda);
	public void Excluir(Cliente cliente);
	
	public Venda Carregar(Long id);
	public List<Venda> Listar();
	public List<Venda> Listar(Cliente cliente);
	
	public Long Total();
}
