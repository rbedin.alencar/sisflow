package br.com.sisflow.venda;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.sisflow.cliente.Cliente;
import br.com.sisflow.contas.receber.ContaReceber;
import br.com.sisflow.contas.receber.ContaReceberRN;
import br.com.sisflow.contas.receber.tipo.ContaReceberTipo;

@Service
@Transactional
public class VendaRN{
	
	@Autowired
	private VendaDAO dao;
	
	@Autowired
	private ContaReceberRN contaReceberRN;
	
	public void Salvar(Venda venda) {
		
		Long id = venda.getId();
		if(id == null) {
			
			venda.setCriado_em(new Date(System.currentTimeMillis()));
			this.dao.Salvar(venda);
			
			/*
			 * Lança no contas a receber
			 */
			ContaReceber contaReceber = new ContaReceber();
			contaReceber.setDescricao("Venda realizada");
			contaReceber.setValor(venda.getTotal());
			contaReceber.setTipo(ContaReceberTipo.VENDA);
			contaReceber.setRecebido(true);
			contaReceber.setData_recebimento(venda.getData_venda());
			contaReceber.setDocumento("VENDA-"+venda.getId().toString()+"");
			contaReceber.setNegocio(venda.getNegocio());
			this.contaReceberRN.Salvar(contaReceber);
		}
		else {
			venda.setAtualizado_em(new Date(System.currentTimeMillis()));
			this.dao.Atualizar(venda);
		}
	}
	
	public void Excluir(Venda venda) {
		this.dao.Excluir(venda);
	}
	
	public void Excluir(Cliente cliente) {
		this.dao.Excluir(cliente);
	}
	
	public Venda Carregar(Long id) {
		return this.dao.Carregar(id);
	}
	
	public List<Venda> Listar(){
		return this.dao.Listar();
	}
	
	public List<Venda> Listar(Cliente cliente){
		return this.dao.Listar(cliente);
	}
	
	public Long Total(){
		return this.dao.Total();
	}
}