package br.com.sisflow.venda;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import br.com.sisflow.venda.oferta.VendaOferta;
import net.sf.jasperreports.engine.JRException;

@Service("vendaReport")
public class VendaReport{
	
	
	public List<Map<String, Object>> Comprovante(Venda venda) throws JRException{
		List<Map<String, Object>> ds = new ArrayList<Map<String, Object>>();
		for(VendaOferta vendaOferta : venda.getVendaOfertas()) {			
			Map<String, Object> m = new HashMap<String, Object>();
			m.put("oferta_id", vendaOferta.getOferta().getId());
			m.put("referencia", vendaOferta.getOferta().getReferencia());
			m.put("nome", vendaOferta.getOferta().getNome());
			m.put("valor", vendaOferta.getValor());
			m.put("quantidade", vendaOferta.getQuantidade());
			m.put("subtotal", vendaOferta.getSubtotal());
			ds.add(m);
		}		
		return ds;
	}	
}
