package br.com.sisflow.venda;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.sisflow.cliente.ClienteRN;
import br.com.sisflow.cliente.endereco.ClienteEndereco;
import br.com.sisflow.cliente.tipo.ClienteTipo;
import br.com.sisflow.conf.SisflowConfiguration;
import br.com.sisflow.core.negocio.NegocioRN;
import br.com.sisflow.core.negocio.tipo.NegocioTipo;
import br.com.sisflow.core.pessoa.fisica.PessoaFisica;
import br.com.sisflow.core.pessoa.juridica.PessoaJuridica;
import br.com.sisflow.core.usuario.UsuarioRN;
import br.com.sisflow.oferta.Oferta;
import br.com.sisflow.oferta.OfertaRN;
import br.com.sisflow.venda.oferta.VendaOferta;
import br.com.sisflow.venda.oferta.VendaOfertaRN;
import br.com.sisflow.venda.pgto.VendaPgto;
import br.com.sisflow.venda.pgto.VendaPgtoRN;
import br.com.sisflow.venda.pgto.tipo.VendaPgtoTipo;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;

@Controller
@Transactional
@RequestMapping("/venda")
public class VendaController{
	
	/* LOGGER */
	
	protected static Logger logger = Logger.getLogger(VendaController.class);
	
	/* CONTEXT */
	
	@Autowired
	private ServletContext context;
	
	/* VALIDATOR */
	
	@Autowired
	private VendaValidator vendaValidator;
	
	/* SERVICES */
	
	@Autowired
	private VendaRN vendaRN;
	
	@Autowired
	private VendaOfertaRN vendaOfertaRN;
	
	@Autowired
	private VendaPgtoRN vendaPgtoRN;
	
	@Autowired
	private ClienteRN clienteRN;
	
	@Autowired
	private NegocioRN negocioRN;

	@Autowired
	private UsuarioRN usuarioRN;
	
	@Autowired
	private OfertaRN ofertaRN;
	
	@Autowired
	private VendaReport vendaReport;
	
	/* BINDER */
	
	@InitBinder
    protected void initBinder(WebDataBinder binder) {
		binder.setValidator(this.vendaValidator);
    }
	
	@InitBinder
	public void initBigDecimalBinder(WebDataBinder binder) throws Exception{
		DecimalFormat df = new DecimalFormat();
		DecimalFormatSymbols dfs = new DecimalFormatSymbols();
		dfs.setGroupingSeparator('.');
		dfs.setDecimalSeparator(',');
		df.setDecimalFormatSymbols(dfs);
		binder.registerCustomEditor(BigDecimal.class, new CustomNumberEditor(BigDecimal.class, df, true));
	}
	
	/* ROUTES */
	
	@GetMapping
	@Cacheable(value="venda")
	public ModelAndView index() {
		ModelAndView mv = new ModelAndView("venda/index");
		mv.addObject("vendaDS", this.vendaRN.Listar());
		return mv;
	}
	
	@GetMapping({"/cadastro", "/cadastro/{cliente_id}"})
	public ModelAndView cadastro(Venda venda, @PathVariable("cliente_id") Optional<Long> cliente_id) {
		
		ModelAndView mv = new ModelAndView("venda/cadastro");
		
		/* DEFAULT */
		if(venda.getId() == null)
			venda.setData_venda(new Date(System.currentTimeMillis()));
		
		/* THIS */
		mv.addObject("venda", venda);
		
		/* DS */		
		if(cliente_id != null && cliente_id.isPresent())
			venda.setCliente(clienteRN.Carregar(cliente_id.get()));
		
		mv.addObject("negocioDS", this.negocioRN.Listar());
		mv.addObject("clienteDS", this.clienteRN.Listar());
		mv.addObject("usuarioDS", this.usuarioRN.Listar());
		
		
		
		mv.addObject("vendaPgtoTipoDS", VendaPgtoTipo.values());
		
		/* VIEW */
		return mv;
	}
	
	@GetMapping("/altera/{id}")
	public ModelAndView altera(@PathVariable("id") Long id) {

		Venda venda = this.vendaRN.Carregar(id);
		if(venda != null)
			return this.cadastro(venda, null);
		else
			return new ModelAndView("redirect:/venda");
	}
	
	@GetMapping("/exclui/{id}")
	@CacheEvict(value="venda", allEntries=true)
	public ModelAndView exclui(@PathVariable("id") Long id, RedirectAttributes redirectAttributes) {

		Venda venda = this.vendaRN.Carregar(id);
		if(venda != null)
			this.vendaRN.Excluir(venda);

		redirectAttributes.addFlashAttribute("msg", "Registro excluido com sucesso!");
		
		return new ModelAndView("redirect:/venda");
	}
	
	@PostMapping("/salvar")
	@CacheEvict(value="venda", allEntries=true)
	public ModelAndView salvar(@ModelAttribute @Valid Venda venda, BindingResult result, RedirectAttributes redirectAttributes, HttpServletRequest request){
		
		if(result.hasErrors())
			return this.cadastro(venda, null);		
		
		/*
		 * VendaOferta
		 */
		List<VendaOferta> vendaOfertas = venda.getVendaOfertas();
		if(vendaOfertas == null)
			vendaOfertas = new ArrayList<VendaOferta>();
		
		for(VendaOferta vo : vendaOfertas) {
			VendaOferta vo_to_delete = this.vendaOfertaRN.Carregar(vo.getId());
			this.vendaOfertaRN.Excluir(vo_to_delete);
		}
		vendaOfertas.clear();

		String[] oferta_id			= request.getParameterValues("oferta_id[]");
		String[] oferta_quantidade 	= request.getParameterValues("oferta_quantidade[]");
		String[] oferta_valor 		= request.getParameterValues("oferta_valor[]");
		String[] oferta_subtotal 	= request.getParameterValues("oferta_subtotal[]");
		
		BigDecimal total 		= new BigDecimal(0);
		BigDecimal total_pgto	= new BigDecimal(0);
		BigDecimal total_troco 	= new BigDecimal(0);
		
		int num_ofertas = oferta_id.length;
		if(oferta_id != null && num_ofertas > 0) {
			for(int i=0; i<num_ofertas; i++) {
				
				if(oferta_id[i].equals(""))
					continue;
				
				Oferta oferta = this.ofertaRN.Carregar(Long.parseLong(oferta_id[i]));
				if(oferta == null)
					continue;

				VendaOferta vendaOferta = new VendaOferta();
				
				vendaOferta.setVenda(venda);
				vendaOferta.setOferta(oferta);
				
				//quantidade
				Integer quantidade = 0;
				try {
					quantidade = Integer.parseInt(oferta_quantidade[i]);
				}
				catch (NumberFormatException e) {
					System.out.println("Não foi possivel obter o valor numérico da quantidade: ["+e.getMessage()+"]");
				}
				vendaOferta.setQuantidade(quantidade);
				
				//subtotal
				BigDecimal subtotal = new BigDecimal(oferta_subtotal[i].replaceAll("\\.", "").replace(",", "."));
				vendaOferta.setSubtotal(subtotal);
				
				//valor
				BigDecimal valor = new BigDecimal(oferta_valor[i].replaceAll("\\.", "").replace(",", "."));
				vendaOferta.setValor(valor);
				
				//add
				vendaOfertas.add(vendaOferta);
				
				//total
				total = total.add(subtotal);
			}
		}
		venda.setVendaOfertas(vendaOfertas);
		
		/*
		 * VendaPgto
		 */
		List<VendaPgto> vendaPgtos = venda.getVendaPgtos();
		if(vendaPgtos == null)
			vendaPgtos = new ArrayList<VendaPgto>();
		
		for(VendaPgto vp : vendaPgtos) {
			VendaPgto vp_to_delete = this.vendaPgtoRN.Carregar(vp.getId());
			this.vendaPgtoRN.Excluir(vp_to_delete);
		}
		vendaPgtos.clear();
		
		String[] pgto_tipo 			= request.getParameterValues("pgto_tipo[]");
		String[] pgto_valor 		= request.getParameterValues("pgto_valor[]");
		String[] pgto_parcelas 		= request.getParameterValues("pgto_parcelas[]");
		String[] pgto_observacao 	= request.getParameterValues("pgto_observacao[]");
		
		int num_pgtos = pgto_valor.length;
		if(pgto_valor != null && num_pgtos > 0) {
			for(int i=0; i<num_pgtos; i++) {
				
				if(pgto_valor[i].equals(""))
					continue;
				
				VendaPgto vendaPgto = new VendaPgto();
				vendaPgto.setVenda(venda);
				
				/*
				 * tipo
				 */
				vendaPgto.setTipo(VendaPgtoTipo.valueOf(pgto_tipo[i]));
				
				/*
				 * Valor
				 */
				BigDecimal valor = new BigDecimal(pgto_valor[i].replaceAll("\\.", "").replace(",", "."));
				vendaPgto.setValor(valor);
				
				total_pgto = total_pgto.add(valor);
				
				/*
				 * parcelas
				 */
				Integer parcelas = 0;
				try {
					parcelas = Integer.parseInt(pgto_parcelas[i]);
				}
				catch (NumberFormatException e) {
					System.out.println("Não foi possivel obter o valor numérico da parcela: ["+e.getMessage()+"]");
				}
				vendaPgto.setParcelas(parcelas);
				
				/*
				 * observacao
				 */
				vendaPgto.setObservacao(pgto_observacao[i]);
				
				/*
				 * push
				 */
				vendaPgtos.add(vendaPgto);
			}
		}
		venda.setVendaPgtos(vendaPgtos);
		
		/*
		 * Troco
		 */
		total_troco = total.subtract(total_pgto);
		venda.setTroco(total_troco);
		
		/*
		 * Total
		 */
		venda.setTotal(total);
		
		/* SAVE */
		this.vendaRN.Salvar(venda);
	
		redirectAttributes.addFlashAttribute("msg", "Registro salvo com sucesso!");
		
		return new ModelAndView("redirect:/venda");
	}
	
	@GetMapping("/comprovante/{id}")
	public void comprovante(@PathVariable("id") Long id, HttpServletResponse response) throws Exception {
		
		Venda venda = this.vendaRN.Carregar(id);
		if(venda != null) {
			
			/*
			 * Params
			 */
			Map<String, Object> params = new HashMap<String, Object>();
			
			/*
			 * Negocio
			 */
			String negocio = SisflowConfiguration.getValue("client.label");
			String negocio_documento = "";
			if(venda.getNegocio().getTipo() == NegocioTipo.PF) {
				PessoaFisica pf = (PessoaFisica)venda.getNegocio().getPessoa();
				negocio = pf.getNome() + " " + pf.getSobrenome();
				negocio_documento = "CPF: " + pf.getCpf();
			}
			else if(venda.getNegocio().getTipo() == NegocioTipo.PJ) {
				PessoaJuridica pj = (PessoaJuridica)venda.getNegocio().getPessoa();
				negocio = pj.getNome_fantasia();
				negocio_documento = "CNPJ: " + pj.getCnpj();
			}
			params.put("negocio", negocio);
			params.put("negocio_documento", negocio_documento);
			
			/*
			 * Cliente
			 */
			String cliente = SisflowConfiguration.getValue("client.label");
			if(venda.getCliente().getTipo() == ClienteTipo.PF) {
				PessoaFisica pf = (PessoaFisica)venda.getCliente().getPessoa();
				cliente = pf.getNome() + " " + pf.getSobrenome();
			}
			else if(venda.getCliente().getTipo() == ClienteTipo.PJ) {
				PessoaJuridica pj = (PessoaJuridica)venda.getCliente().getPessoa();
				cliente = pj.getNome_fantasia();
			}
			params.put("cliente", cliente);
			
			/*
			 * Cliente Endereço
			 */
			String cliente_endereco = "";
			for(ClienteEndereco clienteEndereco : venda.getCliente().getEnderecos()) {
				cliente_endereco = clienteEndereco.getLogradouro() + ", " + clienteEndereco.getNumero();
				if(clienteEndereco.getComplemento().length() > 0)
					cliente_endereco = cliente_endereco + " - " + clienteEndereco.getComplemento();
				cliente_endereco = cliente_endereco + " - " + clienteEndereco.getBairro() + ", " + clienteEndereco.getCidade() + " - " + clienteEndereco.getUf() + "\n";
			}
			params.put("cliente_endereco", cliente_endereco);

			params.put("venda_id", venda.getId());
			params.put("total", venda.getTotal());
			params.put("troco", venda.getTroco());
			params.put("data_venda", venda.getData_venda());
			params.put("responsavel", venda.getUsuario().getNome());
			params.put("observacao", venda.getObservacao());
			
			BigDecimal total_produtos = new BigDecimal(0);
			BigDecimal total_servicos = new BigDecimal(0);
			for(VendaOferta vendaOferta : venda.getVendaOfertas()) {
				switch (vendaOferta.getOferta().getTipo()) {
				case PRODUTO:
					total_produtos = total_produtos.add(vendaOferta.getSubtotal());
					break;
					
				case SERVICO:
					total_servicos = total_servicos.add(vendaOferta.getSubtotal());
					break;
				}
			}
			params.put("total_produtos", total_produtos);
			params.put("total_servicos", total_servicos);
			
			/*
			 * Venda Pgtos.
			 */
			List<Map<String, Object>> venda_pgtos = new ArrayList<Map<String, Object>>();
			for(VendaPgto vendaPgto : venda.getVendaPgtos()) {
				Map<String, Object> m = new HashMap<String, Object>();
				m.put("tipo", vendaPgto.getTipo().name());
				m.put("valor", vendaPgto.getValor());
				venda_pgtos.add(m);
			}
			params.put("venda_pgtos", venda_pgtos);			
			params.put("pathVendaPgtos", this.context.getRealPath("/resources/report/venda/venda_pgtos.jasper"));
			
			/*
			 * Jasper
			 */
			JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(this.vendaReport.Comprovante(venda));

			String jrxmlFile = this.context.getRealPath("/resources/report/venda/comprovante.jasper");
			InputStream is = new FileInputStream(new File(jrxmlFile));

			//JasperReport jasperReport = JasperCompileManager.compileReport(is);
			JasperReport jasperReport = (JasperReport) JRLoader.loadObject(is);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, ds);

			response.setContentType("application/pdf");
			response.setHeader("Content-disposition", "inline; filename=venda_comprovante_" + venda.getId().toString() + ".pdf");
			
			final OutputStream outputStream = response.getOutputStream();
			JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);
			
			response.flushBuffer();
		}
	}
}