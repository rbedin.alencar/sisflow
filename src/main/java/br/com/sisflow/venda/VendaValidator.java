package br.com.sisflow.venda;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class VendaValidator implements Validator {
	
	@Override
	public boolean supports(Class<?> clazz) {
		return Venda.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {		
		
		Venda venda = (Venda) target;
		
		if(venda.getCliente() == null)
			errors.rejectValue("cliente", "field.required.venda.cliente");		
	}
}