package br.com.sisflow.venda;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import br.com.sisflow.cliente.Cliente;
import br.com.sisflow.core.negocio.Negocio;
import br.com.sisflow.core.usuario.Usuario;
import br.com.sisflow.venda.oferta.VendaOferta;
import br.com.sisflow.venda.pgto.VendaPgto;

@Entity
@Table(name="venda")
public class Venda{
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="venda_id")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="negocio_id")
	private Negocio negocio;
	
	@ManyToOne
	@JoinColumn(name="usuario_id")
	private Usuario usuario;

	@Column(columnDefinition="DATETIME", updatable=false)
	@Temporal(TemporalType.TIMESTAMP)
	@CreationTimestamp
	private Date data_venda;
	
	@ManyToOne
	@JoinColumn(name="cliente_id")
	private Cliente cliente;
	
	@Column(nullable=true, columnDefinition="TEXT")
	private String observacao;
	
	@OneToMany(mappedBy="venda", cascade=CascadeType.ALL, orphanRemoval=true)
	private List<VendaOferta> vendaOfertas = new ArrayList<>();
	
	@OneToMany(mappedBy="venda", cascade=CascadeType.ALL, orphanRemoval=true)
	private List<VendaPgto> vendaPgtos = new ArrayList<>();
	
	@Column(precision=10, scale=2)
	private BigDecimal total = new BigDecimal(0);
	
	@Column(precision=10, scale=2)
	private BigDecimal troco = new BigDecimal(0);
	
	@Column(columnDefinition="DATETIME", updatable=false)
	@Temporal(TemporalType.TIMESTAMP)
	@CreationTimestamp
	private Date criado_em;
	
	@Column(columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date atualizado_em;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Negocio getNegocio() {
		return negocio;
	}

	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Date getData_venda() {
		return data_venda;
	}

	public void setData_venda(Date data_venda) {
		this.data_venda = data_venda;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public List<VendaOferta> getVendaOfertas() {
		return vendaOfertas;
	}

	public void setVendaOfertas(List<VendaOferta> vendaOfertas) {
		this.vendaOfertas = vendaOfertas;
	}

	public List<VendaPgto> getVendaPgtos() {
		return vendaPgtos;
	}

	public void setVendaPgtos(List<VendaPgto> vendaPgtos) {
		this.vendaPgtos = vendaPgtos;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public BigDecimal getTroco() {
		return troco;
	}

	public void setTroco(BigDecimal troco) {
		this.troco = troco;
	}

	public Date getCriado_em() {
		return criado_em;
	}

	public void setCriado_em(Date criado_em) {
		this.criado_em = criado_em;
	}

	public Date getAtualizado_em() {
		return atualizado_em;
	}

	public void setAtualizado_em(Date atualizado_em) {
		this.atualizado_em = atualizado_em;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((atualizado_em == null) ? 0 : atualizado_em.hashCode());
		result = prime * result + ((cliente == null) ? 0 : cliente.hashCode());
		result = prime * result + ((criado_em == null) ? 0 : criado_em.hashCode());
		result = prime * result + ((data_venda == null) ? 0 : data_venda.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((negocio == null) ? 0 : negocio.hashCode());
		result = prime * result + ((observacao == null) ? 0 : observacao.hashCode());
		result = prime * result + ((total == null) ? 0 : total.hashCode());
		result = prime * result + ((troco == null) ? 0 : troco.hashCode());
		result = prime * result + ((usuario == null) ? 0 : usuario.hashCode());
		result = prime * result + ((vendaOfertas == null) ? 0 : vendaOfertas.hashCode());
		result = prime * result + ((vendaPgtos == null) ? 0 : vendaPgtos.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Venda other = (Venda) obj;
		if (atualizado_em == null) {
			if (other.atualizado_em != null)
				return false;
		} else if (!atualizado_em.equals(other.atualizado_em))
			return false;
		if (cliente == null) {
			if (other.cliente != null)
				return false;
		} else if (!cliente.equals(other.cliente))
			return false;
		if (criado_em == null) {
			if (other.criado_em != null)
				return false;
		} else if (!criado_em.equals(other.criado_em))
			return false;
		if (data_venda == null) {
			if (other.data_venda != null)
				return false;
		} else if (!data_venda.equals(other.data_venda))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (negocio == null) {
			if (other.negocio != null)
				return false;
		} else if (!negocio.equals(other.negocio))
			return false;
		if (observacao == null) {
			if (other.observacao != null)
				return false;
		} else if (!observacao.equals(other.observacao))
			return false;
		if (total == null) {
			if (other.total != null)
				return false;
		} else if (!total.equals(other.total))
			return false;
		if (troco == null) {
			if (other.troco != null)
				return false;
		} else if (!troco.equals(other.troco))
			return false;
		if (usuario == null) {
			if (other.usuario != null)
				return false;
		} else if (!usuario.equals(other.usuario))
			return false;
		if (vendaOfertas == null) {
			if (other.vendaOfertas != null)
				return false;
		} else if (!vendaOfertas.equals(other.vendaOfertas))
			return false;
		if (vendaPgtos == null) {
			if (other.vendaPgtos != null)
				return false;
		} else if (!vendaPgtos.equals(other.vendaPgtos))
			return false;
		return true;
	}
}