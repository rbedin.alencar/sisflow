package br.com.sisflow.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/auth")
public class AuthController{
	
	protected static Logger logger = Logger.getLogger(AuthController.class);
	
	@GetMapping
	public String index() {
		return "core/auth/index";
	}
}
