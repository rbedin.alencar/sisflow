package br.com.sisflow.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.sisflow.cliente.ClienteRN;
import br.com.sisflow.pet.animal.AnimalRN;
import br.com.sisflow.venda.VendaRN;

@Controller
@RequestMapping("/")
public class HomeController{
	
	protected static Logger logger = Logger.getLogger(HomeController.class);
	
	@Autowired
	private ClienteRN clienteRN;

	@Autowired
	private AnimalRN animalRN;
	
	@Autowired
	private VendaRN vendaRN;
	
	@GetMapping
	public ModelAndView index(){		
		
		ModelAndView mv = new ModelAndView("core/home/index");
		
		mv.addObject("totalCliente", this.clienteRN.Total());
		mv.addObject("totalAnimal", this.animalRN.Total());
		mv.addObject("totalVenda", this.vendaRN.Total());
		
		return mv;
	}
}
