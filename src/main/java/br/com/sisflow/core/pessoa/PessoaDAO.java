package br.com.sisflow.core.pessoa;

import java.util.List;

import javax.transaction.Transactional;

@Transactional
public interface PessoaDAO{
	public void Salvar(Pessoa pessoa);
	public void Atualizar(Pessoa pessoa);
	public void Excluir(Pessoa pessoa);
	public Pessoa Carregar(Long id);
	public List<Pessoa> Listar();
}
