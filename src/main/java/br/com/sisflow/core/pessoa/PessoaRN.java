package br.com.sisflow.core.pessoa;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class PessoaRN{
	
	@Autowired
	private PessoaDAO dao;
	
	public void Salvar(Pessoa pessoa) {
		Long id = pessoa.getId();
		if(id == null)
			this.dao.Salvar(pessoa);
		else
			this.dao.Atualizar(pessoa);
	}
	
	public void Excluir(Pessoa pessoa) {
		this.dao.Excluir(pessoa);
	}
	
	public Pessoa Carregar(Long id) {
		return this.dao.Carregar(id);
	}
	
	public List<Pessoa> Listar(){
		return this.dao.Listar();
	}
}
