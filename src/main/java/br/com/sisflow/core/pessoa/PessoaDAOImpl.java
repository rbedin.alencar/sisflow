package br.com.sisflow.core.pessoa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

@Repository
public class PessoaDAOImpl implements PessoaDAO{
	
	@PersistenceContext
	private EntityManager manager;

	public void Salvar(Pessoa pessoa) {
		manager.persist(pessoa);
	}

	public void Atualizar(Pessoa pessoa) {
		this.manager.merge(pessoa);
	}

	public void Excluir(Pessoa pessoa) {
		this.manager.remove(pessoa);
	}

	public Pessoa Carregar(Long id) {
		return this.manager.find(Pessoa.class, id);
	}

	public List<Pessoa> Listar() {		
		return this.manager.createQuery("select p from Pessoa p", Pessoa.class).getResultList();		
	}
}
