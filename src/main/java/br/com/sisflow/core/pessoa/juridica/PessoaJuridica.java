package br.com.sisflow.core.pessoa.juridica;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import br.com.sisflow.core.pessoa.Pessoa;

@Entity
@Table(name="pessoa_juridica")
@PrimaryKeyJoinColumn(name="pessoa_id")
public class PessoaJuridica extends Pessoa implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1623062498721336683L;

	@Column(length=14, nullable=false)
	private String cnpj;
	
	@Column(length=64, nullable=false)
	private String razao_social;
	
	@Column(length=64, nullable=true)
	private String nome_fantasia;
	
	@Column(length=9, nullable=true)
	private String inscricao_estadual;
	
	@Column(length=11, nullable=true)
	private String inscricao_municipal;
	
	@Column(length=9, nullable=true)
	private String suframa;

	@Column(nullable=true)
	private boolean contribuinte_icms = false;

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getRazao_social() {
		return razao_social;
	}

	public void setRazao_social(String razao_social) {
		this.razao_social = razao_social;
	}

	public String getNome_fantasia() {
		return nome_fantasia;
	}

	public void setNome_fantasia(String nome_fantasia) {
		this.nome_fantasia = nome_fantasia;
	}

	public String getInscricao_estadual() {
		return inscricao_estadual;
	}

	public void setInscricao_estadual(String inscricao_estadual) {
		this.inscricao_estadual = inscricao_estadual;
	}

	public String getInscricao_municipal() {
		return inscricao_municipal;
	}

	public void setInscricao_municipal(String inscricao_municipal) {
		this.inscricao_municipal = inscricao_municipal;
	}

	public String getSuframa() {
		return suframa;
	}

	public void setSuframa(String suframa) {
		this.suframa = suframa;
	}

	public boolean isContribuinte_icms() {
		return contribuinte_icms;
	}

	public void setContribuinte_icms(boolean contribuinte_icms) {
		this.contribuinte_icms = contribuinte_icms;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((cnpj == null) ? 0 : cnpj.hashCode());
		result = prime * result + (contribuinte_icms ? 1231 : 1237);
		result = prime * result + ((inscricao_estadual == null) ? 0 : inscricao_estadual.hashCode());
		result = prime * result + ((inscricao_municipal == null) ? 0 : inscricao_municipal.hashCode());
		result = prime * result + ((nome_fantasia == null) ? 0 : nome_fantasia.hashCode());
		result = prime * result + ((razao_social == null) ? 0 : razao_social.hashCode());
		result = prime * result + ((suframa == null) ? 0 : suframa.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PessoaJuridica other = (PessoaJuridica) obj;
		if (cnpj == null) {
			if (other.cnpj != null)
				return false;
		} else if (!cnpj.equals(other.cnpj))
			return false;
		if (contribuinte_icms != other.contribuinte_icms)
			return false;
		if (inscricao_estadual == null) {
			if (other.inscricao_estadual != null)
				return false;
		} else if (!inscricao_estadual.equals(other.inscricao_estadual))
			return false;
		if (inscricao_municipal == null) {
			if (other.inscricao_municipal != null)
				return false;
		} else if (!inscricao_municipal.equals(other.inscricao_municipal))
			return false;
		if (nome_fantasia == null) {
			if (other.nome_fantasia != null)
				return false;
		} else if (!nome_fantasia.equals(other.nome_fantasia))
			return false;
		if (razao_social == null) {
			if (other.razao_social != null)
				return false;
		} else if (!razao_social.equals(other.razao_social))
			return false;
		if (suframa == null) {
			if (other.suframa != null)
				return false;
		} else if (!suframa.equals(other.suframa))
			return false;
		return true;
	}
}
