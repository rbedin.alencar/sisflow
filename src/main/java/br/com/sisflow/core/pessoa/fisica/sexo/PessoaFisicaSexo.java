package br.com.sisflow.core.pessoa.fisica.sexo;

public enum PessoaFisicaSexo{
	
	M("Masculino"), 
	F("Feminino");
	
	private String descricao;
	
	private PessoaFisicaSexo(String descricao){
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return this.descricao;
	}
}
