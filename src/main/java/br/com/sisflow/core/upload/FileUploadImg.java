package br.com.sisflow.core.upload;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.springframework.web.multipart.MultipartFile;

public class FileUploadImg extends FileUpload{
	
	private Double newWidth 			= null;
	private Double newHeight 			= null;
	private Double aspectRatio 			= null;
	
	private Graphics2D graphics2d 		= null;
	
	private BufferedImage curImg 		= null;
	private BufferedImage newImgLarge 	= null;
	private BufferedImage newImgSmall 	= null;
	
	private String format				= "jpg";
	
	private boolean resize				= false;
	private Integer resizeWidth			= null;
	private Integer	resizeHeight		= null;
	
	//thumb
	private boolean thumb				= false;
	private Integer thumbWidth			= null;
	private Integer thumbHeight			= null;
	
	private boolean preserveAspectRatio = false;
	
	private File file					= null;
	
	public void Resize(Integer resizeWidth, Integer resizeHeigth){	
		this.resize = true;
		this.resizeWidth = resizeWidth;
		this.resizeHeight = resizeHeigth;
	}
	
	public void Thumb(Integer thumbWidth, Integer thumbHeight){	
		this.thumb = true;
		this.thumbWidth = thumbWidth;
		this.thumbHeight = thumbHeight;
	}
	
	public FileUploadImg(MultipartFile file) {
		super(file);
	}
	
	private void ApplyResize() {
		if(this.isPreserveAspectRatio()) {
			this.newWidth	= (double) this.curImg.getWidth();
			this.newHeight 	= (double) this.curImg.getHeight();
			if(this.newWidth >= this.resizeWidth) {
				this.aspectRatio = (this.newHeight/this.newWidth);
				this.newWidth = (double) this.resizeWidth;
				this.newWidth = (this.newWidth * this.aspectRatio);
				while (this.newHeight > this.resizeHeight) {
					this.newWidth = (double) (--this.resizeWidth);
					this.newHeight = (this.newWidth * this.aspectRatio);
				}
			}
			else if(this.newHeight >= this.resizeHeight) {
				this.aspectRatio = (this.newWidth/this.newHeight);
				this.newHeight = (double) this.resizeHeight;
				while (this.newWidth > this.resizeWidth) {
					this.newHeight = (double) (--this.resizeWidth);
					this.newWidth = (this.newHeight * this.aspectRatio);
				}			
			}
		}
		else {
			this.newWidth 	= (double) this.resizeWidth;
			this.newHeight 	= (double) this.resizeHeight;
		}
	}
	
	public String Start() throws IOException {
		
		String source = super.Start();
		if(source != null) {
			
			if(this.isFtpSync()) {
				
				if(this.fileUploadFTP == null)
					this.fileUploadFTP = new FileUploadFTP();
				
				this.fileUploadFTP.setDirectory(this.ftpDir);
			}

			this.file = new File(source);
			this.curImg = ImageIO.read(this.file);
			
			this.newWidth	= (double) this.curImg.getWidth();
			this.newHeight 	= (double) this.curImg.getHeight();
			
			if(this.resize)
				this.ApplyResize();
			
			this.newImgLarge = new BufferedImage(this.newWidth.intValue(), this.newHeight.intValue(), BufferedImage.TYPE_INT_RGB);
			
			this.graphics2d = this.newImgLarge.createGraphics();
			this.graphics2d.drawImage(this.curImg, 0, 0, this.newWidth.intValue(), this.newHeight.intValue(), null);
			
			ImageIO.write(this.newImgLarge, "jpg", this.file); //large
			
			if(this.isFtpSync())
				this.fileUploadFTP.Add(source, this.filename);
			
			if(this.thumb) {
				
				String sourceThumb = null;
				
				this.resizeWidth 	= this.thumbWidth;
				this.resizeHeight 	= this.thumbHeight;
				
				if(this.resize)
					this.ApplyResize();
				
				this.newImgSmall = new BufferedImage(this.newWidth.intValue(), this.newHeight.intValue(), BufferedImage.TYPE_INT_RGB);
				
				this.graphics2d = this.newImgSmall.createGraphics();
				this.graphics2d.drawImage(this.curImg, 0, 0, this.newWidth.intValue(), this.newHeight.intValue(), null);
				
				sourceThumb = this.folder + "/" + "thumb_" + this.filename;
				ImageIO.write(this.newImgSmall, "jpg", new File(sourceThumb));
				
				if(this.isFtpSync())
					this.fileUploadFTP.Add(sourceThumb, "thumb_" + this.filename);
			}
			
			if(this.isFtpSync())
				this.fileUploadFTP.Send();
		}
		
		return source;
	}
	
	/* GETTERS & SETTERS */
	
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}

	public boolean isPreserveAspectRatio() {
		return preserveAspectRatio;
	}
	public void setPreserveAspectRatio(boolean preserveAspectRatio) {
		this.preserveAspectRatio = preserveAspectRatio;
	}
}
