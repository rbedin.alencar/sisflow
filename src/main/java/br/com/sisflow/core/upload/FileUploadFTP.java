package br.com.sisflow.core.upload;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import br.com.sisflow.conf.SisflowConfiguration;

public class FileUploadFTP{

	private Map<String, String> files = new HashMap<>();
	
	private String directory;
	
	private FTPClient ftpClient = null;
	
	private String host;
	private String username;
	private String password;

	public void Add(String source, String filename) {
		this.files.put(source, filename);
	}
	
	public void setDirectory(String directory) {
		this.directory = directory;
	}
	
	public void Connect() throws IOException {
		
		if(this.ftpClient == null)
			this.ftpClient = new FTPClient();
		
		this.host 		= SisflowConfiguration.getValue("ftp.host");
		this.username	= SisflowConfiguration.getValue("ftp.username");
		this.password	= SisflowConfiguration.getValue("ftp.password");
		
		this.ftpClient.connect(host);
		this.ftpClient.login(this.username, this.password);
		
		this.ftpClient.enterLocalActiveMode();
		this.ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
	}
	
	public void Disconnect() throws IOException {
		if(this.ftpClient != null) {
			this.ftpClient.logout();		
			this.ftpClient.disconnect();
		}
	}
	
	public void Send() throws IOException{
		
		this.Connect();
		
		this.Directory();
		
		FileInputStream is = null;
		for(String source : this.files.keySet()) {
			
			String filename = this.files.get(source);
			try {
				is = new FileInputStream(source);
				this.ftpClient.storeFile(filename, is);
			} catch (FileNotFoundException e1) {
				System.out.println("O arquivo não pode ser encontrado: ["+e1.getMessage()+"]");
			} catch (IOException e2) {
				System.out.println("Não foi possivel gravar o arquivo no servidor: ["+e2.getMessage()+"]");
			}
			
			if(is != null)
				is.close();
		}
		
		this.Disconnect();
	}
	
	public void Delete(String filename) throws IOException {
		this.Connect();
		this.ftpClient.deleteFile(filename);
	}

	private void Directory() throws IOException {
		
		boolean dirExists = true;
		
		String[] directories = this.directory.split("/");
		
		for (String dir : directories ) {
			if (!dir.isEmpty()) {
				if(dirExists) {
					dirExists = this.ftpClient.changeWorkingDirectory(dir);
				}
				if (!dirExists) {
					if (!this.ftpClient.makeDirectory(dir)) {
						throw new IOException("Unable to create remote directory '" + dir + "'.  error='" + this.ftpClient.getReplyString()+"'");
					}
					if (!this.ftpClient.changeWorkingDirectory(dir)) {
						throw new IOException("Unable to change into newly created remote directory '" + dir + "'.  error='" + this.ftpClient.getReplyString()+"'");
					}
				}
			}
		}
	}
}
