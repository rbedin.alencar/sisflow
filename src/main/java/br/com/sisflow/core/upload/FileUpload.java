package br.com.sisflow.core.upload;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.web.multipart.MultipartFile;

public abstract class FileUpload{
	
	protected MultipartFile multipartFile;
	
	protected String folder;
	protected String filename;
	
	protected FileUploadFTP fileUploadFTP;
	protected boolean ftpSync;
	protected String ftpDir;
	
	public FileUpload(MultipartFile multipartFile) {
		this.multipartFile = multipartFile;
	}
	
	public String Start() throws IOException{
		
		try {
			byte[] bytes = this.multipartFile.getBytes();
			
			
			if(this.filename == null || "".equals(this.filename))
				this.filename =  this.multipartFile.getOriginalFilename();
			else {
				int index =  this.multipartFile.getOriginalFilename().lastIndexOf('.');
				if(index >= 0)
					this.filename = this.filename + "." +  this.multipartFile.getOriginalFilename().substring(index+1);
			}
			
			
			File directory = new File(folder);
			if(!directory.exists())
				directory.mkdirs();
			
			Path path = Paths.get(folder + "/" + this.filename);
			
			Files.write(path, bytes);			
			
			return path.toString();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/* GETTERS & SETTERS */
	
	public String getFolder() {
		return folder;
	}

	public void setFolder(String folder) {
		this.folder = folder;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public boolean isFtpSync() {
		return ftpSync;
	}

	public void setFtpSync(boolean ftpSync) {
		this.ftpSync = ftpSync;
	}

	public String getFtpDir() {
		return ftpDir;
	}

	public void setFtpDir(String ftpDir) {
		this.ftpDir = ftpDir;
	}
}
