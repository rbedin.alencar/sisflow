package br.com.sisflow.core.negocio;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

@Repository
public class NegocioDAOImpl implements NegocioDAO{
	
	@PersistenceContext
	private EntityManager manager;

	public void Salvar(Negocio negocio) {
		manager.persist(negocio);
	}

	public void Atualizar(Negocio negocio) {
		this.manager.merge(negocio);
	}

	public void Excluir(Negocio negocio) {
		this.manager.remove(negocio);
	}

	public Negocio Carregar(Long id) {
		return this.manager.find(Negocio.class, id);
	}

	public List<Negocio> Listar() {		
		return this.manager.createQuery("select n from Negocio n", Negocio.class).getResultList();		
	}
}
