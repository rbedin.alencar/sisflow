package br.com.sisflow.core.negocio;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class NegocioRN{
	
	@Autowired
	private NegocioDAO dao;
	
	public void Salvar(Negocio negocio) {
		Long id = negocio.getId();
		if(id == null)
			this.dao.Salvar(negocio);
		else
			this.dao.Atualizar(negocio);
	}
	
	public void Excluir(Negocio negocio) {
		this.dao.Excluir(negocio);
	}
	
	public Negocio Carregar(Long id) {
		return this.dao.Carregar(id);
	}
	
	public List<Negocio> Listar(){
		return this.dao.Listar();
	}
}
