package br.com.sisflow.core.negocio;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

@Component
public class NegocioFormatter implements Formatter<Negocio> {
	
	@Autowired
	private NegocioRN negocioRN;
	
	@Override
	public String print(Negocio object, Locale locale) {
		return object.getId().toString();
	}

	@Override
	public Negocio parse(String text, Locale locale) throws ParseException {
		return this.negocioRN.Carregar(Long.parseLong(text));
	}
}
