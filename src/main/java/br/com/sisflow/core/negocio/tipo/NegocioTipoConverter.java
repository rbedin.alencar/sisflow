package br.com.sisflow.core.negocio.tipo;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class NegocioTipoConverter implements Converter<String, NegocioTipo> {

	public NegocioTipo convert(String source) {		
		return NegocioTipo.valueOf(source);
	}
}
