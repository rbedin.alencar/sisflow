package br.com.sisflow.core.negocio;

import java.util.List;

import javax.transaction.Transactional;

@Transactional
public interface NegocioDAO{
	public void Salvar(Negocio negocio);
	public void Atualizar(Negocio negocio);
	public void Excluir(Negocio negocio);
	public Negocio Carregar(Long id);
	public List<Negocio> Listar();
}
