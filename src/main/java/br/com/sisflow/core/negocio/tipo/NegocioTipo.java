package br.com.sisflow.core.negocio.tipo;

public enum NegocioTipo{
	
	PF("Pessoa Física"), 
	PJ("Pessoa Jurídica");
	
	private String descricao;
	
	NegocioTipo(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
}
