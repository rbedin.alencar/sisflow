package br.com.sisflow.core.regra;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

@Component
public class RegraFormatter implements Formatter<Regra> {
	
	@Autowired
	private RegraRN regraRN;
	
	@Override
	public String print(Regra object, Locale locale) {
		return object.getNome();
	}

	@Override
	public Regra parse(String text, Locale locale) throws ParseException {
		return this.regraRN.Carregar(text);
	}
}
