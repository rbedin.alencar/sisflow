package br.com.sisflow.core.regra;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class RegraRN{
	
	@Autowired
	private RegraDAO dao;
	
	public Regra Carregar(String nome) {
		return this.dao.Carregar(nome);
	}
	
	public List<Regra> Listar(){
		return this.dao.Listar();
	}
}
