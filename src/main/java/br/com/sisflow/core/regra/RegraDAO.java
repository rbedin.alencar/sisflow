package br.com.sisflow.core.regra;

import java.util.List;

import javax.transaction.Transactional;

@Transactional
public interface RegraDAO{
	public Regra Carregar(String nome);
	public List<Regra> Listar();
}
