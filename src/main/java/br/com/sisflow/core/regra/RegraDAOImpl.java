package br.com.sisflow.core.regra;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

@Repository
public class RegraDAOImpl implements RegraDAO {
	
	@PersistenceContext
	private EntityManager manager;

	@Override
	public Regra Carregar(String nome) {
		return this.manager.find(Regra.class, nome);
	}
	
	public List<Regra> Listar() {		
		return this.manager.createQuery("select r from Regra r", Regra.class).getResultList();		
	}
}
