package br.com.sisflow.core.usuario;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.sisflow.core.negocio.Negocio;
import br.com.sisflow.core.negocio.NegocioRN;

@RestController
@RequestMapping("/api/usuario")
public class UsuarioControllerAPI{
	
	protected static Logger logger = Logger.getLogger(UsuarioControllerAPI.class);
	
	@Autowired
	private UsuarioRN usuarioRN;
	
	@Autowired
	private NegocioRN negocioRN;
	
	@GetMapping("/listar/{negocio_id}")
	public List<Usuario> Listar(@PathVariable Long negocio_id){
		Negocio negocio = this.negocioRN.Carregar(negocio_id);		
		if(negocio != null)
			return this.usuarioRN.Listar(negocio);
		return null;
	}
}
