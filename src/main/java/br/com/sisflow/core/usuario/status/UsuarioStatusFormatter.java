package br.com.sisflow.core.usuario.status;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

@Component
public class UsuarioStatusFormatter implements Formatter<UsuarioStatus>{

	public String print(UsuarioStatus object, Locale locale) {
		return object.getDescricao();
	}

	public UsuarioStatus parse(String text, Locale locale) throws ParseException {
		return UsuarioStatus.valueOf(text);
	}	
}
