package br.com.sisflow.core.usuario;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletContext;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.sisflow.conf.SisflowConfiguration;
import br.com.sisflow.core.negocio.NegocioRN;
import br.com.sisflow.core.regra.RegraRN;
import br.com.sisflow.core.upload.FileUploadImg;
import br.com.sisflow.core.usuario.status.UsuarioStatus;

@Controller
@Transactional
@RequestMapping("/usuario")
public class UsuarioController{
	
	/* LOGGER */
	
	protected static Logger logger = Logger.getLogger(UsuarioController.class);
	
	/* CONTEXT */
	
	@Autowired
	private ServletContext context;
	
	/* VALIDATOR */
	
	@Autowired
	private UsuarioValidator usuarioValidator;
	
	/* DS */
	
	@Autowired
	private UsuarioRN usuarioRN;
	
	@Autowired
	private NegocioRN negocioRN;
	
	@Autowired
	private RegraRN regraRN;

	/* BINDER */
	
	@InitBinder
    protected void initBinder(WebDataBinder binder) {
		binder.setValidator(this.usuarioValidator);
    }
	
	/* ROUTES */
	
	@GetMapping
	@Cacheable(value="usuario")
	public ModelAndView index() {
		
		ModelAndView mv = new ModelAndView("core/usuario/index");
		
		mv.addObject("usuarioStatusDS", UsuarioStatus.values());
		mv.addObject("usuarioDS", this.usuarioRN.Listar());
		
		return mv;
	}
	
	@GetMapping("/cadastro")
	public ModelAndView cadastro(Usuario usuario) {
		
		ModelAndView mv = new ModelAndView("core/usuario/cadastro");
		
		/* MAPPERS */
		mv.addObject("negocioDS", this.negocioRN.Listar());
		mv.addObject("usuarioStatusDS", UsuarioStatus.values());	
		mv.addObject("regraDS", this.regraRN.Listar());
		mv.addObject("usuario", usuario);
		
		/* PHOTO */
		if(usuario.getId() != null) {
			String url_photo = this.context.getRealPath("/uploads/usuario/" + usuario.getId().toString() + ".jpg");
			File f = new File(url_photo);
			if(f.exists()) {
				url_photo = this.context.getContextPath() + "/uploads/usuario/" + usuario.getId().toString() + ".jpg";
				mv.addObject("photo", "data-default-file='" + url_photo + "'");
			}
		}
		
		//return
		return mv;
	}
	
	@GetMapping("/altera/{id}")
	public ModelAndView altera(@PathVariable("id") Long id) {

		Usuario usuario = this.usuarioRN.Carregar(id);
		if(usuario != null)
			return cadastro(usuario);
		else
			return new ModelAndView("redirect:/usuario");
	}
	
	@GetMapping("/exclui/{id}")
	@CacheEvict(value="usuario", allEntries=true)
	public ModelAndView exclui(@PathVariable("id") Long id, RedirectAttributes redirectAttributes) {

		Usuario usuario = this.usuarioRN.Carregar(id);
		if(usuario != null)
			this.usuarioRN.Excluir(usuario);	

		redirectAttributes.addFlashAttribute("success", "Usuário excluido com sucesso");
		
		return new ModelAndView("redirect:/usuario");
	}
	
	@PostMapping("/salvar")
	@CacheEvict(value="usuario", allEntries=true)
	public ModelAndView salvar(@ModelAttribute @Valid Usuario usuario, BindingResult result, @RequestParam("file") MultipartFile[] files, RedirectAttributes redirectAttributes){
		
		if(result.hasErrors())
			return cadastro(usuario);
		
		this.usuarioRN.Salvar(usuario);
		
		/* UPLOAD */
		for(MultipartFile file : files) {
			if(file.isEmpty()) continue;
			try {
				
				FileUploadImg upload = new FileUploadImg(file);
				upload.setFolder(this.context.getRealPath("/uploads/usuario"));
				upload.setFilename(usuario.getId().toString());
				
				upload.Resize(640, 480);
				upload.setPreserveAspectRatio(true);
				upload.setFormat("jpg");
				
				upload.Thumb(54, 54);
				
				upload.setFtpSync(true);
				upload.setFtpDir(SisflowConfiguration.getValue("client.storage.directory") + "/usuario");
				
				upload.Start();
								
			} catch (IOException e) {
				System.out.println("Não foi possivel realizar o upload do arquivo: " + e.getMessage());
			}
		}
		
		redirectAttributes.addFlashAttribute("success", "Usuário cadastrado com sucesso");
		
		return new ModelAndView("redirect:/usuario");
	}
	
	@GetMapping("/resetasenha/{id}")
	public ModelAndView resetaSenha(@PathVariable("id") Long id) {
		Usuario usuario = this.usuarioRN.Carregar(id);
		if(usuario != null) {
			this.usuarioRN.ProcessaNovaSenha(usuario);
			this.usuarioRN.Salvar(usuario);
		}
		return new ModelAndView("redirect:/usuario");
	}
}
