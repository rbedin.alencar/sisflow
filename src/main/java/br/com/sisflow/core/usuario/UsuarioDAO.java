package br.com.sisflow.core.usuario;

import java.util.List;

import javax.transaction.Transactional;

import br.com.sisflow.core.negocio.Negocio;

@Transactional
public interface UsuarioDAO{
	public void Salvar(Usuario usuario);
	public void Atualizar(Usuario usuario);
	public void Excluir(Usuario usuario);
	public Usuario Carregar(Long id);

	public List<Usuario> Listar();
	public List<Usuario> Listar(Negocio negocio);
	
	public Usuario ExisteUsuarioLogin(String login);
	public Usuario ExisteUsuarioEmail(String email);
}
