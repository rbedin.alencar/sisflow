package br.com.sisflow.core.usuario;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.hibernate.annotations.NaturalId;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import br.com.sisflow.core.negocio.Negocio;
import br.com.sisflow.core.regra.Regra;
import br.com.sisflow.core.usuario.status.UsuarioStatus;

@Entity
@Table(name="usuario")
public class Usuario implements UserDetails, Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7810955782505203314L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="usuario_id")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="negocio_id")
	private Negocio negocio;
	
	@NotBlank(message="{NotBlank.usuario.nome}")
	@Column(length=20, nullable=false)
	private String nome;
	
	@Column(length=45, nullable=false)
	private String sobrenome;

	@Column(length=128, nullable=false, unique=true)
	private String email;
	
	@Column(length=20, nullable=false)
	@NaturalId(mutable=true)
	private String login;
	
	@Column(length=32, nullable=false)
	private String senha;
	
	@Column(length=32, nullable=true)
	private String token;
	
	@Enumerated(EnumType.STRING)
	@Column(length=16, nullable=false)
	private UsuarioStatus status;
	
	/*
	@ManyToMany(fetch=FetchType.EAGER, targetEntity=Regra.class)
	@JoinTable(name="usuario_regra", joinColumns=@JoinColumn(name="usuario_id"), inverseJoinColumns=@JoinColumn(name="regra"))
	private List<Regra> regras = new ArrayList<Regra>();
	*/
	@ManyToMany(cascade=CascadeType.MERGE, fetch=FetchType.LAZY, targetEntity=Regra.class)
	@JoinTable(name="usuario_regra", joinColumns=@JoinColumn(name="usuario_id"), inverseJoinColumns=@JoinColumn(name="regra"))
	private Set<Regra> regras = new HashSet<>();
	
	private Calendar criado_em;
	private Calendar atualizado_em;
	
	/* SRPING SECURITY */
	
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return this.regras;
	}
	public String getPassword() {
		return this.senha;
	}
	public String getUsername() {
		return this.login;
	}
	public boolean isAccountNonExpired() {
		return true;
	}
	public boolean isAccountNonLocked() {
		return true;
	}
	public boolean isCredentialsNonExpired() {
		return true;
	}
	public boolean isEnabled() {
		return true;
	}
	
	/* GETTERS & SETTERS */
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Negocio getNegocio() {
		return negocio;
	}
	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSobrenome() {
		return sobrenome;
	}
	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public UsuarioStatus getStatus() {
		return status;
	}
	public void setStatus(UsuarioStatus status) {
		this.status = status;
	}
	public Set<Regra> getRegras() {
		return regras;
	}
	public void setRegras(Set<Regra> regras) {
		this.regras = regras;
	}
	public Calendar getCriado_em() {
		return criado_em;
	}
	public void setCriado_em(Calendar criado_em) {
		this.criado_em = criado_em;
	}
	public Calendar getAtualizado_em() {
		return atualizado_em;
	}
	public void setAtualizado_em(Calendar atualizado_em) {
		this.atualizado_em = atualizado_em;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((atualizado_em == null) ? 0 : atualizado_em.hashCode());
		result = prime * result + ((criado_em == null) ? 0 : criado_em.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result + ((negocio == null) ? 0 : negocio.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((regras == null) ? 0 : regras.hashCode());
		result = prime * result + ((senha == null) ? 0 : senha.hashCode());
		result = prime * result + ((sobrenome == null) ? 0 : sobrenome.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((token == null) ? 0 : token.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (atualizado_em == null) {
			if (other.atualizado_em != null)
				return false;
		} else if (!atualizado_em.equals(other.atualizado_em))
			return false;
		if (criado_em == null) {
			if (other.criado_em != null)
				return false;
		} else if (!criado_em.equals(other.criado_em))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		if (negocio == null) {
			if (other.negocio != null)
				return false;
		} else if (!negocio.equals(other.negocio))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (regras == null) {
			if (other.regras != null)
				return false;
		} else if (!regras.equals(other.regras))
			return false;
		if (senha == null) {
			if (other.senha != null)
				return false;
		} else if (!senha.equals(other.senha))
			return false;
		if (sobrenome == null) {
			if (other.sobrenome != null)
				return false;
		} else if (!sobrenome.equals(other.sobrenome))
			return false;
		if (status != other.status)
			return false;
		if (token == null) {
			if (other.token != null)
				return false;
		} else if (!token.equals(other.token))
			return false;
		return true;
	}
}