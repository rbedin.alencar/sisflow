package br.com.sisflow.core.usuario;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Random;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

import br.com.sisflow.conf.SisflowConfiguration;
import br.com.sisflow.core.negocio.Negocio;
import br.com.sisflow.core.upload.FileUploadFTP;

@Service
@Transactional
public class UsuarioRN{
	
	@Autowired
	private UsuarioDAO dao;
	
	@Autowired
	private MailSender mailSender;
	
	/* GERA SENHA */
	private String Gerador(int length) {
		
		char[] ALL_CHARS = new char[62];
		Random random = new Random();
		char[] password = new char[length];

		for(int i = 48, j = 0; i < 123; i++) {
			if(Character.isLetterOrDigit(i)) {
				ALL_CHARS[j] = (char) i;
				j++;
			}
		}
		for(int i = 0; i < length; i++)
			password[i] = ALL_CHARS[random.nextInt(ALL_CHARS.length)];
		
		return new String(password);
	}
	
	private String getMD5(String value) throws NoSuchAlgorithmException {
	    MessageDigest m = MessageDigest.getInstance("MD5");
	    byte[] data = m.digest(value.getBytes());
	    StringBuilder sBuilder = new StringBuilder();

	    for (int i = 0; i < data.length; i++) {
	        for (byte b : data)
	        	if(b == 0x00)
	        		sBuilder.append("00");
	            else if ((b & 0x0F) == b) {
	                sBuilder.append("0");
	                break;
	            } else
	                break;

	        BigInteger bigInt = new BigInteger(1, data);
	        sBuilder.append(bigInt.toString(16));
	    }
	    return sBuilder.toString().substring(0, 32);
	}
	
	public void ProcessaNovaSenha(Usuario usuario) {
		
		String senha = this.Gerador(8);
		try {
			usuario.setSenha(this.getMD5(senha));			
		} catch (NoSuchAlgorithmException e) {
			System.out.println("Não foi possivel criptografar a senha de usuário: ["+e.getMessage()+"]");
		}
		
		SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
		simpleMailMessage.setFrom("app@sisflow.com.br");
		simpleMailMessage.setTo(usuario.getEmail());
		simpleMailMessage.setSubject("Bem vindo");
		simpleMailMessage.setText("Usuário: [" + usuario.getLogin() + "] e Senha: [" + senha + "]");	
		this.mailSender.send(simpleMailMessage);
	}
	
	public void Salvar(Usuario usuario){
		Long id = usuario.getId();
		if(id == null) {						
			this.dao.Salvar(usuario);
			
			if(usuario.getId() != null)
				this.ProcessaNovaSenha(usuario);
		}
		else			
			this.dao.Atualizar(usuario);
	}
	
	public void Excluir(Usuario usuario) {
		
		/*
		 * Conecta ao FTP e exclui as imagens 
		 * ligadas ao registro.
		 */
		String[] files_to_delete = {
				usuario.getId() + ".jpg", 
				"thumb_" + usuario.getId() + ".jpg"
				};

		FileUploadFTP fileUploadFTP = new FileUploadFTP();
		for(String f : files_to_delete) {
			try {
				fileUploadFTP.Delete(SisflowConfiguration.getValue("client.storage.directory") + "/usuario/" + f);			
			} catch (IOException e) {
				System.out.println("Não foi possivel excluir o arquivo ["+f+"] do servidor: " + e.getMessage());
			}	
		}
		
		/*
		 * Exclui o registro.
		 */
		this.dao.Excluir(usuario);
	}
	
	public Usuario Carregar(Long id) {
		return this.dao.Carregar(id);
	}
	
	public List<Usuario> Listar(){
		return this.dao.Listar();
	}
	
	public List<Usuario> Listar(Negocio negocio){
		return this.dao.Listar(negocio);
	}
	
	/* CUSTOM */
	
	public Usuario ExisteUsuarioLogin(String login) {
		return this.dao.ExisteUsuarioLogin(login);
	}
	
	public Usuario ExisteUsuarioEmail(String email) {
		return this.dao.ExisteUsuarioEmail(email);
	}
}
