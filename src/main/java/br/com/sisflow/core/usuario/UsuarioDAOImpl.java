package br.com.sisflow.core.usuario;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

import br.com.sisflow.core.negocio.Negocio;

@Repository
public class UsuarioDAOImpl implements UsuarioDAO, UserDetailsService {
	
	@PersistenceContext
	private EntityManager manager;

	@Override
	public void Salvar(Usuario usuario) {
		manager.persist(usuario);
	}

	@Override
	public void Atualizar(Usuario usuario) {
		this.manager.merge(usuario);
	}

	@Override
	public void Excluir(Usuario usuario) {
		this.manager.remove(usuario);
	}

	@Override
	public Usuario Carregar(Long id) {
		return this.manager.find(Usuario.class, id);
	}
	
	@Override
	public List<Usuario> Listar() {		
		return this.manager.createQuery("SELECT u FROM Usuario u", Usuario.class).getResultList();		
	}
	
	@Override
	public List<Usuario> Listar(Negocio negocio) {
		return this.manager.createQuery("SELECT u FROM Usuario u WHERE u.negocio.id = :negocio_id", Usuario.class)
				.setParameter("negocio_id", negocio.getId())
				.getResultList();
	}
	
	/* SPRING SECURITY */
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		List<Usuario> usuarios = this.manager.createQuery("SELECT u FROM Usuario u WHERE u.login=:login", Usuario.class)
				.setParameter("login", username)
				.getResultList();
		
		if(usuarios.isEmpty())
			throw new UsernameNotFoundException("O usuário " + username + " não existe.");
		
		return usuarios.get(0);
	}
	
	/* CUSTOM */
	@Override
	public Usuario ExisteUsuarioLogin(String login){
		String jpql = "select u from Usuario u where u.login=:login";
		try {
			return this.manager.createQuery(jpql, Usuario.class).setParameter("login", login).getSingleResult();
		}
		catch (NoResultException  e) {
			return null;
		}
	}
	
	@Override
	public Usuario ExisteUsuarioEmail(String email){		
		String jpql = "select u from Usuario u where u.email=:email";
		try {
			return this.manager.createQuery(jpql, Usuario.class).setParameter("email", email).getSingleResult();
		}
		catch (NoResultException e) {
			return null;
		}
	}
}
