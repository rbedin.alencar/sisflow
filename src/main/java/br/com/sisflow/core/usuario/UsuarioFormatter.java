package br.com.sisflow.core.usuario;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

@Component
public class UsuarioFormatter implements Formatter<Usuario> {
	
	@Autowired
	private UsuarioRN usuarioRN;
	
	@Override
	public String print(Usuario object, Locale locale) {
		return object.getId().toString();
	}

	@Override
	public Usuario parse(String text, Locale locale) throws ParseException {
		return this.usuarioRN.Carregar(Long.parseLong(text));
	}
}
