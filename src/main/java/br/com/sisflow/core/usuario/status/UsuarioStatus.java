package br.com.sisflow.core.usuario.status;

public enum UsuarioStatus{
	
	ACTIVE("Ativo"), 
	INACTIVE("Inativo"), 
	REGISTRATION("Aguardando ativação");
	
	private String descricao;
	
	UsuarioStatus(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
}
