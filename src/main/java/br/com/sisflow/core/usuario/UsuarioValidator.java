package br.com.sisflow.core.usuario;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class UsuarioValidator implements Validator {

	@Autowired
	private UsuarioRN usuarioRN;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return Usuario.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nome", 	"field.required.usuario.nome");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", 	"field.required.usuario.email");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "login", 	"field.required.usuario.login");
		
		Usuario usuario 			= (Usuario) target;
		Usuario usuarioExistente	= null;
		
		usuarioExistente = this.usuarioRN.ExisteUsuarioLogin(usuario.getLogin());
		if(usuarioExistente != null && (usuario.getId() == null || usuarioExistente.getId() != usuario.getId()))
			errors.rejectValue("login", "field.exists.usuario.login");
		
		usuarioExistente = this.usuarioRN.ExisteUsuarioEmail(usuario.getEmail());
		if(usuarioExistente != null && (usuario.getId() == null || usuarioExistente.getId() != usuario.getId()))
			errors.rejectValue("email", "field.exists.usuario.email");
		
	}

}
