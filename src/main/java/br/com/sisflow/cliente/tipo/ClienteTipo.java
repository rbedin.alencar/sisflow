package br.com.sisflow.cliente.tipo;

public enum ClienteTipo{
	
	PF("Pessoa Física"), 
	PJ("Pessoa Jurídica");
	
	private String descricao;
	
	ClienteTipo(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
}
