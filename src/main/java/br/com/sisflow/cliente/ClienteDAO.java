package br.com.sisflow.cliente;

import java.util.List;

import javax.transaction.Transactional;

import br.com.sisflow.cliente.origem.ClienteOrigem;
import br.com.sisflow.core.negocio.Negocio;

@Transactional
public interface ClienteDAO{
	public void Salvar(Cliente cliente);
	public void Atualizar(Cliente cliente);
	public void Excluir(Cliente cliente);
	public Cliente Carregar(Long id);
	
	public List<Cliente> Listar();
	public List<Cliente> Listar(Negocio negocio);
	public List<Cliente> Listar(ClienteOrigem clienteOrigem);
	
	public Long Total();
}
