package br.com.sisflow.cliente.endereco;

import java.util.List;

import javax.transaction.Transactional;

@Transactional
public interface ClienteEnderecoDAO{
	public void Salvar(ClienteEndereco clienteEndereco);
	public void Atualizar(ClienteEndereco clienteEndereco);
	public void Excluir(ClienteEndereco clienteEndereco);
	public ClienteEndereco Carregar(Long id);
	public List<ClienteEndereco> Listar();
}
