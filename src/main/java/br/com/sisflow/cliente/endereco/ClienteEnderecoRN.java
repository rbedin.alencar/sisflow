package br.com.sisflow.cliente.endereco;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class ClienteEnderecoRN{
	
	@Autowired
	private ClienteEnderecoDAO dao;
	
	public void Salvar(ClienteEndereco clienteEndereco) {
		
		Long id = clienteEndereco.getId();
		if(id == null)
			this.dao.Salvar(clienteEndereco);
		else
			this.dao.Atualizar(clienteEndereco);
	}
	
	public void Excluir(ClienteEndereco clienteEndereco) {
		this.dao.Excluir(clienteEndereco);
	}
	
	public ClienteEndereco Carregar(Long id) {
		return this.dao.Carregar(id);
	}
	
	public List<ClienteEndereco> Listar(){
		return this.dao.Listar();
	}
}
