package br.com.sisflow.cliente.endereco;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

@Repository
public class ClienteEnderecoImpl implements ClienteEnderecoDAO {

	@PersistenceContext
	private EntityManager manager;

	public void Salvar(ClienteEndereco clienteEndereco) {
		manager.persist(clienteEndereco);
	}

	public void Atualizar(ClienteEndereco clienteEndereco) {
		this.manager.merge(clienteEndereco);
	}

	public void Excluir(ClienteEndereco clienteEndereco) {
		this.manager.remove(clienteEndereco);
	}

	public ClienteEndereco Carregar(Long id) {
		return this.manager.find(ClienteEndereco.class, id);
	}

	public List<ClienteEndereco> Listar() {		
		return this.manager.createQuery("select ce from ClienteEndereco ce", ClienteEndereco.class).getResultList();		
	}
}
