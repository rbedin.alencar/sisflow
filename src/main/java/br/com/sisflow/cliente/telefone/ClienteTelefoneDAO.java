package br.com.sisflow.cliente.telefone;

import java.util.List;

import javax.transaction.Transactional;

@Transactional
public interface ClienteTelefoneDAO{
	public void Salvar(ClienteTelefone clienteTelefone);
	public void Atualizar(ClienteTelefone clienteTelefone);
	public void Excluir(ClienteTelefone clienteTelefone);
	public ClienteTelefone Carregar(Long id);
	public List<ClienteTelefone> Listar();
}
