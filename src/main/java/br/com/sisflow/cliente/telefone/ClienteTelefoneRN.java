package br.com.sisflow.cliente.telefone;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class ClienteTelefoneRN{
	
	@Autowired
	private ClienteTelefoneDAO dao;
	
	public void Salvar(ClienteTelefone clienteTelefone) {
		
		Long id = clienteTelefone.getId();
		if(id == null)
			this.dao.Salvar(clienteTelefone);
		else
			this.dao.Atualizar(clienteTelefone);
	}
	
	public void Excluir(ClienteTelefone clienteTelefone) {
		this.dao.Excluir(clienteTelefone);
	}
	
	public ClienteTelefone Carregar(Long id) {
		return this.dao.Carregar(id);
	}
	
	public List<ClienteTelefone> Listar(){
		return this.dao.Listar();
	}
}
