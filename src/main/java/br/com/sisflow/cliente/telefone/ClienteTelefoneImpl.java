package br.com.sisflow.cliente.telefone;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

@Repository
public class ClienteTelefoneImpl implements ClienteTelefoneDAO {

	@PersistenceContext
	private EntityManager manager;

	public void Salvar(ClienteTelefone clienteTelefone) {
		manager.persist(clienteTelefone);
	}

	public void Atualizar(ClienteTelefone clienteTelefone) {
		this.manager.merge(clienteTelefone);
	}

	public void Excluir(ClienteTelefone clienteTelefone) {
		this.manager.remove(clienteTelefone);
	}

	public ClienteTelefone Carregar(Long id) {
		return this.manager.find(ClienteTelefone.class, id);
	}

	public List<ClienteTelefone> Listar() {		
		return this.manager.createQuery("select ct from ClienteTelefone ct", ClienteTelefone.class).getResultList();		
	}
}
