package br.com.sisflow.cliente.telefone.tipo;

public enum ClienteTelefoneTipo{
	
	RES("Residencial", "(99) 9999-9999"), 
	CEL("Celular", "(99) 99999-9999");
	
	private String descricao, mascara;
	
	ClienteTelefoneTipo(String descricao, String mascara) {
		this.descricao = descricao;
		this.mascara = mascara;
	}

	public String getDescricao() {
		return descricao;
	}
	
	public String getMascara() {
		return mascara;
	}
}
