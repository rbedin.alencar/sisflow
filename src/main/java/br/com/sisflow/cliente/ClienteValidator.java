package br.com.sisflow.cliente;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import br.com.sisflow.cliente.tipo.ClienteTipo;

@Component
public class ClienteValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return Cliente.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		
		Cliente cliente = (Cliente) target;
		
		/* PF */
		if(cliente.getTipo() == ClienteTipo.PF) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "pessoaFisica.nome", "field.required.cliente.pessoaFisica.nome");	
		}
		
		/* PJ */
		if(cliente.getTipo() == ClienteTipo.PJ) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "pessoaJuridica.razao_social", "field.required.cliente.pessoaJuridica.razao_social");
		}
		
		
	}
}
