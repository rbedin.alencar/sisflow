package br.com.sisflow.cliente.email;

import java.util.List;

import javax.transaction.Transactional;

@Transactional
public interface ClienteEmailDAO{
	public void Salvar(ClienteEmail clienteEmail);
	public void Atualizar(ClienteEmail clienteEmail);
	public void Excluir(ClienteEmail clienteEmail);
	public ClienteEmail Carregar(Long id);
	public List<ClienteEmail> Listar();
}
