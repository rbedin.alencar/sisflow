package br.com.sisflow.cliente.email;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

@Repository
public class ClienteEmailImpl implements ClienteEmailDAO {

	@PersistenceContext
	private EntityManager manager;

	public void Salvar(ClienteEmail clienteEmail) {
		manager.persist(clienteEmail);
	}

	public void Atualizar(ClienteEmail clienteEmail) {
		this.manager.merge(clienteEmail);
	}

	public void Excluir(ClienteEmail clienteEmail) {
		this.manager.remove(clienteEmail);
	}

	public ClienteEmail Carregar(Long id) {
		return this.manager.find(ClienteEmail.class, id);
	}

	public List<ClienteEmail> Listar() {		
		return this.manager.createQuery("select ce from ClienteEmail ce", ClienteEmail.class).getResultList();		
	}
}
