package br.com.sisflow.cliente.email;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class ClienteEmailRN{
	
	@Autowired
	private ClienteEmailDAO dao;
	
	public void Salvar(ClienteEmail clienteEmail) {
		
		Long id = clienteEmail.getId();
		if(id == null)
			this.dao.Salvar(clienteEmail);
		else
			this.dao.Atualizar(clienteEmail);
	}
	
	public void Excluir(ClienteEmail clienteEmail) {
		this.dao.Excluir(clienteEmail);
	}
	
	public ClienteEmail Carregar(Long id) {
		return this.dao.Carregar(id);
	}
	
	public List<ClienteEmail> Listar(){
		return this.dao.Listar();
	}
}
