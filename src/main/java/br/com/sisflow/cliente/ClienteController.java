package br.com.sisflow.cliente;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.sisflow.cliente.email.ClienteEmail;
import br.com.sisflow.cliente.email.ClienteEmailRN;
import br.com.sisflow.cliente.endereco.ClienteEndereco;
import br.com.sisflow.cliente.endereco.ClienteEnderecoRN;
import br.com.sisflow.cliente.origem.ClienteOrigemRN;
import br.com.sisflow.cliente.telefone.ClienteTelefone;
import br.com.sisflow.cliente.telefone.ClienteTelefoneRN;
import br.com.sisflow.cliente.telefone.tipo.ClienteTelefoneTipo;
import br.com.sisflow.cliente.tipo.ClienteTipo;
import br.com.sisflow.conf.SisflowConfiguration;
import br.com.sisflow.core.negocio.NegocioRN;
import br.com.sisflow.core.pessoa.PessoaRN;
import br.com.sisflow.core.pessoa.fisica.PessoaFisica;
import br.com.sisflow.core.pessoa.fisica.sexo.PessoaFisicaSexo;
import br.com.sisflow.core.pessoa.juridica.PessoaJuridica;
import br.com.sisflow.core.upload.FileUploadImg;
import br.com.sisflow.core.usuario.UsuarioRN;
import br.com.sisflow.pet.animal.Animal;
import br.com.sisflow.pet.animal.AnimalRN;
import br.com.sisflow.pet.hospedagem.HospedagemRN;
import br.com.sisflow.venda.VendaRN;

@Controller
@Transactional
@RequestMapping("/cliente")
public class ClienteController{
	
	/* LOGGER */
	
	protected static Logger logger = Logger.getLogger(ClienteController.class);
	
	/* CONTEXT */
	
	@Autowired
	private ServletContext context;
	
	/* VALIDATOR */
	
	@Autowired
	private ClienteValidator clienteValidator;
	
	/* SERVICES */
	
	@Autowired
	private ClienteRN clienteRN;
	
	@Autowired
	private NegocioRN negocioRN;
	
	@Autowired
	private UsuarioRN usuarioRN;
	
	@Autowired
	private ClienteOrigemRN clienteOrigemRN;
	
	@Autowired
	private PessoaRN pessoaRN;
	
	@Autowired
	private ClienteEnderecoRN clienteEnderecoRN;
	
	@Autowired
	private ClienteTelefoneRN clienteTelefoneRN;
	
	@Autowired
	private ClienteEmailRN clienteEmailRN;
	
	/* PET */
	@Autowired
	private HospedagemRN hospedagemRN;
	
	@Autowired
	private AnimalRN animalRN;
	
	@Autowired
	private VendaRN vendaRN;

	/* BINDER */
	
	@InitBinder
    protected void initBinder(WebDataBinder binder) {
		binder.setValidator(this.clienteValidator);
    }
	
	/* ROUTES */
	
	@GetMapping
	@Cacheable(value="cliente")
	public ModelAndView index() {
		ModelAndView mv = new ModelAndView("cliente/index");
		mv.addObject("clienteDS", this.clienteRN.Listar());
		return mv;
	}
	
	@GetMapping("/cadastro")
	public ModelAndView cadastro(Cliente cliente) {
		
		ModelAndView mv = new ModelAndView("cliente/cadastro");
		
		/* THIS */
		mv.addObject("cliente", cliente);
		
		/* DS */
		Map<Boolean, String> ativoDS = new LinkedHashMap<Boolean, String>();
		ativoDS.put(true, "Ativo");
		ativoDS.put(false, "Inativo");
		mv.addObject("ativoDS", ativoDS);	
		
		mv.addObject("negocioDS", this.negocioRN.Listar());
		
		mv.addObject("usuarioDS", this.usuarioRN.Listar());
		
		mv.addObject("clienteTipoDS", ClienteTipo.values());
		
		mv.addObject("clienteOrigemDS", this.clienteOrigemRN.Listar());
		
		mv.addObject("clienteTelefoneTipoDS", ClienteTelefoneTipo.values());
		
		mv.addObject("pessoaFisicaSexoDS", PessoaFisicaSexo.values());
		
		Map<Boolean, String> contribuinteIcmsDS = new LinkedHashMap<Boolean, String>();
		contribuinteIcmsDS.put(true, "Sim");
		contribuinteIcmsDS.put(false, "Não");
		mv.addObject("contribuinteIcmsDS", contribuinteIcmsDS);	
		
		/* PHOTO */
		
		/* VIEW */
		return mv;
	}
	
	@GetMapping("/altera/{id}")
	public ModelAndView altera(@PathVariable("id") Long id) {

		Cliente cliente = this.clienteRN.Carregar(id);
		if(cliente != null) {
			
			if(cliente.getTipo() == ClienteTipo.PF) {
				PessoaFisica pf = (PessoaFisica) cliente.getPessoa();
				cliente.setPessoaFisica(pf);
			}
			if(cliente.getTipo() == ClienteTipo.PJ) {
				PessoaJuridica pj = (PessoaJuridica) cliente.getPessoa();
				cliente.setPessoaJuridica(pj);
			}

			return cadastro(cliente);
		}
		else
			return new ModelAndView("redirect:/cliente");
	}
	
	@GetMapping("/exclui/{id}")
	@CacheEvict(value= {"cliente", "animal", "venda"}, allEntries=true)
	public ModelAndView exclui(@PathVariable("id") Long id, RedirectAttributes redirectAttributes) {

		Cliente cliente = this.clienteRN.Carregar(id);
		if(cliente != null) {
			
			/*
			 * Verifica os animais ligados ao 
			 * cliente e exclui um por um.
			 */
			List<Animal> animais = this.animalRN.Listar(cliente);
			for(Animal a : animais)
				this.animalRN.Excluir(a);

			/*
			 * Exclui o registro.
			 */
			this.clienteRN.Excluir(cliente);	
		}

		redirectAttributes.addFlashAttribute("msg", "Cliente excluido com sucesso!");
		
		return new ModelAndView("redirect:/cliente");
	}
	
	@GetMapping("/visualiza/{id}")
	public ModelAndView visualiza(@PathVariable("id") Long id, RedirectAttributes redirectAttributes) {
		
		/* LOAD */
		Cliente cliente = this.clienteRN.Carregar(id);
		if(cliente == null)
			return new ModelAndView("redirect:/cliente");
		
		/* VIEW */
		ModelAndView mv = new ModelAndView("cliente/visualiza");
		
		/* DS */
		mv.addObject("cliente", cliente);
		
		/* PET */
		mv.addObject("hospedagemDS", this.hospedagemRN.Listar(cliente));
		mv.addObject("animalDS", this.animalRN.Listar(cliente));
		mv.addObject("vendaDS", this.vendaRN.Listar(cliente));
		
		/* CLASS */
		mv.addObject("bodyClass", "page-profile");

		/* RETURN */
		return mv;
	}
	
	@PostMapping("/salvar")
	@CacheEvict(value="cliente", allEntries=true)
	public ModelAndView salvar(@ModelAttribute @Valid Cliente cliente, BindingResult result, @RequestParam("file") MultipartFile[] files, RedirectAttributes redirectAttributes, HttpServletRequest request){
		
		if(result.hasErrors())
			return cadastro(cliente);

		/* PF */
		if(cliente.getTipo() == ClienteTipo.PF) {
			
			PessoaFisica pf = cliente.getPessoaFisica();
			
			//cpf
			String cpf = pf.getCpf();
			cpf = cpf.replaceAll("[^0-9]", "");
			pf.setCpf(cpf);	

			//rg
			String rg = pf.getRg();
			rg = rg.replaceAll("[^0-9]", "");
			pf.setRg(rg);
			
			//data nascimento
			if(pf.getData_nascimento() != null) {
				Date fmtDate = null;
				DateFormat df = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy", Locale.US);
				try {
					fmtDate = (Date) df.parse(pf.getData_nascimento().toString());
				} catch (ParseException e) {
					logger.info("Não foi possivel formatar a data: " + e.getMessage());
				}
				finally {
					pf.setData_nascimento(fmtDate);
				}
			}
			
			if(cliente.getPessoaJuridica().getId() != null)
				this.pessoaRN.Excluir(cliente.getPessoaJuridica());
			
			cliente.setPessoa(pf);
		}
		
		/* PJ */
		if(cliente.getTipo() == ClienteTipo.PJ) {
			
			PessoaJuridica pj = cliente.getPessoaJuridica();

			//cnpj
			String cnpj = pj.getCnpj();
			cnpj = cnpj.replaceAll("[^0-9]", "");
			pj.setCnpj(cnpj);
			
			//inscricao estadual
			String inscricao_estadual = pj.getInscricao_estadual();
			inscricao_estadual = inscricao_estadual.replaceAll("[^0-9]", "");
			pj.setInscricao_estadual(inscricao_estadual);
			
			//inscricao municipal
			String inscricao_municipal = pj.getInscricao_municipal();
			inscricao_municipal = inscricao_municipal.replaceAll("[^0-9]", "");
			pj.setInscricao_municipal(inscricao_municipal);
			
			if(cliente.getPessoaFisica().getId() != null)
				this.pessoaRN.Excluir(cliente.getPessoaFisica());
			
			cliente.setPessoa(pj);
		}
		
		/* ENDERECO */
		List<ClienteEndereco> enderecos = cliente.getEnderecos();
		if(enderecos == null)
			enderecos = new ArrayList<ClienteEndereco>();
		
		for(ClienteEndereco clienteEndereco : enderecos) {
			ClienteEndereco cen = this.clienteEnderecoRN.Carregar(clienteEndereco.getId());
			this.clienteEnderecoRN.Excluir(cen);
		}
		enderecos.clear();
		
		String[] endereco_cep 			= request.getParameterValues("endereco_cep[]");
		String[] endereco_logradouro 	= request.getParameterValues("endereco_logradouro[]");
		String[] endereco_numero 		= request.getParameterValues("endereco_numero[]");
		String[] endereco_bairro 		= request.getParameterValues("endereco_bairro[]");
		String[] endereco_cidade 		= request.getParameterValues("endereco_cidade[]");
		String[] endereco_uf 			= request.getParameterValues("endereco_uf[]");
		String[] endereco_pais 			= request.getParameterValues("endereco_pais[]");
		String[] endereco_complemento 	= request.getParameterValues("endereco_complemento[]");
		
		int num_enderecos = endereco_logradouro.length;
		if(endereco_logradouro != null && num_enderecos > 0) {
			for(int i=0; i<num_enderecos; i++) {
				
				if(endereco_logradouro[i].equals(""))
					continue;
				
				//cep
				endereco_cep[i] = endereco_cep[i].replaceAll("[^0-9]", "");
				
				//numero
				Integer numero = 0;
				try {
					numero = Integer.parseInt(endereco_numero[i]);
				}
				catch (NumberFormatException e) {
					System.out.println("Não foi possivel obter o valor numérico do endereço: ["+e.getMessage()+"]");
				}
	
				//new
				ClienteEndereco clienteEndereco = new ClienteEndereco(
						endereco_cep[i], 
						endereco_logradouro[i], 
						numero, 
						endereco_bairro[i], 
						endereco_cidade[i], 
						endereco_uf[i], 
						endereco_pais[i], 
						endereco_complemento[i], 
						cliente
						);
				enderecos.add(clienteEndereco);
			}
		}
		cliente.setEnderecos(enderecos);
		
		/* TELEFONE */
		List<ClienteTelefone> telefones = cliente.getTelefones();
		if(telefones == null)
			telefones = new ArrayList<ClienteTelefone>();
		
		for(ClienteTelefone clienteTelefone : telefones) {
			ClienteTelefone ct = this.clienteTelefoneRN.Carregar(clienteTelefone.getId());
			this.clienteTelefoneRN.Excluir(ct);
		}
		telefones.clear();

		String[] telefone_numero 	= request.getParameterValues("telefone_numero[]");
		String[] telefone_descricao = request.getParameterValues("telefone_descricao[]");
		String[] telefone_tipo		= request.getParameterValues("telefone_tipo[]");
		
		if(telefone_numero != null && telefone_numero.length > 0) {		
			int num_telefones = telefone_numero.length;
			for(int i=0; i<num_telefones; i++) {
				telefone_numero[i] = telefone_numero[i].replaceAll("[^0-9]", "");				
				ClienteTelefone clienteTelefone = new ClienteTelefone(telefone_numero[i], telefone_descricao[i], ClienteTelefoneTipo.valueOf(telefone_tipo[i]), cliente);
				telefones.add(clienteTelefone);
			}
		}
		cliente.setTelefones(telefones);
		
		/* EMAIL */
		List<ClienteEmail> emails = cliente.getEmails();
		if(emails == null)
			emails = new ArrayList<ClienteEmail>();
		
		for(ClienteEmail clienteEmail : emails) {
			ClienteEmail ce = this.clienteEmailRN.Carregar(clienteEmail.getId());
			this.clienteEmailRN.Excluir(ce);
		}
		emails.clear();

		String[] email_email 		= request.getParameterValues("email_email[]");
		String[] email_descricao 	= request.getParameterValues("email_descricao[]");
		
		if(email_email != null && email_email.length > 0) {		
			int num_emails = email_email.length;
			for(int i=0; i<num_emails; i++) {	
				ClienteEmail clienteEmail = new ClienteEmail(email_email[i], email_descricao[i], false, cliente);
				emails.add(clienteEmail);
			}
		}
		cliente.setEmails(emails);
		
		
		/* SAVE */
		this.clienteRN.Salvar(cliente);
		
		/* UPLOAD */
		boolean useClientStorage = false;
		try {
			useClientStorage = Boolean.parseBoolean(SisflowConfiguration.getValue("client.storage.directory"));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		for(MultipartFile file : files) {
			if(file.isEmpty()) continue;
			try {
				
				FileUploadImg upload = new FileUploadImg(file);
				upload.setFolder(this.context.getRealPath("/uploads/cliente"));
				upload.setFilename(cliente.getId().toString());
				
				upload.Resize(640, 480);
				upload.setPreserveAspectRatio(true);
				upload.setFormat("jpg");
				
				upload.Thumb(54, 54);
				
				upload.setFtpSync(useClientStorage);
				upload.setFtpDir(SisflowConfiguration.getValue("client.storage.directory") + "/cliente");
				
				upload.Start();
								
			} catch (IOException e) {
				System.out.println("Não foi possivel realizar o upload do arquivo: " + e.getMessage());
			}
		}
		
		redirectAttributes.addFlashAttribute("msg", "Registro salvo com sucesso!");
		
		return new ModelAndView("redirect:/cliente");
	}
}
