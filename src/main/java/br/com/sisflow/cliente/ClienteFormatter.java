package br.com.sisflow.cliente;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

@Component
public class ClienteFormatter implements Formatter<Cliente> {
	
	@Autowired
	private ClienteRN clienteRN;
	
	@Override
	public String print(Cliente object, Locale locale) {
		return object.getId().toString();
	}

	@Override
	public Cliente parse(String text, Locale locale) throws ParseException {
		return this.clienteRN.Carregar(Long.parseLong(text));
	}
}
