package br.com.sisflow.cliente;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.sisflow.cliente.endereco.ClienteEndereco;
import br.com.sisflow.core.negocio.Negocio;
import br.com.sisflow.core.negocio.NegocioRN;

@RestController
@RequestMapping("/api/cliente")
public class ClienteControllerAPI{
	
	protected static Logger logger = Logger.getLogger(ClienteControllerAPI.class);

	@Autowired
	private ClienteRN clienteRN;
	
	@Autowired
	private NegocioRN negocioRN;
	
	@RequestMapping(value="/endereco/{cep}", method=RequestMethod.GET, produces={"application/json"})
	public ClienteEndereco Endereco(@PathVariable String cep){
		
		HttpURLConnection connection = null;
		try {
			
			URL url = new URL("https://viacep.com.br/ws/"+cep+"/json");
			
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Content-length", "0");
			connection.setUseCaches(false);
			connection.setAllowUserInteraction(false);
			connection.connect();

			int status = connection.getResponseCode();
			
			switch (status) {
			case 200:
			case 201:
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				StringBuilder sb = new StringBuilder();
				String line;
				while((line = bufferedReader.readLine()) != null)
					sb.append(line+"\n");
				bufferedReader.close();
				
				JSONObject jsonObject = new JSONObject(sb.toString());
				
				return new ClienteEndereco(
						jsonObject.getString("cep"), 
						jsonObject.getString("logradouro"), 
						null, 
						jsonObject.getString("bairro"), 
						jsonObject.getString("localidade"), 
						jsonObject.getString("uf"), 
						"BR", 
						null, 
						null
						);
			case 500:
				return null;
			}
			
		} catch (MalformedURLException e1) {
			logger.info("Não foi possivel abrir a URL: [" + e1.getMessage() + "]");
		} catch (IOException e2) {
			logger.info("Não foi possivel conectar com o servidor de dados: [" + e2.getMessage() + "]");
		}
		return null;
	}
	
	@GetMapping("/listar/{negocio_id}")
	public List<Cliente> Listar(@PathVariable Long negocio_id){
		Negocio negocio = this.negocioRN.Carregar(negocio_id);		
		if(negocio != null)
			return this.clienteRN.Listar(negocio);
		return new ArrayList<Cliente>();
	}
}
