package br.com.sisflow.cliente;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.sisflow.cliente.email.ClienteEmail;
import br.com.sisflow.cliente.endereco.ClienteEndereco;
import br.com.sisflow.cliente.origem.ClienteOrigem;
import br.com.sisflow.cliente.telefone.ClienteTelefone;
import br.com.sisflow.cliente.tipo.ClienteTipo;
import br.com.sisflow.core.negocio.Negocio;
import br.com.sisflow.core.pessoa.Pessoa;
import br.com.sisflow.core.pessoa.fisica.PessoaFisica;
import br.com.sisflow.core.pessoa.juridica.PessoaJuridica;
import br.com.sisflow.core.usuario.Usuario;

@Entity
@Table(name="cliente")
@Inheritance(strategy=InheritanceType.JOINED)
public class Cliente implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 26048899761275045L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="cliente_id")
	private Long id;
	
	@Enumerated(EnumType.STRING)
	@Column(length=2, nullable=false)
	private ClienteTipo tipo;
	
	@OneToOne(cascade=CascadeType.ALL, orphanRemoval=true)
	@JoinColumn(name="pessoa_id", nullable=false)
	private Pessoa pessoa;
	
	@Transient
	private PessoaFisica pessoaFisica;
	
	@Transient
	private PessoaJuridica pessoaJuridica;
	
	@ManyToOne
	@JoinColumn(name="negocio_id", nullable=false)
	private Negocio negocio;
	
	@ManyToOne
	@JoinColumn(name="cliente_origem_id", nullable=false)
	private ClienteOrigem origem;

	@OneToMany(mappedBy="cliente", targetEntity=ClienteTelefone.class, fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	@JsonIgnoreProperties("cliente")
	private List<ClienteTelefone> telefones = new ArrayList<ClienteTelefone>();
	
	@OneToMany(mappedBy="cliente", targetEntity=ClienteEmail.class, fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	@JsonIgnoreProperties("cliente")
	private List<ClienteEmail> emails = new ArrayList<ClienteEmail>();
	
	@OneToMany(mappedBy="cliente", targetEntity=ClienteEndereco.class, fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	@JsonIgnoreProperties("cliente")
	private List<ClienteEndereco> enderecos = new ArrayList<ClienteEndereco>();

	@Column(nullable=true, columnDefinition="TEXT")
	private String observacao;
	
	@Column(nullable=false, columnDefinition="bit default 1")
	private boolean ativo = true;

	@ManyToOne
	@JoinColumn(name="usuario_responsavel_id")
	private Usuario usuario_responsavel;
	
	@ManyToOne
	@JoinColumn(name="usuario_cadastro_id")
	private Usuario usuario_cadastro;
	
	@ManyToOne
	@JoinColumn(name="usuario_atualiza_id")
	private Usuario usuario_atualiza;
	
	@Column(columnDefinition="DATETIME", updatable=false)
	@Temporal(TemporalType.TIMESTAMP)
	@CreationTimestamp
	private Date criado_em;
	
	@Column(columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	private Date atualizado_em;

	/* GETTERS & SETTERS */
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ClienteTipo getTipo() {
		return tipo;
	}

	public void setTipo(ClienteTipo tipo) {
		this.tipo = tipo;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public PessoaFisica getPessoaFisica() {
		return pessoaFisica;
	}

	public void setPessoaFisica(PessoaFisica pessoaFisica) {
		this.pessoaFisica = pessoaFisica;
	}

	public PessoaJuridica getPessoaJuridica() {
		return pessoaJuridica;
	}

	public void setPessoaJuridica(PessoaJuridica pessoaJuridica) {
		this.pessoaJuridica = pessoaJuridica;
	}

	public Negocio getNegocio() {
		return negocio;
	}

	public void setNegocio(Negocio negocio) {
		this.negocio = negocio;
	}

	public ClienteOrigem getOrigem() {
		return origem;
	}

	public void setOrigem(ClienteOrigem origem) {
		this.origem = origem;
	}

	public List<ClienteTelefone> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<ClienteTelefone> telefones) {
		this.telefones = telefones;
	}

	public List<ClienteEmail> getEmails() {
		return emails;
	}

	public void setEmails(List<ClienteEmail> emails) {
		this.emails = emails;
	}

	public List<ClienteEndereco> getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(List<ClienteEndereco> enderecos) {
		this.enderecos = enderecos;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public Usuario getUsuario_responsavel() {
		return usuario_responsavel;
	}

	public void setUsuario_responsavel(Usuario usuario_responsavel) {
		this.usuario_responsavel = usuario_responsavel;
	}

	public Usuario getUsuario_cadastro() {
		return usuario_cadastro;
	}

	public void setUsuario_cadastro(Usuario usuario_cadastro) {
		this.usuario_cadastro = usuario_cadastro;
	}

	public Usuario getUsuario_atualiza() {
		return usuario_atualiza;
	}

	public void setUsuario_atualiza(Usuario usuario_atualiza) {
		this.usuario_atualiza = usuario_atualiza;
	}

	public Date getCriado_em() {
		return criado_em;
	}

	public void setCriado_em(Date criado_em) {
		this.criado_em = criado_em;
	}

	public Date getAtualizado_em() {
		return atualizado_em;
	}

	public void setAtualizado_em(Date atualizado_em) {
		this.atualizado_em = atualizado_em;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (ativo ? 1231 : 1237);
		result = prime * result + ((atualizado_em == null) ? 0 : atualizado_em.hashCode());
		result = prime * result + ((criado_em == null) ? 0 : criado_em.hashCode());
		result = prime * result + ((emails == null) ? 0 : emails.hashCode());
		result = prime * result + ((enderecos == null) ? 0 : enderecos.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((negocio == null) ? 0 : negocio.hashCode());
		result = prime * result + ((observacao == null) ? 0 : observacao.hashCode());
		result = prime * result + ((origem == null) ? 0 : origem.hashCode());
		result = prime * result + ((pessoa == null) ? 0 : pessoa.hashCode());
		result = prime * result + ((pessoaFisica == null) ? 0 : pessoaFisica.hashCode());
		result = prime * result + ((pessoaJuridica == null) ? 0 : pessoaJuridica.hashCode());
		result = prime * result + ((telefones == null) ? 0 : telefones.hashCode());
		result = prime * result + ((tipo == null) ? 0 : tipo.hashCode());
		result = prime * result + ((usuario_atualiza == null) ? 0 : usuario_atualiza.hashCode());
		result = prime * result + ((usuario_cadastro == null) ? 0 : usuario_cadastro.hashCode());
		result = prime * result + ((usuario_responsavel == null) ? 0 : usuario_responsavel.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cliente other = (Cliente) obj;
		if (ativo != other.ativo)
			return false;
		if (atualizado_em == null) {
			if (other.atualizado_em != null)
				return false;
		} else if (!atualizado_em.equals(other.atualizado_em))
			return false;
		if (criado_em == null) {
			if (other.criado_em != null)
				return false;
		} else if (!criado_em.equals(other.criado_em))
			return false;
		if (emails == null) {
			if (other.emails != null)
				return false;
		} else if (!emails.equals(other.emails))
			return false;
		if (enderecos == null) {
			if (other.enderecos != null)
				return false;
		} else if (!enderecos.equals(other.enderecos))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (negocio == null) {
			if (other.negocio != null)
				return false;
		} else if (!negocio.equals(other.negocio))
			return false;
		if (observacao == null) {
			if (other.observacao != null)
				return false;
		} else if (!observacao.equals(other.observacao))
			return false;
		if (origem == null) {
			if (other.origem != null)
				return false;
		} else if (!origem.equals(other.origem))
			return false;
		if (pessoa == null) {
			if (other.pessoa != null)
				return false;
		} else if (!pessoa.equals(other.pessoa))
			return false;
		if (pessoaFisica == null) {
			if (other.pessoaFisica != null)
				return false;
		} else if (!pessoaFisica.equals(other.pessoaFisica))
			return false;
		if (pessoaJuridica == null) {
			if (other.pessoaJuridica != null)
				return false;
		} else if (!pessoaJuridica.equals(other.pessoaJuridica))
			return false;
		if (telefones == null) {
			if (other.telefones != null)
				return false;
		} else if (!telefones.equals(other.telefones))
			return false;
		if (tipo != other.tipo)
			return false;
		if (usuario_atualiza == null) {
			if (other.usuario_atualiza != null)
				return false;
		} else if (!usuario_atualiza.equals(other.usuario_atualiza))
			return false;
		if (usuario_cadastro == null) {
			if (other.usuario_cadastro != null)
				return false;
		} else if (!usuario_cadastro.equals(other.usuario_cadastro))
			return false;
		if (usuario_responsavel == null) {
			if (other.usuario_responsavel != null)
				return false;
		} else if (!usuario_responsavel.equals(other.usuario_responsavel))
			return false;
		return true;
	}
}
