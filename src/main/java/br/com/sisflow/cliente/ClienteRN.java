package br.com.sisflow.cliente;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.sisflow.cliente.origem.ClienteOrigem;
import br.com.sisflow.conf.SisflowConfiguration;
import br.com.sisflow.core.negocio.Negocio;
import br.com.sisflow.core.upload.FileUploadFTP;
import br.com.sisflow.venda.VendaRN;

@Service
@Transactional
public class ClienteRN{
	
	@Autowired
	private ClienteDAO dao;
	
	@Autowired
	private VendaRN vendaRN;
	
	public void Salvar(Cliente cliente) {
		
		Long id = cliente.getId();
		if(id == null) {
			cliente.setCriado_em(new Date(System.currentTimeMillis()));
			this.dao.Salvar(cliente);
		}
		else {
			cliente.setAtualizado_em(new Date(System.currentTimeMillis()));
			this.dao.Atualizar(cliente);
		}
	}
	
	public void Excluir(Cliente cliente) {
		
		/*
		 * Conecta ao FTP e exclui as imagens 
		 * ligadas ao registro.
		 */
		String[] files_to_delete = {
				cliente.getId() + ".jpg", 
				"thumb_" + cliente.getId() + ".jpg"
				};

		FileUploadFTP fileUploadFTP = new FileUploadFTP();
		for(String f : files_to_delete) {
			try {
				fileUploadFTP.Delete(SisflowConfiguration.getValue("client.storage.directory") + "/cliente/" + f);			
			} catch (IOException e) {
				System.out.println("Não foi possivel excluir o arquivo ["+f+"] do servidor: " + e.getMessage());
			}	
		}
		
		/*
		 * Venda
		 * 
		 * Exclui todas vendas relacionadas 
		 * com o cliente.
		 */
		this.vendaRN.Excluir(cliente);
		
		/*
		 * Exclui o registro.
		 */
		this.dao.Excluir(cliente);
	}
	
	public Cliente Carregar(Long id) {
		return this.dao.Carregar(id);
	}
	
	public List<Cliente> Listar(){
		return this.dao.Listar();
	}
	
	public List<Cliente> Listar(Negocio negocio){
		return this.dao.Listar(negocio);
	}
	
	public List<Cliente> Listar(ClienteOrigem clienteOrigem){
		return this.dao.Listar(clienteOrigem);
	}
	
	public Long Total(){
		return this.dao.Total();
	}
}
