package br.com.sisflow.cliente;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import br.com.sisflow.cliente.origem.ClienteOrigem;
import br.com.sisflow.core.negocio.Negocio;

@Repository
public class ClienteDAOImpl implements ClienteDAO {

	@PersistenceContext
	private EntityManager manager;

	public void Salvar(Cliente cliente) {
		manager.persist(cliente);
	}

	public void Atualizar(Cliente cliente) {
		this.manager.merge(cliente);
	}

	public void Excluir(Cliente cliente) {
		this.manager.remove(cliente);
	}

	public Cliente Carregar(Long id) {
		return this.manager.find(Cliente.class, id);
	}

	public List<Cliente> Listar() {		
		return this.manager.createQuery("select c from Cliente c", Cliente.class).getResultList();		
	}

	@Override
	public List<Cliente> Listar(ClienteOrigem clienteOrigem) {
		return this.manager.createQuery("select c from Cliente c where c.origem.id =:cliente_origem_id", Cliente.class)
				.setParameter("cliente_origem_id", clienteOrigem.getId())
				.getResultList();	
	}
	
	@Override
	public List<Cliente> Listar(Negocio negocio) {
		return this.manager.createQuery("select c from Cliente c where c.negocio.id = :negocio_id", Cliente.class)
				.setParameter("negocio_id", negocio.getId())
				.getResultList();	
	}
	
	@Override
	public Long Total() {
		return (Long) this.manager.createQuery("select count(c) from Cliente c").getSingleResult();
	}
}
