package br.com.sisflow.cliente.origem;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

@Component
public class ClienteOrigemFormatter implements Formatter<ClienteOrigem>{

	@Autowired
	private ClienteOrigemRN clienteOrigemRN;
	
	public String print(ClienteOrigem object, Locale locale) {
		return object.getId().toString();
	}

	public ClienteOrigem parse(String text, Locale locale) throws ParseException {
		return this.clienteOrigemRN.Carregar(Long.parseLong(text));
	}	
}
