package br.com.sisflow.cliente.origem;

import java.util.List;

import javax.transaction.Transactional;

@Transactional
public interface ClienteOrigemDAO{
	public void Salvar(ClienteOrigem clienteOrigem);
	public void Atualizar(ClienteOrigem clienteOrigem);
	public void Excluir(ClienteOrigem clienteOrigem);
	public ClienteOrigem Carregar(Long id);
	public List<ClienteOrigem> Listar();
}
