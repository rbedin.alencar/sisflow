package br.com.sisflow.cliente.origem;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

@Repository
public class ClienteOrigemDAOImpl implements ClienteOrigemDAO {

	@PersistenceContext
	private EntityManager manager;

	public void Salvar(ClienteOrigem clienteOrigem) {
		manager.persist(clienteOrigem);
	}

	public void Atualizar(ClienteOrigem clienteOrigem) {
		this.manager.merge(clienteOrigem);
	}

	public void Excluir(ClienteOrigem usuario) {
		this.manager.remove(usuario);
	}

	public ClienteOrigem Carregar(Long id) {
		return this.manager.find(ClienteOrigem.class, id);
	}

	public List<ClienteOrigem> Listar() {		
		return this.manager.createQuery("select co from ClienteOrigem co", ClienteOrigem.class).getResultList();		
	}
}
