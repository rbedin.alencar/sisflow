package br.com.sisflow.cliente.origem;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class ClienteOrigemValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return ClienteOrigem.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "descricao", 	"field.required");
	}

}
