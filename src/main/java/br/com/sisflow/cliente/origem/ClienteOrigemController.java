package br.com.sisflow.cliente.origem;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@Transactional
@RequestMapping("/cliente/origem")
public class ClienteOrigemController{
	
	/* LOGGER */
	
	protected static Logger logger = Logger.getLogger(ClienteOrigemController.class);
	
	/* VALIDATOR */
	
	@Autowired
	private ClienteOrigemValidator clienteOrigemValidator;
	
	/* SERVICE */
	
	@Autowired
	private ClienteOrigemRN clienteOrigemRN;
	
	/* BINDER */
	
	@InitBinder
    protected void initBinder(WebDataBinder binder) {
		binder.setValidator(this.clienteOrigemValidator);
    }
	
	/* ROUTES */
	
	@GetMapping
	@Cacheable(value="cliente.origem")
	public ModelAndView index() {
		ModelAndView mv = new ModelAndView("cliente/origem/index");
		mv.addObject("clienteOrigemDS", this.clienteOrigemRN.Listar());
		return mv;
	}
	
	@GetMapping("/cadastro")
	public ModelAndView cadastro(ClienteOrigem clienteOrigem) {
		ModelAndView mv = new ModelAndView("cliente/origem/cadastro");
		
		/* THIS */
		mv.addObject("clienteOrigem", clienteOrigem);
		
		return mv;
	}
	
	@GetMapping("/altera/{id}")
	public ModelAndView altera(@PathVariable("id") Long id) {

		ClienteOrigem clienteOrigem = this.clienteOrigemRN.Carregar(id);
		if(clienteOrigem != null)
			return cadastro(clienteOrigem);
		else
			return new ModelAndView("redirect:/cliente/origem");
	}
	
	@GetMapping("/exclui/{id}")
	@CacheEvict(value= {"cliente.origem", "cliente"}, allEntries=true)
	public ModelAndView exclui(@PathVariable("id") Long id, RedirectAttributes redirectAttributes) {
		
		ClienteOrigem clienteOrigem = this.clienteOrigemRN.Carregar(id);
		if(clienteOrigem != null) {
			
			try {
				this.clienteOrigemRN.Excluir(clienteOrigem);	
				redirectAttributes.addFlashAttribute("msg", "Origem excluída com sucesso!");
			}
			catch (Exception e) {
				redirectAttributes.addFlashAttribute("msg", e.getMessage());
			}	
		}
		
		return new ModelAndView("redirect:/cliente/origem");
	}
	
	@PostMapping("/salvar")
	@CacheEvict(value="cliente.origem", allEntries=true)
	public ModelAndView salvar(@ModelAttribute @Valid ClienteOrigem clienteOrigem, BindingResult result, RedirectAttributes redirectAttributes){
		
		if(result.hasErrors())
			return cadastro(clienteOrigem);
		
		this.clienteOrigemRN.Salvar(clienteOrigem);

		redirectAttributes.addFlashAttribute("success", "Registro salvo com sucesso!");
		
		return new ModelAndView("redirect:/cliente/origem");
	}
}
