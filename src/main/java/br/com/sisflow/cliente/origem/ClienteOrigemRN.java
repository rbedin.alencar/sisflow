package br.com.sisflow.cliente.origem;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.sisflow.cliente.Cliente;
import br.com.sisflow.cliente.ClienteRN;

@Service
@Transactional
public class ClienteOrigemRN{
	
	@Autowired
	private ClienteOrigemDAO dao;

	@Autowired
	private ClienteRN clienteRN;
	
	public void Salvar(ClienteOrigem clienteOrigem) {
		
		Long id = clienteOrigem.getId();
		if(id == null)
			this.dao.Salvar(clienteOrigem);
		else
			this.dao.Atualizar(clienteOrigem);
	}
	
	public void Excluir(ClienteOrigem clienteOrigem) throws Exception {		
		Long id = clienteOrigem.getId();
		if(id == 1)
			throw new Exception("Não é possivel excluir o registro padrão do sistema.");
		else {
			List<Cliente> clientes = this.clienteRN.Listar(clienteOrigem);
			if(clientes.size() > 0) {
				ClienteOrigem origemDefault = this.Carregar(Long.parseLong("1"));
				for(Cliente c : clientes) {
					c.setOrigem(origemDefault);
					this.clienteRN.Salvar(c);
				}
			}
			this.dao.Excluir(clienteOrigem);
		}
	}
	
	public ClienteOrigem Carregar(Long id) {
		return this.dao.Carregar(id);
	}
	
	public List<ClienteOrigem> Listar(){
		return this.dao.Listar();
	}
}
