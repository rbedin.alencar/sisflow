<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:default>
	<jsp:attribute name="body">
		<div class="page">
		
			<div class="page-header">
				<h1 class="page-title font-size-26 font-weight-100">Estatísticas do sistema</h1>
			</div>

			<div class="page-content container-fluid">
				
				<div class="row">
				
					<div class="col-xl-3 col-md-6 info-panel">
						<div class="card card-shadow">
			            	<div class="card-block bg-white p-20">
			              		<button type="button" class="btn btn-floating btn-sm btn-warning"><i class="fa fa-users"></i></button>
			              		<span class="ml-15 font-weight-400">CLIENTES</span>
				              	<div class="content-text text-center mb-0">
				                	<span class="font-size-40 font-weight-100">${totalCliente}</span>
				                	<p class="blue-grey-400 font-weight-100 m-0">total de clientes cadastrados</p>
				              	</div>
			            	</div>
			          	</div>
			        </div>
			        
					<div class="col-xl-3 col-md-6 info-panel">
						<div class="card card-shadow">
			            	<div class="card-block bg-white p-20">
			              		<button type="button" class="btn btn-floating btn-sm btn-primary"><i class="fa fa-shopping-cart"></i></button>
			              		<span class="ml-15 font-weight-400">VENDAS</span>
				              	<div class="content-text text-center mb-0">
				                	<span class="font-size-40 font-weight-100">${totalVenda}</span>
				                	<p class="blue-grey-400 font-weight-100 m-0">total de vendas realizadas</p>
				              	</div>
			            	</div>
			          	</div>
			        </div>
			        
					<div class="col-xl-3 col-md-6 info-panel">
						<div class="card card-shadow">
			            	<div class="card-block bg-white p-20">
			              		<button type="button" class="btn btn-floating btn-sm btn-primary"><i class="fa fa-paw"></i></button>
			              		<span class="ml-15 font-weight-400">ANIMAIS</span>
				              	<div class="content-text text-center mb-0">
				                	<span class="font-size-40 font-weight-100">${totalAnimal}</span>
				                	<p class="blue-grey-400 font-weight-100 m-0">total de animais cadastrados</p>
				              	</div>
			            	</div>
			          	</div>
			        </div>
		        
		        </div>
				
				
			</div>
		</div>
	</jsp:attribute>
</t:default>