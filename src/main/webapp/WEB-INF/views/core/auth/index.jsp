<%@page import="br.com.sisflow.conf.SisflowConfiguration" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html class="no-js css-menubar" lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<title>Sisflow</title>
		<link rel="apple-touch-icon" href="${pageContext.request.contextPath}/resources/custom/img/i_60.png">
		<link rel="apple-touch-icon" href="${pageContext.request.contextPath}/resources/custom/img/i_76.png" sizes="76x76">
		<link rel="apple-touch-icon" href="${pageContext.request.contextPath}/resources/custom/img/i_120.png" sizes="120x120">
		<link rel="apple-touch-icon" href="${pageContext.request.contextPath}/resources/custom/img/i_152.png" sizes="152x152">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-touch-fullscreen" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="default">
		<meta name="description" content="sisflow.com.br" />
		<meta name="author" content="sisflow.com.br" />
				
		<link rel="apple-touch-icon" href="${pageContext.request.contextPath}/resources/assets/images/apple-touch-icon.png">
		<link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/assets/images/favicon.ico">
		
		<!-- core -->
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/css/bootstrap.min.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/css/bootstrap-extend.min.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/site.min.css">
		
		<!-- plugins -->
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/animsition/animsition.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/asscrollable/asScrollable.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/switchery/switchery.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/intro-js/introjs.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/slidepanel/slidePanel.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/examples/css/pages/login.css">
		
		<!-- custom -->
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/custom/css/default.min.css">
		
		<!-- fonts -->
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/fonts/font-awesome/font-awesome.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/fonts/web-icons/web-icons.min.css">		
    	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/fonts/brand-icons/brand-icons.min.css">
    	<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
		
		<!--[if lt IE 9]>
			<script src="${pageContext.request.contextPath}/resources/global/vendor/html5shiv/html5shiv.min.js"></script>
		<![endif]-->
		
		<!--[if lt IE 10]>
			<script src="${pageContext.request.contextPath}/resources/global/vendor/media-match/media.match.min.js"></script>
			<script src="${pageContext.request.contextPath}/resources/global/vendor/respond/respond.min.js"></script>
		<![endif]-->
		
		<script src="${pageContext.request.contextPath}/resources/global/vendor/breakpoints/breakpoints.js"></script>
		<script>
			Breakpoints();
		</script>
	</head>
	<body class="page-login layout-full page-dark animsition">
		
		<!--[if lt IE 8]>
            <p class="browserupgrade">Voc� esta usando um navegador desatualizado. Atualize seu navegador para melhorar sua experi�ncia.</p>
        <![endif]-->

		<div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">
			<div class="page-content vertical-align-middle animation-slide-top animation-duration-1">
			
                <div class="brand">
                	<h2 class="brand-text"><%=SisflowConfiguration.getValue("client.label")%></h2>
                </div>
                <p>Entrar na sua conta</p>
                
				<c:if test="${msg != null}">
				<div class="alert alert-danger alert-dismissible" role="alert">
					${msg}
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span></button>
        		</div>
        		</c:if>
                
                <form name="login" id="login" method="post" action="${pageContext.request.contextPath}/auth" role="form" autocomplete="off">
                	
                	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}">
                	
                    <div class="form-group">
                    	<label class="sr-only" for="login">Login</label>
                    	<input type="text" class="form-control" id="login" name="login" placeholder="Login">
                    </div>
                    <div class="form-group">
                    	<label class="sr-only" for="inputPassword">Senha</label>
                    	<input type="password" class="form-control" id="senha" name="senha" placeholder="Senha">
                    </div>
                    <div class="form-group clearfix">
                    	<div class="checkbox-custom checkbox-inline checkbox-primary float-left">
                    		<input type="checkbox" id="remember" name="remember">
                    		<label for="remember">Lembrar</label>
                    	</div>
                    	<a class="float-right" href="forgot-password.html">Esqueceu a senha?</a>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">Entrar</button>
                
                </form>
                
                <footer class="page-copyright page-copyright-inverse">
                	<p>&copy;2018 <%=SisflowConfiguration.getValue("system.name")%>. <%=SisflowConfiguration.getValue("system.version")%></p>
                </footer>
                
			</div>
		</div>
		
		<!-- core -->
	    <script src="${pageContext.request.contextPath}/resources/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/global/vendor/jquery/jquery.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/global/vendor/popper-js/umd/popper.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/global/vendor/bootstrap/bootstrap.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/global/vendor/animsition/animsition.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/global/vendor/mousewheel/jquery.mousewheel.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/global/vendor/asscrollable/jquery-asScrollable.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
		
		<!-- plugins -->
	    <script src="${pageContext.request.contextPath}/resources/global/vendor/switchery/switchery.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/global/vendor/intro-js/intro.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/global/vendor/screenfull/screenfull.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/global/vendor/slidepanel/jquery-slidePanel.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- scripts -->
	    <script src="${pageContext.request.contextPath}/resources/global/js/Component.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/global/js/Plugin.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/global/js/Base.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/global/js/Config.js"></script>
	    
	    <script src="${pageContext.request.contextPath}/resources/assets/js/Section/Menubar.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/assets/js/Section/GridMenu.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/assets/js/Section/Sidebar.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/assets/js/Section/PageAside.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/assets/js/Plugin/menu.js"></script>
			
	    <script src="${pageContext.request.contextPath}/resources/global/js/config/colors.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/assets/js/config/tour.js"></script>
	    <script>Config.set('assets', '${pageContext.request.contextPath}/resources/assets');</script>
	    
	    <!-- page -->
	    <script src="${pageContext.request.contextPath}/resources/assets/js/Site.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/global/js/Plugin/asscrollable.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/global/js/Plugin/slidepanel.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/global/js/Plugin/switchery.js"></script>

	    <script>
	      (function(document, window, $){
	        'use strict';

	    	var login = $('input[name="login"]');
	    	var senha = $('input[name="senha"]');	
	    	
	    	if(login.val().length > 0)
	    		senha.focus();
	    	else
	    		login.focus();
	    
	        var Site = window.Site;
	        $(document).ready(function(){
	          Site.run();
	        });
	      })(document, window, jQuery);
	    </script>
	</body>
</html>