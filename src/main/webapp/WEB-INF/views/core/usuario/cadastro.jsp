<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>


<t:default>
	<jsp:attribute name="body">
	
		<div class="page">	
			<div class="page-header">			
				<h1 class="page-title">Cadastro de Usu�rio</h1>
				<ol class="breadcrumb">
		    		<li class="breadcrumb-item"><a href="${pageContext.request.contextPath}">Home</a></li>
		    		<li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/usuario">Usu�rio</a></li>
		    		<li class="breadcrumb-item active">Cadastro</li>
		    	</ol>
			</div>
			<div class="page-content">
				<div class="row">
					<div class="col-xs-12 col-md-12">

						<spring:hasBindErrors name="usuario">
							<div class="alert alert-danger">
							<ul>
							<c:forEach var="error" items="${errors.allErrors}">
								<li><spring:message code="${error.code}" text="${error.defaultMessage}"/></li>
							</c:forEach>
							</ul>
							</div>
						</spring:hasBindErrors>
						
						<form:form action="${spring:mvcUrl('UC#salvar').build()}" method="post" modelAttribute="usuario" enctype="multipart/form-data" autocomplete="off" role="form">

							<form:input type="hidden" path="id"/>
							<form:input type="hidden" path="senha"/>
							
							<div class="row">
								<div class="col-sm-12 col-md-8">
									
									<div class="panel panel-bordered">
										<div class="panel-heading">
	                						<h3 class="panel-title">Informa��es B�sicas</h3>
	                					</div>	
	                					<div class="panel-body"> 
	                						<div class="row">
	                						 	<div class="col-xs-12 col-md-12">
	                						 		
	                						 		<div class="row">
	                						 			 
	                						 			<div class="col-xs-12 col-md-4">
	                						 				<div class="form-group">
	                						 					<label class="form-control-label" for="cliente">Neg�cio</label>
	                						 					<form:select path="negocio" class="form-control" data-plugin="select2">
	                						 						<c:forEach var="row" items="${negocioDS}">
	                						 							<option value="${row.id}" ${row.id == usuario.negocio.id ? "selected=selected" : null}>
	                						 								<c:if test="${row.tipo == 'PF'}">${row.pessoa.nome} ${row.pessoa.sobrenome}</c:if>
	                						 								<c:if test="${row.tipo == 'PJ'}">${row.pessoa.razao_social}</c:if>	                						 								
	                						 							</option>
	                						 						</c:forEach>
	                						 					</form:select>
	                						 				</div>
	                						 			</div>
	                						 			
	                						 			<div class="col-xs-12 col-md-4">
	                						 				<div class="form-group">
	                						 					<c:set var="e"><form:errors path="nome"/></c:set>
	                						 					<label class="form-control-label" for="nome">Nome</label>
	                						 					<form:input path="nome" class="form-control ${not empty e ? 'is-invalid' : null}" autocomplete="off" placeholder="Nome" />
	                						 				</div>
	                						 			</div>
	                						 			
	                						 			<div class="col-xs-12 col-md-4">
	                						 				<div class="form-group">
	                						 					<label class="form-control-label" for="nome">Sobrenome</label>
	                						 					<form:input path="sobrenome" class="form-control" autocomplete="off" placeholder="Sobrenome" />
	                						 				</div>
	                						 			</div>
	             
	                						 		</div>
	                						 		
	                						 		<div class="row">

             						 					<div class="col-xs-12 col-md-4">
	                						 				<div class="form-group">
	                						 					<c:set var="e"><form:errors path="email"/></c:set>
	                						 					<label class="form-control-label" for="email">E-mail</label>
	                						 					<form:input path="email" class="form-control ${not empty e ? 'is-invalid' : null}" />
	                						 				</div>
	                						 			</div>
	                						 			
             						 					<div class="col-xs-12 col-md-4">
	                						 				<div class="form-group">
	                						 					<c:set var="e"><form:errors path="login"/></c:set>
	                						 					<label class="form-control-label" for="login">Login</label>
	                						 					<form:input path="login" class="form-control ${not empty e ? 'is-invalid' : null}" />
	                						 				</div>
	                						 			</div>
	                						 			
	                						 			<div class="col-xs-12 col-md-4">
	                						 				<div class="form-group">
	                						 					<label class="form-control-label" for="status">Status</label>
	                						 					<form:select path="status" items="${usuarioStatusDS}" itemLabel="descricao" class="form-control" data-plugin="selectpicker" />
	                						 				</div>
	                						 			</div>
	                						 			
	                						 			<div class="col-xs-12 col-md-8">
	                						 				<div class="form-group">
	                						 					<label class="form-control-label" for="status">Permiss�es</label>
	                						 					<form:select path="regras" items="${regraDS}" itemValue="nome" itemLabel="descricao" class="form-control select2-primary" multiple="multiple" data-plugin="select2" />
	                						 				</div>
	                						 			</div>

	                						 		</div>

	                						 	</div>
	                						</div>
	                					</div>									
									</div>
									
								</div>
								<div class="col-sm-12 col-md-4">
								
									<div class="panel panel-bordered">
										<div class="panel-heading">
	                						<h3 class="panel-title">Imagem</h3>
	                					</div>	
	                					<div class="panel-body"> 
	                						<div class="row">
	                						 	<div class="col-xs-12 col-md-12">
	                						 		
	                						 		<div class="form-group text-center">
				                		    			<input type="file" name="file" id="file" 
				                		    				data-plugin="dropify" ${photo} 
				                		    				data-allowed-file-extensions="jpg jpeg png"
				                		    				data-max-file-size="2M"
				                		    				data-height="135"
				                		    				data-min-width="150"/>
			                		    				<small>clique sobre a imagem para alterar</small>
				            		    			</div>
	                						 		
	                						 	</div>
                						 	</div>
               						 	</div>
              						</div>
								
								</div>
							</div>

							<div class="row">
        						<div class="col-xs-12 col-md-8"><p>Ao clicar em salvar, as informa��es inseridas ser�o enviadas � base de dados.</p></div>
        						<div class="col-xs-12 col-md-4"><button class="btn btn-primary btn-block" type="submit">Salvar</button></div>
        					</div>

						</form:form>
						
					</div>
				</div>
			</div>
		</div>

	</jsp:attribute>
	
	<jsp:attribute name="css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/blueimp-file-upload/jquery.fileupload.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/dropify/dropify.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/select2/select2.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/bootstrap-select/bootstrap-select.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/custom/js/plugins/bootcomplete.js-master/dist/bootcomplete.css">
	</jsp:attribute>
	
	<jsp:attribute name="plugins">
		<script src="${pageContext.request.contextPath}/resources/custom/js/plugins/jquery.mask.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/vendor/dropify/dropify.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/vendor/select2/select2.full.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/vendor/bootstrap-select/bootstrap-select.js"></script>
		<script src="${pageContext.request.contextPath}/resources/custom/js/plugins/bootcomplete.js-master/dist/jquery.bootcomplete.js"></script>
	</jsp:attribute>
	
	<jsp:attribute name="scripts">
		<script src="${pageContext.request.contextPath}/resources/custom/js/usuario/cadastro.min.js"></script>
	</jsp:attribute>
	
</t:default>