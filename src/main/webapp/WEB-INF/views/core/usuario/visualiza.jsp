<!DOCTYPE html>

<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
	<head>
		<meta charset="UTF-8">
		<title>Sistema Website</title>
	</head>
	<body>
	
		<h1>Visualiza��o de Usu�rio: ${usuario.usuario_id}</h1>

		<table>
		
			<tr>
				<td>Nome:</td>
				<td>${usuario.nome} ${usuario.sobrenome}</td>
			</tr>
			<tr>
				<td>E-mail:</td>
				<td>${usuario.email}</td>
			</tr>
			<tr>
				<td>Login:</td>
				<td>${usuario.login}</td>
			</tr>
			<tr>
				<td>Neg�cio:</td>
				<td>${usuario.negocio_id}</td>
			</tr>
			<tr>
				<td>Status:</td>
				<td>${usuario.status}</td>
			</tr>
		</table>
		
		<a href="../">Retornar a Usu�rio</a>
		
	</body>
</html>