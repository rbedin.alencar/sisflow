<%@page import="java.util.Date"%>
<%@page import="br.com.sisflow.conf.SisflowConfiguration" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="storage"><%=SisflowConfiguration.getValue("client.storage")%></c:set>
<c:set var="timestamp"><%= System.currentTimeMillis() %></c:set>

<t:default>
	<jsp:attribute name="body">
		
		<div class="page">	
			<div class="page-header">
				<h1 class="page-title">Usu�rios</h1>
				<ol class="breadcrumb">
		    		<li class="breadcrumb-item"><a href="${pageContext.request.contextPath}">Home</a></li>
		    		<li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/usuario">Usu�rios</a></li>
		    		<li class="breadcrumb-item active">Visualiza��o</li>
		    	</ol>
		    	<div class="page-header-actions">
		    		<a href="${pageContext.request.contextPath}/usuario/cadastro" class="btn btn-sm btn-icon btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Novo Usu�rio</a>    	
		    	</div>
			</div>
			<div class="page-content">
				
				<c:if test="${msg != null}">
				<div class="alert alert-alt alert-primary alert-dismissible" role="alert">
					${msg}
					<button type="button" class="close" data-dismiss="alert" aria-label="Fechar"><span aria-hidden="true">�</span></button>
        		</div>
        		</c:if>
				
				<div class="panel">
					<header class="panel-heading">
						<div class="panel-actions"></div>
						<h3 class="panel-title">Ger�nciamento de Usu�rio</h3>
					</header>
					<div class="panel-body">
						
						<c:if test="${not empty usuarioDS}">
						<table class="table table-hover dataTable table-striped w-full" data-plugin="dataTable">
							<thead>
								<tr>
									<th class="text-left" width="20">&nbsp;</th>
		    						<th class="text-left">Usu�rio</th>
		    						<th class="text-left">E-mail</th>
		    						<th class="text-left">Login</th>
		    						<th class="text-left">Status</th>
		    						<th class="text-left">Neg�cio</th>
		    						<th class="text-left">Criado em</th>
		    						<th class="text-right">A��O</th>
								</tr>
							</thead>
							<tfoot>
		    					<tr>
									<th class="text-left" width="20">&nbsp;</th>
		    						<th class="text-left">Usu�rio</th>
		    						<th class="text-left">E-mail</th>
		    						<th class="text-left">Login</th>
		    						<th class="text-left">Status</th>
		    						<th class="text-left">Neg�cio</th>
		    						<th class="text-left">Criado em</th>
		    						<th class="text-right">A��O</th>
		    					</tr>
		    				</tfoot>
							<tbody>
								<c:forEach var="row" items="${usuarioDS}">
								<tr>
									<td class="align-middle">
									<span class="avatar"><img src="${storage}/usuario/thumb_${row.id}.jpg?t=${timestamp}" alt="" class="mg-fluid" /></span>
									</td>
									<td class="align-middle">${row.nome} ${row.sobrenome}</td>
									<td class="align-middle"><a href="mailto:${row.email}">${row.email}</a></td>
									<td class="align-middle">${row.login}</td>
									<td class="align-middle">${row.status.descricao}</td>
									<td class="align-middle">
										<c:if test="${row.negocio.tipo == 'PF'}">${row.negocio.pessoa.nome} ${row.negocio.pessoa.sobrenome}</c:if>
										<c:if test="${row.negocio.tipo == 'PJ'}">${row.negocio.pessoa.nome_fantasia}</c:if>
									</td>
									<td class="align-middle"><fmt:formatDate pattern="dd/MM/yyyy" value="${row.criado_em}"/></td>
									<td class="text-nowrap text-right align-middle">
										<a href="${pageContext.request.contextPath}/usuario/resetasenha/${row.id}" class="btn btn-sm btn-icon btn-flat btn-primary" data-toggle="tooltip" data-original-title="Resetar Senha"><i class="fa fa-key"></i></a>
										<a href="${pageContext.request.contextPath}/usuario/altera/${row.id}" class="btn btn-sm btn-icon btn-flat btn-primary" data-toggle="tooltip" data-original-title="Alterar"><i class="fa fa-pencil"></i></a>
										<a href="${pageContext.request.contextPath}/usuario/exclui/${row.id}" onclick="if(!confirm('Tem certeza que deseja excluir o registro?')) return false;" class="btn btn-sm btn-icon btn-flat btn-danger" data-toggle="tooltip" data-original-title="Excluir"><i class="fa fa-trash-o"></i></a>
									</td>
								</tr>
								</c:forEach>
							</tbody>
						</table>
						</c:if>
						
					</div>
				</div>
				
			</div>
		</div>
	</jsp:attribute>
	
	<jsp:attribute name="css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/datatables.net-bs4/dataTables.bootstrap4.css">
	</jsp:attribute>
	
	<jsp:attribute name="plugins">
		<script src="${pageContext.request.contextPath}/resources/global/vendor/datatables.net/jquery.dataTables.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/vendor/datatables.net-bs4/dataTables.bootstrap4.js"></script>
	</jsp:attribute>
	
	<jsp:attribute name="scripts">
		<script src="${pageContext.request.contextPath}/resources/custom/js/usuario/index.min.js"></script>
	</jsp:attribute>
	
</t:default>