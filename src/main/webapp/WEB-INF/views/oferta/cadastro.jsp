<%@page import="br.com.sisflow.conf.SisflowConfiguration" %>
<%@page import="java.io.File"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:default>
	<jsp:attribute name="body">
	
		<div class="page">	
			<div class="page-header">			
				<h1 class="page-title">Cadastro de Ofertas</h1>
				<ol class="breadcrumb">
		    		<li class="breadcrumb-item"><a href="${pageContext.request.contextPath}">Home</a></li>
		    		<li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/oferta">Ofertas</a></li>
		    		<li class="breadcrumb-item active">Cadastro</li>
		    	</ol>
			</div>
			<div class="page-content">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						
						<form:form id="cadastro" method="POST" action="${spring:mvcUrl('OC#salvar').build()}" enctype="multipart/form-data" modelAttribute="oferta" autocomplete="off" role="form">

							<spring:hasBindErrors name="oferta">
							<div class="alert alert-danger">
								<ul>
									<c:forEach var="error" items="${errors.allErrors}">
									<li><spring:message code="${error.code}" text="${error.defaultMessage}"/></li>
									</c:forEach>
								</ul>
							</div>
							</spring:hasBindErrors>
							
							<form:input type="hidden" path="id"/>

							<div class="row">
								<div class="col-sm-12 col-md-12">
									
									<div class="panel panel-bordered">
										<div class="panel-heading">
	                						<h3 class="panel-title">Informa��es B�sicas</h3>
	                					</div>	
	                					<div class="panel-body"> 
	                						<div class="row">
	                						 	<div class="col-xs-12 col-md-12">
	                						 		
	                						 		<div class="row">
	                						 		
	                						 			<div class="col-xs-12 col-md-2">
	                						 				<div class="form-group">
	                						 					<label class="form-control-label" for="id">C�digo</label>
	                						 					<form:input path="id" type="number" readonly="true" class="form-control" />
	                						 				</div>
	                						 			</div>
	                						 			
	                						 			<div class="col-xs-12 col-md-2">
	                						 				<div class="form-group">
	                						 					<label class="form-control-label" for="tipo">Tipo</label>
	                						 					<c:choose>
	                						 						<c:when test="${oferta.id == null}">
	                						 							<form:select path="tipo" items="${ofertaTipoDS}" itemLabel="descricao" class="form-control" data-plugin="selectpicker" />
	                						 						</c:when>
	                						 						<c:otherwise>
	                						 							<form:input path="tipo" type="text" readonly="true" class="form-control" />
	                						 						</c:otherwise>
	                						 					</c:choose>
	                						 				</div>
	                						 			</div>
	                						 			
		          						 				<div class="col-xs-12 col-md-2">
		               						 				<div class="form-group">
		               						 					<label class="form-control-label" for="referencia">Refer�ncia</label>
		                                                        <div class="input-group input-group-icon">
												                    <div class="input-group-prepend">
													                    <div class="input-group-text bg-white">
													                    	<i class="fa fa-tag" aria-hidden="true"></i>
													                    </div>
												                    </div>
												                    <c:set var="e"><form:errors path="referencia"/></c:set>
	                						 						<form:input path="referencia" class="form-control ${not empty e ? 'is-invalid' : null}" autocomplete="off" placeholder="Refer�ncia" />
		                                                        </div>
		               						 				</div>
		               						 			</div>	
		               						 			
	                						 			<div class="col-xs-12 col-md-2">
	                						 				<div class="form-group">
	                						 					<label class="form-control-label" for="status">Status</label>
	                						 					<form:select path="status" items="${ofertaStatusDS}" itemLabel="descricao" class="form-control" data-plugin="selectpicker" />
	                						 				</div>
	                						 			</div>
	                						 			
	                						 			<div class="col-xs-12 col-md-4">
	                						 				<div class="form-group">
	                						 					<label class="form-control-label" for="cliente">Neg�cio</label>
	                						 					<form:select path="negocio" class="form-control" data-plugin="select2">
	                						 						<c:forEach var="row" items="${negocioDS}">
	                						 							<option value="${row.id}" ${row.id == contaPagar.negocio.id ? "selected=selected" : null}>
	                						 								<c:if test="${row.tipo == 'PF'}">${row.pessoa.nome} ${row.pessoa.sobrenome}</c:if>
	                						 								<c:if test="${row.tipo == 'PJ'}">${row.pessoa.razao_social}</c:if>	                						 								
	                						 							</option>
	                						 						</c:forEach>
	                						 					</form:select>
	                						 				</div>
	                						 			</div>
	                						 			
                						 			</div>
                						 			
                						 			<!-- produto -->
                						 			<div class="row row-produto" class="hidden">
                						 			
	                						 			<div class="col-xs-12 col-md-4">
	                						 				<div class="form-group">
	                						 					<label class="form-control-label" for="codigo_barra">C�digo de Barra</label>
	                						 					<c:catch var="exception">${oferta.codigo_barra}</c:catch>
	                						 					<input type="text" name="codigo_barra" value='<c:if test="${empty exception}">${oferta.codigo_barra}</c:if>' class="form-control ${not empty e ? 'is-invalid' : null}" autocomplete="off" placeholder="C�digo de Barra" />
	                						 				</div>
	                						 			</div>
	                						 			
	                						 			<div class="col-xs-12 col-md-2">
	                						 				<div class="form-group">
	                						 					<label class="form-control-label" for="estoque_max">Estoque m�ximo</label>
	                						 					<c:catch var="exception">${oferta.estoque_max}</c:catch>
	                						 					<input type="number" name="estoque_max" value='<c:if test="${empty exception}">${oferta.estoque_max}</c:if>' class="form-control ${not empty e ? 'is-invalid' : null}" autocomplete="off" placeholder="Estoque m�ximo" />
	                						 				</div>
	                						 			</div>
	                						 			
	                						 			<div class="col-xs-12 col-md-2">
	                						 				<div class="form-group">
	                						 					<label class="form-control-label" for="estoque_min">Estoque m�nimo</label>
	                						 					<c:catch var="exception">${oferta.estoque_min}</c:catch>
	                						 					<input type="number" name="estoque_min" value='<c:if test="${empty exception}">${oferta.estoque_min}</c:if>' class="form-control ${not empty e ? 'is-invalid' : null}" autocomplete="off" placeholder="Estoque m�nimo" />
	                						 				</div>
	                						 			</div>
	                						 			
	                						 			<div class="col-xs-12 col-md-4">
	                						 				<div class="form-group">
	                						 					<label class="form-control-label" for="ativo">Controle de v�lidade</label><br/>
	                						 					<div class="float-left" style="margin-top: 3px;">
	                						 						<input type="checkbox" name="controle_validade" value="1" class="form-control" data-plugin="switchery"/>	                						 						
	                						 					</div>
	                						 				</div>
	                						 			</div>
                						 			
                						 			</div>
                						 			
                						 			<div class="row">

	                						 			<div class="col-xs-12 col-md-8">
	                						 				<div class="form-group">
	                						 					<c:set var="e"><form:errors path="nome"/></c:set>
	                						 					<label class="form-control-label" for="descricao">Nome</label>
	                						 					<form:input path="nome" class="form-control ${not empty e ? 'is-invalid' : null}" autocomplete="off" placeholder="Nome" maxlength="128" />
	                						 				</div>
	                						 			</div>
	                						 			
		          						 				<div class="col-xs-12 col-md-4">
		               						 				<div class="form-group">
		               						 					<label class="form-control-label" for="valor">Valor</label>
		                                                        <div class="input-group input-group-icon">
												                    <div class="input-group-prepend">
													                    <div class="input-group-text bg-white">R$</div>
												                    </div>
												                    <fmt:formatNumber value="${oferta.valor}" var="valor" minFractionDigits="2" maxFractionDigits="10"/>
		                                                        	<form:input path="valor" class="form-control" value="${valor}" autocomplete="off" placeholder="Valor" />
		                                                        </div>
		               						 				</div>
		               						 			</div>	
	                						 			
	                						 			<div class="col-xs-12 col-md-12">
	                						 				<div class="form-group">
	                						 					<c:set var="e"><form:errors path="descricao"/></c:set>
	                						 					<label class="form-control-label" for="descricao">Descri��o</label>
	                						 					<form:input path="descricao" class="form-control ${not empty e ? 'is-invalid' : null}" autocomplete="off" placeholder="Descri��o" />
	                						 				</div>
	                						 			</div>

	                						 		</div>

	                						 	</div>
	                						</div>
	                					</div>									
									</div>
									
								</div>
								
							</div>

							<div class="row">
        						<div class="col-xs-12 col-md-8"><p>Ao clicar em salvar, as informa��es inseridas ser�o enviadas � base de dados.</p></div>
        						<div class="col-xs-12 col-md-4"><button class="btn btn-primary btn-block" type="submit"><i class="fa fa-floppy-o"></i> Salvar</button></div>
        					</div>

						</form:form>
						
					</div>
				</div>
			</div>
		</div>

	</jsp:attribute>
	
	<jsp:attribute name="css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/blueimp-file-upload/jquery.fileupload.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/dropify/dropify.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/select2/select2.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/bootstrap-select/bootstrap-select.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/custom/js/plugins/bootcomplete.js-master/dist/bootcomplete.css">
	</jsp:attribute>
	
	<jsp:attribute name="plugins">
		<script src="${pageContext.request.contextPath}/resources/custom/js/plugins/jquery.mask.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/vendor/dropify/dropify.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/vendor/select2/select2.full.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/vendor/bootstrap-select/bootstrap-select.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/vendor/bootstrap-datepicker/bootstrap-datepicker.pt-BR.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/custom/js/plugins/bootcomplete.js-master/dist/jquery.bootcomplete.js"></script>
	</jsp:attribute>
	
	<jsp:attribute name="scripts">
		<script src="${pageContext.request.contextPath}/resources/custom/js/oferta/cadastro.min.js"></script>
	</jsp:attribute>
	
</t:default>