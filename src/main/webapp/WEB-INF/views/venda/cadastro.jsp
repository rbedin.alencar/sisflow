<%@page import="br.com.sisflow.conf.SisflowConfiguration" %>
<%@page import="java.io.File"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:default>
	<jsp:attribute name="body">
	
		<div class="page">	
			<div class="page-header">			
				<h1 class="page-title">Cadastro de Vendas</h1>
				<ol class="breadcrumb">
		    		<li class="breadcrumb-item"><a href="${pageContext.request.contextPath}">Home</a></li>
		    		<li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/venda">Venda</a></li>
		    		<li class="breadcrumb-item active">Cadastro</li>
		    	</ol>
			</div>
			<div class="page-content">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						
						<form:form id="cadastro" method="POST" action="${spring:mvcUrl('VC#salvar').build()}" enctype="multipart/form-data" modelAttribute="venda" autocomplete="off" role="form">

							<spring:hasBindErrors name="venda">
							<div class="alert alert-danger">
								<ul>
									<c:forEach var="error" items="${errors.allErrors}">
									<li><spring:message code="${error.code}" text="${error.defaultMessage}"/></li>
									</c:forEach>
								</ul>
							</div>
							</spring:hasBindErrors>
							
							<form:input type="hidden" path="id"/>

							<div class="row">
								<div class="col-sm-12 col-md-12">
									
									<div class="panel panel-bordered">
										<div class="panel-heading">
	                						<h3 class="panel-title">Informa��es B�sicas</h3>
	                					</div>	
	                					<div class="panel-body"> 
	                						<div class="row">
	                						 	<div class="col-xs-12 col-md-12">
	                						 		
	                						 		<div class="row">
	                						 		
	                						 			<div class="col-xs-12 col-md-2">
	                						 				<div class="form-group">
	                						 					<label class="form-control-label" for="id">C�digo</label>
	                						 					<form:input path="id" type="number" readonly="true" class="form-control" />
	                						 				</div>
	                						 			</div>
	                						 			
	                						 			<div class="col-xs-12 col-md-3">
	                						 				<div class="form-group">
	                						 					<label class="form-control-label" for="cliente">Cliente</label>
	                						 					<form:select path="cliente" class="form-control" data-plugin="select2">
	                						 						<c:forEach var="row" items="${clienteDS}">
	                						 							<option value="${row.id}" ${row.id == venda.cliente.id ? "selected=selected" : null}>
	                						 								<c:if test="${row.tipo == 'PF'}">${row.pessoa.nome} ${row.pessoa.sobrenome}</c:if>
	                						 								<c:if test="${row.tipo == 'PJ'}">${row.pessoa.razao_social}</c:if>	                						 								
	                						 							</option>
	                						 						</c:forEach>
	                						 					</form:select>
	                						 				</div>
	                						 			</div>
	                						 			
	                						 			<div class="col-xs-12 col-md-3">
	                						 				<div class="form-group">
	                						 					<label class="form-control-label" for="cliente">Neg�cio</label>
	                						 					<form:select path="negocio" class="form-control" data-plugin="select2">
	                						 						<c:forEach var="row" items="${negocioDS}">
	                						 							<option value="${row.id}" ${row.id == venda.negocio.id ? "selected=selected" : null}>
	                						 								<c:if test="${row.tipo == 'PF'}">${row.pessoa.nome} ${row.pessoa.sobrenome}</c:if>
	                						 								<c:if test="${row.tipo == 'PJ'}">${row.pessoa.razao_social}</c:if>	                						 								
	                						 							</option>
	                						 						</c:forEach>
	                						 					</form:select>
	                						 				</div>
	                						 			</div>
	                						 			
	                						 			<div class="col-xs-12 col-md-2">
	                						 				<div class="form-group">
	                						 					<label class="form-control-label" for="usuario_responsavel">Respons�vel</label>
	                						 					<form:select path="usuario" items="${usuarioDS}" itemLabel="nome" itemValue="id" class="form-control" data-plugin="select2" />
	                						 				</div>
	                						 			</div>
	                						 			
		               						 			<div class="col-xs-12 col-md-2">
		               						 				<div class="form-group">
		               						 					<label class="form-control-label" for="data_venda">Data da venda</label>
		                                                        <div class="input-group input-group-icon">
												                    <div class="input-group-prepend">
													                    <div class="input-group-text">
													                    	<i class="fa fa-calendar" aria-hidden="true"></i>
													                    </div>
												                    </div>
												                    <fmt:formatDate value="${venda.data_venda}" var="data_venda" pattern="dd/MM/yyyy"/>
		                                                        	<form:input path="data_venda" value="${data_venda}" class="form-control" autocomplete="off" data-plugin="datepicker" />
		                                                        </div>
		               						 				</div>
		               						 			</div>

                						 			</div>
                						 			
                						 			<div class="row">
                						 			
	                						 			<div class="col-xs-12 col-md-12">
	                						 				<div class="form-group">
	                						 					<c:set var="e"><form:errors path="observacao"/></c:set>
	                						 					<label class="form-control-label" for="descricao">Obser��o</label>
	                						 					<form:input path="observacao" class="form-control ${not empty e ? 'is-invalid' : null}" autocomplete="off" placeholder="Observa��o" />
	                						 				</div>
	                						 			</div>
	                						 			
                						 			</div>

	                						 	</div>
	                						</div>
	                					</div>									
									</div>
									
								</div>
								
							</div>
							
							<!-- VENDA.OFERTA -->
							<div class="row">
								<div class="col-sm-12 col-md-12">
									
									<div class="panel panel-bordered panel-venda-oferta">
										<div class="panel-heading">
	                						<h3 class="panel-title">Produtos/Servi�os</h3>
	                					</div>	
	                					<div class="panel-body"> 
	                						
	                						<!-- SEARCH -->
		                					<div class="row">
		                						<div class="col-sm-12">
		                							<div class="form-group">
               						 					<label class="form-control-label" for="oferta">Ref./Produto/Servi�o</label>
               						 					<input type="text" name="oferta" class="form-control typeahead" autocomplete="off" placeholder="Produto/Servi�o" data-provide="typeahead" />
               						 				</div>
		                						
		                						</div>
	                						</div>
	                						<!-- SEARCH -->
	                						
		                					<!-- template row -->
		                					<div class="row template" style="display: none;">
		                					
		                						<div class="col-sm-12 col-md-5">
		                							<div class="form-group">
		                								<label class="form-control-label">Produto/Servi�o</label>
		                								<input type="text" name="oferta_nome[]" disabled class="form-control" />
		                								<input type="hidden" name="oferta_id[]" class="form-control" />
		                							</div>
		                						</div>
		                						
		                						<div class="col-sm-12 col-md-2">
		                							<div class="form-group">
		                								<label class="form-control-label" for="quantidade">Quantidade</label>
		                                        		<input type="text" name="oferta_quantidade[]" class="form-control" autocomplete="off" placeholder="Quantidade" value="1" />
		                							</div>
		                						</div>
		                						
		                						<div class="col-sm-12 col-md-2">
		                							<div class="form-group">
		                								<label class="form-control-label">Valor</label>
		                                        		<input type="text" name="oferta_valor[]" class="form-control" autocomplete="off" placeholder="Valor" value="0" />
		                							</div>
		                						</div>
		                						
		                						<div class="col-sm-12 col-md-2">
		                							<div class="form-group">
		                								<label class="form-control-label">Subtotal</label>
		                                        		<input type="text" name="oferta_subtotal[]" class="form-control" autocomplete="off" placeholder="Subtotal" value="0" />
		                							</div>
		                						</div>

		                						<div class="col-xs-12 col-md-1">
		                							<div class="form-group">
		                    							<label class="form-control-label">&nbsp;</label>
		                    							<a href="#" class="btn btn-icon btn-block btn-danger btn-remover"><i class="fa fa-trash"></i></a>
		                							</div>
		            							</div>
		                						
		                					</div>
		                					<!-- end template -->

		                					<ul class="list-group list-group-dividered list-group-full list-venda-oferta">
		                					<c:forEach items="${venda.vendaOfertas}" var="row" varStatus="status">
		                					
		                						<li class="list-group-item">
				                					<div class="row template">
				                					
				                						<div class="col-sm-12 col-md-5">
				                							<div class="form-group">
				                								<label class="form-control-label">Produto/Servi�o</label>
				                								<input type="text" name="oferta_nome[]" value="${row.oferta.nome}" disabled class="form-control" />
				                								<input type="hidden" name="oferta_id[]" value="${row.id}" class="form-control" />
				                							</div>
				                						</div>
				                						
				                						<div class="col-sm-12 col-md-2">
				                							<div class="form-group">
				                								<label class="form-control-label" for="quantidade">Quantidade</label>
				                                        		<input type="text" name="oferta_quantidade[]" class="form-control" autocomplete="off" placeholder="Quantidade" value="${row.quantidade}" />
				                							</div>
				                						</div>
				                						
				                						<div class="col-sm-12 col-md-2">
				                							<div class="form-group">
				                								<label class="form-control-label">Valor</label>
				                                        		<input type="text" name="oferta_valor[]" class="form-control" autocomplete="off" placeholder="Valor" value="${row.valor}" />
				                							</div>
				                						</div>
				                						
				                						<div class="col-sm-12 col-md-2">
				                							<div class="form-group">
				                								<label class="form-control-label">Subtotal</label>
				                                        		<input type="text" name="oferta_subtotal[]" class="form-control" autocomplete="off" placeholder="Subtotal" value="${row.subtotal}" />
				                							</div>
				                						</div>
		
				                						<div class="col-xs-12 col-md-1">
				                							<div class="form-group">
				                    							<label class="form-control-label">&nbsp;</label>
				                    							<a href="#" class="btn btn-icon btn-block btn-danger btn-remover"><i class="fa fa-trash"></i></a>
				                							</div>
				            							</div>
				                						
				                					</div>
		                						</li>
		                						
		                					</c:forEach>
		                					</ul>
		                					
	                						
                						</div>
               						</div>
               						
            					</div>
							</div>													
							<!-- VENDA.OFERTA -->

							<!-- CAIXA -->
							<div class="row">

								<div class="col-sm-12 col-md-3">
									<div class="card card-inverse bg-success card-total">
										<div class="card-block">
											<h4 class="card-title">R$<span>0,00</span></h4>
											<p class="card-text">Valor Total</p>
										</div>
									</div>
            					</div>

								<div class="col-sm-12 col-md-9">
									
									<div class="panel panel-bordered panel-venda-pgto">
										<div class="panel-heading">
		                					<h3 class="panel-title">Pagamento</h3>
		                					<div class="panel-actions">
		                						<a href="javascript:void(0);" class="btn btn-primary btn-sm" data-btn="add"><i class="fa fa-plus"></i> Novo Pagamento</a>
		                					</div>
		                				</div>	
	                					<div class="panel-body"> 
	                					
		                					<!-- template row -->
		                					<div class="row template" style="display: none;">
		                					
		                						<div class="col-xs-12 col-md-4">
		                							<div class="form-group">
		                								<label class="form-control-label">Forma de Pagamento</label>
		                                                <div class="input-group input-group-icon">
										                    <div class="input-group-prepend">
											                    <div class="input-group-text">
											                    	<i class="fa fa-credit-card" aria-hidden="true"></i>
											                    </div>
										                    </div>
										                    <select name="pgto_tipo[]" class="form-control">
										                    	<c:forEach items="${vendaPgtoTipoDS}" var="row">
										                    		<option value="${row}">${row.descricao}</option>
										                    	</c:forEach>
										                    </select>
		                                                </div>
		            								</div>
		                						</div>
		                						<div class="col-xs-12 col-md-2">
		                							<div class="form-group">
		                								<label class="form-control-label">Valor</label>
		                								<input type="text" name="pgto_valor[]" value="" class="form-control" placeholder="Valor" autocomplete="off" disabled="disabled" />
		            								</div>
		                						</div>
		                						<div class="col-xs-12 col-md-1">
		                							<div class="form-group">
		                								<label class="form-control-label">Parcelas</label>
		                								<input type="text" name="pgto_parcelas[]" value="1" class="form-control" placeholder="Qtd." autocomplete="off" disabled="disabled" />
		            								</div>
		                						</div>
		                						<div class="col-xs-12 col-md-4">
		                							<div class="form-group">
		                								<label class="form-control-label">Observa��o</label>
		                								<input type="text" name="pgto_observacao[]" value="" class="form-control" placeholder="Observa��o" autocomplete="off" disabled="disabled" />
		            								</div>
		                						</div>
		                						<div class="col-xs-12 col-md-1">
		                							<div class="form-group">
		                    							<label class="form-control-label">&nbsp;</label>
		                    							<a href="#" class="btn btn-icon btn-block btn-danger btn-remover"><i class="fa fa-trash"></i></a>
		                							</div>
		            							</div>
		                					</div>
		                					<!-- end template -->
	                						
	                						
                							<ul class="list-group list-group-dividered list-group-full list-venda-pgto"></ul>
		                						                					                						
                						</div>
               						</div>
               						
            					</div>
							</div>


							<div class="row">
        						<div class="col-xs-12 col-md-8"><p>Ao clicar em salvar, as informa��es inseridas ser�o enviadas � base de dados.</p></div>
        						<div class="col-xs-12 col-md-4"><button class="btn btn-primary btn-block" type="submit"><i class="fa fa-floppy-o"></i> Salvar</button></div>
        					</div>

						</form:form>
						
					</div>
				</div>
			</div>
		</div>

	</jsp:attribute>
	
	<jsp:attribute name="css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/blueimp-file-upload/jquery.fileupload.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/dropify/dropify.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/select2/select2.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/bootstrap-select/bootstrap-select.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">		
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/bootstrap-touchspin/bootstrap-touchspin.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/typeahead-js/typeahead.css">
		
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/examples/css/forms/advanced.css">
		<!-- 
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/custom/js/plugins/bootcomplete.js-master/dist/bootcomplete.css">
		-->
	</jsp:attribute>
	
	<jsp:attribute name="plugins">
		<script src="${pageContext.request.contextPath}/resources/custom/js/plugins/jquery.mask.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/vendor/dropify/dropify.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/vendor/select2/select2.full.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/vendor/bootstrap-select/bootstrap-select.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/vendor/bootstrap-datepicker/bootstrap-datepicker.pt-BR.min.js"></script>		
		<script src="${pageContext.request.contextPath}/resources/global/vendor/bootstrap-touchspin/bootstrap-touchspin.min.js"></script>
		
		
		<script src="${pageContext.request.contextPath}/resources/global/vendor/typeahead-js/bloodhound.min.js"></script>	
		<script src="${pageContext.request.contextPath}/resources/global/vendor/typeahead-js/typeahead.jquery.min.js"></script>		
		<!-- 
		<script src="${pageContext.request.contextPath}/resources/custom/js/plugins/bootstrap3-typeahead/bootstrap3-typeahead.min.js"></script>
		-->		
	</jsp:attribute>
	
	<jsp:attribute name="scripts">
		<script src="${pageContext.request.contextPath}/resources/custom/js/venda/cadastro.min.js"></script>
	</jsp:attribute>
	
</t:default>