<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:default>
	<jsp:attribute name="body">
		
		<div class="page">	
			<div class="page-header">
				<h1 class="page-title">Contas a receber</h1>
				<ol class="breadcrumb">
		    		<li class="breadcrumb-item"><a href="${pageContext.request.contextPath}">Home</a></li>
		    		<li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/contas">Movimentações</a></li>
		    		<li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/contas/receber">Contas a receber</a></li>
		    		<li class="breadcrumb-item active">Visualização</li>
		    	</ol>
		    	<div class="page-header-actions">
		    		<a href="${pageContext.request.contextPath}/contas/receber/cadastro" class="btn btn-sm btn-icon btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Nova Receita</a>    	
		    	</div>
			</div>
			<div class="page-content">
			
				<c:if test="${msg != null}">
				<div class="alert alert-alt alert-primary alert-dismissible" role="alert">
					${msg}
					<button type="button" class="close" data-dismiss="alert" aria-label="Fechar"><span aria-hidden="true">×</span></button>
        		</div>
        		</c:if>
			
				<div class="panel">
					<header class="panel-heading">
						<div class="panel-actions"></div>
						<h3 class="panel-title">Gerênciamento de Contas a receber</h3>
					</header>
					<div class="panel-body">
						
						<c:if test="${not empty contaReceberDS}">
						<table class="table table-hover dataTable table-striped w-full" data-plugin="dataTable">
							<thead>
								<tr>
									<th class="text-left">Recebimento</th>
		    						<th class="text-left">Descrição</th>
		    						<th class="text-left">Documento</th>
		    						<th class="text-left">Valor</th>
		    						<th class="text-left">Tipo</th>
		    						<th class="text-left">Status</th>
		    						<th class="text-right"></th>
								</tr>
							</thead>
							<tfoot>
		    					<tr>
									<th class="text-left">Recebimento</th>
		    						<th class="text-left">Descrição</th>
		    						<th class="text-left">Documento</th>
		    						<th class="text-left">Valor</th>
		    						<th class="text-left">Tipo</th>
		    						<th class="text-left">Status</th>
		    						<th class="text-right"></th>
		    					</tr>
		    				</tfoot>
							<tbody>
								<c:forEach var="row" items="${contaReceberDS}">
								<tr>
									<td class="align-middle">
										<i class="fa fa-calendar mr-10" aria-hidden="true"></i>
										<span><fmt:formatDate pattern="dd/MM/yyyy" value="${row.data_recebimento}"/></span>
									</td>
									<td class="align-middle">${row.descricao}</td>
									<td class="align-middle">${row.documento}</td>
									<td class="align-middle">R$ <fmt:formatNumber pattern="#,##0.00" value="${row.valor}" type="currency" currencySymbol="R$" /></td>
									<td class="align-middle">${row.tipo.descricao}</td>
									<td class="align-middle">
										<c:if test="${row.recebido}"><div class="badge badge-table badge-success">Recebido</div></c:if>
										<c:if test="${not row.recebido}"><div class="badge badge-table badge-danger">Pendente</div></c:if>
									</td>
									<td class="text-nowrap text-right align-middle">
										<a href="${pageContext.request.contextPath}/contas/receber/altera/${row.id}" class="btn btn-sm btn-icon btn-flat btn-primary" data-toggle="tooltip" data-original-title="Alterar"><i class="fa fa-pencil"></i></a>
										<a href="${pageContext.request.contextPath}/contas/receber/exclui/${row.id}" onclick="if(!confirm('Tem certeza que deseja excluir o registro?')) return false;" class="btn btn-sm btn-icon btn-flat btn-danger" data-toggle="tooltip" data-original-title="Excluir"><i class="fa fa-trash-o"></i></a>
									</td>
								</tr>
								</c:forEach>
							</tbody>
						</table>
						</c:if>
						
					</div>
				</div>
				
			</div>
		</div>
	</jsp:attribute>
	
	<jsp:attribute name="css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/datatables.net-bs4/dataTables.bootstrap4.css">
	</jsp:attribute>
	
	<jsp:attribute name="plugins">
		<script src="${pageContext.request.contextPath}/resources/global/vendor/datatables.net/jquery.dataTables.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/vendor/datatables.net-bs4/dataTables.bootstrap4.js"></script>
	</jsp:attribute>
	
	<jsp:attribute name="scripts">
		<script src="${pageContext.request.contextPath}/resources/custom/js/contas/receber/index.min.js"></script>
	</jsp:attribute>
	
</t:default>