<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<t:default>
	<jsp:attribute name="body">
		
		<div class="page">	
			<div class="page-header">
				<h1 class="page-title">Contas</h1>
				<ol class="breadcrumb">
		    		<li class="breadcrumb-item"><a href="${pageContext.request.contextPath}">Home</a></li>
		    		<li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/contas">Movimentta��es</a></li>
		    		<li class="breadcrumb-item active">Visualiza��o</li>
		    	</ol>
		    	<div class="page-header-actions">    	
		    	</div>
			</div>
			<div class="page-content">
			
				<!-- search -->
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-bordered">
							<div class="panel-heading">
								<h3 class="panel-title">Filtro</h3>
								<div class="panel-actions panel-actions-keep">
									<a class="panel-action icon wb-minus" data-toggle="panel-collapse" aria-hidden="true"></a>
								</div>
							</div>
							<div class="panel-body">
							
								<form:form id="filtro" method="GET" action="${pageContext.request.contextPath}/contas" modelAttribute="filtro" autocomplete="off" role="form">
									
									<div class="row">
       						 			<div class="col-xs-12 col-md-2">
       						 				<div class="form-group">
       						 					<label class="form-control-label" for="mes">M�s</label>
       						 					<form:select path="mes" items="${mesDS}" class="form-control" data-plugin="selectpicker"/>
       						 				</div>
       						 			</div>
       						 			
       						 			<div class="col-xs-12 col-md-1">
       						 				<div class="form-group">
       						 					<label class="form-control-label" for="ano">Ano</label>
       						 					<form:select path="ano" items="${anoDS}" class="form-control" data-plugin="selectpicker"/>
       						 				</div>
       						 			</div>
       						 			
       						 			<div class="col-xs-12 col-md-1">
       						 				<div class="form-group">
	       						 				<label class="form-control-label" for="submit">Filtrar</label>
	       						 				<button id="submit" class="btn btn-primary btn-block" type="submit"><i class="fa fa-search mr-5"></i> Filtrar</button>
       						 				</div>
       						 			</div>
       						 			
									</div>
								</form:form>
							
								<!-- 
								<form id="filtro" method="GET" action="${pageContext.request.contextPath}/module/conta" autocomplete="off" role="form">
									
									<div class="row">
										
       						 			<div class="col-xs-12 col-md-2">
       						 				<div class="form-group">
       						 					<label class="form-control-label" for="mes">M�s</label>
       						 					<select name="mes" class="form-control" data-plugin="selectpicker">
       						 						<c:forEach var="row" items="${mesDS}">
       						 							<option value="${row.key}">${row.value}</option>
       						 						</c:forEach>
       						 					</select>
       						 				</div>
       						 			</div>
       						 			
       						 			<div class="col-xs-12 col-md-1">
       						 				<div class="form-group">
       						 					<label class="form-control-label" for="ano">Ano</label>
       						 					<select name="ano" class="form-control" data-plugin="selectpicker">
       						 					
       						 						<jsp:useBean id="date" class="java.util.Date" />
       						 						<fmt:formatDate value="${date}" pattern="yyyy" var="ano" />
       						 						
       						 						<c:forEach var="row" items="${anoDS}">
       						 							<option value="${row.key}" ${row.key == ano ? "selected=selected" : null}>${row.value}</option>
       						 						</c:forEach>
       						 					</select>
       						 				</div>
       						 			</div>       						 													
										
       						 			<div class="col-xs-12 col-md-2">
       						 				<div class="form-group">
       						 					<label class="form-control-label" for="tipo">Tipo</label>
       						 					<select name="tipo" class="form-control" data-plugin="selectpicker">
       						 						<option value="0">Todos os tipos</option>
       						 						<option value="1">Contas a pagar</option>
       						 						<option value="2">Contas a receber</option>
       						 					</select>
       						 				</div>
       						 			</div>
       						 			
       						 			<div class="col-xs-12 col-md-2">
       						 				<div class="form-group">
       						 					<label class="form-control-label" for="tipo">Status</label>
       						 					<select name="tipo" class="form-control" data-plugin="selectpicker">
       						 						<option value="0">Todos</option>
       						 						<option value="true">Finalizados</option>
       						 						<option value="false">Pendentes</option>
       						 					</select>
       						 				</div>
       						 			</div>
										
       						 			<div class="col-xs-12 col-md-4">
       						 				<div class="form-group">
       						 					<label class="form-control-label" for="negocio">Neg�cio</label>
       						 					<select name="negocio" class="form-control" data-plugin="select2">
       						 						<c:forEach var="row" items="${negocioDS}">
       						 							<option value="${row.id}" ${row.id == contaPagar.negocio.id ? "selected=selected" : null}>
       						 								<c:if test="${row.tipo == 'PF'}">${row.pessoa.nome} ${row.pessoa.sobrenome}</c:if>
       						 								<c:if test="${row.tipo == 'PJ'}">${row.pessoa.razao_social}</c:if>	                						 								
       						 							</option>
       						 						</c:forEach>
       						 					</select>
       						 				</div> 
       						 			</div>
       						 			
       						 			<div class="col-xs-12 col-md-1">
       						 				<div class="form-group">
	       						 				<label class="form-control-label" for="submit">Filtrar</label>
	       						 				<button id="submit" class="btn btn-primary btn-block" type="submit"><i class="fa fa-search mr-5"></i> Filtrar</button>
       						 				</div>
       						 			</div>
										
									</div>
																		
								</form>	
								 -->													
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					
					<div class="col-sm-4">
						<div class="card card-block p-20 bg-blue-600">
							<div class="card-watermark darker font-size-40 m-15">
								<i class="icon fa fa-usd" aria-hidden="true"></i>
							</div>
							<div class="counter counter-md counter-inverse text-left">
								<div class="counter-number-group">
									<span class="counter-number-related text-capitalize">+ R$</span>
									<span class="counter-number"><fmt:formatNumber pattern="#,##0.00" value="${totalContaReceber}" type="currency" currencySymbol="R$"/></span>
								</div>
								<div class="counter-label">Total de recebimentos.</div>
							</div>
						</div>
					</div>
					
					<div class="col-sm-4">
						<div class="card card-block p-20 bg-red-600">
							<div class="card-watermark darker font-size-40 m-15">
								<i class="icon fa fa-usd" aria-hidden="true"></i>
							</div>
							<div class="counter counter-md counter-inverse text-left">
								<div class="counter-number-group">
									<span class="counter-number-related text-capitalize">- R$</span>
									<span class="counter-number"><fmt:formatNumber pattern="#,##0.00" value="${totalContaPagar}" type="currency" currencySymbol="R$"/></span>
								</div>
								<div class="counter-label">Total de contas a pagar.</div>
							</div>
						</div>
					</div>
				
					<div class="col-sm-4">
						<div class="card card-block p-20 bg-green-600">
							<div class="card-watermark darker font-size-40 m-15">
								<i class="icon fa fa-usd" aria-hidden="true"></i>
							</div>
							<div class="counter counter-md counter-inverse text-left">
								<div class="counter-number-group">
									<span class="counter-number-related text-capitalize">R$</span>
									<span class="counter-number"><fmt:formatNumber pattern="#,##0.00" value="${totalConta}" type="currency" currencySymbol="R$"/></span>
								</div>
								<div class="counter-label">Saldo atual.</div>
							</div>
						</div>
					</div>

				</div>

				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-bordered">
							<div class="panel-heading">
								<h3 class="panel-title">Contas a pagar</h3>
								<div class="panel-actions">
									<div class="dropdown">
										<a class="btn btn-primary btn-sm" href="${pageContext.request.contextPath}/contas/pagar/cadastro"><i class="fa fa-plus mr-5" aria-hidden="true"></i> Nova conta</a>
										
										<c:if test="${not empty contaPagarDS}">
										<a class="btn btn-default btn-sm" href="${pageContext.request.contextPath}/contas/pagar/status"><i class="fa fa-check mr-5" aria-hidden="true"></i> Status</a>
										<a class="btn btn-danger btn-sm" href="${pageContext.request.contextPath}/contas/pagar/exclui"><i class="fa fa-trash mr-5" aria-hidden="true"></i> Excluir</a>
										</c:if>
									</div>
								</div>
							</div>
							<div class="panel-body">
								
								<c:if test="${not empty contaPagarDS}">
								<table class="table table-hover dataTable table-striped w-full" data-plugin="dataTable">
									<thead>
										<tr>
											<th class="text-center w-50">
												<span class="checkbox-custom checkbox-primary">
													<input class="selectable-all" type="checkbox">
													<label></label>
												</span>
											</th>
											<th class="text-left">Vencimento</th>
				    						<th class="text-left">Descri��o</th>
				    						<th class="text-left">Documento</th>
				    						<th class="text-left">Valor</th>
				    						<th class="text-left">Tipo</th>
				    						<th class="text-left">Status</th>
				    						<th class="text-right"></th>
										</tr>
									</thead>
									<tfoot>
				    					<tr>
											<th class="text-left"></th>
											<th class="text-left">Vencimento</th>
				    						<th class="text-left">Descri��o</th>
				    						<th class="text-left">Documento</th>
				    						<th class="text-left">Valor</th>
				    						<th class="text-left">Tipo</th>
				    						<th class="text-left">Status</th>
				    						<th class="text-right"></th>
				    					</tr>
				    				</tfoot>
									<tbody>
										<c:forEach var="row" items="${contaPagarDS}">
										<tr>
											<td class="align-middle text-left">
												<span class="checkbox-custom checkbox-primary">
													<input class="selectable-item" type="checkbox" id="row-${row.id}" value="${row.id}">
													<label for="row-${row.id}"></label>
												</span>
											</td>
											<td class="align-middle">
												<i class="fa fa-calendar mr-10" aria-hidden="true"></i>
												<span><fmt:formatDate pattern="dd/MM/yyyy" value="${row.data_vencimento}"/></span>
											</td>
											<td class="align-middle">${row.descricao}</td>
											<td class="align-middle">${row.documento}</td>
											<td class="align-middle">R$ <fmt:formatNumber pattern="#,##0.00" value="${row.valor}" type="currency" currencySymbol="R$" /></td>
											<td class="align-middle">${row.tipo.descricao}</td>
											<td class="align-middle">
												<c:if test="${row.cobranca_paga}"><div class="badge badge-table badge-success">Paga</div></c:if>
												<c:if test="${not row.cobranca_paga}"><div class="badge badge-table badge-danger">Pendente</div></c:if>
											</td>
											<td class="text-nowrap text-right align-middle">
												<a href="${pageContext.request.contextPath}/contas/pagar/altera/${row.id}" class="btn btn-sm btn-icon btn-flat btn-primary" data-toggle="tooltip" data-original-title="Alterar"><i class="fa fa-pencil"></i></a>
												<a href="${pageContext.request.contextPath}/contas/pagar/exclui/${row.id}" onclick="if(!confirm('Tem certeza que deseja excluir o registro?')) return false;" class="btn btn-sm btn-icon btn-flat btn-danger" data-toggle="tooltip" data-original-title="Excluir"><i class="fa fa-trash-o"></i></a>
											</td>
										</tr>
										</c:forEach>
									</tbody>
								</table>
								</c:if>
								
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-bordered">
							<div class="panel-heading">
								<h3 class="panel-title">Contas a receber</h3>
								<div class="panel-actions">
									<div class="dropdown">
										<a class="btn btn-primary btn-sm" href="${pageContext.request.contextPath}/contas/receber/cadastro"><i class="fa fa-plus mr-5" aria-hidden="true"></i> Novo recebimento</a>
										
										<c:if test="${not empty contaReceberDS}">
										<a class="btn btn-default btn-sm" href="${pageContext.request.contextPath}/contas/receber/status"><i class="fa fa-check mr-5" aria-hidden="true"></i> Status</a>
										<a class="btn btn-danger btn-sm" href="${pageContext.request.contextPath}/contas/receber/exclui"><i class="fa fa-trash mr-5" aria-hidden="true"></i> Excluir</a>
										</c:if>
									</div>
								</div>
							</div>
							<div class="panel-body">
										
								<c:if test="${not empty contaReceberDS}">
								<div class="table-responsive">
									<table class="table table-hover dataTable table-striped w-full" data-plugin="dataTable">
										<thead>
											<tr>
												<th class="text-left">Recebimento</th>
					    						<th class="text-left">Descri��o</th>
					    						<th class="text-left">Documento</th>
					    						<th class="text-left">Valor</th>
					    						<th class="text-left">Tipo</th>
					    						<th class="text-left">Status</th>
					    						<th class="text-right"></th>
											</tr>
										</thead>
										<tfoot>
					    					<tr>
												<th class="text-left">Recebimento</th>
					    						<th class="text-left">Descri��o</th>
					    						<th class="text-left">Documento</th>
					    						<th class="text-left">Valor</th>
					    						<th class="text-left">Tipo</th>
					    						<th class="text-left">Status</th>
					    						<th class="text-right"></th>
					    					</tr>
					    				</tfoot>
										<tbody>
											<c:forEach var="row" items="${contaReceberDS}">
											<tr>
												<td class="align-middle">
													<i class="fa fa-calendar mr-10" aria-hidden="true"></i>
													<span><fmt:formatDate pattern="dd/MM/yyyy" value="${row.data_recebimento}"/></span>
												</td>
												<td class="align-middle">${row.descricao}</td>
												<td class="align-middle">${row.documento}</td>
												<td class="align-middle">R$ <fmt:formatNumber pattern="#,##0.00" value="${row.valor}" type="currency" currencySymbol="R$" /></td>
												<td class="align-middle">${row.tipo.descricao}</td>
												<td class="align-middle">
													<c:if test="${row.recebido}"><div class="badge badge-table badge-success">Recebido</div></c:if>
													<c:if test="${not row.recebido}"><div class="badge badge-table badge-danger">Pendente</div></c:if>
												</td>
												<td class="text-nowrap text-right align-middle">
													<a href="${pageContext.request.contextPath}/contas/receber/altera/${row.id}" class="btn btn-sm btn-icon btn-flat btn-primary" data-toggle="tooltip" data-original-title="Alterar"><i class="fa fa-pencil"></i></a>
													<a href="${pageContext.request.contextPath}/contas/receber/exclui/${row.id}" onclick="if(!confirm('Tem certeza que deseja excluir o registro?')) return false;" class="btn btn-sm btn-icon btn-flat btn-danger" data-toggle="tooltip" data-original-title="Excluir"><i class="fa fa-trash-o"></i></a>
												</td>
											</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
								</c:if>
								
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</jsp:attribute>
	
	<jsp:attribute name="css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/select2/select2.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/bootstrap-select/bootstrap-select.css">
	
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/datatables.net-bs4/dataTables.bootstrap4.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/datatables.net-bs4/dataTables.bootstrap4.css">
	</jsp:attribute>
	
	<jsp:attribute name="plugins">
		<script src="${pageContext.request.contextPath}/resources/global/vendor/select2/select2.full.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/vendor/bootstrap-select/bootstrap-select.js"></script>
		
		<script src="${pageContext.request.contextPath}/resources/global/vendor/datatables.net/jquery.dataTables.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/vendor/datatables.net-bs4/dataTables.bootstrap4.js"></script>
	</jsp:attribute>
	
	<jsp:attribute name="scripts">
		<script src="${pageContext.request.contextPath}/resources/global/js/Plugin/asselectable.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/js/Plugin/selectable.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/js/Plugin/panel.js"></script>
	
		<script src="${pageContext.request.contextPath}/resources/custom/js/contas/index.min.js"></script>
	</jsp:attribute>
	
</t:default>