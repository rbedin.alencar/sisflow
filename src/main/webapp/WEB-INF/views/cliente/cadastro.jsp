<%@page import="br.com.sisflow.conf.SisflowConfiguration" %>
<%@page import="java.io.File"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<c:set var="storage"><%=SisflowConfiguration.getValue("client.storage")%></c:set>
<c:set var="timestamp"><%= System.currentTimeMillis() %></c:set>

<t:default>
	<jsp:attribute name="body">
	
		<div class="page">	
			<div class="page-header">			
				<h1 class="page-title">Cadastro de Cliente</h1>
				<ol class="breadcrumb">
		    		<li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/home">Home</a></li>
		    		<li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/cliente">Cliente</a></li>
		    		<li class="breadcrumb-item active">Cadastro</li>
		    	</ol>
			</div>
			<div class="page-content">
				<div class="row">
					<div class="col-xs-12 col-md-12">
						
						<form:form action="${spring:mvcUrl('CC#salvar').build()}" method="post" modelAttribute="cliente" enctype="multipart/form-data" autocomplete="off" role="form">

							<spring:hasBindErrors name="cliente">
							<div class="alert alert-danger">
								<ul>
									<c:forEach var="error" items="${errors.allErrors}">
									<li><spring:message code="${error.code}" text="${error.defaultMessage}"/></li>
									</c:forEach>
								</ul>
							</div>
							</spring:hasBindErrors>
							
							<form:input type="hidden" path="id"/>

							<div class="row">
								<div class="col-sm-12 col-md-9">
									
									<div class="panel panel-bordered">
										<div class="panel-heading">
	                						<h3 class="panel-title">Informa��es B�sicas</h3>
	                					</div>	
	                					<div class="panel-body"> 
	                						<div class="row">
	                						 	<div class="col-xs-12 col-md-12">
	                						 		
	                						 		<div class="row">
	                						 		
	                						 			<div class="col-xs-12 col-md-2">
	                						 				<div class="form-group">
	                						 					<label class="form-control-label" for="id">C�digo</label>
	                						 					<form:input path="id" type="number" readonly="true" class="form-control" />
	                						 				</div>
	                						 			</div>
	                						 			
	                						 			<div class="col-xs-12 col-md-2">
	                						 				<div class="form-group">
	                						 					<label class="form-control-label" for="ativo">Status</label>
	                						 					<form:select path="ativo" items="${ativoDS}" class="form-control" data-plugin="selectpicker" />
	                						 				</div>
	                						 			</div>
	                						 			 
	                						 			<div class="col-xs-12 col-md-4">
	                						 				<div class="form-group">
	                						 					<label class="form-control-label" for="cliente">Neg�cio</label>
	                						 					<form:select path="negocio" class="form-control" data-plugin="select2">
	                						 						<c:forEach var="row" items="${negocioDS}">
	                						 							<option value="${row.id}" ${row.id == cliente.negocio.id ? "selected=selected" : null}>
	                						 								<c:if test="${row.tipo == 'PF'}">${row.pessoa.nome} ${row.pessoa.sobrenome}</c:if>
	                						 								<c:if test="${row.tipo == 'PJ'}">${row.pessoa.nome_fantasia}</c:if>	                						 								
	                						 							</option>
	                						 						</c:forEach>
	                						 					</form:select>
	                						 				</div>
	                						 			</div>
	                						 			
	                						 			<div class="col-xs-12 col-md-4">
	                						 				<div class="form-group">
	                						 					<label class="form-control-label" for="usuario_responsavel">Respons�vel</label>
	                						 					<form:select path="usuario_responsavel" items="${usuarioDS}" itemLabel="nome" itemValue="id" class="form-control" data-plugin="select2" />
	                						 				</div>
	                						 			</div>
	                						 			
	                						 			<div class="col-xs-12 col-md-4">
	                						 				<div class="form-group">
	                						 					<label class="form-control-label" for="tipo">Tipo</label>
	                						 					<form:select path="tipo" items="${clienteTipoDS}" itemLabel="descricao" class="form-control" data-plugin="selectpicker" />
	                						 				</div>
	                						 			</div>
	                						 			
	                						 			<div class="col-xs-12 col-md-4">
	                						 				<div class="form-group">
	                						 					<label class="form-control-label" for="origem">Origem</label>
	                						 					<form:select path="origem" items="${clienteOrigemDS}" itemLabel="descricao" class="form-control" data-plugin="select2" />
	                						 				</div>
	                						 			</div>	             
	                						 		</div>

	                						 	</div>
	                						</div>
	                					</div>									
									</div>
									
								</div>
								<div class="col-sm-12 col-md-3">
								
									<div class="panel panel-bordered">
										<div class="panel-heading">
	                						<h3 class="panel-title">Imagem</h3>
	                					</div>	
	                					<div class="panel-body"> 
	                						<div class="row">
	                						 	<div class="col-xs-12 col-md-12">
	                						 		
	                						 		<div class="form-group text-center">
				                		    			<input type="file" name="file" id="file-upload" 
				                		    				data-plugin="dropify" data-default-file="${storage}/cliente/${cliente.id}.jpg" 
				                		    				data-allowed-file-extensions="jpg jpeg png"
				                		    				data-max-file-size="2M"
				                		    				data-height="135"
				                		    				data-min-width="150"/>
			                		    				<small>clique sobre a imagem para alterar</small>
				            		    			</div>
	                						 		
	                						 	</div>
                						 	</div>
               						 	</div>
              						</div>
								
								</div>
							</div>
							
							<!-- PF -->
		                	<div id="cliente-pf" class="row" style="display: none;">
		                		<div class="col-xs-12 col-md-12">
		                			<div class="panel panel-bordered">
		                				<div class="panel-heading">
		                					<h3 class="panel-title">Pessoa F�sica</h3>
		                				</div>
		                				<div class="panel-body">
		            						<div class="row">
		            							
		            							<form:input type="hidden" path="pessoaFisica.id"/>
		            							
               						 			<div class="col-xs-12 col-md-2">
               						 				<div class="form-group">
               						 					<label class="form-control-label" for="sexo">Sexo</label>
               						 					<form:select path="pessoaFisica.sexo" items="${pessoaFisicaSexoDS}" itemLabel="descricao" class="form-control" data-plugin="selectpicker" />
               						 				</div>
               						 			</div>
		                                        
               						 			<div class="col-xs-12 col-md-2">
               						 				<div class="form-group">
               						 					<label class="form-control-label" for="pessoaFisica.nome">Nome</label>
               						 					<form:input path="pessoaFisica.nome" class="form-control ${not empty e ? 'is-invalid' : null}" autocomplete="off" placeholder="Nome" maxlength="32" />
               						 				</div>
               						 			</div>		                                        
		                                        <div class="col-xs-12 col-md-2">
		                                            <div class="form-group">
		                                                <label class="form-control-label" for="sobrenome">Sobrenome</label>
		                                                <form:input path="pessoaFisica.sobrenome" class="form-control ${not empty e ? 'is-invalid' : null}" autocomplete="off" placeholder="Sobrenome" maxlength="32" />
		                                            </div>
		                                        </div>
               						 			<div class="col-xs-12 col-md-2">
               						 				<div class="form-group">
               						 					<label class="form-control-label" for="data_nascimento">Data de nascimento</label>
               						 					<form:input path="pessoaFisica.data_nascimento" class="form-control" autocomplete="off" />
               						 				</div>
               						 			</div>
		                                        <div class="col-xs-12 col-md-2">
		                                            <div class="form-group">
		                                                <label class="form-control-label" for="rg">RG</label>
		                                                <form:input path="pessoaFisica.rg" class="form-control ${not empty e ? 'is-invalid' : null}" autocomplete="off" placeholder="RG" maxlength="13" />
		                                            </div>
		                                        </div>
		                                        <div class="col-xs-12 col-md-2">
		                                            <div class="form-group">
		                                                <label class="form-control-label" for="cpf">CPF</label>
		                                                <form:input path="pessoaFisica.cpf" class="form-control ${not empty e ? 'is-invalid' : null}" autocomplete="off" placeholder="CPF" maxlength="11" />
		                                            </div>
		                                        </div>
		            							
		            						</div>
		                				</div>
		                			</div>
		                		</div>
		            		</div>
		            		
		            		<div id="cliente-pj" class="row" style="display: none;">
		                		<div class="col-xs-12 col-md-12">
		                			<div class="panel panel-bordered">
		                				<div class="panel-heading">
		                					<h3 class="panel-title">Pessoa Jur�dica</h3>
		                				</div>
		                				<div class="panel-body">
		            						<div class="row">
		
               						 			<form:input type="hidden" path="pessoaJuridica.id"/>
               						 			
               						 			<div class="col-xs-12 col-md-3">
               						 				<div class="form-group">
               						 					<label class="form-control-label" for="pessoaJuridica.razao_social">Raz�o Social</label>
               						 					<form:input path="pessoaJuridica.razao_social" class="form-control ${not empty e ? 'is-invalid' : null}" autocomplete="off" placeholder="Raz�o social" maxlength="64" />
               						 				</div>
               						 			</div>

		                                        <div class="col-xs-12 col-md-3">
		                                            <div class="form-group">
		                                                <label class="form-control-label" for="nome_fantasia">Nome Fantasia</label>
		                                                <form:input path="pessoaJuridica.nome_fantasia" class="form-control ${not empty e ? 'is-invalid' : null}" autocomplete="off" placeholder="Nome fantasia" maxlength="64" />
		                                            </div>
		                                        </div>
		                                        <div class="col-xs-12 col-md-2">
		                                            <div class="form-group">
		                                                <label class="form-control-label" for="cnpj">CNPJ</label>
		                                                <form:input path="pessoaJuridica.cnpj" class="form-control ${not empty e ? 'is-invalid' : null}" autocomplete="off" placeholder="CNPJ" />
		                                            </div>
		                                        </div>
		                                        <div class="col-xs-12 col-md-1">
		                                            <div class="form-group">
		                                                <label class="form-control-label" for="inscricao_estadual">Insc. Estadual</label>
		                                                <form:input path="pessoaJuridica.inscricao_estadual" class="form-control ${not empty e ? 'is-invalid' : null}" autocomplete="off" placeholder="Insc. Estadual" />
		                                            </div>
		                                        </div>
		                                        <div class="col-xs-12 col-md-1">
		                                            <div class="form-group">
		                                                <label class="form-control-label" for="inscricao_municipal">Insc. Municipal</label>
		                                                <form:input path="pessoaJuridica.inscricao_municipal" class="form-control ${not empty e ? 'is-invalid' : null}" autocomplete="off" placeholder="Insc. Municipal" />
		                                            </div>
		                                        </div>
		                                        <div class="col-xs-12 col-md-1">
		                                            <div class="form-group">
		                                                <label class="form-control-label" for="suframa">Suframa</label>
		                                                <form:input path="pessoaJuridica.suframa" class="form-control ${not empty e ? 'is-invalid' : null}" autocomplete="off" placeholder="Suframa" />
		                                            </div>
		                                        </div>
               						 			<div class="col-xs-12 col-md-1">
               						 				<div class="form-group">
               						 					<label class="form-control-label" for="contribuinte_icms">Contrib. ICMS</label>
               						 					<form:select path="pessoaJuridica.contribuinte_icms" items="${contribuinteIcmsDS}" class="form-control" data-plugin="selectpicker" />
               						 				</div>
               						 			</div>
		
		            						</div>
		                				</div>
		                			</div>
		                		</div>
		            		</div>
		            		
		            		
		            		<div class="row">
		            			<!-- enderecos -->
		            			<div class="col-sm-12">
		            				
		            				<div class="panel panel-bordered panel-cliente-endereco">
		            					<div class="panel-heading">
		                					<h3 class="panel-title">Endere�o</h3>
		                					<div class="panel-actions">
		                						<a href="javascript:void(0);" class="btn btn-primary btn-sm" data-btn="add"><i class="fa fa-plus"></i> Novo Endere�o</a>
		                					</div>
		                				</div>
		                				<div class="panel-body">
		                					<!-- template row -->
		                					<div class="row template" style="display: none;">
		                					
		                						<div class="col-sm-12 col-md-2">
		                							<div class="form-group">
		                								<label class="form-control-label">CEP</label>
		                								<div class="input-group">
		                									<input type="text" name="endereco_cep[]" id="" class="form-control" autocomplete="off" placeholder="CEP" />
		                									<div class="input-group-append">
											                    <div class="input-group-text">
											                    	<a href="javascript:void(0);" class="btn-enderecoporcep"><i class="fa fa-search"></i></a>
											                    </div>
										                    </div>
		                								</div>
		                							</div>
		                						</div>
		                						
		                						<div class="col-sm-12 col-md-3">
		                							<div class="form-group">
		                								<label class="form-control-label" for="logradouro">Endere�o</label>
		                                        		<input type="text" name="endereco_logradouro[]" class="form-control" autocomplete="off" placeholder="Logradouro" />
		                							</div>
		                						</div>
		                						
		                						<div class="col-sm-12 col-md-1">
		                							<div class="form-group">
		                								<label class="form-control-label">N�mero</label>
		                                        		<input type="text" name="endereco_numero[]" class="form-control" autocomplete="off" placeholder="N�" />
		                							</div>
		                						</div>
		                						
		                						<div class="col-sm-12 col-md-2">
		                							<div class="form-group">
		                								<label class="form-control-label">Bairro</label>
		                                        		<input type="text" name="endereco_bairro[]" class="form-control" autocomplete="off" placeholder="Bairro" />
		                							</div>
		                						</div>
		                						
		                						<div class="col-sm-12 col-md-2">
		                							<div class="form-group">
		                								<label class="form-control-label">Cidade</label>
		                                        		<input type="text" name="endereco_cidade[]" class="form-control" autocomplete="off" placeholder="Cidade" />
		                							</div>
		                						</div>
		                						
		                						<div class="col-sm-12 col-md-1">
		                							<div class="form-group">
		                								<label class="form-control-label">UF</label>
		                                        		<input type="text" name="endereco_uf[]" class="form-control" autocomplete="off" placeholder="UF" />
		                							</div>
		                						</div>
		                						
		                						<div class="col-sm-12 col-md-1">
		                							<div class="form-group">
		                								<label class="form-control-label">Pa�s</label>
		                                        		<input type="text" name="endereco_pais[]" class="form-control" autocomplete="off" placeholder="pais" />
		                							</div>
		                						</div>
		                						
		                						<div class="col-sm-12 col-md-11">
		                							<div class="form-group">
		                								<label class="form-control-label">Complmento</label>
		                                        		<input type="text" name="endereco_complemento[]" class="form-control" autocomplete="off" placeholder="complemento" />
		                							</div>
		                						</div>
		                						
		                						<div class="col-xs-12 col-md-1">
		                							<div class="form-group">
		                    							<label class="form-control-label">&nbsp;</label>
		                    							<a href="#" class="btn btn-icon btn-block btn-danger btn-remover"><i class="fa fa-trash"></i></a>
		                							</div>
		            							</div>
		                						
		                					</div>
		                					<!-- end template -->
		                					
		                					<ul class="list-group list-group-dividered list-group-full">
		                					<c:forEach items="${cliente.enderecos}" var="row" varStatus="status">
		                					
		                						<form:hidden path="enderecos[${status.index}].id"/>
		                						<form:hidden path="enderecos[${status.index}].cep"/>
		                						<form:hidden path="enderecos[${status.index}].logradouro"/>
		                						<form:hidden path="enderecos[${status.index}].numero"/>
		                						<form:hidden path="enderecos[${status.index}].bairro"/>
		                						<form:hidden path="enderecos[${status.index}].cidade"/>
		                						<form:hidden path="enderecos[${status.index}].uf"/>
		                						<form:hidden path="enderecos[${status.index}].pais"/>
		                						<form:hidden path="enderecos[${status.index}].complemento"/>
		                						
		                						<li class="list-group-item">
		                						
				                					<div class="row template">
				                					
				                						<div class="col-sm-12 col-md-2">
				                							<div class="form-group">
				                								<label class="form-control-label">CEP</label>
				                								<div class="input-group">
				                									<input type="text" name="endereco_cep[]" value="${row.cep}" class="form-control" autocomplete="off" placeholder="CEP" />
				                									<div class="input-group-append">
													                    <div class="input-group-text">
													                    	<a href="javascript:void(0);" class="btn-enderecoporcep"><i class="fa fa-search"></i></a>
													                    </div>
												                    </div>
				                								</div>
				                							</div>
				                						</div>
				                						
				                						<div class="col-sm-12 col-md-3">
				                							<div class="form-group">
				                								<label class="form-control-label" for="logradouro">Endere�o</label>
				                                        		<input type="text" name="endereco_logradouro[]" value="${row.logradouro}" class="form-control" autocomplete="off" placeholder="Logradouro" />
				                							</div>
				                						</div>
				                						
				                						<div class="col-sm-12 col-md-1">
				                							<div class="form-group">
				                								<label class="form-control-label">N�mero</label>
				                                        		<input type="text" name="endereco_numero[]" value="${row.numero}" class="form-control" autocomplete="off" placeholder="N�" />
				                							</div>
				                						</div>
				                						
				                						<div class="col-sm-12 col-md-2">
				                							<div class="form-group">
				                								<label class="form-control-label">Bairro</label>
				                                        		<input type="text" name="endereco_bairro[]" value="${row.bairro}" class="form-control" autocomplete="off" placeholder="Bairro" />
				                							</div>
				                						</div>
				                						
				                						<div class="col-sm-12 col-md-2">
				                							<div class="form-group">
				                								<label class="form-control-label">Cidade</label>
				                                        		<input type="text" name="endereco_cidade[]" value="${row.cidade}" class="form-control" autocomplete="off" placeholder="Cidade" />
				                							</div>
				                						</div>
				                						
				                						<div class="col-sm-12 col-md-1">
				                							<div class="form-group">
				                								<label class="form-control-label">UF</label>
				                                        		<input type="text" name="endereco_uf[]" value="${row.uf}" class="form-control" autocomplete="off" placeholder="UF" />
				                							</div>
				                						</div>
				                						
				                						<div class="col-sm-12 col-md-1">
				                							<div class="form-group">
				                								<label class="form-control-label">Pa�s</label>
				                                        		<input type="text" name="endereco_pais[]" value="${row.pais}" class="form-control" autocomplete="off" placeholder="pais" />
				                							</div>
				                						</div>
				                						
				                						<div class="col-sm-12 col-md-11">
				                							<div class="form-group">
				                								<label class="form-control-label">Complmento</label>
				                                        		<input type="text" name="endereco_complemento[]" value="${row.complemento}" class="form-control" autocomplete="off" placeholder="complemento" />
				                							</div>
				                						</div>
				                						
				                						<div class="col-xs-12 col-md-1">
				                							<div class="form-group">
				                    							<label class="form-control-label">&nbsp;</label>
				                    							<a href="#" class="btn btn-icon btn-block btn-danger btn-remover"><i class="fa fa-trash"></i></a>
				                							</div>
				            							</div>
				                						
				                					</div>	
		                						
		                						</li>
	                						</c:forEach>
	                						</ul>
		                					
		                				</div>
		            				</div>
		            				
		            			</div>
		            		</div>

		            		<div class="row">		
		            		
		            			<!-- telefones -->
			            		<div class="col-xs-12 col-md-6">
			            			<div class="panel panel-bordered panel-cliente-telefone">
		                				<div class="panel-heading">
		                					<h3 class="panel-title">Telefones de Contato</h3>
		                					<div class="panel-actions">
		                						<a href="javascript:void(0);" class="btn btn-primary btn-sm" data-btn="add"><i class="fa fa-plus"></i> Novo Telefone</a>
		                					</div>
		                				</div>
		                				<div class="panel-body">
		                					
		                					<!-- template row -->
		                					<div class="row template" style="display: none;">
		                						<div class="col-xs-12 col-md-3">
		                							<div class="form-group">
		                    							<label class="form-control-label">Tipo</label>
		                    							<select name="telefone_tipo[]" class="form-control" disabled="disabled">
		                									<c:forEach items="${clienteTelefoneTipoDS}" var="tipo">
		                									<option value="${tipo}" data-mascara="${tipo.mascara}">${tipo.descricao}</option>
		                									</c:forEach>
		                								</select>
		                                          	</div>
		                						</div>
		                						<div class="col-xs-12 col-md-4">
		                							<div class="form-group">
		                								<label class="form-control-label">Telefone</label>
		                                                <div class="input-group input-group-icon">
										                    <div class="input-group-prepend">
											                    <div class="input-group-text">
											                    	<i class="fa fa-phone" aria-hidden="true"></i>
											                    </div>
										                    </div>
		                                                	<input type="text" name="telefone_numero[]" value="" class="form-control" placeholder="N�mero" autocomplete="off" disabled="disabled" />
		                                                </div>
		            								</div>
		                						</div>
		                						<div class="col-xs-12 col-md-4">
		                							<div class="form-group">
		                								<label class="form-control-label">Descri��o</label>
		                								<input type="text" name="telefone_descricao[]" value="" class="form-control" placeholder="Descri��o" autocomplete="off" disabled="disabled" />
		            								</div>
		                						</div>
		                						<div class="col-xs-12 col-md-1">
		                							<div class="form-group">
		                    							<label class="form-control-label">&nbsp;</label>
		                    							<a href="#" class="btn btn-icon btn-block btn-danger btn-remover"><i class="fa fa-trash"></i></a>
		                							</div>
		            							</div>
		                					</div>
		                					<!-- end template -->
		                					
		                					<ul class="list-group list-group-dividered list-group-full">
		                					<c:forEach items="${cliente.telefones}" var="row" varStatus="status">
		                					
		                						<form:hidden path="telefones[${status.index}].id"/>
		                						<form:hidden path="telefones[${status.index}].numero"/>
		                						<form:hidden path="telefones[${status.index}].descricao"/>
		                						<form:hidden path="telefones[${status.index}].tipo" value="${row.tipo}"/>
		                					
		                						<li class="list-group-item">		
		                        					<div class="row template">
		                        						<div class="col-xs-12 col-md-3">
		                        							<div class="form-group">
		                            							<label class="form-control-label">Tipo</label>
		                            							<select name="telefone_tipo[]" class="form-control">
				                									<c:forEach items="${clienteTelefoneTipoDS}" var="telefoneTipo">
				                									<option value="${telefoneTipo}" data-mascara="${telefoneTipo.mascara}" ${telefoneTipo == row.tipo ? 'selected="selected"' : null}>${telefoneTipo.descricao}</option>
				                									</c:forEach>
			                									</select>
		                                                  	</div>
		                        						</div>
		                        						<div class="col-xs-12 col-md-4">
		                        							<div class="form-group">
		                        								<label class="form-control-label">Telefone</label>
		                                                        <div class="input-group input-group-icon">
												                    <div class="input-group-prepend">
													                    <div class="input-group-text">
													                    	<i class="fa fa-phone" aria-hidden="true"></i>
													                    </div>
												                    </div>
		                                                        	<input name="telefone_numero[]" value="${row.numero}" class="form-control" placeholder="N�mero" autocomplete="off"/>
		                                                        </div>
		                    								</div>
		                        						</div>
		                        						<div class="col-xs-12 col-md-4">
		                        							<div class="form-group">
		                        								<label class="form-control-label">Descri��o</label>
		                        								<input name="telefone_descricao[]" value="${row.descricao}" class="form-control" placeholder="Descri��o" autocomplete="off"/>
		                    								</div>
		                        						</div>
		                        						<div class="col-xs-12 col-md-1">
		                        							<div class="form-group">
		                            							<label class="form-control-label">&nbsp;</label>
		                            							<a href="#" class="btn btn-icon btn-block btn-danger btn-remover"><i class="fa fa-trash"></i></a>
		                        							</div>
		                    							</div>
		                        					</div>
			                						
		                						</li>
		                					</c:forEach>
		                					</ul>
		                				</div>
			            			</div>
			            		</div>
			            		
			            		<!-- emails -->
			            		<div class="col-xs-12 col-md-6">
			            			<div class="panel panel-bordered panel-cliente-email">
		                				<div class="panel-heading">
		                					<h3 class="panel-title">E-mails de contato</h3>
		                					<div class="panel-actions">
		                						<a href="javascript:void(0);" class="btn btn-primary btn-sm" data-btn="add"><i class="fa fa-plus"></i> Novo Email</a>
		                					</div>
		                				</div>
		                				<div class="panel-body">
		                					
		                					<!-- template row -->
		                					<div class="row template" style="display: none;">
		                						<div class="col-xs-12 col-md-8">
		                							<div class="form-group">
		                								<label class="form-control-label">E-mail</label>
		                                                <div class="input-group input-group-icon">
										                    <div class="input-group-prepend">
											                    <div class="input-group-text">
											                    	<i class="fa fa-envelope" aria-hidden="true"></i>
											                    </div>
										                    </div>
		                                                	<input type="text" name="email_email[]" value="" class="form-control" placeholder="E-mail" autocomplete="off" disabled="disabled" />
		                                                </div>
		            								</div>
		                						</div>
		                						<div class="col-xs-12 col-md-3">
		                							<div class="form-group">
		                								<label class="form-control-label">Descri��o</label>
		                								<input type="text" name="email_descricao[]" value="" class="form-control" placeholder="Descri��o" autocomplete="off" disabled="disabled" />
		            								</div>
		                						</div>
		                						<div class="col-xs-12 col-md-1">
		                							<div class="form-group">
		                    							<label class="form-control-label">&nbsp;</label>
		                    							<a href="#" class="btn btn-icon btn-block btn-danger btn-remover"><i class="fa fa-trash"></i></a>
		                							</div>
		            							</div>
		                					</div>
		                					<!-- end template -->
		                					
		                					<ul class="list-group list-group-dividered list-group-full">
		                					<c:forEach items="${cliente.emails}" var="row" varStatus="status">
		                					
		                						<form:hidden path="emails[${status.index}].id"/>
		                						<form:hidden path="emails[${status.index}].email"/>
		                						<form:hidden path="emails[${status.index}].descricao"/>
		                					
		                						<li class="list-group-item">		
		                        					<div class="row template">
		                        						<div class="col-xs-12 col-md-8">
		                        							<div class="form-group">
		                                                        <label class="form-control-label">E-mail</label>
		                                                        <div class="input-group input-group-icon">
												                    <div class="input-group-prepend">
													                    <div class="input-group-text">
													                    	<i class="fa fa-envelope" aria-hidden="true"></i>
													                    </div>
												                    </div>
												                    <input name="email_email[]" value="${row.email}" class="form-control" placeholder="E-mail" autocomplete="off"/>
											                    </div>
		                    								</div>
		                        						</div>
		                        						<div class="col-xs-12 col-md-3">
		                        							<div class="form-group">
		                        								<label class="form-control-label">Descri��o</label>
		                        								<input name="email_descricao[]" value="${row.descricao}" class="form-control" placeholder="Descri��o" autocomplete="off"/>
		                    								</div>
		                        						</div>
		                        						<div class="col-xs-12 col-md-1">
		                        							<div class="form-group">
		                            							<label class="form-control-label">&nbsp;</label>
		                            							<a href="#" class="btn btn-icon btn-block btn-danger btn-remover"><i class="fa fa-trash"></i></a>
		                        							</div>
		                    							</div>
		                        					</div>
			                						
		                						</li>
		                					</c:forEach>
		                					</ul>
		                				</div>
			            			</div>
			            		</div>
			            		
		            		</div>
		            		


		                	<div class="panel">
		                		<div class="panel-heading"><h3 class="panel-title">Observa��o</h3></div>
		                		<div class="panel-body">
		                			<div class="row">
		                                <div class="col-sm-12">
		                                    <div class="form-group">
		                                        <form:textarea path="observacao" rows="3" class="form-control ${not empty e ? 'is-invalid' : null}" autocomplete="off" placeholder="Observa��o" />
		                                    </div>
		                                </div>
		                			</div>
		        				</div>
		        			</div>

							<div class="row">
        						<div class="col-xs-12 col-md-8"><p>Ao clicar em salvar, as informa��es inseridas ser�o enviadas � base de dados.</p></div>
        						<div class="col-xs-12 col-md-4"><button class="btn btn-primary btn-block" type="submit"><i class="fa fa-floppy-o"></i> Salvar</button></div>
        					</div>

						</form:form>
						
					</div>
				</div>
			</div>
		</div>

	</jsp:attribute>
	
	<jsp:attribute name="css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/blueimp-file-upload/jquery.fileupload.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/dropify/dropify.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/select2/select2.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/bootstrap-select/bootstrap-select.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/custom/js/plugins/bootcomplete.js-master/dist/bootcomplete.css">
	</jsp:attribute>
	
	<jsp:attribute name="plugins">
		<script src="${pageContext.request.contextPath}/resources/custom/js/plugins/jquery.mask.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/vendor/dropify/dropify.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/vendor/select2/select2.full.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/vendor/bootstrap-select/bootstrap-select.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/vendor/bootstrap-datepicker/bootstrap-datepicker.pt-BR.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/custom/js/plugins/bootcomplete.js-master/dist/jquery.bootcomplete.js"></script>
	</jsp:attribute>
	
	<jsp:attribute name="scripts">
		<script src="${pageContext.request.contextPath}/resources/custom/js/cliente/cadastro.min.js"></script>
	</jsp:attribute>
	
</t:default>