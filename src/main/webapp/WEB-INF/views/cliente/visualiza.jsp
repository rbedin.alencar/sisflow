<%@page import="br.com.sisflow.conf.SisflowConfiguration" %>
<%@page import="java.io.File"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<c:set var="storage"><%=SisflowConfiguration.getValue("client.storage")%></c:set>
<c:set var="timestamp"><%=System.currentTimeMillis()%></c:set>

<t:default>
	<jsp:attribute name="body">
		
		<div class="page">
			<div class="page-content container-fluid">
				<div class="row">
					<!-- profile -->
					<div class="col-lg-3">
						<div class="card card-shadow text-center">
							<div class="card-block">
								<a class="avatar avatar-lg" href="javascript:void(0)">
									<img src="${storage}/cliente/${cliente.id}.jpg" alt="Avatar">
								</a>	
								
								<h4 class="profile-user">
									<c:catch var="e_pf">${cliente.pessoa.nome} ${cliente.pessoa.sobrenome}</c:catch>
									<c:catch var="e_pj">${cliente.pessoa.razao_social}</c:catch>
								</h4>
								
								<p class="profile-job">${cliente.origem.descricao}</p>
								<c:forEach items="${cliente.enderecos}" var="row">
									<p>
										${row.logradouro}, ${row.numero} - ${row.bairro}<br/>
										${row.cidade} / ${row.uf} - CEP: ${row.cep}
									</p>
								</c:forEach>
								
								<!-- 
                  				<div class="profile-social">
                  					<a class="icon bd-twitter" href="javascript:void(0)"></a>
                  					<a class="icon bd-facebook" href="javascript:void(0)"></a>
                  					<a class="icon bd-dribbble" href="javascript:void(0)"></a>
                  					<a class="icon bd-github" href="javascript:void(0)"></a>
                  				</div>
                  				 -->
                  				 
                  				<a href="${pageContext.request.contextPath}/cliente/altera/${cliente.id}" class="btn btn-primary btn-block">Alterar Dados</a>                  				
                  				<a href="${pageContext.request.contextPath}/pet/animal/cadastro/${cliente.id}" class="btn btn-primary btn-block">Adicionar Animal</a>
                  				<a href="${pageContext.request.contextPath}/venda/cadastro/${cliente.id}" class="btn btn-primary btn-block">Nova Venda</a>
                  				<a href="${pageContext.request.contextPath}/pet/hospedagem/cadastro/${cliente.id}" class="btn btn-primary btn-block">Nova Hospedagem</a>
                  				
                  				<a href="${pageContext.request.contextPath}/cliente/exclui/${cliente.id}" onclick="if(!confirm('Tem certeza que deseja excluir o registro?')) return false;" class="btn btn-primary btn-block">Excluir</a>
                  				
                  				<a href="javascript:history.back();" class="btn btn-default btn-block">Voltar</a>
							</div>
						</div>
					</div>
					
					<!-- tabs -->
					<div class="col-lg-9">
						
						<div class="panel">
							<div class="panel-body nav-tabs-animate nav-tabs-horizontal" data-plugin="tabs">
								<ul class="nav nav-tabs nav-tabs-line" role="tablist">
									<li class="nav-item" role="presentation"><a class="active nav-link" data-toggle="tab" href="#vendas" aria-controls="vendas" role="tab">Vendas</a></li>
									<li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#hospedagens" aria-controls="hospedagens" role="tab">Hospedagens</a></li>
									<li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#animais" aria-controls="animais" role="tab">Animais</a></li>
									<li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#informacoes" aria-controls="informacoes" role="tab">Informa��es</a></li>
									<li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#localizacao" aria-controls="localizacao" role="tab">Localiza��o</a></li>
								</ul>
								<div class="tab-content">
								
									<!-- tab -->
									<div class="tab-pane active animation-slide-left pt-30" id="vendas" role="tabpanel">
										<div class="row">
											<div class="col-sm-12">

												<c:if test="${not empty vendaDS}">
												<div class="panel-group" id="panelAccordion" aria-multiselectable="true" role="tablist">
													
													<!-- panel -->
													<c:forEach var="row" items="${vendaDS}">
													<div class="panel">
														<div class="panel-heading" id="panelHead${row.id}" role="tab" style="border-bottom: 1px solid #eee;">
															<a class="panel-title" data-toggle="collapse" href="#panelCollapse${row.id}" data-parent="#panelAccordion" aria-expanded="true" aria-controls="panelCollapse${row.id}">
																Venda #${row.id} - <fmt:formatDate pattern="dd/MM/yyyy" value="${row.data_venda}"/> �s <fmt:formatDate pattern="HH:mm" value="${row.data_venda}"/>
															</a>
														</div>
														<div class="panel-collapse collapse" id="panelCollapse${row.id}" aria-labelledby="panelHead${row.id}" role="tabpanel">
															<div class="panel-body">
															
																<div class="row">
																
																	<div class="col-sm-4">
																		<div class="card card-block p-20 bg-green-600">
																			<div class="card-watermark darker font-size-40 m-15">
																				<i class="icon fa fa-usd" aria-hidden="true"></i>
																			</div>
																			<div class="counter counter-md counter-inverse text-left">
																				<div class="counter-number-group">
																					<span class="counter-number-related text-capitalize">R$</span>
																					<span class="counter-number"><fmt:formatNumber pattern="#,##0.00" value="${row.total}" type="currency" currencySymbol="R$"/></span>
																				</div>
																				<div class="counter-label">Total da Venda</div>
																			</div>
																		</div>
																	</div>
																	
																	<div class="col-sm-4">
																		<div class="card card-block p-20 bg-blue-600">
																			<div class="card-watermark darker font-size-40 m-15">
																				<i class="icon fa fa-shopping-cart" aria-hidden="true"></i>
																			</div>
																			<div class="counter counter-md counter-inverse text-left">
																				<div class="counter-number-group">
																					<span class="counter-number-related text-capitalize"></span>
																					<span class="counter-number">${fn:length(row.vendaOfertas)}</span>
																				</div>
																				<div class="counter-label">Total de Produtos/Servi�os</div>
																			</div>
																		</div>
																	</div>
																	
																	<div class="col-sm-4">
																		<a href="${pageContext.request.contextPath}/venda/comprovante/${row.id}" target="_blank" class="btn btn-primary btn-block"><i class="fa fa-print"></i> Imprimir Comprovante de Venda</a>
																	</div>
																
																</div>
																
																<p>${row.observacao}</p>
																
																<c:if test="${not empty row.vendaOfertas}">
																<table class="table table-hover dataTable table-striped w-full" data-plugin="dataTable">
																	<thead>
																		<tr>
												    						<th class="text-left">Nome</th>
												    						<th class="text-left">Referencia</th>
												    						<th class="text-left">Quantidade</th>
												    						<th class="text-left">Valor (R$)</th>
												    						<th class="text-left">Subtotal (R$)</th>
																		</tr>
																	</thead>
																	<tfoot>
												    					<tr>
												    						<th class="text-left">Nome</th>
												    						<th class="text-left">Referencia</th>
												    						<th class="text-left">Quantidade</th>
												    						<th class="text-left">Valor (R$)</th>
												    						<th class="text-left">Subtotal (R$)</th>
												    					</tr>
												    				</tfoot>
																	<tbody>
																		<c:forEach items="${row.vendaOfertas}" var="rowVendaOferta">
																		<tr>
																			<td class="align-middle">${rowVendaOferta.oferta.nome}</td>
																			<td class="align-middle">${rowVendaOferta.oferta.referencia}</td>
																			<td class="align-middle">${rowVendaOferta.quantidade}</td>
																			<td class="align-middle"><fmt:formatNumber pattern="#,##0.00" value="${rowVendaOferta.valor}" type="currency" currencySymbol="R$" /></td>
																			<td class="align-middle"><fmt:formatNumber pattern="#,##0.00" value="${rowVendaOferta.subtotal}" type="currency" currencySymbol="R$" /></td>
																		</tr>
																		</c:forEach>
																	</tbody>
																</table>
																</c:if>
															
																
															
															</div>
														</div>
													</div>
													</c:forEach>

												</div>
												</c:if>
												
											</div>
										</div>
									</div>	
									
									<!-- tab -->
									<div class="tab-pane active animation-slide-left pt-30" id="hospedagens" role="tabpanel">
										<div class="row">
											<div class="col-sm-12">
												
												<c:if test="${not empty hospedagemDS}">
												<div class="panel-group" id="panelAccordion" aria-multiselectable="true" role="tablist">
													
													<!-- panel -->
													<c:forEach var="row" items="${hospedagemDS}">
													<div class="panel">
														<div class="panel-heading" id="panelHead${row.id}" role="tab" style="border-bottom: 1px solid #eee;">
															<a class="panel-title" data-toggle="collapse" href="#panelCollapse${row.id}" data-parent="#panelAccordion" aria-expanded="true" aria-controls="panelCollapse${row.id}">Hospedagem #${row.id}</a>
														</div>
														<div class="panel-collapse collapse" id="panelCollapse${row.id}" aria-labelledby="panelHead${row.id}" role="tabpanel">
															<div class="panel-body">
																
																<div class="row">
																	
																	<div class="col-sm-4">
																		<div class="card card-block p-20">
																			<div class="card-watermark darker font-size-40 m-15">
																				<i class="icon fa fa-calendar" aria-hidden="true"></i>
																			</div>
																			<div class="counter counter-md text-left">
																				<div class="counter-number-group">
																					<fmt:formatDate value="${row.data_checkin}" var="data_checkin" pattern="dd/MM/yyyy"/>
																					<span class="counter-number-related text-capitalize">${data_checkin}</span>
																				</div>
																				<div class="counter-label text-capitalize">Check-in</div>
																			</div>
																		</div>
																	</div>
																	
																	<div class="col-sm-4">
																		<div class="card card-block p-20">
																			<div class="card-watermark darker font-size-40 m-15">
																				<i class="icon fa fa-calendar" aria-hidden="true"></i>
																			</div>
																			<div class="counter counter-md text-left">
																				<div class="counter-number-group">
																					<fmt:formatDate value="${row.data_checkout}" var="data_checkout" pattern="dd/MM/yyyy"/>
																					<span class="counter-number-related text-capitalize">${data_checkout}</span>
																				</div>
																				<div class="counter-label text-capitalize">Check-out</div>
																			</div>
																		</div>
																	</div>
															
																	<div class="col-sm-4">
																		<div class="card card-block p-20 bg-green-600">
																			<div class="card-watermark darker font-size-40 m-15">
																				<i class="icon fa fa-usd" aria-hidden="true"></i>
																			</div>
																			<div class="counter counter-md counter-inverse text-left">
																				<div class="counter-number-group">
																					<span class="counter-number-related text-capitalize">R$</span>
																					<span class="counter-number"><fmt:formatNumber pattern="#,##0.00" value="${valor}" type="currency" currencySymbol="R$" /></span>
																				</div>
																				<div class="counter-label text-capitalize">Valor total</div>
																			</div>
																		</div>
																	</div>
																	
																</div>
																
																<div class="row">
																
																</div>
																
																<p>De moveat laudatur vestra parum doloribus labitur sentire partes, eripuit praesenti
                          congressus ostendit alienae, voluptati ornateque accusamus
                          clamat reperietur convicia albucius, veniat quocirca
                          vivendi aristotele errorem epicurus. Suppetet. Aeternum
                          animadversionis, turbent cn partem porrecta c putamus
                          diceret decore. Vero itaque incursione.</p>
															</div>
														</div>
													</div>
													</c:forEach>

												</div>
												</c:if>
												
											</div>
										</div>
									</div>	

									<!-- tab -->
									<div class="tab-pane animation-slide-left pt-30" id="animais" role="tabpanel">
										<div class="row">
											<div class="col-sm-12">
											
												<c:if test="${not empty animalDS}">
												<table class="table table-hover dataTable table-striped w-full" data-plugin="dataTable">
													<thead>
														<tr>
															<th class="text-left" width="20">&nbsp;</th>
								    						<th class="text-left">Nome</th>
								    						<th class="text-left">Sexo</th>
								    						<th class="text-left">Especie</th>
								    						<th class="text-left">Ra�a</th>
								    						<th class="text-right"></th>
														</tr>
													</thead>
													<tfoot>
								    					<tr>
															<th class="text-left" width="20">&nbsp;</th>
								    						<th class="text-left">Nome</th>
								    						<th class="text-left">Sexo</th>
								    						<th class="text-left">Especie</th>
								    						<th class="text-left">Ra�a</th>
								    						<th class="text-right"></th>
								    					</tr>
								    				</tfoot>
													<tbody>
														<c:forEach items="${animalDS}" var="row">
														<tr>
															<td class="align-middle">
																<a href="${pageContext.request.contextPath}/pet/animal/visualiza/${row.id}">
																	<span class="avatar"><img src="${storage}/animal/${row.id}.jpg?t=${timestamp}" alt="" class="mg-fluid" /></span>
																</a>
															</td>
															<td class="align-middle"><a href="${pageContext.request.contextPath}/pet/animal/visualiza/${row.id}">${row.nome}</a></td>
															<td class="align-middle">${row.sexo.descricao}</td>
															<td class="align-middle">${row.especie.nome}</td>
															<td class="align-middle">${row.raca.nome}</td>
															<td class="text-nowrap text-right align-middle">
																<a href="${pageContext.request.contextPath}/pet/animal/altera/${row.id}" class="btn btn-sm btn-icon btn-flat btn-primary" data-toggle="tooltip" data-original-title="Alterar"><i class="fa fa-pencil"></i></a>
																<a href="${pageContext.request.contextPath}/pet/animal/exclui/${row.id}" onclick="if(!confirm('Tem certeza que deseja excluir o registro?')) return false;" class="btn btn-sm btn-icon btn-flat btn-danger" data-toggle="tooltip" data-original-title="Excluir"><i class="fa fa-trash-o"></i></a>
															</td>
														</tr>
														</c:forEach>
													</tbody>
												</table>
												</c:if>
											
											</div>
										</div>
									</div>
									 
									<!-- tab -->
									<div class="tab-pane animation-slide-left" id="informacoes" role="tabpanel">
									</div>
									
									<!-- tab -->
									<div class="tab-pane animation-slide-left" id="localizacao" role="tabpanel">
									
									</div>
									
								</div>
							</div>
						</div>
						
					</div>
				</div>				
			</div>
		</div>
		
	</jsp:attribute>

	<jsp:attribute name="css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/examples/css/pages/profile.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/examples/css/widgets/statistics.css">
	</jsp:attribute>
	
	<jsp:attribute name="plugins">
	</jsp:attribute>
	
	<jsp:attribute name="scripts">
		<script src="${pageContext.request.contextPath}/resources/custom/js/cliente/visualiza.min.js"></script>
	</jsp:attribute>
	
</t:default>