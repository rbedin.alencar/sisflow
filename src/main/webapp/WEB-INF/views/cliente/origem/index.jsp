<%@page import="br.com.sisflow.conf.SisflowConfiguration" %>
<%@page import="java.io.File"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:default>
	<jsp:attribute name="body">
		
		<div class="page">	
			<div class="page-header">
				<h1 class="page-title">Origens</h1>
				<ol class="breadcrumb">
		    		<li class="breadcrumb-item"><a href="${pageContext.request.contextPath}">Configura��es</a></li>
		    		<li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/cliente">Cliente</a></li>
		    		<li class="breadcrumb-item active">Origens</li>
		    	</ol>
		    	<div class="page-header-actions">
		    		<a href="${pageContext.request.contextPath}/cliente/origem/cadastro" class="btn btn-sm btn-icon btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Adicionar</a>    	
		    	</div>
			</div>
			<div class="page-content">
				
				<c:if test="${msg != null}">
				<div class="alert alert-alt alert-primary alert-dismissible" role="alert">
					${msg}
					<button type="button" class="close" data-dismiss="alert" aria-label="Fechar"><span aria-hidden="true">�</span></button>
        		</div>
        		</c:if>
				
				<div class="panel">
					<header class="panel-heading">
						<div class="panel-actions"></div>
						<h3 class="panel-title">Ger�nciamento de Origens</h3>
					</header>
					<div class="panel-body">
						
						<c:if test="${not empty clienteOrigemDS}">
						<table class="table table-hover dataTable table-striped w-full" data-plugin="dataTable">
							<thead>
								<tr>
		    						<th class="text-left">Descri��o</th>
		    						<th class="text-left">Ativo</th>
		    						<th class="text-right">A��O</th>
								</tr>
							</thead>
							<tfoot>
		    					<tr>
		    						<th class="text-left">Descri��o</th>
		    						<th class="text-left">Ativo</th>
		    						<th class="text-right">A��O</th>
		    					</tr>
		    				</tfoot>
							<tbody>
								<c:forEach var="row" items="${clienteOrigemDS}">
								<tr>
									<td class="align-middle">${row.descricao}</td>
									<td class="align-middle">${row.ativo ? 'Sim' : 'N�o'}</td>
									<td class="text-nowrap text-right align-middle">
										<a href="${pageContext.request.contextPath}/cliente/origem/altera/${row.id}" class="btn btn-sm btn-icon btn-flat btn-primary" data-toggle="tooltip" data-original-title="Alterar"><i class="fa fa-pencil"></i></a>
										<a href="${pageContext.request.contextPath}/cliente/origem/exclui/${row.id}" onclick="if(!confirm('Tem certeza que deseja excluir o registro?')) return false;" class="btn btn-sm btn-icon btn-flat btn-danger" data-toggle="tooltip" data-original-title="Excluir"><i class="fa fa-trash-o"></i></a>
									</td>
								</tr>
								</c:forEach>
							</tbody>
						</table>
						</c:if>
						
					</div>
				</div>
				
			</div>
		</div>
	</jsp:attribute>
	
	<jsp:attribute name="css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/datatables.net-bs4/dataTables.bootstrap4.css">
	</jsp:attribute>
	
	<jsp:attribute name="plugins">
		<script src="${pageContext.request.contextPath}/resources/global/vendor/datatables.net/jquery.dataTables.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/vendor/datatables.net-bs4/dataTables.bootstrap4.js"></script>
	</jsp:attribute>
	
	<jsp:attribute name="scripts">
		<script src="${pageContext.request.contextPath}/resources/custom/js/usuario/index.min.js"></script>
	</jsp:attribute>
	
</t:default>