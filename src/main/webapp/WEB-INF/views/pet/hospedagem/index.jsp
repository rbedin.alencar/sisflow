<%@page import="br.com.sisflow.conf.SisflowConfiguration" %>
<%@page import="java.io.File"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<c:set var="storage"><%=SisflowConfiguration.getValue("client.storage")%></c:set>
<c:set var="timestamp"><%= System.currentTimeMillis() %></c:set>

<t:default>
	<jsp:attribute name="body">
		
		<div class="page">	
			<div class="page-header">
				<h1 class="page-title">Hospedagens</h1>
				<ol class="breadcrumb">
		    		<li class="breadcrumb-item"><a href="${pageContext.request.contextPath}">Home</a></li>
		    		<li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/pet/hospedagem">Hospedagens</a></li>
		    		<li class="breadcrumb-item active">Gerenciamento</li>
		    	</ol>
		    	<div class="page-header-actions">
		    		<a href="${pageContext.request.contextPath}/pet/hospedagem/cadastro" class="btn btn-sm btn-icon btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Nova Hospedagem</a>    	
		    	</div>
			</div>
			<div class="page-content">
				
				<c:if test="${msg != null}">
				<div class="alert alert-alt alert-primary alert-dismissible" role="alert">
					${msg}
					<button type="button" class="close" data-dismiss="alert" aria-label="Fechar"><span aria-hidden="true">�</span></button>
        		</div>
        		</c:if>
				
				<div class="panel">
					<header class="panel-heading">
						<div class="panel-actions"></div>
						<h3 class="panel-title">Gerenciamento de Hospedagem</h3>
					</header>
					<div class="panel-body">
						
						<c:if test="${not empty animalDS}">
						<table class="table table-hover dataTable table-striped w-full" data-plugin="dataTable">
							<thead>
								<tr>
		    						<th class="text-left">C�d.</th>
		    						<th class="text-left">Cliente</th>
		    						<th class="text-left">Animal</th>
		    						<th class="text-left">Ra�a</th>
		    						<th class="text-left">Check-in</th>
		    						<th class="text-left">Check-out</th>
		    						<th class="text-left">Status</th>
		    						<th class="text-right">A��O</th>
								</tr>
							</thead>
							<tfoot>
		    					<tr>
		    						<th class="text-left">C�d.</th>
		    						<th class="text-left">Cliente</th>
		    						<th class="text-left">Animal</th>
		    						<th class="text-left">Ra�a</th>
		    						<th class="text-left">Check-in</th>
		    						<th class="text-left">Check-out</th>
		    						<th class="text-left">Status</th>
		    						<th class="text-right">A��O</th>
		    					</tr>
		    				</tfoot>
							<tbody>
								<c:forEach var="row" items="${hospedagemDS}">
								<tr>
									<td class="align-middle">${row.id}</td>
									<td class="align-middle">
										<c:if test="${row.cliente.tipo == 'PF'}">${row.cliente.pessoa.nome} ${row.cliente.pessoa.sobrenome}</c:if>
										<c:if test="${row.cliente.tipo == 'PJ'}">${row.cliente.pessoa.nome_fantasia}</c:if>
									</td>
									<td class="align-middle"><a href="${pageContext.request.contextPath}/pet/animal/visualiza/${row.id}">${row.nome}</a></td>
									<td class="align-middle">${row.animal.raca.nome}</td>
									<td class="align-middle">${row.data_checkin}</td>
									<td class="align-middle">${row.data_checkout}</td>
									<td class="align-middle">
										<fmt:formatDate pattern="dd/MM/yyyy" value="${row.data_checkin}"/>
										<i class="fa fa-calendar ml-10" aria-hidden="true"></i>
									</td>
									<td class="align-middle">
										<c:choose>
											<c:when test="${row.data_checkout == null}">N�o definido</c:when>
											<c:otherwise>
												<span><fmt:formatDate pattern="dd/MM/yyyy" value="${row.data_checkout}"/></span>
												<i class="fa fa-calendar ml-10" aria-hidden="true"></i>
											</c:otherwise>
										</c:choose>
									</td>
									<td class="align-middle">${row.status.descricao}</td>
									<td class="text-nowrap text-right align-middle">
										<a href="${pageContext.request.contextPath}/pet/hospedagem/altera/${row.id}" class="btn btn-sm btn-icon btn-flat btn-primary" data-toggle="tooltip" data-original-title="Alterar"><i class="fa fa-pencil"></i></a>
										<a href="${pageContext.request.contextPath}/pet/hospedagem/exclui/${row.id}" onclick="if(!confirm('Tem certeza que deseja excluir o registro?')) return false;" class="btn btn-sm btn-icon btn-flat btn-danger" data-toggle="tooltip" data-original-title="Excluir"><i class="fa fa-trash-o"></i></a>
									</td>
								</tr>
								</c:forEach>
							</tbody>
						</table>
						</c:if>
						
					</div>
				</div>
				
			</div>
		</div>
	</jsp:attribute>
	
	<jsp:attribute name="css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/datatables.net-bs4/dataTables.bootstrap4.css">
	</jsp:attribute>
	
	<jsp:attribute name="plugins">
		<script src="${pageContext.request.contextPath}/resources/global/vendor/datatables.net/jquery.dataTables.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/vendor/datatables.net-bs4/dataTables.bootstrap4.js"></script>
	</jsp:attribute>
	
	<jsp:attribute name="scripts">
		<script src="${pageContext.request.contextPath}/resources/custom/js/pet/hospedagem/index.min.js"></script>
	</jsp:attribute>
	
</t:default>