<%@page import="br.com.sisflow.conf.SisflowConfiguration" %>
<%@page import="java.io.File"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<c:set var="storage"><%=SisflowConfiguration.getValue("client.storage")%></c:set>
<c:set var="timestamp"><%=System.currentTimeMillis()%></c:set>

<t:default>
	<jsp:attribute name="body">
	
		<div class="page">	
			<div class="page-header">			
				<h1 class="page-title">Cadastro de Hospedagem</h1>
				<ol class="breadcrumb">
		    		<li class="breadcrumb-item"><a href="${pageContext.request.contextPath}">Home</a></li>
		    		<li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/pet/hospedagem">Hospedagem</a></li>
		    		<li class="breadcrumb-item active">Cadastro</li>
		    	</ol>
			</div>
			<div class="page-content">
				<div class="row">
					<div class="col-xs-12 col-md-12">
						
						<form:form action="${spring:mvcUrl('HC#salvar').build()}" method="post" modelAttribute="hospedagem" enctype="multipart/form-data" autocomplete="off" role="form">

							<spring:hasBindErrors name="hospedagem">
							<div class="alert alert-danger">
								<ul>
									<c:forEach var="error" items="${errors.allErrors}">
									<li><spring:message code="${error.code}" text="${error.defaultMessage}"/></li>
									</c:forEach>
								</ul>
							</div>
							</spring:hasBindErrors>
							
							<form:input type="hidden" path="id"/>
							
							<div class="row">
								<div class="col-sm-12 col-md-8">

									<div class="panel panel-bordered">
										<div class="panel-heading">
	                						<h3 class="panel-title">Informa��es B�sicas</h3>
	                					</div>	
	                					<div class="panel-body"> 
	                						<div class="row">
	                						 	<div class="col-xs-12 col-md-12">
	                						 		
	                						 		<div class="row">
	                						 		
	                						 			<div class="col-xs-12 col-md-4">
	                						 				<div class="form-group">
	                						 					<label class="form-control-label" for="id">C�digo</label>
	                						 					<form:input path="id" type="number" readonly="true" class="form-control" />
	                						 				</div>
	                						 			</div>
	                						 			
	                						 			<div class="col-xs-12 col-md-4">
	                						 				<div class="form-group">
	                						 					<label class="form-control-label" for="cliente">Neg�cio</label>
	                						 					<form:select path="negocio" class="form-control" data-plugin="select2">
	                						 						<c:forEach var="row" items="${negocioDS}">
	                						 							<option value="${row.id}" ${row.id == hospedagem.negocio.id ? "selected=selected" : null}>
	                						 								<c:if test="${row.tipo == 'PF'}">${row.pessoa.nome} ${row.pessoa.sobrenome}</c:if>
	                						 								<c:if test="${row.tipo == 'PJ'}">${row.pessoa.nome_fantasia}</c:if>	                						 								
	                						 							</option>
	                						 						</c:forEach>
	                						 					</form:select>
	                						 				</div>
	                						 			</div>
	                						 			
	                						 			<div class="col-xs-12 col-md-4">
	                						 				<div class="form-group">
	                						 					<label class="form-control-label" for="cliente">Cliente</label>
	                						 					<form:select path="cliente" class="form-control" data-plugin="select2">
	                						 						<option value="" selected="selected">Selecione</option>
	                						 						<c:forEach var="row" items="${clienteDS}">
	                						 							<option value="${row.id}" ${row.id == hospedagem.cliente.id ? "selected=selected" : null}>
	                						 								<c:if test="${row.tipo == 'PF'}">${row.pessoa.nome} ${row.pessoa.sobrenome}</c:if>
	                						 								<c:if test="${row.tipo == 'PJ'}">${row.pessoa.razao_social}</c:if>	                						 								
	                						 							</option>
	                						 						</c:forEach>
	                						 					</form:select>
	                						 				</div>
	                						 			</div>
	                						 			


	                						 		</div>
	                						 		
	                						 		<div class="row">
	                						 		
	                						 			<div class="col-xs-12 col-md-6">
	                						 				<div class="form-group">
	                						 					<label class="form-control-label" for="animais">Animais</label>
												                <form:select path="animais" items="${animalDS}" itemLabel="nome" class="form-control" data-plugin="select2" multiple="multiple"/>
	                						 				</div>
	                						 			</div>
	                						 			
	                						 			<div class="col-xs-12 col-md-6">
	                						 				<div class="form-group">
	                						 					<label class="form-control-label" for="ofertaHospedagem">Hospedagem</label>
	                						 					<form:select path="ofertaHospedagem" items="${ofertaHospedagemDS}" itemLabel="nome" class="form-control" data-plugin="select2" />
	                						 				</div>
	                						 			</div>
	                						 		
	                						 		</div>
	                						 		
	                						 		<div class="row">
	                						 			
	                						 		
		               						 			<div class="col-xs-12 col-md-4">
		               						 				<div class="form-group">
		               						 					<label class="form-control-label" for="data_checkin">Data de Check-in</label>
		                                                        <div class="input-group input-group-icon">
												                    <div class="input-group-prepend">
													                    <div class="input-group-text">
													                    	<i class="fa fa-calendar" aria-hidden="true"></i>
													                    </div>
												                    </div>
												                    <fmt:formatDate value="${hospedagem.data_checkin}" var="data_checkin" pattern="dd/MM/yyyy"/>
		                                                        	<form:input path="data_checkin" value="${data_checkin}" class="form-control" autocomplete="off" data-plugin="datepicker" />
		                                                        </div>
		               						 				</div>
		               						 			</div>
		               						 			<div class="col-xs-12 col-md-2">
		               						 				<div class="form-group">
		               						 					<label class="form-control-label" for="hora_checkin">Hora de Check-in</label>
		                                                        <div class="input-group input-group-icon">
												                    <div class="input-group-prepend">
													                    <div class="input-group-text">
													                    	<i class="fa fa-clock-o" aria-hidden="true"></i>
													                    </div>
												                    </div>
												                    <fmt:formatDate value="${hospedagem.hora_checkin}" var="hora_checkin" pattern="HH:mm"/>
		                                                        	<form:input path="hora_checkin" value="${hora_checkin}" class="form-control" autocomplete="off" />
		                                                        </div>
		               						 				</div>
		               						 			</div>
		               						 			
		               						 			<div class="col-xs-12 col-md-4">
		               						 				<div class="form-group">
		               						 					<label class="form-control-label" for="data_checkout">Check-out</label>
		                                                        <div class="input-group input-group-icon">
												                    <div class="input-group-prepend">
													                    <div class="input-group-text">
													                    	<i class="fa fa-calendar" aria-hidden="true"></i>
													                    </div>
												                    </div>
		                                                        	<form:input path="data_checkout" class="form-control" autocomplete="off" data-plugin="datepicker" />
		                                                        </div>
		               						 				</div>
		               						 			</div>
		               						 			<div class="col-xs-12 col-md-2">
		               						 				<div class="form-group">
		               						 					<label class="form-control-label" for="hora_checkin">Hora de Check-Out</label>
		                                                        <div class="input-group input-group-icon">
												                    <div class="input-group-prepend">
													                    <div class="input-group-text">
													                    	<i class="fa fa-clock-o" aria-hidden="true"></i>
													                    </div>
												                    </div>
												                    <fmt:formatDate value="${hospedagem.hora_checkout}" var="hora_checkout" pattern="HH:mm"/>
		                                                        	<form:input path="hora_checkout" value="${hora_checkout}" class="form-control" autocomplete="off" />
		                                                        </div>
		               						 				</div>
		               						 			</div>
	                						 		
	                						 		</div>

	                						 	</div>
	                						</div>
	                					</div>									
									</div>
									
								</div>
								
								<div class="col-sm-12 col-md-4">
				                	<div class="panel">
				                		<div class="panel-heading"><h3 class="panel-title">Observa��es</h3></div>
				                		<div class="panel-body">
				                			<div class="row">
				                                <div class="col-sm-12">
				                                    <div class="form-group">
				                                        <form:textarea path="observacao" rows="10" class="form-control ${not empty e ? 'is-invalid' : null}" autocomplete="off" placeholder="Observa��o" />
				                                    </div>
				                                </div>
				                			</div>
				        				</div>
				        			</div>
								</div>

							</div>
							
							<div class="row">
								<div class="col-xs-12 col-md-2">
									<button class="btn btn-primary btn-block" type="submit"><i class="fa fa-floppy-o"></i> Salvar</button>
								</div>
							</div>							

						</form:form>
						
					</div>
				</div>
			</div>
		</div>

	</jsp:attribute>
	
	<jsp:attribute name="css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/blueimp-file-upload/jquery.fileupload.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/dropify/dropify.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/select2/select2.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/bootstrap-select/bootstrap-select.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/custom/js/plugins/bootcomplete.js-master/dist/bootcomplete.css">
	</jsp:attribute>
	
	<jsp:attribute name="plugins">
		<script src="${pageContext.request.contextPath}/resources/custom/js/plugins/jquery.mask.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/vendor/dropify/dropify.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/vendor/select2/select2.full.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/vendor/bootstrap-select/bootstrap-select.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/vendor/bootstrap-datepicker/bootstrap-datepicker.pt-BR.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/custom/js/plugins/bootcomplete.js-master/dist/jquery.bootcomplete.js"></script>
	</jsp:attribute>
	
	<jsp:attribute name="scripts">
		<script src="${pageContext.request.contextPath}/resources/custom/js/pet/hospedagem/cadastro.min.js"></script>
	</jsp:attribute>
	
</t:default>