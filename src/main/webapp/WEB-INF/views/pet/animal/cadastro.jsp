<%@page import="br.com.sisflow.conf.SisflowConfiguration" %>
<%@page import="java.io.File"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<c:set var="storage"><%=SisflowConfiguration.getValue("client.storage")%></c:set>
<c:set var="timestamp"><%=System.currentTimeMillis()%></c:set>

<t:default>
	<jsp:attribute name="body">
	
		<div class="page">	
			<div class="page-header">			
				<h1 class="page-title">Cadastro de Animal</h1>
				<ol class="breadcrumb">
		    		<li class="breadcrumb-item"><a href="${pageContext.request.contextPath}">Home</a></li>
		    		<li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/pet/animal">Animal</a></li>
		    		<li class="breadcrumb-item active">Cadastro</li>
		    	</ol>
			</div>
			<div class="page-content">
				<div class="row">
					<div class="col-xs-12 col-md-12">
						
						<form:form action="${spring:mvcUrl('AC#salvar').build()}" method="post" modelAttribute="animal" enctype="multipart/form-data" autocomplete="off" role="form">

							<spring:hasBindErrors name="animal">
							<div class="alert alert-danger">
								<ul>
									<c:forEach var="error" items="${errors.allErrors}">
									<li><spring:message code="${error.code}" text="${error.defaultMessage}"/></li>
									</c:forEach>
								</ul>
							</div>
							</spring:hasBindErrors>
							
							<form:input type="hidden" path="id"/>
							
							<div class="row">
								<div class="col-sm-12 col-md-9">

									<div class="panel panel-bordered">
										<div class="panel-heading">
	                						<h3 class="panel-title">Informa��es B�sicas</h3>
	                					</div>	
	                					<div class="panel-body"> 
	                						<div class="row">
	                						 	<div class="col-xs-12 col-md-12">
	                						 		
	                						 		<div class="row">
	                						 			 
	                						 			<div class="col-xs-12 col-md-4">
	                						 				<div class="form-group">
	                						 					<label class="form-control-label" for="cliente">Cliente</label>
	                						 					<form:select path="cliente" class="form-control" data-plugin="select2">
	                						 						<c:forEach var="row" items="${clienteDS}">
	                						 							<option value="${row.id}" ${row.id == animal.cliente.id ? "selected=selected" : null}>
	                						 								<c:if test="${row.tipo == 'PF'}">${row.pessoa.nome} ${row.pessoa.sobrenome}</c:if>
	                						 								<c:if test="${row.tipo == 'PJ'}">${row.pessoa.razao_social}</c:if>	                						 								
	                						 							</option>
	                						 						</c:forEach>
	                						 					</form:select>
	                						 				</div>
	                						 			</div>
	                						 			
	                						 			<div class="col-xs-12 col-md-4">
	                						 				<div class="form-group">
	                						 					<c:set var="e"><form:errors path="nome"/></c:set>
	                						 					<label class="form-control-label" for="nome">Nome</label>
	                						 					<form:input path="nome" class="form-control ${not empty e ? 'is-invalid' : null}" autocomplete="off" placeholder="Nome" maxlength="45" />
	                						 				</div>
	                						 			</div>
	                						 				                						 			
	                						 			<div class="col-xs-12 col-md-2">
	                						 				<div class="form-group">
	                						 					<label class="form-control-label" for="data_nascimento">Data de nascimento</label>
	                						 					<form:input type="text" path="data_nascimento" class="form-control" autocomplete="off" />
	                						 				</div>
	                						 			</div>
	                						 		
	                						 			<div class="col-xs-12 col-md-2">
	                						 				<div class="form-group">
	                						 					<label class="form-control-label" for="sexo">Sexo</label>
	                						 					<form:select path="sexo" items="${animalSexoDS}" itemLabel="descricao" class="form-control" data-plugin="selectpicker" />
	                						 				</div>
	                						 			</div>

	                						 		</div>
	                						 		
	                						 		<div class="row">

	                						 			<div class="col-xs-12 col-md-4">
	                						 				<div class="form-group">
	                						 					<label class="form-control-label" for="especie">Esp�cie</label>
	                						 					<form:select path="especie" items="${animalEspecieDS}" itemValue="id" itemLabel="nome" cssClass="form-control" data-plugin="selectpicker" />
	                						 				</div>
	                						 			</div>
	                						 			
	                						 			<div class="col-xs-12 col-md-4">
	                						 				<div class="form-group">
	                						 					<label class="form-control-label" for="raca">Ra�a</label>
	                						 					<form:select path="raca" items="${animalRacaDS}" itemValue="id" itemLabel="nome" cssClass="form-control" data-plugin="select2" />
	                						 				</div>
	                						 			</div>
	                						 			
	                						 			<div class="col-xs-12 col-md-4">
	                						 				<div class="form-group">
	                						 					<label class="form-control-label" for="porte">Porte</label>
	                						 					<form:select path="porte" items="${animalPorteDS}" itemLabel="descricao" class="form-control" data-plugin="selectpicker" />
	                						 				</div>
	                						 			</div>

	                						 		</div>
	                						 		
	                						 		<div class="row">

	                						 			<div class="col-xs-12 col-md-4">
	                						 				<div class="form-group">
	                						 					<label class="form-control-label" for="pelagem">Pelagem</label>
	                						 					<form:select path="pelagem" items="${animalPelagemDS}" itemValue="id" itemLabel="nome" cssClass="form-control" data-plugin="selectpicker" />
	                						 				</div>
	                						 			</div>
	                						 			
	                						 			<div class="col-xs-12 col-md-4">
	                						 				<div class="form-group">
	                						 					<label class="form-control-label" for="peso">Peso</label>
	                						 					<form:input path="peso" class="form-control" autocomplete="off" placeholder="Peso" />
	                						 				</div>
	                						 			</div>
	                						 		
             						 					<div class="col-xs-12 col-md-4">
	                						 				<div class="form-group">
	                						 					<label class="form-control-label" for="pedigree">Pedigree</label>
	                						 					<form:input path="pedigree" class="form-control" />
	                						 				</div>
	                						 			</div>
	                						 		
	                						 		</div>
	                						 		
	                						 	</div>
	                						</div>
	                					</div>									
									</div>
									
								</div>
								<div class="col-sm-12 col-md-3">
								
									<div class="panel panel-bordered">
										<div class="panel-heading">
	                						<h3 class="panel-title">Imagem</h3>
	                					</div>	
	                					<div class="panel-body"> 
	                						<div class="row">
	                						 	<div class="col-xs-12 col-md-12">
	                						 		
	                						 		<div class="form-group text-center">
				                		    			<input type="file" name="file" id="file-upload" 
				                		    				data-plugin="dropify" data-default-file="${storage}/animal/${animal.id}.jpg" 
				                		    				data-allowed-file-extensions="jpg jpeg png"
				                		    				data-max-file-size="2M"
				                		    				data-height="135"
				                		    				data-min-width="150"/>
			                		    				<small>clique sobre a imagem para alterar</small>
				            		    			</div>
	                						 		
	                						 	</div>
                						 	</div>
               						 	</div>
              						</div>
								
								</div>
							</div>

							<div class="row">
								<div class="col-sm-12 col-md-12">
									
									<div class="panel panel-bordered">
										<div class="panel-heading">
	                						<h3 class="panel-title">Vacinas</h3>
	                					</div>	
	                					<div class="panel-body"> 
	                						<div class="row">
	                						 	<div class="col-xs-12 col-md-12">
	                						 		<form:textarea path="vacinas" cssClass="form-control no-resize" rows="2" />
	                						 	</div>
                						 	</div>
               						 	</div>
           						 	</div>
								
								</div>							
							</div>
							
							<div class="row">
								<div class="col-sm-12 col-md-12">
									
									<div class="panel panel-bordered">
										<div class="panel-heading">
	                						<h3 class="panel-title">Observa��es</h3>
	                					</div>	
	                					<div class="panel-body"> 
	                						<div class="row">
	                						 	<div class="col-xs-12 col-md-12">
	                						 		<form:textarea path="observacao" cssClass="form-control no-resize" rows="5" />
	                						 	</div>
                						 	</div>
               						 	</div>
           						 	</div>
								
								</div>							
							</div>
							
							<div class="row">
        						<div class="col-xs-12 col-md-8"><p>Ao clicar em salvar, as informa��es inseridas ser�o enviadas � base de dados.</p></div>
        						<div class="col-xs-12 col-md-4"><button class="btn btn-primary btn-block" type="submit">Salvar</button></div>
        					</div>

							

						</form:form>
						
					</div>
				</div>
			</div>
		</div>

	</jsp:attribute>
	
	<jsp:attribute name="css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/blueimp-file-upload/jquery.fileupload.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/dropify/dropify.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/select2/select2.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/bootstrap-select/bootstrap-select.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/custom/js/plugins/bootcomplete.js-master/dist/bootcomplete.css">
	</jsp:attribute>
	
	<jsp:attribute name="plugins">
		<script src="${pageContext.request.contextPath}/resources/custom/js/plugins/jquery.mask.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/vendor/dropify/dropify.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/vendor/select2/select2.full.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/vendor/bootstrap-select/bootstrap-select.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/vendor/bootstrap-datepicker/bootstrap-datepicker.pt-BR.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/custom/js/plugins/bootcomplete.js-master/dist/jquery.bootcomplete.js"></script>
	</jsp:attribute>
	
	<jsp:attribute name="scripts">
		<script src="${pageContext.request.contextPath}/resources/custom/js/pet/animal/cadastro.min.js"></script>
	</jsp:attribute>
	
</t:default>