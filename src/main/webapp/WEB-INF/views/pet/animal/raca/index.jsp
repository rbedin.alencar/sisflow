<%@page import="br.com.sisflow.conf.SisflowConfiguration" %>
<%@page import="java.io.File"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<c:set var="storage"><%=SisflowConfiguration.getValue("client.storage")%></c:set>
<c:set var="timestamp"><%=System.currentTimeMillis()%></c:set>

<t:default>
	<jsp:attribute name="body">
		
		<div class="page">	
			<div class="page-header">
				<h1 class="page-title">Esp�cies</h1>
				<ol class="breadcrumb">
		    		<li class="breadcrumb-item"><a href="${pageContext.request.contextPath}">Configura��es</a></li>
		    		<li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/pet/animal">Animal</a></li>
		    		<li class="breadcrumb-item active">Ra�as</li>
		    	</ol>
		    	<div class="page-header-actions">
		    		<a href="${pageContext.request.contextPath}/pet/animal/raca/cadastro" class="btn btn-sm btn-icon btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Nova Ra�a</a>    	
		    	</div>
			</div>
			<div class="page-content">
			
				<c:if test="${msg != null}">
				<div class="alert alert-alt alert-primary alert-dismissible" role="alert">
					${msg}
					<button type="button" class="close" data-dismiss="alert" aria-label="Fechar"><span aria-hidden="true">�</span></button>
        		</div>
        		</c:if>
			
				<div class="panel">
					<header class="panel-heading">
						<div class="panel-actions"></div>
						<h3 class="panel-title">Ger�nciamento de Ra�as</h3>
					</header>
					<div class="panel-body">
						
						<c:if test="${not empty animalRacaDS}">
						<table class="table table-hover dataTable table-striped w-full" data-plugin="dataTable">
							<thead>
								<tr>
									<th class="text-left">C�d.</th>
									<th class="text-left">Ra�a</th>
									<th class="text-left">Esp�cie</th>
									<th class="text-left">Ativo</th>
		    						<th class="text-right"></th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th class="text-left">C�d.</th>
									<th class="text-left">Ra�a</th>
									<th class="text-left">Esp�cie</th>
									<th class="text-left">Ativo</th>
		    						<th class="text-right"></th>
								</tr>
		    				</tfoot>
							<tbody>
								<c:forEach var="row" items="${animalRacaDS}">
								<tr>
									<td class="align-middle">${row.id}</td>
									<td class="align-middle">${row.nome}</td>
									<td class="align-middle">${row.especie.nome}</td>
									<td class="align-middle">
										<c:if test="${row.ativo}"><div class="badge badge-table badge-success">Ativo</div></c:if>
										<c:if test="${not row.ativo}"><div class="badge badge-table badge-danger">Inativo</div></c:if>
									</td>
									<td class="text-nowrap text-right align-middle">
										<a href="${pageContext.request.contextPath}/pet/animal/raca/altera/${row.id}" class="btn btn-sm btn-icon btn-flat btn-primary" data-toggle="tooltip" data-original-title="Alterar"><i class="fa fa-pencil"></i></a>
										<a href="${pageContext.request.contextPath}/pet/animal/raca/exclui/${row.id}" onclick="if(!confirm('Tem certeza que deseja excluir o registro?')) return false;" class="btn btn-sm btn-icon btn-flat btn-danger" data-toggle="tooltip" data-original-title="Excluir"><i class="fa fa-trash-o"></i></a>
									</td>
								</tr>
								</c:forEach>
							</tbody>
						</table>
						</c:if>
						
					</div>
				</div>
				
			</div>
		</div>
	</jsp:attribute>
	
	<jsp:attribute name="css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/datatables.net-bs4/dataTables.bootstrap4.css">
	</jsp:attribute>
	
	<jsp:attribute name="plugins">
		<script src="${pageContext.request.contextPath}/resources/global/vendor/datatables.net/jquery.dataTables.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/vendor/datatables.net-bs4/dataTables.bootstrap4.js"></script>
	</jsp:attribute>
	
	<jsp:attribute name="scripts">
		<script src="${pageContext.request.contextPath}/resources/custom/js/animal/raca/index.min.js"></script>
	</jsp:attribute>
	
</t:default>