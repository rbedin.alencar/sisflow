<%@page import="br.com.sisflow.conf.SisflowConfiguration" %>>
<%@page import="java.io.File"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<c:set var="storage"><%=SisflowConfiguration.getValue("client.storage")%></c:set>
<c:set var="timestamp"><%= System.currentTimeMillis() %></c:set>

<t:default>
	<jsp:attribute name="body">
	
		<div class="page">	
			<div class="page-header">			
				<h1 class="page-title">Visualiza��o de animal #${animal.id}</h1>
				<ol class="breadcrumb">
		    		<li class="breadcrumb-item"><a href="${pageContext.request.contextPath}">Home</a></li>
		    		<li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/pet/animal">Animal</a></li>
		    		<li class="breadcrumb-item active">Visualiza��o</li>
		    	</ol>
		    	<div class="page-header-actions">
		    		<a href="${pageContext.request.contextPath}/pet/animal/altera/${animal.id}" class="btn btn-sm btn-primary" data-toggle="tooltip" data-original-title="Alterar"><i class="fa fa-pencil" aria-hidden="true"></i></a>
		    		<a href="${pageContext.request.contextPath}/pet/animal/visualiza/${animal.id}" class="btn btn-sm btn-default" data-toggle="tooltip" data-original-title="Atualizar"><i class="fa fa-refresh" aria-hidden="true"></i></a>
		    		<a href="${pageContext.request.contextPath}/pet/animal/exclui/${animal.id}" class="btn btn-sm btn-danger" data-toggle="tooltip" data-original-title="Excluir" onclick="if(!confirm('Tem certeza de que deseja excluir permanentemente o registro?')) return false;"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
		    	</div>
			</div>
			<div class="page-content">
				<div class="row">
					<div class="col-xs-12 col-md-5">
					
						<div class="panel panel-bordered panel-dark">
							<div class="panel-heading">
    							<h3 class="panel-title">${animal.especie.nome} - ${animal.raca.nome}
    								<c:if test="${animal.raca.url != null}">
    									<a href="${animal.raca.url}" target="_blank" title="Mais informa��es sobre a ra�a" class="badge badge-primary">Info.</a>
    								</c:if>
    							</h3>
    						</div>
							<div class="panel-body">
								<div class="row">
    								<h5 class="col-sm-4">Nome</h5>
    								<div class="col-sm-8 text-sm-right text-info">${animal.nome}</div>
    							</div>
	    						<div class="row">
	    							<h5 class="col-sm-4">Sexo</h5>
	    							<div class="col-sm-8 text-sm-right text-info">${animal.sexo.descricao}</div>
	    						</div>
		    					<div class="row">
		    						<h5 class="col-sm-4">Esp�cie</h5>
		    						<div class="col-sm-8 text-sm-right text-info">${animal.especie.nome}</div>
		    					</div>
		    					<div class="row">
		    						<h5 class="col-sm-4">Porte</h5>
		    						<div class="col-sm-8 text-sm-right text-info">${animal.porte.descricao}</div>
		    					</div>
		    					<div class="row">
		    						<h5 class="col-sm-4">Cor ou Pelagem</h5>
		    						<div class="col-sm-8 text-sm-right text-info">${animal.pelagem.nome}</div>
		    					</div>
		    					<div class="row">
		    						<h5 class="col-sm-4">Castrado</h5>
		    						<div class="col-sm-8 text-sm-right text-info">
		    							<c:choose>
		    								<c:when test="${animal.castrado}">Sim</c:when>
		    								<c:otherwise>N�o</c:otherwise>
		    							</c:choose>
		    						</div>
		    					</div>
		    					<div class="row">
		    						<h5 class="col-sm-4">Pedigree</h5>
		    						<div class="col-sm-8 text-sm-right text-info">
		    							<c:choose>
		    								<c:when test="${animal.pedigree}">Sim</c:when>
		    								<c:otherwise>N�o</c:otherwise>
		    							</c:choose>
		    						</div>
		    					</div>
		    					<div class="row">
		    						<h5 class="col-sm-4">Data de nascimento</h5>
		    						<div class="col-sm-8 text-sm-right text-info">
		    							<fmt:formatDate value="${animal.data_nascimento}" pattern="dd 'de' MMMMM 'de' yyyy"/>
		    						</div>
		    					</div>
   							</div>
    					</div>
    					
						<div class="panel panel-bordered">
							<div class="panel-heading">
    							<h3 class="panel-title">Vacinas</h3>
    						</div>
							<div class="panel-body">
								${animal.vacinas}
   							</div>
    					</div>
    					
						<div class="panel panel-bordered">
							<div class="panel-heading">
    							<h3 class="panel-title">Observa��es</h3>
    						</div>
							<div class="panel-body">
								${animal.observacao}
   							</div>
    					</div>
    											
					</div>
					
					<div class="col-xs-12 col-md-7">
					
						<div class="panel panel-bordered">
							<div class="panel-heading">
    							<h3 class="panel-title">Hist�rico</h3>
    						</div>
							<div class="panel-body">
								
								
								
   							</div>
    					</div>
    					
						<div class="panel panel-bordered">
							<div class="panel-heading">
    							<h3 class="panel-title">Informa��es do Cliente</h3>
    						</div>
							<div class="panel-body">
								<div class="row">
    								<h5 class="col-sm-4">Cliente</h5>
    								<div class="col-sm-8 text-sm-right text-info"><a href="${pageContext.request.contextPath}/cliente/visualiza/${animal.cliente.id}">
			 							<c:catch var="e_pf">${animal.cliente.pessoa.nome} ${animal.cliente.pessoa.sobrenome}</c:catch>
										<c:catch var="e_pj">${animal.cliente.pessoa.razao_social}</c:catch>
    								</a></div>
    							</div>
	    						<div class="row">
	    							<h5 class="col-sm-4">Tipo</h5>
	    							<div class="col-sm-8 text-sm-right text-info">
	    								<c:if test="${animal.cliente.tipo == 'PF'}">Pessoa F�sica</c:if>
	    								<c:if test="${animal.cliente.tipo == 'PJ'}">Pessoa Jur�dica</c:if>
	    							</div>
	    						</div>
		    					<div class="row">
		    						<h5 class="col-sm-4">Endere�o</h5>
		    						<div class="col-sm-8 text-sm-right text-info">
										<c:forEach items="${animal.cliente.enderecos}" var="row">
											<p>
												${row.logradouro}, ${row.numero} - ${row.bairro}<br/>
												${row.cidade} / ${row.uf} - CEP: ${row.cep}
											</p>
										</c:forEach>
		    						</div>
		    					</div>
   							</div>
    					</div>
					
					</div>
					
				</div>
			</div>
		</div>

	</jsp:attribute>
	
	<jsp:attribute name="plugins">
	</jsp:attribute>
	
	<jsp:attribute name="scripts">
		<script src="${pageContext.request.contextPath}/custom/js/pet/animal/visualiza.min.js"></script>
	</jsp:attribute>
	
</t:default>