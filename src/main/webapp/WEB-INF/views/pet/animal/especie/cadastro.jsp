<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>


<t:default>
	<jsp:attribute name="body">
	
		<div class="page">	
			<div class="page-header">			
				<h1 class="page-title">Cadastro de Esp�cie</h1>
				<ol class="breadcrumb">
		    		<li class="breadcrumb-item"><a href="${pageContext.request.contextPath}">Configura��es</a></li>
		    		<li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/pet/animal">Animal</a></li>
		    		<li class="breadcrumb-item active">Esp�cies</li>
		    	</ol>
			</div>
			<div class="page-content">
				<div class="row">
					<div class="col-xs-12 col-md-6">

						<spring:hasBindErrors name="animalEspecie">
						<div class="alert alert-danger">
							<ul>
								<c:forEach var="error" items="${errors.allErrors}">
								<li><spring:message code="${error.code}" text="${error.defaultMessage}"/></li>
								</c:forEach>
							</ul>
						</div>
						</spring:hasBindErrors>
						
						<form:form action="${spring:mvcUrl('AEC#salvar').build()}" method="post" modelAttribute="animalEspecie" enctype="multipart/form-data">

							<form:input type="hidden" path="id"/>
							
							<div class="row">
								<div class="col-sm-12">
									
									<div class="panel panel-bordered">
										<div class="panel-heading">
	                						<h3 class="panel-title">Informa��es B�sicas</h3>
	                					</div>	
	                					<div class="panel-body"> 
	                						<div class="row">
	                						 	<div class="col-xs-12 col-md-12">

	                						 		<div class="row">

	                						 			<div class="col-xs-12 col-sm-10">
	                						 				<div class="form-group">
	                						 					<c:set var="e"><form:errors path="nome"/></c:set>
	                						 					<label class="form-control-label" for="nome">Descri��o</label>
	                						 					<form:input path="nome" class="form-control ${not empty e ? 'is-invalid' : null}" autocomplete="off"/>
	                						 				</div>
	                						 			</div>

	                						 			<div class="col-xs-12 col-sm-2">
	                						 				<div class="form-group">
	                						 					<label class="form-control-label" for="ativo">Ativo</label><br/>
	                						 					<div class="float-left" style="margin-top: 3px;">
	                						 						<form:checkbox path="ativo" class="form-control" data-plugin="switchery"/>	                						 						
	                						 					</div>
	                						 				</div>
	                						 			</div>

	                						 		</div>

	                						 	</div>
	                						</div>
	                					</div>									
									</div>
									
								</div>
							</div>

							<div class="row">
        						<div class="col-xs-12 col-md-8"><p>Ao clicar em salvar, as informa��es inseridas ser�o enviadas � base de dados.</p></div>
        						<div class="col-xs-12 col-md-4"><button class="btn btn-primary btn-block" type="submit">Salvar</button></div>
        					</div>

						</form:form>
						
					</div>
				</div>
			</div>
		</div>

	</jsp:attribute>
	
	<jsp:attribute name="css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/blueimp-file-upload/jquery.fileupload.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/dropify/dropify.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/select2/select2.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/bootstrap-select/bootstrap-select.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/custom/js/plugins/bootcomplete.js-master/dist/bootcomplete.css">
	</jsp:attribute>
	
	<jsp:attribute name="plugins">
		<script src="${pageContext.request.contextPath}/resources/custom/js/plugins/jquery.mask.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/vendor/dropify/dropify.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/vendor/select2/select2.full.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/vendor/bootstrap-select/bootstrap-select.js"></script>
		<script src="${pageContext.request.contextPath}/resources/custom/js/plugins/bootcomplete.js-master/dist/jquery.bootcomplete.js"></script>
	</jsp:attribute>
	
	<jsp:attribute name="scripts">
		<script src="${pageContext.request.contextPath}/resources/custom/js/pet/animal/especie/cadastro.min.js"></script>
	</jsp:attribute>
	
</t:default>