<%@ tag import="br.com.sisflow.conf.SisflowConfiguration"%>
<%@ tag import="br.com.sisflow.core.usuario.Usuario" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<%@ attribute name="css" fragment="true" %>
<%@ attribute name="body" fragment="true" %>
<%@ attribute name="plugins" fragment="true" %>
<%@ attribute name="scripts" fragment="true" %>

<sec:authentication property="principal" var="usuario" />
<c:set var="storage"><%=SisflowConfiguration.getValue("client.storage")%></c:set>
<c:set var="timestamp"><%= System.currentTimeMillis() %></c:set>

<!DOCTYPE html>
<html class="no-js css-menubar" lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<title>Sisflow</title>
		<link rel="apple-touch-icon" href="${pageContext.request.contextPath}/resources/custom/img/i_60.png">
		<link rel="apple-touch-icon" href="${pageContext.request.contextPath}/resources/custom/img/i_76.png" sizes="76x76">
		<link rel="apple-touch-icon" href="${pageContext.request.contextPath}/resources/custom/img/i_120.png" sizes="120x120">
		<link rel="apple-touch-icon" href="${pageContext.request.contextPath}/resources/custom/img/i_152.png" sizes="152x152">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-touch-fullscreen" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="default">
		<meta name="description" content="sisflow.com.br" />
		<meta name="author" content="sisflow.com.br" />

		<link rel="apple-touch-icon" href="<c:url value="/resources/resources/assets/images/apple-touch-icon.png" />">
		<link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/assets/images/favicon.ico">
		
		<!-- core -->
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/css/bootstrap.min.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/css/bootstrap-extend.min.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/site.min.css">
		
		<!-- plugins -->
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/animsition/animsition.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/asscrollable/asScrollable.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/switchery/switchery.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/intro-js/introjs.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/vendor/slidepanel/slidePanel.css">
		
		<jsp:invoke fragment="css"/>
		
		<!-- custom -->
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/custom/css/default.min.css">
		
		<!-- fonts -->
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/fonts/font-awesome/font-awesome.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/fonts/web-icons/web-icons.min.css">		
    	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/global/fonts/brand-icons/brand-icons.min.css">
    	<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
		
		<!--[if lt IE 9]>
			<script src="${pageContext.request.contextPath}/resources/global/vendor/html5shiv/html5shiv.min.js"></script>
		<![endif]-->
		
		<!--[if lt IE 10]>
			<script src="${pageContext.request.contextPath}/resources/global/vendor/media-match/media.match.min.js"></script>
			<script src="${pageContext.request.contextPath}/resources/global/vendor/respond/respond.min.js"></script>
		<![endif]-->
		
		<script src="${pageContext.request.contextPath}/resources/global/vendor/breakpoints/breakpoints.js"></script>
		<script>
			Breakpoints();
		</script>
	</head>
	<body class="animsition site-menubar-fold site-menubar-keep ${bodyClass}" data-auto-menubar="false">
		
		<!--[if lt IE 8]>
            <p class="browserupgrade">Voc� esta usando um navegador desatualizado. Atualize seu navegador para melhorar sua experi�ncia.</p>
        <![endif]-->

		
		<nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega" role="navigation">
		
			<div class="navbar-header">
				<button type="button" class="navbar-toggler hamburger hamburger-close navbar-toggler-left hided" data-toggle="menubar">
			    	<span class="sr-only">Navega��o</span>
			    	<span class="hamburger-bar"></span>
			  	</button>
			  	<button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-collapse" data-toggle="collapse">
			    	<i class="icon wb-more-horizontal" aria-hidden="true"></i>
			  	</button>
			  	<div class="navbar-brand navbar-brand-center site-gridmenu-toggle" data-toggle="gridmenu">
			    	<img class="navbar-brand-logo" src="${pageContext.request.contextPath}/resources/assets/images/logo.png" title="Remark">
			    	<span class="navbar-brand-text hidden-xs-down"> Sistema</span>
			  	</div>
			  	<button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-search" data-toggle="collapse">
			    	<span class="sr-only">Pesquisa</span>
			    	<i class="icon wb-search" aria-hidden="true"></i>
			  	</button>
			</div>
			
			<div class="navbar-container container-fluid">
				<div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
					<ul class="nav navbar-toolbar">
						<li class="nav-item hidden-float" id="toggleMenubar">
							<a class="nav-link" data-toggle="menubar" href="#" role="button">
				                <i class="icon hamburger hamburger-arrow-left">
				                	<span class="sr-only">Toggle menubar</span>
				                  	<span class="hamburger-bar"></span>
				                </i>
			              	</a>
			            </li>
			            <li class="nav-item hidden-sm-down" id="toggleFullscreen">
			              	<a class="nav-link icon icon-fullscreen" data-toggle="fullscreen" href="#" role="button">
			                	<span class="sr-only">Toggle fullscreen</span>
			              	</a>
			            </li>
			            <li class="nav-item hidden-float">
			              	<a class="nav-link icon wb-search" data-toggle="collapse" href="#" data-target="#site-navbar-search" role="button">
			                	<span class="sr-only">Toggle Search</span>
			              	</a>
			            </li>
					</ul>
					
					<ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">
					
						<li class="nav-item dropdown">
							<a class="nav-link navbar-avatar" data-toggle="dropdown" href="#" aria-expanded="false" data-animation="scale-up" role="button">
								<span class="avatar avatar-online">
	                  				<img src="${storage}/usuario/${usuario.id}.jpg" alt="${usuario.nome}">
	                  				<i></i>
	                			</span>
              				</a>
              				<div class="dropdown-menu" role="menu">
				                <a class="dropdown-item" href="javascript:void(0)" role="menuitem"><i class="icon wb-user" aria-hidden="true"></i> Minha conta</a>
				                <a class="dropdown-item" href="javascript:void(0)" role="menuitem"><i class="icon wb-settings" aria-hidden="true"></i> Configura��es</a>
				                <div class="dropdown-divider" role="presentation"></div>
								<a class="dropdown-item" href="${pageContext.request.contextPath}/logout" role="menuitem"><i class="icon wb-power" aria-hidden="true"></i> Sair</a>
            				</div>
           				</li>
           				
           				<!-- notification -->
           				<li class="nav-item dropdown">
           					<a class="nav-link" data-toggle="dropdown" href="javascript:void(0)" title="Notifica��es" aria-expanded="false" data-animation="scale-up" role="button">
				          		<i class="icon wb-bell" aria-hidden="true"></i>
								<span class="badge badge-pill badge-danger up">1</span>
				            </a>
				            <div class="dropdown-menu dropdown-menu-right dropdown-menu-media" role="menu">
				            	<div class="dropdown-menu-header">
				            		<h5>NOTIFICA��ES</h5>
				            		<span class="badge badge-round badge-danger">1 Nova(s)</span>
				            	</div>
				            	<div class="list-group">
				            		<div data-role="container">
				            			<div data-role="content">
				            				<!-- item -->
				            				<a class="list-group-item dropdown-item" href="javascript:void(0)" role="menuitem">
					                        	<span class="media">
					                          		<span class="pr-10">
					                            		<i class="icon wb-order bg-red-600 white icon-circle" aria-hidden="true"></i>
					                          		</span>
					                          		<span class="media-body">
					                            		<span class="media-heading">Bem vindo ao sistema</span>
					                            		<time class="media-meta" datetime="2018-06-12T20:50:48+08:00">1 min. atr�s</time>
					                          		</span>
					                        	</span>
					                      	</a>
				            			</div>
				            		</div>
				            	</div>
				            	<div class="dropdown-menu-footer">
				            		<a class="dropdown-menu-footer-btn" href="javascript:void(0)" role="button"><i class="icon wb-settings" aria-hidden="true"></i></a>
				                  	<a class="dropdown-item" href="javascript:void(0)" role="menuitem">Todas notifica��es</a>
				                </div>
				            </div>
           				</li>
           				
           				<!-- messages -->
           				<li class="nav-item dropdown">
           					<a class="nav-link" data-toggle="dropdown" href="javascript:void(0)" title="Messages" aria-expanded="false" data-animation="scale-up" role="button">
           						<i class="icon wb-envelope" aria-hidden="true"></i>
           						<span class="badge badge-pill badge-info up">1</span>
              				</a>
              				<div class="dropdown-menu dropdown-menu-right dropdown-menu-media" role="menu">
              					<div class="dropdown-menu-header" role="presentation">
              						<h5>MENSAGENS</h5>
              						<span class="badge badge-round badge-info">1 Nova(s)</span>
              					</div>
              					<div class="list-group" role="presentation">
              						<div data-role="container">
              							<div data-role="content">
              								<!-- item -->
              								<a class="list-group-item" href="javascript:void(0)" role="menuitem">
				                        		<span class="media">
				                          			<span class="pr-10">
				                            			<span class="avatar avatar-sm avatar-online">
				                              				<img src="${pageContext.request.contextPath}/resources/global/portraits/2.jpg" alt="..." />
				                              				<i></i>
				                            			</span>
				                          			</span>
				                          			<span class="media-body">
				                            			<span class="media-heading">Administrador</span>
				                            			<span class="media-meta">
				                              				<time datetime="2018-06-17T20:22:05+08:00">1 min. atr�s.</time>
				                            			</span><br/>
				                            			<span class="media-detail">O sistema foi atualizado.</span>
				                          			</span>
				                        		</span>
				                      		</a>
              							</div>
              						</div>
              					</div>
								<div class="dropdown-menu-footer" role="presentation">
									<a class="dropdown-menu-footer-btn" href="javascript:void(0)" role="button"><i class="icon wb-settings" aria-hidden="true"></i></a>
				                  	<a class="dropdown-item" href="javascript:void(0)" role="menuitem">See all messages</a>
				                </div>
              				</div>
           				</li>
           				
           				<li class="nav-item" id="toggleChat">
		              		<a class="nav-link" data-toggle="site-sidebar" href="javascript:void(0)" title="Chat" data-url="../site-sidebar.tpl">
		                		<i class="icon wb-chat" aria-hidden="true"></i>
		              		</a>
		            	</li>
           									
					</ul>
				</div>
				
				<!-- search -->
				<div class="collapse navbar-search-overlap" id="site-navbar-search">
					<form role="search">
						<div class="form-group">
					  		<div class="input-search">
					    		<i class="input-search-icon wb-search" aria-hidden="true"></i>
				    			<input type="text" class="form-control" name="site-search" placeholder="Search...">
				    			<button type="button" class="input-search-close icon wb-close" data-target="#site-navbar-search" data-toggle="collapse" aria-label="Close"></button>
					  		</div>
						</div>
					</form>				
				</div>
			</div>
		
		</nav>
		
		<div class="site-menubar">
			<div class="site-menubar-body">
				<div>
					<div>
						
						<ul class="site-menu" data-plugin="menu">
							<li class="site-menu-category">Menu</li>
							<li class="site-menu-item">
								<a href="${pageContext.request.contextPath}">
									<i class="site-menu-icon fa-home" aria-hidden="true"></i>
									<span class="site-menu-title">Home</span>
								</a>
							</li>
							<li class="site-menu-item">
								<a href="${pageContext.request.contextPath}/cliente">
									<i class="site-menu-icon fa-user" aria-hidden="true"></i>
									<span class="site-menu-title">Clientes</span>
								</a>
							</li>
							<li class="site-menu-item">
								<a href="${pageContext.request.contextPath}/pet/animal">
									<i class="site-menu-icon fa-paw" aria-hidden="true"></i>
									<span class="site-menu-title">Animais</span>
								</a>
							</li>
							
							<li class="site-menu-item has-sub">
								<a href="javascript:void(0)">
									<i class="site-menu-icon fa-bed" aria-hidden="true"></i>
									<span class="site-menu-title">Hospedagem</span>
									<span class="site-menu-arrow"></span>
								</a>
								<ul class="site-menu-sub">
									<li class="site-menu-item"><a href="${pageContext.request.contextPath}/pet/hospedagem"><span class="site-menu-title">Ger�nciamento</span></a></li>
									<li class="site-menu-item"><a href="${pageContext.request.contextPath}/pet/hospedagem/agenda"><span class="site-menu-title">Agenda</span></a></li>
								</ul>
							</li>

							<li class="site-menu-item has-sub">
								<a href="javascript:void(0)">
									<i class="site-menu-icon fa-shopping-cart" aria-hidden="true"></i>
									<span class="site-menu-title">Vendas</span>
									<span class="site-menu-arrow"></span>
								</a>
								<ul class="site-menu-sub">
									<li class="site-menu-item"><a href="${pageContext.request.contextPath}/venda"><span class="site-menu-title">Ger�nciamento</span></a></li>
									<li class="site-menu-item"><a href="${pageContext.request.contextPath}/venda/cadastro"><span class="site-menu-title">Nova venda</span></a></li>
								</ul>
							</li>

							<li class="site-menu-item has-sub">
								<a href="javascript:void(0)">
									<i class="site-menu-icon fa-usd" aria-hidden="true"></i>
									<span class="site-menu-title">Financeiro</span>
									<span class="site-menu-arrow"></span>
								</a>
								<ul class="site-menu-sub">
									<li class="site-menu-item"><a href="${pageContext.request.contextPath}/contas"><span class="site-menu-title">Movimenta��es</span></a></li>
									<li class="site-menu-item"><a href="${pageContext.request.contextPath}/contas/pagar"><span class="site-menu-title">Contas a pagar</span></a></li>
									<li class="site-menu-item"><a href="${pageContext.request.contextPath}/contas/receber"><span class="site-menu-title">Contas a receber</span></a></li>
								</ul>
							</li>
							
							<li class="site-menu-item has-sub">
								<a href="javascript:void(0)">
									<i class="site-menu-icon fa-gear" aria-hidden="true"></i>
									<span class="site-menu-title">Configura��es</span>
									<span class="site-menu-arrow"></span>
								</a>
								<ul class="site-menu-sub">
									<li class="site-menu-item"><a href="${pageContext.request.contextPath}/usuario"><span class="site-menu-title">Usu�rios</span></a></li>
									<li class="site-menu-item has-sub">
										<a href="javascript:void(0);">
											<span class="site-menu-title">Clientes</span>
											<span class="site-menu-arrow"></span>
										</a>
										<ul class="site-menu-sub">
											<li class="site-menu-item"><a href="${pageContext.request.contextPath}/cliente/origem"><span class="site-menu-title">Origens</span></a></li>
										</ul>
									</li>
									
									<li class="site-menu-item"><a href="${pageContext.request.contextPath}/oferta"><span class="site-menu-title">Produtos/Servi�os</span></a></li>
									
									<li class="site-menu-item"><a href="${pageContext.request.contextPath}/pet/animal/especie"><span class="site-menu-title">Esp�cies</span></a></li>
									<li class="site-menu-item"><a href="${pageContext.request.contextPath}/pet/animal/raca"><span class="site-menu-title">Ra�as</span></a></li>
									<li class="site-menu-item"><a href="${pageContext.request.contextPath}/pet/animal/pelagem"><span class="site-menu-title">Pelagem</span></a></li>
								</ul>
							</li>
							
						</ul>

					</div>
				</div>
			</div>
					
		</div>
		
		<div class="site-gridmenu">
			<div>
				<div>
					<ul>
					<!-- 
						<li>
              				<a href="../apps/mailbox/mailbox.html">
                				<i class="icon wb-envelope"></i>
                				<span>Mailbox</span>
              				</a>
            			</li>
            		 -->
					</ul>
				</div>
			</div>
		</div>
		
		<!-- page -->
		<jsp:invoke fragment="body"/>
		
		<!-- footer -->
		<footer class="site-footer">
			<div class="site-footer-legal"><small>� 2018 Sistema PET vers�o 1.0</small></div>
			<div class="site-footer-right"><small>Desenvolvido por <a href="http://sisflow.com.br">sisflow.com.br</a></small></div>
		</footer>
		
		<!-- core -->
	    <script src="${pageContext.request.contextPath}/resources/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/global/vendor/jquery/jquery.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/global/vendor/popper-js/umd/popper.min.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/global/vendor/bootstrap/bootstrap.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/global/vendor/animsition/animsition.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/global/vendor/mousewheel/jquery.mousewheel.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/global/vendor/asscrollable/jquery-asScrollable.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
		
		<!-- plugins -->
	    <script src="${pageContext.request.contextPath}/resources/global/vendor/switchery/switchery.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/global/vendor/intro-js/intro.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/global/vendor/screenfull/screenfull.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/global/vendor/slidepanel/jquery-slidePanel.js"></script>
		<script src="${pageContext.request.contextPath}/resources/global/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- plugins:custom -->
		<!-- 
		<script src="${pageContext.request.contextPath}/custom/js/webchat.min.js"></script>
		 -->
		 
		<jsp:invoke fragment="plugins"/>
		
		<!-- scripts -->
	    <script src="${pageContext.request.contextPath}/resources/global/js/Component.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/global/js/Plugin.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/global/js/Base.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/global/js/Config.js"></script>
	    
	    <script src="${pageContext.request.contextPath}/resources/assets/js/Section/Menubar.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/assets/js/Section/GridMenu.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/assets/js/Section/Sidebar.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/assets/js/Section/PageAside.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/assets/js/Plugin/menu.js"></script>
			
	    <script src="${pageContext.request.contextPath}/resources/global/js/config/colors.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/assets/js/config/tour.js"></script>
	    <script>Config.set('assets', '${pageContext.request.contextPath}/resources/assets');</script>
	    
	    <!-- page -->
	    <script src="${pageContext.request.contextPath}/resources/assets/js/Site.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/global/js/Plugin/asscrollable.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/global/js/Plugin/slidepanel.js"></script>
	    <script src="${pageContext.request.contextPath}/resources/global/js/Plugin/switchery.js"></script>
	    
	    <!-- page:scripts -->
		<jsp:invoke fragment="scripts"/>

	    <script>
	      (function(document, window, $){
	        'use strict';
	    
	        var Site = window.Site;
	        $(document).ready(function(){
	          Site.run();
	        });
	      })(document, window, jQuery);
	    </script>
	</body>
</html>